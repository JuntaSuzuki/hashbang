var app = app || {};

$(function() {

  // Message section functions
  app.message = {

    maxCount: app.vars.message,
    remainCount: 0,

    init: function() {
      app.message.add();
      app.message.edit();
      app.message.delete();
      app.message.cancel();

      app.message.editDisplay();
      app.message.remainCount = app.message.maxCount - $message.length;
    },

    display: function(res, message) {
      switch (res.code) {
        case '200':
          if ($messageBox.text() == 'メッセージがまだ登録されていません') {
            $messageBox.text('');
          }

          var format  = '<div class="clearfix message-item" data-id="%s" data-message="%s">';
              format += '<div class="message-text">%s</div>';
              format += '<div class="message-btn">';
              format += '<button type="button" class="btn btn-rounded-20 btn-sm mr-5 message-edit btn-blue">%s</button>';
              format += '<button type="button" class="btn btn-rounded-20 btn-sm mr-5 message-delete btn-danger">%s</button>';
              format += '</div>';
              format += '</div>';

          var dom = sprintf(
            format,
            res.id,
            res.fullmessage,
            res.message,
            '編集',
            '削除'
          );

          $messageBox.append(dom);
          app.message.remainCount--;
          app.message.updateMsg();
          $messageValue.val('');
          $('.alert').fadeOut();
          break;

        case '400':
          $('.message-exist').show();
          break;

        case '500':
          $('.message-error').show();
          break;
      }
    },

    add: function() {
      $(document).on('tap', '#messageAdd', function() {
        var message = $messageValue.val();
        if (message == '') {
          return false;
        }

        if (app.message.remainCount == 0) {
          $('.alert').show();
          return false;
        }

        var option = {
          url : '/messages/add'
        }
        var data = {
          message : message
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            app.message.display(res, message);
          });
      });
    },

    edit: function() {
      $(document).on('tap', '#messageEdit', function() {
        var message = $messageValue.val();
        if (message == '') {
          return false;
        }

        var id = $messageId.val();
        if (id == '') {
          return false;
        }

        var option = {
          url : '/messages/edit'
        }
        var data = {
          id : id,
          message : message
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.code == 200) {
              var $messages = $('.message-item');
              var el;
              $messages.each(function(index, message) {
                var id = $(message).data('id');
                if (id == res.id) {
                  el = $(message);
                  return false;
                }
              });
              el.children('.message-text').text(res.message);
              el.data('message', res.fullmessage);

              app.message.reset();

              $('.alert').fadeOut();
            }
          });
      });

    },

    editDisplay: function() {
      $(document).on('tap', '.message-edit', function() {
        var el = $(this).parents('.message-item');
        $messageValue.val(el.data('message'));
        $('#messageAdd').text('更新する').attr('id', 'messageEdit');
        $messageCancel.show();
        $messageId.val(el.data('id'));
      });
    },

    delete: function() {
      $(document).on('tap', '.message-delete', function() {
        var el = $(this).parents('.message-item');
        var option = {
          url : '/messages/delete'
        }
        var data = {
          id : el.data('id')
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              app.message.reset();

              el.remove();
              app.message.remainCount++;
              app.message.updateMsg();
              $('.alert').fadeOut();
              if (app.message.remainCount == app.message.maxCount) {
                $messageBox.text('メッセージがまだ登録されていません');
              }
            }
          });
      });
    },

    cancel: function() {
      $(document).on('tap', '#messageCancel', function() {
        $messageValue.val('');
        $('#messageEdit').text('登録').attr('id', 'messageAdd');
        $messageCancel.hide();
        $messageId.val('');
      });
    },

    reset: function() {
      $messageValue.val('');
      $('#messageEdit').text('登録').attr('id', 'messageAdd');
      $messageCancel.hide();
      $messageId.val('');
    },

    updateMsg: function() {
      var msg = '';
      switch (app.message.remainCount) {
        case app.message.maxCount:
          msg = app.message.maxCount + '個登録可能';
          break;

        case 0:
          msg = 'これ以上登録できません';
          break;

        default:
          msg = 'あと ' + app.message.remainCount + ' 個 登録可能';
          break;
      }
      $messageMsg.text(msg);
    }
  };

  // initialize when document ready

  app.documentOnReady = {
    init: function(){
      app.message.init();
    },
  };

  // global variables

  var $window = $(window),
      $body = $('body'),
      $boxsBody = $('.boxs-body'),
      $message = $('.message-item'),
      $messageValue = $('#messageValue'),
      $messageCancel = $('#messageCancel'),
      $messageMsg = $('#messageMsg'),
      $messageBox = $('.message-box'),
      $messageId = $('#MessageId');


  // initializing
  $(document).ready( app.documentOnReady.init );

});
