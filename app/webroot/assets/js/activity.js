var app = app || {};

$(function() {

  // activity section functions
  app.activity = {

    limit: app.vars.activity_limit,
    offset: 1,

    init: function() {
      app.activity.more();
    },

    more: function() {
      $moreActivity.click(function() {
        $moreActivity.html('<i class="fa fa-spinner fa-pulse fa-fw"></i> 読み込み中...');
        setTimeout(app.activity.moreActivity, 500);
      });
    },

    moreActivity: function() {
      var option = {
        url : '/activities/more'
      }

      var data = {
        limit : app.activity.limit,
        offset : app.activity.offset * app.activity.limit
      }

      app.global.ajaxPost(option, data, 'html')
        .done(function(res) {
          app.activity.offset++;
          if (app.vars.activity_total <= app.activity.limit * app.activity.offset + 1) {
            $moreActivity.hide();
          }

          $timeline.append($(res).hide().fadeIn(300));
          $moreActivity.html('もっと見る');
      });
    }
  }

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.activity.init();
    },
  };

  // global variables
  var $moreActivity = $('#moreActivity')
      $timeline = $('#timeline_item');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
