var app = app || {};

$(function() {

  var planPrice = app.vars.plan_price;

  // Subscription section functions
  app.subscription = {

    init: function() {
      app.subscription.changePlan();
      app.subscription.changeMonth();

      if ($planId.val() != 4) {
        $month.prop('disabled', true);
      }
    },

    changePlan: function() {
      $planId.on('change', function() {
        var planId = parseInt($(this).val(), 10);

        if (planId != 4) {
          $month.val(1);
          $month.prop('disabled', true);
        } else {
          $month.prop('disabled', false);
        }

        app.subscription.changeDisplay();
      });
    },

    changeMonth: function() {
      $month.on('change', function() {
        app.subscription.changeDisplay();
      });
    },

    changeDisplay: function() {
      var planId = parseInt($planId.val(), 10);
      var month = parseInt($month.val(), 10);
      var day = planPrice[planId][month].day - 1;

      var dt = new Date($contractStartDate.text());
      dt.setDate(dt.getDate() + day);
      $contractEndDate.text(app.global.formatDate(dt, 'YYYY/MM/DD'));

      $amount.text(app.global.separate(planPrice[planId][month].price));
      $paymentPlanId.val(planPrice[planId][month].id);
    },
  };


  // initialize when document ready
  app.documentOnReady = {
    init: function() {
      app.subscription.init();
    },
  };

  // global variables
  var $planId = $('#plan_id'),
      $month = $('#month'),
      $contractStartDate = $('#contract_start_date'),
      $contractEndDate = $('#contract_end_date'),
      $amount = $('#amount'),
      $paymentPlanId = $('#PaymentPlanId');
 

  // initializing
  $(document).ready( app.documentOnReady.init );

});
