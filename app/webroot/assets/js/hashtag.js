var app = app || {};

$(function() {

  // hashtag section functions
  app.hashtag = {

    maxCount: app.vars.hashtag,
    remainCount: 0,

    init: function() {
      app.hashtag.input();
      app.hashtag.delete();

      app.hashtag.remainCount = app.hashtag.maxCount - $hashtag.length;

      $(document).on('tap', '.hash-search-row', function() {
        app.hashtag.add($(this).data('name'));
      });

      $(document).on('tap', function(e){
        if( !$(e.target).closest('#tagValue').length ){
          $hashSearch.hide();
        }
      });

      $tagSearch.on('tap', function() {
        var queryValue = $tagValue.val();

        // ２文字以上の英数字
        if (queryValue.length >= 2 && !queryValue.match(/[^A-Za-z0-9]+/)) {
          app.hashtag.search(queryValue);
        }
      });
    },

    input: function() {
      $tagValue.on('tap', function() {
        if ($tagValue.val() !== '') {
          $hashSearch.show();
        }
      });

      $tagValue.keyup(function(e) {
        var queryValue = $tagValue.val();

        // ２文字以上の英数字
        if (queryValue.length >= 2 && !queryValue.match(/[^A-Za-z0-9]+/)) {
          $hashtagSearchText.text(queryValue);
          $hashtagSearch.show();
          $hashSearch.hide();
          app.hashtag.search(queryValue);
        } 
        // それ以外
        else {
          // Enter Key押下時
          if (e.which == 13) {
            $hashtagSearchText.text(queryValue);
            $hashtagSearch.show();
            $hashSearch.hide();
            app.hashtag.search(queryValue);
          }
          // 入力が何もない場合、検索結果を隠す
          if (queryValue == '') {
            $hashSearch.hide().children().html('');
          }
        }
      });
    },

    search: function(queryValue) {
      $tagSearch.html('<i class="fa fa-spinner fa-spin fa-fw"></i>');

      var option = {
        url : '/hashtags/search'
      }
      var data = {
        q : queryValue
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
          if (res !== null && res.length > 0) {
            $tagSearch.html('検索');
            app.hashtag.display(queryValue, res);
          }

          if (res.length == 0) {
            $tagSearch.html('検索');
            var hashSearchRow = '<div class="hash-search-row">';
                hashSearchRow += '<span>ハッシュタグが見つかりません</span>';
                hashSearchRow += '</div>';
            $hashSearchBox.html(hashSearchRow);
          }

          $hashtagSearch.hide();
        });
    },

    display: function(queryValue, hashtags) {
      var hashSearchRow = '';
      for (var i = 0; i < hashtags.length; i++) {
        hashSearchRow += '<div class="hash-search-row" data-name="' + hashtags[i].name + '">';
        hashSearchRow += '<span class="name">';
        hashSearchRow += '<span class="highlight">' + hashtags[i].name + '</span>';
        hashSearchRow += '<span class="count">(' + hashtags[i].media_count + ')</span>';
        hashSearchRow += '</div>';
      }

      if (hashtags.length > 0) {
        $hashSearch.show();
        $hashSearchBox.html(hashSearchRow);
      }
    },

    add: function(hashtagName) {
      if (hashtagName == '') {
        return false;
      }

      if (app.hashtag.remainCount == 0) {
        $('.alert').show();
        return false;
      }

      var option = {
        url : '/hashtags/add'
      }
      var data = {
        hashtagName : hashtagName
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
          switch (res.code) {
            case '200':
              var tag = '<li class="hashtag label label-info" data-id="' + res.id + '">' + res.message + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
              if ($hashtagBox.children('ul').length == 0) {
                $hashtagBox.text('');
                $hashtagBox.append('<ul></ul>');
              }
              $hashtagBox.children('ul').append(tag);
              app.hashtag.remainCount--;
              app.hashtag.updateMsg();
              $('.alert').fadeOut();
              break;

            case '400':
              $('.hashtag-exist').show();
              break;

            case '500':
              $('.hashtag-error').show();
              break;
          }
      });
    },

    delete: function() {
      $(document).on('tap', '.hashtag', function() {
        var option = {
          url : '/hashtags/delete'
        }
        var data = {
          id : $(this).data('id')
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              $(this).remove();
              app.hashtag.remainCount++;
              app.hashtag.updateMsg();
              $('.alert').fadeOut();
              if (app.hashtag.remainCount == app.hashtag.maxCount) {
                $hashtagBox.text('ハッシュタグがまだ登録されていません');
              }
            }
          });

        $(this).remove();
      });
    },

    updateMsg: function() {
      var msg = '';
      switch (app.hashtag.remainCount) {
        case 15:
          msg = '15個登録可能';
          break;

        case 0:
          msg = 'これ以上登録できません';
          break;

        default:
          msg = 'あと ' + app.hashtag.remainCount + ' 個 登録可能';
          break;
      }
      $hashtagMsg.text(msg);
    }
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.hashtag.init();
    },
  };

  // global variables
  var $tagValue = $('#tagValue'),
      $hashtag = $('.hashtag'),
      $hashSearch = $('.hash-search'),
      $hashSearchBox = $('.hash-search-box'),
      $hashtagMsg = $('#hashtagMsg'),
      $tagSearch = $('#tagSearch'),
      $hashtagBox = $('.hashtag-box'),
      $hashtagSearch = $('#hashtagSearch'),
      $hashtagSearchText = $('#hashtagSearchText');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
