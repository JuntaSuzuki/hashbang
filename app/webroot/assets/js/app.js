var app = app || {};

$(function() {

  // global inicialization functions
  app.global = {

    debugFlag: false,

    init: function() {
      app.global.notAllowSubmit();
      app.global.message();
    },

    debug: function(str) {
      if (app.global.debugFlag) console.log(str);
    },

    notAllowSubmit: function() {
      $(document).on("keypress", "input:not(.allow_submit)", function(e) {
        return e.which !== 13;
      });
    },

    message: function() {
      setTimeout(function() {
        $('.message').fadeOut();
      }, 2500);
    },

    // Ajax POST function
    ajaxPost: function(option, data, dataType) {

      if (option.formName != undefined) {
        var $form = $(option.formName);
        var query = $form.serialize();
        var param = $form.serializeArray();
      }

      var data = data || {};
      if (Object.keys(data).length === 0) {
        for (var o in param) {
          var key = param[o].name;
          data[key] = param[o].value;
        }
      }
      app.global.debug(data);
      app.global.debug(option);
      var dataType = dataType || 'json';
      return $.ajax({
        url: option.url,
        type: "POST",
        data: data,
        dataType: dataType,
        success : function(obj){
          app.global.debug(obj);
          app.global.debug('success');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          app.global.debug("XMLHttpRequest : " + XMLHttpRequest.status);
          app.global.debug("textStatus : " + textStatus);
          app.global.debug("errorThrown : " + errorThrown.message);
        },
      });

    },

    /**
     * 日付をフォーマットする
     * @param  {Date}   date     日付
     * @param  {String} [format] フォーマット
     * @return {String}          フォーマット済み日付
     */
    formatDate: function(date, format) {
      if (!format) format = 'YYYY-MM-DD hh:mm:ss.SSS';
      format = format.replace(/YYYY/g, date.getFullYear());
      format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
      format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
      format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
      format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
      format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
      if (format.match(/S/g)) {
        var milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
        var length = format.match(/S/g).length;
        for (var i = 0; i < length; i++) format = format.replace(/S/, milliSeconds.substring(i, i + 1));
      }
      return format;
    },

    // 数値を３桁区切りにする
    separate: function(num){
      return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    },

  };


  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.global.init();
    },
  };


  // global variables
  var $window = $(window),
      $body = $('body');


  // initializing
  $(document).ready( app.documentOnReady.init );


  $('.pay-btn').click(function (e) {
    $('#plan_id').val($(this).data('plan-id'));
    $('#payjp_checkout_box input[type="button"]').click();
  });

});
