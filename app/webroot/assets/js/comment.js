var app = app || {};

$(function() {

  // comment section functions
  app.comment = {

    maxCount: app.vars.comment,
    remainCount: 0,

    init: function() {
      app.comment.add();
      app.comment.delete();

      app.comment.remainCount = app.comment.maxCount - $comment.length;
    },

    display: function(res, comment) {
      switch (res.code) {
        case '200':
          var tag = '<li class="comment label label-success" data-id="' + res.id + '">' + res.message + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
          if ($commentBox.children('ul').length == 0) {
            $commentBox.text('');
            $commentBox.append('<ul></ul>');
          }
          $commentBox.children('ul').append(tag);
          app.comment.remainCount--;
          app.comment.updateMsg();
          $commentValue.val('');
          $('.alert').fadeOut();
          break;

        case '400':
          $('.comment-exist').show();
          break;

        case '500':
          $('.comment-error').show();
          break;
      }
    },

    add: function() {
      $commentAdd.on('tap', function() {
        var comment = $commentValue.val();
        if (comment == '') {
          return false;
        }

        if (app.comment.remainCount == 0) {
          $('.alert').show();
          return false;
        }

        var option = {
          url : '/comments/add'
        }
        var data = {
          comment : comment
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            app.comment.display(res, comment);
          });
      });

    },

    delete: function() {
      $(document).on('tap', '.comment', function() {
        var option = {
          url : '/comments/delete'
        }
        var data = {
          id : $(this).data('id')
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              $(this).remove();
              app.comment.remainCount++;
              app.comment.updateMsg();
              $('.alert').fadeOut();
              if (app.comment.remainCount == app.comment.maxCount) {
                $commentBox.text('コメントがまだ登録されていません');
              }
            }
          });

        $(this).remove();
      });
    },

    updateMsg: function() {
      var msg = '';
      switch (app.comment.remainCount) {
        case app.comment.maxCount:
          msg = app.comment.maxCount + '個登録可能';
          break;

        case 0:
          msg = 'これ以上登録できません';
          break;

        default:
          msg = 'あと ' + app.comment.remainCount + ' 個 登録可能';
          break;
      }
      $commentMsg.text(msg);
    }
  };

  // initialize when document ready

  app.documentOnReady = {
    init: function(){
      app.comment.init();
    },
  };

  // global variables

  var $window = $(window),
      $body = $('body'),
      $boxsBody = $('.boxs-body'),
      $comment = $('.comment'),
      $commentValue = $('#commentValue'),
      $commentAdd = $('#commentAdd'),
      $commentMsg = $('#commentMsg');
      $commentBox = $('.comment-box');


  // initializing
  $(document).ready( app.documentOnReady.init );

});
