var app = app || {};

$(function() {

  // auto section functions
  app.auto = {

    init: function() {
      app.auto.autoAction();
      app.auto.autoSpeed();
      app.auto.slider();
      app.auto.sliderUnfollow();
    },

    autoSetting: function(type, _this, auto) {
      var auto = (auto === false) ? false : $(_this).prop('checked');
      var option = {
        url : '/auto/setting'
      }
      var data = {
        type : type,
        auto : auto
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
        });
    },

    autoSpeedSetting: function(type, _this) {
      var option = {
        url : '/auto/speedSetting'
      }
      var data = {
        type : type,
        speed : $(_this).data('id')
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
        });
    },

    //チェックボックスの同期
    syncCheckbox: function(elemetnt, checked) {
      $(elemetnt).prop('checked', checked);
    },

    autoAction: function() {
      // 自動いいね
      $('.autoLikeSwitch').click(function() {
        app.auto.syncCheckbox('.autoLikeSwitch', $(this).prop('checked'));
        app.auto.autoSetting('like', this);
      });

      // 自動フォロー
      $('.autoFollowSwitch').click(function() {
        // 自動一括フォローチェック
        if ($('.autoBulkUnfollowSwitch').prop('checked')) {
          alert('自動一括フォロー解除の設定をOFFにしてください');
          return false;
        }
        app.auto.syncCheckbox('.autoFollowSwitch', $(this).prop('checked'));
        app.auto.autoSetting('follow', this);
      });

      // 自動コメント
      $('.autoCommentSwitch').click(function() {
        app.auto.syncCheckbox('.autoCommentSwitch', $(this).prop('checked'));
        app.auto.autoSetting('comment', this);
      });

      // 自動メッセージ
      $('.autoMessageSwitch').click(function() {
        app.auto.syncCheckbox('.autoMessageSwitch', $(this).prop('checked'));
        app.auto.autoSetting('message', this);
      });

      // 自動フォロー解除
      $('.autoUnfollowSwitch').click(function() {
        // 自動一括フォローチェック
        if ($('.autoBulkUnfollowSwitch').prop('checked')) {
          alert('自動一括フォロー解除の設定をOFFにしてください');
          return false;
        }
        app.auto.syncCheckbox('.autoUnfollowSwitch', $(this).prop('checked'));
        app.auto.autoSetting('unfollow', this);
      });

      // 自動一括フォロー解除
      $('.autoBulkUnfollowSwitch').click(function() {
        app.auto.syncCheckbox('.autoBulkUnfollowSwitch', $(this).prop('checked'));
        app.auto.autoSetting('bulk_unfollow', this);

        // 自動フォローを解除、自動フォロー解除を解除
        if ($(this).prop('checked')) {
          app.auto.syncCheckbox('.autoFollowSwitch', false);
          app.auto.autoSetting('follow', this, false);

          app.auto.syncCheckbox('.autoUnfollowSwitch', false);
          app.auto.autoSetting('unfollow', this, false);
        }
      });

      // 自動フォローバック
      $('.autoFollowBackSwitch').click(function() {
        app.auto.syncCheckbox('.autoFollowBackSwitch', $(this).prop('checked'));
        app.auto.autoSetting('followback', this);
      });
    },

    autoSpeed: function() {
      // 自動いいねスピード調整
      $('#autoLikeSpeed label').click(function() {
        app.auto.autoSpeedSetting('like', this);
      });

      // 自動フォロースピード調整
      $('#autoFollowSpeed label').click(function() {
        app.auto.autoSpeedSetting('follow', this);
      });

      // 自動コメントスピード調整
      $('#autoCommentSpeed label').click(function() {
        app.auto.autoSpeedSetting('comment', this);
      });

      // 自動フォロー解除スピード調整
      $('#autoUnfollowSpeed label').click(function() {
        app.auto.autoSpeedSetting('unfollow', this);
      });

      // 自動メッセージスピード調整
      $('#autoMessageSpeed label').click(function() {
        app.auto.autoSpeedSetting('message', this);
      });
    },

    slider: function() {
      if (typeof $.fn.slider != 'function') {
        return false;
      }

      // 自動いいねする投稿のいいね数
      $("#slider").slider({ 
        id: "sliderRange",
        min: 0,
        max: 1000,
        range: true,
        tooltip: "hide",
        step: 5
      });
      $("#slider").on("slide", function(e) {
        $("#min_like_count").text(e.value[0]);
        $("#max_like_count").text(e.value[1]);
      });
      $("#slider").on("slideStop", function(e) {
        var option = {
          url : '/auto/likeCount'
        }
        var data = {
          min_like_count : e.value[0],
          max_like_count : e.value[1]
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            app.global.debug(res);
          })
      });
    },

    sliderUnfollow: function() {
      if (typeof $.fn.slider != 'function') {
        return false;
      }

      // 自動いいねする投稿のいいね数
      $("#sliderUnfollow").slider({ 
        min: 1,
        max: 30,
        tooltip: "hide",
        step: 1
      });
      $("#sliderUnfollow").on("slide", function(e) {
        $("#unfollow_day").text(e.value);
      });
      $("#sliderUnfollow").on("slideStop", function(e) {
        var option = {
          url : '/auto/unfollowDay'
        }
        var data = {
          unfollow_day : e.value
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            app.global.debug(res);
          })
      });
    }
  }

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.auto.init();
    },
  };

  // global variables

  // initializing
  $(document).ready( app.documentOnReady.init );

});
