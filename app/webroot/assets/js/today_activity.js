var app = app || {};

$(function() {

  var types = {
    like : {
      id : 1,
      name : 'like'
    },
    follow : {
      id : 2,
      name : 'follow'
    },
    comment : {
      id : 3,
      name : 'comment'
    },
    message : {
      id : 4,
      name : 'message'
    },
    unfollow : {
      id : 5,
      name : 'unfollow'
    }
  }

  var didScroll = false;

  // todayActivity section functions
  app.todayActivity = {

    like_limit: app.vars.activity_limit,
    follow_limit: app.vars.activity_limit,
    comment_limit: app.vars.activity_limit,
    message_limit: app.vars.activity_limit,
    like_offset: 1,
    follow_offset: 1,
    comment_offset: 1,
    message_offset: 1,

    init: function() {
      app.todayActivity.more();
    },

    more: function() {
      $('#activity_like .wrap-reset').on('scroll', function() {
        var contentHeight = $(this).get(0).scrollHeight - $(this).parent().innerHeight();
        if (didScroll == false && (contentHeight - 300) < $(this).scrollTop()) {
          didScroll = true;
          app.todayActivity.moreActivity(types.like, this);
        }
      });

      $('#activity_follow .wrap-reset').on('scroll', function() {
        var contentHeight = $(this).get(0).scrollHeight - $(this).parent().innerHeight();
        if (didScroll == false && (contentHeight - 300) < $(this).scrollTop()) {
          didScroll = true;
          app.todayActivity.moreActivity(types.follow, this);
        }
      });

      $('#activity_comment .wrap-reset').on('scroll', function() {
        var contentHeight = $(this).get(0).scrollHeight - $(this).parent().innerHeight();
        if (didScroll == false && (contentHeight - 300) < $(this).scrollTop()) {
          didScroll = true;
          app.todayActivity.moreActivity(types.comment, this);
        }
      });

      $('#activity_message .wrap-reset').on('scroll', function() {
        var contentHeight = $(this).get(0).scrollHeight - $(this).parent().innerHeight();
        if (didScroll == false && (contentHeight - 300) < $(this).scrollTop()) {
          didScroll = true;
          app.todayActivity.moreActivity(types.message, this);
        }
      });
    },

    moreActivity: function(type, _this) {
      var option = {
        url : '/activities/moreToday'
      }

      var data = {
        limit : app.todayActivity[type.name + '_limit'],
        offset : app.todayActivity[type.name + '_offset'] * app.todayActivity[type.name + '_limit'],
        type : type.id
      }

      app.global.ajaxPost(option, data, 'html')
        .done(function(res) {
          app.todayActivity[type.name + '_offset']++;
          $(_this).append($(res).hide().fadeIn(300));
          didScroll = false;
      });
    }
  }

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.todayActivity.init();
    },
  };

  // global variables
  // var $window = $(window),
  //     $moreActivity = $('#moreActivity')
  //     $timeline = $('#timeline_item');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
