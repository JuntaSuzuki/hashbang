var app = app || {};

$(function() {

  // Instagram section functions
  app.instagram = {

    init: function() {
      app.instagram.login();
      app.instagram.add();
    },

    check: function(username, password) {
      if (username == '') {
        $instagramUsername.parents('.form-group').addClass('has-error');
        $instagramUsername.parent().next('.js-error').show();
      } else {
        $instagramUsername.parents('.form-group').removeClass('has-error');
        $instagramUsername.parent().next('.js-error').hide();
      }

      if (password == '') {
        $instagramPassword.parents('.form-group').addClass('has-error');
        $instagramPassword.parent().next('.js-error').show();
      } else {
        $instagramPassword.parents('.form-group').removeClass('has-error');
        $instagramPassword.parent().next('.js-error').hide();
      }
    },

    login: function() {
      $instagramAccount.on('tap', function() {

        var username = $instagramUsername.val();
        var password = $instagramPassword.val();
        app.instagram.check(username, password);

        if (username == '' || password == '') {
          return false;
        }

        var data = {
          username : username,
          password : password,
          validate: true
        }

        var option = {
          url : '/instagrams/login'
        }

        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (!res.success) {
              $form.find('span.error-message').remove();
              $('.input').removeClass('has-error');
              for (var el in res.data) {
                console.log(el);
                var e = $('input[name="data[Instagram][' + el + ']"]');
                e.parents('.input').removeClass('has-error');
                if (el !== 'success') {
                  for (var i = 0; i < res.data[el].length; i++) {
                    var error = $('<span class="error-message">').text(res.data[el][i]);
                    e.parents('.input').addClass('has-error');
                    e.after(error);
                  };
                }
              }
              return false;
            } else {
              $form.find('span.error-message').remove();
              $('.input').removeClass('has-error');
            }

            $().overlay('show', {
              'text' : '<p><b>インスタグラムに問い合わせ中...コネクトの完了まで数分かかる場合がございます。</b></p>',
              'element' : '<div class="circle"></div><div class="circle1"></div>'
            });

            delete data.validate;

            app.global.ajaxPost(option, data, 'json')
              .done(function(res) {
                if (res.success) {
                  location.reload();
                } else {
                  $().overlay('update', {
                    'text' : '<p><b>インスタグラムのアカウント名とパスワードが違います。<br>正しいアカウント名とパスワードを入力してください。</b></p>',
                    'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
                  });
                }
              })
              .fail(function(res) {
                app.global.debug(res);
                $().overlay('update', {
                    'text' : '<p><b>アカウント名とパスワードが違います。<br>正しいアカウント名とパスワードを入力してください。</b></p>',
                    'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
                  });
              });
          });
      });
    },

    add: function() {
      $instagramAccountAdd.on('tap', function() {

        var username = $instagramUsername.val();
        var password = $instagramPassword.val();
        app.instagram.check(username, password);

        if (username == '' || password == '') {
          return false;
        }

        var data = {
          username : username,
          password : password,
          validate: true
        }

        var option = {
          url : '/instagrams/add'
        }

        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (!res.success) {
              $form.find('span.error-message').remove();
              $('.input').removeClass('has-error');
              for (var el in res.data) {
                console.log(el);
                var e = $('input[name="data[Instagram][' + el + ']"]');
                e.parents('.input').removeClass('has-error');
                if (el !== 'success') {
                  for (var i = 0; i < res.data[el].length; i++) {
                    var error = $('<span class="error-message">').text(res.data[el][i]);
                    e.parents('.input').addClass('has-error');
                    e.after(error);
                  };
                }
              }
              return false;
            } else {
              $form.find('span.error-message').remove();
              $('.input').removeClass('has-error');
            }

            $().overlay('show', {
              'text' : '<p><b>インスタグラムに問い合わせ中...コネクトの完了まで数分かかる場合がございます。</b></p>',
              'element' : '<div class="circle"></div><div class="circle1"></div>'
            });

            delete data.validate;

            app.global.ajaxPost(option, data, 'json')
              .done(function(res) {
                if (res.success) {
                  location.reload();
                } else {
                  $().overlay('update', {
                    'text' : '<p><b>インスタグラムのアカウント名とパスワードが違います。<br>正しいアカウント名とパスワードを入力してください。</b></p>',
                    'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
                  });
                }
              })
              .fail(function(res) {
                app.global.debug(res);
                $().overlay('update', {
                    'text' : '<p><b>アカウント名とパスワードが違います。<br>正しいアカウント名とパスワードを入力してください。</b></p>',
                    'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
                  });
              });
          });
      });
    },
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.instagram.init();
    },
  };

  // global variables
  var $instagramAccount = $('#instagramAccount'),
      $instagramUsername = $('#InstagramUsername'),
      $instagramPassword = $('#InstagramPassword')
      $form = $('form'),
      $instagramAccountAdd = $('#instagramAccountAdd');


  // initializing
  $(document).ready( app.documentOnReady.init );

  $('.pay-btn').on('tap', function (e) {
    $('#plan_id').val($(this).data('plan-id'));
    $('#payjp_checkout_box input[type="button"]').click();
  });

});
