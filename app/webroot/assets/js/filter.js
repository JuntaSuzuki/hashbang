var app = app || {};

$(function() {

  // filter section functions
  app.filter = {

    maxCount: app.vars.filter,
    remainCount: 0,

    init: function() {
      app.filter.add();
      app.filter.delete();

      app.filter.remainCount = app.filter.maxCount - $filter.length;
    },

    display: function(res, filter) {
      switch (res.code) {
        case '200':
          var tag = '<li class="filter label label-warning" data-id="' + res.id + '">' + res.message + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
          if ($filterBox.children('ul').length == 0) {
            $filterBox.text('');
            $filterBox.append('<ul></ul>');
          }
          $filterBox.children('ul').append(tag);
          app.filter.remainCount--;
          app.filter.updateMsg();
          $filterValue.val('');
          $('.alert').fadeOut();
          break;

        case '400':
          $('.filter-exist').show();
          break;

        case '500':
          $('.filter-error').show();
          break;
      }
    },

    add: function() {
      $filterAdd.on('tap', function() {
        var filter = $filterValue.val();
        if (filter == '') {
          return false;
        }

        if (app.filter.remainCount == 0) {
          $('.alert').show();
          return false;
        }

        var option = {
          url : '/filters/add'
        }
        var data = {
          filter : filter
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            app.filter.display(res, filter);
          });
      });

    },

    delete: function() {
      $(document).on('tap', '.filter', function() {
        var option = {
          url : '/filters/delete'
        }
        var data = {
          id : $(this).data('id')
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              $(this).remove();
              app.filter.remainCount++;
              app.filter.updateMsg();
              $('.alert').fadeOut();
              if (app.filter.remainCount == app.filter.maxCount) {
                $filterBox.text('キーワードがまだ登録されていません');
              }
            }
          });

        $(this).remove();
      });
    },

    updateMsg: function() {
      var msg = '';
      switch (app.filter.remainCount) {
        case app.filter.maxCount:
          msg = app.filter.maxCount + '個登録可能';
          break;

        case 0:
          msg = 'これ以上登録できません';
          break;

        default:
          msg = 'あと ' + app.filter.remainCount + ' 個 登録可能';
          break;
      }
      $filterMsg.text(msg);
    }
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.filter.init();
    },
  };

  // global variables
  var $filter = $('.filter'),
      $filterValue = $('#filterValue'),
      $filterAdd = $('#filterAdd'),
      $filterMsg = $('#filterMsg'),
      $filterBox = $('.filter-box');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
