var app = app || {};

$(function() {

  // hashtagAnalytics section functions
  app.hashtagAnalytics = {

    init: function() {
      app.hashtagAnalytics.change();
    },

    change: function() {
      $period.on('change', function() {
        $('.table-responsive').prepend('<div class="hashtag-analytics-loading"><div class="circle"></div><div class="circle1"></div></div>');

        var option = {
          url : '/analytics/hashtag'
        }

        var data = {
          period : $(this).val()
        }

        app.global.ajaxPost(option, data, 'html')
          .done(function(res) {
            if (res) {
              $('.hashtag-analytics-loading').fadeOut();
              $result.html(res);
              $('.table-responsive').hide().fadeIn('slow');
            }
          });
      });
    },

  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.hashtagAnalytics.init();
    },
  };

  // global variables
  var $period = $('#period')
      $result = $('#result');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
