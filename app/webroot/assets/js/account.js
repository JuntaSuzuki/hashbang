var app = app || {};

$(function() {

  // Account section functions
  app.account = {

    init: function() {
      app.account.setAccount();
    },

    setAccount: function() {
      $account.on('tap', function() {

        $errorMessage.remove();
        var email = $userEmail.val();
        if (email == '') {
          $userEmail.parents('.form-group').addClass('has-error');
          $userEmail.parent().next('.js-error').show();
        } else {
          $userEmail.parents('.form-group').removeClass('has-error');
          $userEmail.parent().next('.js-error').hide();
        }

        var password = $userPassword.val();
        if (password == '') {
          $userPassword.parents('.form-group').addClass('has-error');
          $userPassword.parent().next('.js-error').show();
        } else {
          $userPassword.parents('.form-group').removeClass('has-error');
          $userPassword.parent().next('.js-error').hide();
        }

        if (email == '' || password == '') {
          return false;
        }

        $('#UserAccountForm').submit();
      });
    }
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.account.init();
    },
  };

  // global variables
  var $account = $('#account'),
      $errorMessage = $('.error-message'),
      $userEmail = $('#UserEmail'),
      $userPassword = $('#UserPassword');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
