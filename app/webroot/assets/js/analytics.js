var app = app || {};

$(function() {

  var types = {
    like : {
      id : 1,
      name : "like",
      color : "#b71c67"
    },
    follow : {
      id : 2,
      name : "follow",
      color : "#549b6f"
    },
    comment : {
      id : 3,
      name : "comment",
      color : "#f4c32f"
    },
    unfollow : {
      id : 5,
      name : "unfollow",
      color : "#080404"
    },
    follower : {
      id : 11,
      name : "follower",
      color : "#7cb5ec"
    },
    following : {
      id : 12,
      name : "following",
      color : "#7cb5ec"
    }
  }

  // analytics section functions
  app.analytics = {

    init: function() {
      app.analytics.graph();
      app.analytics.drawGraph(types.follower, 2);
      app.analytics.drawGraph(types.following, 2);
      app.analytics.drawGraph(types.like, 2);
      app.analytics.drawGraph(types.follow, 2);
      app.analytics.drawGraph(types.comment, 2);
      app.analytics.drawGraph(types.unfollow, 2);
    },

    graph: function() {
      $period.on('change', function() {
        var period = $(this).val();
        app.analytics.drawGraph(types.follower, period);
        app.analytics.drawGraph(types.following, period);
        app.analytics.drawGraph(types.like, period);
        app.analytics.drawGraph(types.follow, period);
        app.analytics.drawGraph(types.comment, period);
        app.analytics.drawGraph(types.unfollow, period);
      });
    },

    drawGraph: function(type, period) {
      var option = {
        url : '/analytics/analytics'
      }

      var data = {
        type : type.id,
        period : period
      }

      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
          var categories = [];
          for (var i = 0; i < res.result.length; i++) {
            categories.push(res.result[i].day);
          };

          var data = [];
          for (var i = 0; i < res.result.length; i++) {
            data.push(res.result[i].count);
          };

          Highcharts.chart('container_' + type.name, {
            chart: {
              type: 'spline',
            },
            title: {
              text: res.title
            },
            subtitle: {
              text: res.subtitle
            },
            xAxis: {
              categories: categories,
            },
            yAxis: {
              title: {
                text: false
              }
            },
            plotOptions: {
              spline: {
                dataLabels: {
                  enabled: true
                },
                enableMouseTracking: false
              }
            },
            credits: {
              enabled: false
            },
            series: [{
              name: res.title,
              data: data,
              color: type.color,
            }]
          });
        });
    }
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.analytics.init();
    },
  };

  // global variables
  var $period = $('#period')
      $result = $('#result');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
