var app = app || {};

$(function() {

  var limit = 20;
  var offset = 1;
  var didScroll = false;
  var didScrollFinished = false;

  // Whitelist section functions
  app.whitelist = {

    init: function() {
      app.whitelist.more();
      app.whitelist.add();
      app.whitelist.deleteList();
      app.whitelist.deleteLabel();
    },

    more: function() {
      $('.wrap-reset').on('scroll', function() {
        var contentHeight = $(this).get(0).scrollHeight - $(this).parent().innerHeight();
        if (!didScrollFinished && !didScroll && (contentHeight - 300) < $(this).scrollTop()) {
          didScroll = true;
          app.whitelist.moreFollowing(this);
        }
      });
    },

    moreFollowing: function(_this) {
      var option = {
        url : '/whitelists/moreFollowing'
      }

      var data = {
        limit : limit,
        offset : offset * limit
      }

      app.global.ajaxPost(option, data, 'html')
        .done(function(res) {
          if (res == '') {
            didScrollFinished = true;
          }
          offset++;
          $(_this).append($(res).hide().fadeIn(300));
          didScroll = false;
      });
    },

    add: function() {
      $(document).on('tap', '.whitelist-add', function() {
        var _this = $(this);
        var id = _this.data('id');

        _this.html('<i class="fa fa-spinner fa-spin fa-fw"></i>');

        var option = {
          url : '/whitelists/add'
        }
        var data = {
          id : id
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              _this.removeClass('btn-blue whitelist-add').addClass('btn-default whitelist-del').html('ホワイトリストに追加中');

              var format  = '<li class="whitelist whitelist-labeldel label" data-id="%s">';
                  format += '<img class="media-object br-5" src="%s" onerror="this.src=\'%s\';">';
                  format += '<span class="pr-10">%s</span>';
                  format += '<i class="fa fa-times" aria-hidden="true"></i>';
                  format += '</li>';

              if ($('.whitelist-labeldel').length == 0) {
                $whitelistMsg.text('クリックするとホワイトリストから解除されます。');
                $whitelistBox.append('<ul></ul>');
              }

              var media = _this.parents('.media');
              var whitelist = sprintf(
                format,
                id,
                media.find('img').attr('src'),
                '/img/no-image.png',
                media.find('.media-text').text()
              );
              $whitelistBox.find('ul').append($(whitelist).css({opacity: '0'}).animate({opacity: '1'}, 500));
            }
          });
      });
    },

    deleteList: function() {
      $(document).on('tap', '.whitelist-del', function() {
        var _this = $(this);
        var id = _this.data('id');

        _this.html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
        var option = {
          url : '/whitelists/delete'
        }
        var data = {
          id : id
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              _this.removeClass('btn-default whitelist-del').addClass('btn-blue whitelist-add').html('<i class="fa fa-plus" aria="hidden"></i> ホワイトリストに追加する');

              $('.whitelist-labeldel[data-id=' + id + ']').fadeOut(500).queue(function() {
                this.remove();

                if ($('.whitelist-labeldel').length == 0) {
                  $whitelistMsg.text('フォローユーザーはまだ登録されていません');
                  $whitelistBox.find('ul').remove();
                }
              });
            }
          });
      });
    },

    deleteLabel: function() {
      $(document).on('tap', '.whitelist-labeldel', function() {
        var _this = $(this);
        var id = _this.data('id');

        var option = {
          url : '/whitelists/delete'
        }
        var data = {
          id : id
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              _this.fadeOut(500).queue(function() {
                $('.whitelist-del[data-id=' + id + ']').removeClass('btn-default whitelist-del').addClass('btn-blue whitelist-add').html('<i class="fa fa-plus" aria="hidden"></i> ホワイトリストに追加する');
                this.remove();

                if ($('.whitelist-labeldel').length == 0) {
                  $whitelistMsg.text('フォローユーザーはまだ登録されていません');
                  $whitelistBox.find('ul').remove();
                }
              });
            }
          });
      });
    },
  };


  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.whitelist.init();
    },
  };

  // global variables
  var $whitelistBox = $('.whitelist-box'),
      $whitelistMsg = $('#whitelist_msg');


  // initializing
  $(document).ready( app.documentOnReady.init );

});
