var app = app || {};

$(function() {

  // hashtag section functions
  app.location = {

    maxCount: app.vars.location,
    remainCount: 0,
    markerShow: 0,
    map: null,
    infowindow: new google.maps.InfoWindow(),

    init: function() {
      app.location.input();
      app.location.add();
      app.location.delete();

      app.location.remainCount = app.location.maxCount - $location.length;

      // マップ表示
      google.maps.event.addDomListener(window, 'load', app.location.initialize);
    },

    // マップ初期化
    initialize: function() {
      var mapOptions = {
        zoom: 14,
        minZoom: 11,
        maxZoom: 19,
        center: new google.maps.LatLng(
          35.6699961,
          139.5675657 
        ),
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        scrollwheel: false,
      };

      app.location.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    },

    input: function() {
      $locationValue.on('tap', function() {
        if ($locationValue.val() !== '') {
          app.location.searchLatLng($locationValue.val());
        }
      });

      $locationValue.keyup(function(e) {
        var address = $locationValue.val();

        // ２文字以上の英数字
        if (address.length >= 2 && !address.match(/[^A-Za-z0-9]+/)) {
          app.location.searchLatLng($locationValue.val());
        } 
        // それ以外
        else {
          // Enter Key押下時
          if (e.which == 13) {
            $locationSearchText.text($locationValue.val());
            $locationSearch.show();
            app.location.searchLatLng($locationValue.val());
          }
        }
      });
    },

    // 住所から地図を検索
    searchLatLng: function(address) {
      var geocoder = new google.maps.Geocoder();
      // 住所から座標を取得する
      geocoder.geocode({
          'address': address, //検索する住所　〒◯◯◯-◯◯◯◯ 住所　みたいな形式でも検索できる
          'region': 'jp'
        },
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0].geometry) {
              var lat = results[0].geometry.location.lat();
              var lng = results[0].geometry.location.lng();

              // 地図表示
              app.location.drawMap(
                lat,
                lng,
                results[0].geometry.viewport
              );

              // ロケーション検索
              app.location.search(lat, lng, address);
            }
          } else {
            $alertFail.show();
          }
          app.location.markerShow = 0;
        }
      );
    },

    search: function(lat, lng, address) {
      var option = {
        url : '/locations/search'
      }
      var data = {
        lat: lat,
        lng: lng
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
          if (res !== null && res.length > 0) {
            $alertFail.fadeOut();

            $locationListResult.html('');
            var locationSearchRow = '<ul>';
            for (var i = 0; i < res.length; i++) {
              locationSearchRow += '<li class="location-add location label label-primary" data-id="' + res[i].id + '" data-lat="' + res[i].latitude + '" data-lng="' + res[i].longitude + '">';
              locationSearchRow += res[i].name;
              locationSearchRow += ' <i class="fa fa-plus" aria-hidden="true"></i>';
              locationSearchRow += '</li>';

              // マーカー表示
              app.location.marker(res[i]);
            }
            locationSearchRow += '</ul>';
            $locationListResult.append(locationSearchRow);
          }
          $locationList.fadeIn();
          $locationSearch.hide();
        });
    },

    display: function(queryValue, locations) {
      var locationSearchRow = '';
      for (var i = 0; i < locations.length; i++) {
        locationSearchRow += '<div class="location-search-row" data-name="' + locations[i].name + '">';
        locationSearchRow += '<span class="name">';
        locationSearchRow += '<span class="highlight">' + locations[i].name + '</span>';
        locationSearchRow += '<span class="count">(' + locations[i].media_count + ')</span>';
        locationSearchRow += '</div>';
      }

      if (locations.length > 0) {
        $locationSearch.show();
        $locationSearchBox.html(locationSearchRow);
      }
    },

    marker: function(location) {
      var markerImage = {
        url : '/img/map-icon.png',
        scaledSize: new google.maps.Size(20, 30)
      }

      var myLatLng = new google.maps.LatLng(location.latitude, location.longitude);
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: app.location.map,
        icon: markerImage,
      });

      // マーカー情報
      app.location.markerInfo(marker, location.name);
    },

    markerInfo: function(marker, name) {
      google.maps.event.addListener(marker, 'click', function (event) {
        app.location.infowindow.setContent(name);
        app.location.infowindow.open(app.location.map, marker);
      });
    },

    // 地図表示
    drawMap: function(lat, lng, viewport) {
      var center = new google.maps.LatLng(lat, lng);
      app.location.map.setCenter(center);
    },

    add: function() {
      $(document).on('tap', '.location-add', function() {
        if (app.location.remainCount == 0) {
          $alertNomore.show();
          return false;
        }

        var option = {
          url : '/locations/add'
        }
        var data = {
          id: $(this).data('id'),
          lat: $(this).data('lat'),
          lng: $(this).data('lng'),
          name: $(this).text()
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            switch (res.code) {
              case '200':
                var tag = '<li class="location location-del label label-primary" data-id="' + res.id + '">' + res.message + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
                if ($locationBox.children('ul').length == 0) {
                  $locationBox.text('');
                  $locationBox.append('<ul></ul>');
                }
                $locationBox.children('ul').append(tag);
                app.location.remainCount--;
                app.location.updateMsg();
                $('.alert').fadeOut();
                break;

              case '400':
                $('.hashtag-exist').show();
                break;

              case '500':
                $('.hashtag-error').show();
                break;
            }
          });
      });

    },

    delete: function() {
      $(document).on('tap', '.location-del', function() {
        var option = {
          url : '/locations/delete'
        }
        var data = {
          id : $(this).data('id')
        }
        app.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (res.success) {
              $(this).remove();
              app.location.remainCount++;
              app.location.updateMsg();
              $('.alert').fadeOut();
              if (app.location.remainCount == app.location.maxCount) {
                $locationBox.text('エリアがまだ登録されていません');
              }
            }
          });

        $(this).remove();
      });
    },

    updateMsg: function() {
      var msg = '';
      switch (app.location.remainCount) {
        case app.location.maxCount:
          msg = app.location.maxCount + '個登録可能';
          break;

        case 0:
          msg = 'これ以上登録できません';
          break;

        default:
          msg = 'あと ' + app.location.remainCount + ' 個 登録可能';
          break;
      }
      $locationMsg.text(msg);
    }
  };

  // initialize when document ready
  app.documentOnReady = {
    init: function(){
      app.location.init();
    },
  };

  // global variables
  var $locationValue = $('#locationValue'),
      $location = $('.location-del'),
      $locationBox = $('.location-box'),
      $locationList = $('.location-list'),
      $locationListResult = $('.location-list-result'),
      $alertFail = $('.alert-fail'),
      $alertNomore = $('.alert-nomore'),
      $locationMsg = $('#locationMsg'),
      $locationSearch = $('#locationSearch'),
      $locationSearchText = $('#locationSearchText');

  // initializing
  $(document).ready( app.documentOnReady.init );

});
