
(function( $ ) {

  var settings = {
    'text' : '',
    'element' : ''
  }

  var Overlay = {
    styleSheet : '\
      <style>\
      #overlay{\
        display: none;\
        width: 100%;\
        height:100%;\
        text-align: center;\
        position: fixed;\
        top: 0;\
        z-index: 100;\
        background-color: rgba(255,255,255,0.94);\
      }\
      #overlay p {\
        margin-bottom: 20px;\
        font-size: 16px;\
      }\
      .child {\
        position: absolute;\
        top: 50%;\
        left: 50%;\
        -webkit-transform: translate(-50%, -50%);\
        transform: translate(-50%, -50%);\
      }\
      </style>'
    ,
  }

  var methods = {
    init : function( options ) {
      settings = $.extend( settings, options );
      return this;
    },
    show : function( options ) {
      settings = $.extend( settings, options );
      overlay = '\
        <div class="parent">\
          <div class="child">\
            <div id="overlayContainer">\
              ' + settings.text + '\
              ' + settings.element + '\
            </div>\
          </div>\
        </div>';
      element = $('<div>').attr('id', 'overlay').append(Overlay.styleSheet).append(overlay);
      $('body').append(element);
      $('#overlay').fadeIn('slow');
    },
    hide : function( ) {
      $('#overlay').remove();
    },
    update : function( options ) {
      settings = $.extend( settings, options );
      $('#overlayContainer').html(settings.text + settings.element);
    }
  };

  $.fn.overlay = function( method ) {

    // メソッド呼び出し部分
    if ( methods[method] ) {
      // 呼び出すメソッドを指定した場合
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      // 引数がオブジェクトである場合、また、呼び出すメソッドを指定しない場合
      // init() を呼び出す
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.flatHeight' );
    }

  };
})( jQuery );

$(function() {

  $(document).on('click', '#closeOverlay', function() {
    $().overlay('hide');
  });

});
