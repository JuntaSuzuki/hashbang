/**
 * デバッグ用
 */
var debug_flag = true; // デバッグフラグ
var debug = function(str) {
  if (debug_flag) console.log(str);
}

/**
 * AjaxPOST関数
 */
var ajaxPost = function(option, data, dataType) {

  if (option.formName != undefined) {
    var $form = $(option.formName);
    var query = $form.serialize();
    var param = $form.serializeArray();
  }

  var data = data || {};
  if (Object.keys(data).length === 0) {
    for (var o in param) {
      var key = param[o].name;
      data[key] = param[o].value;
    }
  }
  debug(data);
  debug(option);
  var dataType = dataType || 'json';
  return $.ajax({
    url: option.url,
    type: "POST",
    data: data,
    dataType: dataType,
    success : function(obj){
      debug(obj);
      debug('success');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      debug("XMLHttpRequest : " + XMLHttpRequest.status);
      debug("textStatus : " + textStatus);
      debug("errorThrown : " + errorThrown.message);
    },
  });
}

var message = function() {
  $('.message').slideUp(500);
}

/**
 * 日付をフォーマットする
 * @param  {Date}   date     日付
 * @param  {String} [format] フォーマット
 * @return {String}          フォーマット済み日付
 */
var formatDate = function (date, format) {
  if (!format) format = 'YYYY-MM-DD hh:mm:ss.SSS';
  format = format.replace(/YYYY/g, date.getFullYear());
  format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
  format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
  format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
  format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
  format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
  if (format.match(/S/g)) {
    var milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
    var length = format.match(/S/g).length;
    for (var i = 0; i < length; i++) format = format.replace(/S/, milliSeconds.substring(i, i + 1));
  }
  return format;
};