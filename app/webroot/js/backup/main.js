$(function() {

  $("#menu-toggle").click(function(e){
    e.preventDefault()
    if (!$(".dashboard-left").hasClass('active')) {
      $(".dashboard-left").addClass('active')
      $("#menu-toggle").addClass('active')
      $("#menu-toggle i").removeClass('fa-bars').addClass('fa-times')
    } else {
      $(".dashboard-left").removeClass('active')
      $("#menu-toggle").removeClass('active')
      $("#menu-toggle i").removeClass('fa-times').addClass('fa-bars')
    }
  })

  $('.pay-btn').click(function (e) {
    $('#plan_id').val($(this).data('plan-id'));
    $('#payjp_checkout_box input[type="button"]').click();
  });


  // セッションメッセージ用
  setTimeout('message()', 4000);

  $('#openSignInModal').click(function() {
    $('#signUpModal').modal('hide');
    $('#signInModal').modal('show');
  });

  $('#openSignUpModal').click(function() {
    $('#signInModal').modal('hide');
    $('#signUpModal').modal('show');
  });

  $('#openPasswordModal').click(function() {
    $('#signInModal').modal('hide');
    $('#passwordModal').modal('show');
  });

  $('#openAccountModal').click(function() {
    $('#accountModal').modal('show');
  });

  // switchery
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

  elems.forEach(function(html) {
    var switchery = new Switchery(html);
  });

  $('.switchbtn').click(function() {
    $(this).toggleClass("active");
  });

  // SignUp
  $('#signUp').click(function() {
    var option = {
      url : '/users/sign_up'
    }
    var data = {
      email : $('#signUpModal #UserEmail').val(),
      password : $('#signUpModal #UserPassword').val()
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res.success) {
          location.href = App.vars.dashboard_url;
        }
      })
      .fail(function(res) {
      });
  });

  // SingIn
  $('#signIn').click(function() {
    var option = {
      url : '/users/sign_in'
    }
    var data = {
      email : $('#signInModal #UserEmail').val(),
      password : $('#signInModal #UserPassword').val()
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res.success) {
          location.href = App.vars.dashboard_url;
        }
      })
      .fail(function(res) {
      });
  });

  // Account
  $('#account').click(function() {

    $('.error-message').remove();
    var email = $('#UserEmail').val();
    if (email == '') {
      $('#UserEmail').parents('.form-group').addClass('has-error');
      $('#UserEmail').parent().next('.js-error').show();
    } else {
      $('#UserEmail').parents('.form-group').removeClass('has-error');
      $('#UserEmail').parent().next('.js-error').hide();
    }

    var password = $('#UserPassword').val();
    if (password == '') {
      $('#UserPassword').parents('.form-group').addClass('has-error');
      $('#UserPassword').parent().next('.js-error').show();
    } else {
      $('#UserPassword').parents('.form-group').removeClass('has-error');
      $('#UserPassword').parent().next('.js-error').hide();
    }

    if (email == '' || password == '') {
      return false;
    }

    $('#UserAccountForm').submit();
  });

  // Instagram Account
  $('.instagram-account').click(function() {

    var username = $('#InstagramUsername').val();
    if (username == '') {
      $('#InstagramUsername').parents('.form-group').addClass('has-error');
      $('#InstagramUsername').parent().next('.js-error').show();
    } else {
      $('#InstagramUsername').parents('.form-group').removeClass('has-error');
      $('#InstagramUsername').parent().next('.js-error').hide();
    }

    var password = $('#InstagramPassword').val();
    if (password == '') {
      $('#InstagramPassword').parents('.form-group').addClass('has-error');
      $('#InstagramPassword').parent().next('.js-error').show();
    } else {
      $('#InstagramPassword').parents('.form-group').removeClass('has-error');
      $('#InstagramPassword').parent().next('.js-error').hide();
    }

    if (username == '' || password == '') {
      return false;
    }

    $().overlay('show', {
      'text' : '<p><b>インスタグラムに問い合わせ中...コネクトの完了まで数分かかる場合がございます。</b></p>',
      'element' : '<div class="circle"></div><div class="circle1"></div>'
    });

    var option = {
      url : '/users/instagram'
    }
    var data = {
      username : username,
      password : password
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        debug(res);
        $().overlay('update', {
          'text' : '<p><b>アカウントの設定が完了しました。</b></p>',
          'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
        });
        $('.dashboard-profile-avator img').attr('src', res.profile_picture);
        $('.dashboard-profile-link span').text(res.username);
        $('#InstagramPassword').val('');
      })
      .fail(function(res) {
        debug(res);
        $().overlay('update', {
            'text' : '<p><b>アカウント名とパスワードが違います。正しいアカウント名とパスワードを入力してください。</b></p>',
            'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
          });
      });
  });


  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
    if ($('#follow_by_count_container').children('.highcharts-container').length > 0) {
      $('#follow_by_count_container').highcharts().reflow();
    }
    if ($('#follow_count_container').children('.highcharts-container').length > 0) {
      $('#follow_count_container').highcharts().reflow();
    }
    if ($('#like_container').children('.highcharts-container').length > 0) {
      $('#like_container').highcharts().reflow();
    }
    if ($('#follow_container').children('.highcharts-container').length > 0) {
      $('#follow_container').highcharts().reflow();
    }
  })

});
