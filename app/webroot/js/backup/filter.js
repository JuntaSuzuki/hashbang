$(function() {

  var filterMaxCount = 30;

  // カウント保持用オブジェクト
  var Filter = {
    remainCount: filterMaxCount - $('.filter').length
  }

  // テキストボックスでEnter Keyを押下時のsubmitを無効化
  $(document).on("keypress", "input:not(.allow_submit)", function(event) {
    return event.which !== 13;
  });

  function updateMsg() {
    var msg = '';
    switch (Filter.remainCount) {
      case filterMaxCount:
        msg = filterMaxCount + '個登録可能';
        break;

      case 0:
        msg = 'これ以上登録できません';
        break;

      default:
        msg = 'あと ' + Filter.remainCount + ' 個 登録可能';
        break;
    }
    $('#filterMsg').text(msg);
  }

  function displayFilter(res, filter) {
    switch (res.code) {
      case '200':
        var tag = '<li class="filter label label-warning" data-id="' + res.id + '">' + filter + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
        $('.filter-box').append(tag);
        Filter.remainCount--;
        updateMsg();
        $('#filterValue').val('');
        break;

      case '400':
        $('.filter-exist').show();
        break;

      case '500':
        $('.filter-error').show();
        break;
    }
  }

  // コメントを登録する
  $('#filterAdd').click(function() {
    var filter = $('#filterValue').val();
    if (filter == '') {
      return false;
    }

    if (Filter.remainCount == 0) {
      $('.filter-max-count').show();
      return false;
    }

    var option = {
      url : '/filters/add'
    }
    var data = {
      filter : filter
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        displayFilter(res, filter);
      });

  });

  // 登録中のコメントを削除する
  $(document).on('click', '.filter', function() {
    var option = {
      url : '/filters/delete'
    }
    var data = {
      id : $(this).data('id')
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res == 0) {
          $(this).remove();
          Filter.remainCount++;
          updateMsg();
          $('.alert').hide();
        }
      });

    $(this).remove();
  });
});