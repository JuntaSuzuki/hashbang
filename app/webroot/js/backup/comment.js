$(function() {

  var commentMaxCount = 30;

  // カウント保持用オブジェクト
  var Comment = {
    remainCount: commentMaxCount - $('.comment').length
  }

  // テキストボックスでEnter Keyを押下時のsubmitを無効化
  $(document).on("keypress", "input:not(.allow_submit)", function(event) {
    return event.which !== 13;
  });

  function updateMsg() {
    var msg = '';
    switch (Comment.remainCount) {
      case commentMaxCount:
        msg = commentMaxCount + '個登録可能';
        break;

      case 0:
        msg = 'これ以上登録できません';
        break;

      default:
        msg = 'あと ' + Comment.remainCount + ' 個 登録可能';
        break;
    }
    $('#commentMsg').text(msg);
  }

  function displayComment(res, comment) {
    switch (res.code) {
      case '200':
        var tag = '<li class="comment label label-success" data-id="' + res.id + '">' + comment + ' <i class="fa fa-times" aria-hidden="true"></i></li>';
        $('.comment-box').append(tag);
        Comment.remainCount--;
        updateMsg();
        $('#commentValue').val('');
        break;

      case '400':
        $('.comment-exist').show();
        break;

      case '500':
        $('.comment-error').show();
        break;
    }
  }

  // コメントを登録する
  $('#commentAdd').click(function() {
    var comment = $('#commentValue').val();
    if (comment == '') {
      return false;
    }

    if (Comment.remainCount == 0) {
      $('.comment-max-count').show();
      return false;
    }

    var option = {
      url : '/comments/add'
    }
    var data = {
      comment : comment
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        displayComment(res, comment);
      });
  });

  // 登録中のコメントを削除する
  $(document).on('click', '.comment', function() {
    var option = {
      url : '/comments/delete'
    }
    var data = {
      id : $(this).data('id')
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res == 0) {
          $(this).remove();
          Comment.remainCount++;
          updateMsg();
          $('.alert').hide();
        }
      });

    $(this).remove();
  });

});
