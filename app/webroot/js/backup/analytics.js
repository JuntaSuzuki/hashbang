
var dispAnalytics = function(json, opt) {
  if (json.result.length <= 2) {
    console.log('2以下');
    $('#' + opt.container).text('表示できるデータがまだありません');
    return;
  }
  var categories = [];
  var data = [];
  for (var i = 0; i < json.result.length; i++) {
    categories[i] = formatDate(new Date(json.result[i].day), 'MM/DD');
    data[i] = json.result[i].count;
  };
  
  Highcharts.chart(opt.container, {
    title: {
      text: false,
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: categories
    },
    yAxis: {
      title: {
        text: false
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: opt.plotLines_color
      }]
    },
    tooltip: {
      valueSuffix: opt.valueSuffix
    },
    legend: {
      enabled: false,
    },
    series: [{
      name: opt.name,
      color: opt.series_color,
      data: data
    }]
  });
}

$(window).load(function() {
  var now = new Date();
  var since = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 8);
  var until = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);

  var data = { 
    since: formatDate(since, 'YYYY-MM-DD'), until: formatDate(until, 'YYYY-MM-DD')
  }

  // フォロワー数
  $.getJSON("/analytics/followBy", data, function(json){
    var opt = {
      container: 'followed_by_count_container',
      plotLines_color: '#333333',
      valueSuffix: '回',
      name: 'フォロワー',
      series_color: '#9d62c1'
    }
    dispAnalytics(json, opt);
  });

  // フォロー数
  $.getJSON("/analytics/follow", data, function(json){
    var opt = {
      container: 'follow_count_container',
      plotLines_color: '#333333',
      valueSuffix: '回',
      name: 'フォロー',
      series_color: '#9d62c1'
    }
    dispAnalytics(json, opt);
  });

  // 自動いいね
  $.getJSON("/analytics/autoLike", data, function(json){
    var opt = {
      container: 'like_container',
      plotLines_color: '#333333',
      valueSuffix: '回',
      name: '自動いいね',
      series_color: '#8dc21f'
    }
    dispAnalytics(json, opt);
  });

  // 自動フォロー
  $.getJSON("/analytics/autoFollow", data, function(json){
    var opt = {
      container: 'follow_container',
      plotLines_color: '#333333',
      valueSuffix: '人',
      name: '自動フォロー',
      series_color: '#f0ad4e'
    }
    dispAnalytics(json, opt);
  });

});
