$(function() {

  function autoSetting(type, _this) {
    var option = {
      url : '/auto/setting'
    }
    var data = {
      type : type,
      auto : $(_this).children('.switchbtn').hasClass('active')
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
      });
  }

  // 自動いいね
  $('#autoLike').click(function() {
    autoSetting('like', this);
  });

  // 自動フォロー
  $('#autoFollow').click(function() {
    autoSetting('follow', this);
  });

  // 自動コメント
  $('#autoComment').click(function() {
    autoSetting('comment', this);
  });

  // 自動フォロー解除
  $('#autoUnfollow').click(function() {
    autoSetting('unfollow', this);
  });

  // 自動フォローバック
  $('#autoFollowback').click(function() {
    autoSetting('followback', this);
  });

  // 自動いいねする投稿のいいね数
  $("#slider").slider({ 
    id: "sliderRange",
    min: 0,
    max: 1000,
    range: true,
    tooltip: "hide",
    step: 5
  });
  $("#slider").on("slide", function(e) {
    $("#min_like_count").text(e.value[0]);
    $("#max_like_count").text(e.value[1]);
  });
  $("#slider").on("slideStop", function(e) {
    var option = {
      url : '/auto/likeCount'
    }
    var data = {
      min_like_count : e.value[0],
      max_like_count : e.value[1]
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        debug(res);
      })
  });

});
