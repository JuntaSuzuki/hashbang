$(function() {

  /**
   * ToDo
   * 検索結果が表示された後に、入力を追加して検索結果がなかった場合、内容が切り替わらない
   * 
   */

  // カウント保持用オブジェクト
  var Hashtag = {
    remainCount: 15 - $('.hashtag').length
  }

  // テキストボックスでEnter Keyを押下時のsubmitを無効化
  $(document).on("keypress", "input:not(.allow_submit)", function(event) {
    return event.which !== 13;
  });

  // ハッシュタグ入力
  $('#tagValue').keyup(function(e) {
    var queryValue = $('#tagValue').val();

    // ２文字以上の英数字
    if (queryValue.length >= 2 && !queryValue.match(/[^A-Za-z0-9]+/)) {
      searchHashtag(queryValue);
    } 
    // それ以外
    else {
      // Enter Key押下時
      if (e.which == 13) {
        searchHashtag(queryValue);
      }
      // 入力が何もない場合、検索結果を隠す
      if (queryValue == '') {
        $('.hash-search').hide().children().html('');
      }
    }
  });

  // ハッシュタグ入力のテキストボックスをクリックした際に、
  // 入力があれば、検索結果を表示する
  $('#tagValue').click(function(e) {
    var v = $('#tagValue').val();
    if (v !== '') {
      $('.hash-search').show();
    }
  });

  // ハッシュタグを検索する
  function searchHashtag(queryValue) {
    $('.hashtag-loading').show();

    var option = {
      url : '/hashtags/search'
    }
    var data = {
      q : queryValue
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res !== null && res.length > 0) {
          $('.hashtag-loading').hide();
          displayHashtag(queryValue, res);
        }

        if (res.length == 0) {
          $('.hashtag-loading').hide();
          var hashSearchRow = '<div class="hash-search-row">';
          hashSearchRow += '<span>ハッシュタグが見つかりません</span>';
          hashSearchRow += '</div>';
          $('.hash-search-box').html(hashSearchRow);
        }
      });
  }

  // 検索結果を表示する
  function displayHashtag(queryValue, hashtags) {
    var hashSearchRow = '';
    for (var i = 0; i < hashtags.length; i++) {
      hashSearchRow += '<div class="hash-search-row" data-name="' + hashtags[i].name + '">';
      hashSearchRow += '<span class="name">';
      hashSearchRow += '<span class="highlight">' + hashtags[i].name + '</span>';
      hashSearchRow += '<span class="count">(' + hashtags[i].media_count + ')</span>';
      hashSearchRow += '</div>';
    }

    if (hashtags.length > 0) {
      $('.hash-search').show();
      $('.hash-search-box').html(hashSearchRow);
    }
  }

  function updateMsg() {
    var msg = '';
    switch (Hashtag.remainCount) {
      case 15:
        msg = '15個登録可能';
        break;

      case 0:
        msg = 'これ以上登録できません';
        break;

      default:
        msg = 'あと ' + Hashtag.remainCount + ' 個 登録可能';
        break;
    }
    $('#hashtagMsg').text(msg);
  }

  // 検索結果のハッシュタグ名を押下時、テキストボックスに反映させる
  $(document).on('click', '.hash-search-row', function() {
    $('#tagValue').val($(this).data('name'));
  });

  // テキストボックス以外をクリックした場合、検索結果を隠す
  $(document).on('click', function(e){
    if( !$(e.target).closest('#tagValue').length ){
      $('.hash-search').hide();
    }
  });

  // ハッシュタグを登録する
  $('#tagAdd').click(function() {
    var hashtagName = $('#tagValue').val();
    if (hashtagName == '') {
      console.log();
      return false;
    }

    if (Hashtag.remainCount == 0) {
      $('.alert').show();
      return false;
    }

    var option = {
      url : '/hashtags/add'
    }
    var data = {
      hashtagName : hashtagName
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res == 0) {
          var tag = '<span class="hashtag label label-info">' + hashtagName + ' <i class="fa fa-times" aria-hidden="true"></i></span>';
          $('.tag-box').append(tag);
          Hashtag.remainCount--;
          updateMsg();
        }
      });
  });

  // 登録中のハッシュタグを削除する
  $(document).on('click', '.hashtag', function() {
    var option = {
      url : '/hashtags/delete'
    }
    var data = {
      id : $(this).data('id')
    }
    ajaxPost(option, data, 'json')
      .done(function(res) {
        if (res == 0) {
          $(this).remove();
          Hashtag.remainCount++;
          updateMsg();
          $('.alert').hide();
        }
      });

    $(this).remove();
  });

});
