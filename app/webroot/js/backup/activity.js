$(function() {

  var limit = App.vars.activity_limit;
  var offset = 1;

  function moreActivity() {
    var option = {
      url : '/api/moreActivity'
    }
    var data = {
      limit : limit,
      offset : offset * limit
    }
    ajaxPost(option, data, 'html')
      .done(function(res) {
        offset++;
        if (App.vars.activity_total <= limit * offset + 1) {
          $('#moreActivity').hide();
        }

        $('.timeline').append($(res).hide().fadeIn(300));
        $('#moreActivity').html('もっと見る');
      });
  }

  // アクティビティをもっとみる
  $('#moreActivity').click(function() {
    $('#moreActivity').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> 読み込み中...');
    setTimeout(moreActivity, 500);
  });

});
