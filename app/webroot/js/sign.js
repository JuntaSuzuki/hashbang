var App = App || {};

$(function() {

  // sign inicialization functions
  App.sign = {

    debugFlag: true,

    init: function() {
      App.sign.signUp();
      App.global.close();
    },

    signUp: function() {
      $signUp.click(function() {
        var _this = this;
        $(_this).prop("disabled", true);

        var option = {
          url : '/users/sign_up'
        }
        var data = {
          username : $username.val(),
          password : $password.val(),
          validate: true
        }

        App.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (!res.success) {
              $signUpForm.find('span.error-message').remove();
              $('.input').removeClass('has-error');
              for (var el in res.data) {
                var e = $('input[name="data[Instagram][' + el + ']"]');
                e.parents('.input').removeClass('has-error');
                if (el !== 'success') {
                  for (var i = 0; i < res.data[el].length; i++) {
                    var error = $('<span class="error-message">').text(res.data[el][i]);
                    e.parents('.input').addClass('has-error');
                    e.after(error);
                  };
                }
              }
              $(_this).prop("disabled", false);
              return false;
            } else {
              $signUpForm.find('span.error-message').remove();
              $('.input').removeClass('has-error');
            }

            $().overlay('show', {
              'text' : '<p><b>アカウントを登録しています...<br>完了まで数分かかる場合がございます。</b></p>',
              'element' : '<div class="circle"></div><div class="circle1"></div>'
            });

            delete data.validate;

            App.global.ajaxPost(option, data, 'json')
              .done(function(res) {
                if (res.success) {
                  location.href = '/users/complete';
                } else {
                  $().overlay('update', {
                    'text' : '<p><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> インスタグラムアカウントにアクセスできません。<br><br>\
                              インスタグラムのユーザー名またはパスワードが違います。<br>\
                              お使いのデバイスでインスタグラムにログインできているかご確認の上、再度入力をお試しください。<br><br>\
                              インスタグラムアプリにログインし、『アカウントの認証を実行』画面が表示される場合は、<br>\
                              インスタグラム側の手順に従って認証完了後、再度入力をお試しください。</b></p>\
                              <div class="row pb20">\
                              <div class="col-md-4 col-md-offset-2 md-mb-10"><a href="https://www.instagram.com/" target="_blank"><button type="button" class="btn btn-primary btn-block">インスタグラムを確認</button></a></div>\
                              <div class="col-md-4 close-button"><button type="button" class="btn btn-info btn-block" id="closeOverlay">入力に戻る</button></div>\
                              </div>\
                              <p><b>この問題が解消できない場合は <a href="/pages/contact" target="_blank">お問い合わせフォーム</a> よりお問い合わせください。</b></p>',
                    'element' : ''
                  });
                  $(_this).prop("disabled", false);
                }
              })
              .fail(function(res) {
                App.global.debug(res);
                $().overlay('update', {
                  'text' : '<p><b>インスタグラムのアカウント名とパスワードが違います。正しいアカウント名とパスワードを入力してください。</b></p>',
                  'element' : '<button type="button" class="btn btn-info" id="closeOverlay">閉じる</button>'
                });
                $(_this).prop("disabled", false);
              });
          });
      });
    },
  };

  // initialize when document ready
  App.documentOnReady = {
    init: function(){
      App.sign.init();
    }
  };

  // variables
  var $signUp = $('#signUp'),
      $username = $('#InstagramUsername'),
      $password = $('#InstagramPassword')
      $signUpForm = $('#InstagramSignUpForm');

  // initializing
  $(document).ready( App.documentOnReady.init );

});
