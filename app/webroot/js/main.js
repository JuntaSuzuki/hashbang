var App = App || {};

$(function() {

  // global inicialization functions
  App.global = {

    debugFlag: false,

    init: function() {
      App.global.notAllowSubmit();
      App.global.close();
      App.global.message();
    },

    debug: function(str) {
      if (App.global.debugFlag) console.log(str);
    },

    notAllowSubmit: function() {
      $(document).on("keypress", "input:not(.allow_submit)", function(e) {
        return e.which !== 13;
      });
    },

    message: function() {
      setTimeout(function() {
        $('.message').fadeOut();
      }, 4000);
    },

    close: function() {
      $('.close').click(function() {
        $(this).parent('#flashMessage').fadeOut('slow').queue(function() {
          this.remove();
        });
      });
    },

    // Ajax POST function
    ajaxPost: function(option, data, dataType) {

      if (option.formName != undefined) {
        var $form = $(option.formName);
        var query = $form.serialize();
        var param = $form.serializeArray();
      }

      var data = data || {};
      if (Object.keys(data).length === 0) {
        for (var o in param) {
          var key = param[o].name;
          data[key] = param[o].value;
        }
      }
      App.global.debug(data);
      App.global.debug(option);
      var dataType = dataType || 'json';
      return $.ajax({
        url: option.url,
        type: "POST",
        data: data,
        dataType: dataType,
        success : function(obj){
          App.global.debug(obj);
          App.global.debug('success');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          App.global.debug("XMLHttpRequest : " + XMLHttpRequest.status);
          App.global.debug("textStatus : " + textStatus);
          App.global.debug("errorThrown : " + errorThrown.message);
        },
      });

    },
  };

  // initialize when document ready
  App.documentOnReady = {
    init: function(){
      App.global.init();
    },
  };

  // global variables
  var $window = $(window),
      $body = $('body');

  // initializing
  $(document).ready( App.documentOnReady.init );

});
