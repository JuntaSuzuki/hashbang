var App = App || {};

$(function() {

  // contact inicialization functions
  App.contact = {

    init: function() {
      App.contact.send();
    },

    send: function() {
      $contactBtn.click(function() {
        var option = {
          url : '/pages/contact'
        }
        var data = {
          name : $ContactName.val(),
          email : $ContactEmail.val(),
          subject : $ContactSubject.val(),
          content : $ContactContent.val(),
          validate: true
        }

        App.global.ajaxPost(option, data, 'json')
          .done(function(res) {
            if (!res.success) {
              $contactForm.find('span.error-message').remove();
              $('.has-error').removeClass('has-error');
              for (var el in res.data) {
                if (el !== 'success') {
                  var e = $('input[name="data[Contact][' + el + ']"]');
                  for (var i = 0; i < res.data[el].length; i++) {
                    var error = $('<span class="error-message">').text(res.data[el][i]);
                    e.parents('.input').addClass('has-error');
                    e.after(error);
                  };
                }

                if (el !== 'success') {
                  var e = $('textarea[name="data[Contact][' + el + ']"]');
                  for (var i = 0; i < res.data[el].length; i++) {
                    var error = $('<span class="error-message">').text(res.data[el][i]);
                    e.parent().addClass('has-error');
                    e.after(error);
                  };
                }
              }
              return false;
            }

            $contactForm.find('span.error-message').remove();
            $('.has-error').removeClass('has-error');

            delete data.validate;

            $().overlay('show', {
              'text' : '<p><b>お問い合わせを送信しています...</b></p>',
              'element' : '<div class="circle"></div><div class="circle1"></div>'
            });

            App.global.ajaxPost(option, data, 'json')
              .done(function(res) {
                if (res.success) {
                  location.reload();
                }
              });
          });
      });
    },
  };

  // initialize when document ready
  App.documentOnReady = {
    init: function(){
      App.contact.init();
    }
  };

  // variables
  var $contactBtn = $('#contactBtn'),
      $ContactName = $('#ContactName'),
      $ContactEmail = $('#ContactEmail')
      $ContactSubject = $('#ContactSubject'),
      $ContactContent = $('#ContactContent'),
      $contactForm = $('#contactForm');

  // initializing
  $(document).ready( App.documentOnReady.init );

});
