<?php
/**
 * メール設定
 */
$config['mail'] = array(
    'from_email' => 'support@hashbang.jp',
    'no_reply_from_email' => 'no-reply@hashbang.jp',
    'from_name' => __('#BANGサポート'),
    'contact_from_name' => __('#BANG お問い合わせ'),
    'bcc' => true,
    'bcc_email' => 'support@hashbang.jp',
    'admin_email' => 'juntkn6.28@gmail.com'
);


/**
 * 各種設定
 */
$config['url'] = 'https://hashbang.jp';
$config['activity_limit'] = 20;
$config['dashboard_url'] = '/dashboard';

$config['nofollow'] = false;


/**
 * PAY.JP
 */
$config['test'] = false;
if ($config['test']) {
    // PAY.JP（テスト環境用）
    $config['payjp-public'] = 'pk_test_4acc89948fe03927bab2ca83';
    $config['payjp-secret'] = 'sk_test_1a7f3667abcb5e5ec142d87a';
} else {
    // PAY.JP（本番環境用）
    $config['payjp-public'] = 'pk_live_f98784fa42556858ae549361';
    $config['payjp-secret'] = 'sk_live_e7c04593a029c9ed7ff4d228c2bcde81464fd9ed4535e5ec55209588';
}

/**
 * ハッシュタグ・コメント・フィルターの上限数
 */
$config['hashtag_max_count'] = 15;
$config['comment_max_count'] = 30;
$config['filter_max_count'] = 30;
$config['location_max_count'] = 30;
$config['message_max_count'] = 3;

/**
 *  お試しプランの有効期限日数
 */
$config['expiration_days_of_trial'] = 7;

/**
 *  プラン
 */
$config['plan'] = [
    BASIC_PLAN => __('ベーシックプラン'),
    MESSAGE_PLAN => __('メッセージプラン'),
    SET_PLAN => __('セットプラン')
];
$config['month'] = [
    1 => __('1ヶ月（30日）'),
    3 => __('3ヶ月（90日）'),
    6 => __('6ヶ月（180日）'),
    12 => __('12ヶ月（365日）'),
];
$config['plan_price'] = [
    BASIC_PLAN => [
        1 => [
            'id' => 2,
            'price' => 2000,
            'day' => 30
        ]
    ],
    MESSAGE_PLAN => [
        1 => [
            'id' => 3,
            'price' => 2000,
            'day' => 30
        ]
    ],
    SET_PLAN => [
        1  => [
            'id' => 4,
            'price' => 3500,
            'day' => 30
        ],
        3  => [
            'id' => 5,
            'price' => 9000,
            'day' => 90
        ],
        6  => [
            'id' => 6,
            'price' => 15000,
            'day' => 180
        ],
        12  => [
            'id' => 7,
            'price' => 24000,
            'day' => 365
        ]
    ]
];

$config['payment_status'] = [
    0 => __('未決済'),
    1 => __('決済済み'),
];


/**
 *  自動更新
 */
$config['auto_subscription_status'] = [
    0 => __('未設定'),
    1 => __('決済済み'),
];

