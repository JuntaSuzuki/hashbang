<?php
/**
 * 各種定数
 *
 */
define('VENDOR','/Applications/XAMPP/htdocs/hashbang/app/Vendor/');

define('MEDIA_COUNT', 5);
define('SLOW_START_PERIOD', 5);


/**
 * アクション定数
 *
 */
define('LIKE', 1);
define('FOLLOW', 2);
define('COMMENT', 3);
define('MESSAGE', 4);
define('UNFOLLOW', 5);
define('FOLLOWBACK', 6);

define('FOLLOWER_COUNT', 11);
define('FOLLOWING_COUNT', 12);

/**
 * プラン定数
 */
define('FREE_PLAN', 1);
define('BASIC_PLAN', 2);
define('MESSAGE_PLAN', 3);
define('SET_PLAN', 4);
define('SET_PLAN1', 4);
define('SET_PLAN3', 5);
define('SET_PLAN6', 6);
define('SET_PLAN12', 7);
define('GOLD_PLAN', 99);

define('HASHBANG', true);
define('MANUAL', false);