<?php 
class AppSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $activities = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'アクション番号'),
		'instagram_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'mtb_hashtag_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ハッシュタグマスタ番号'),
		'mtb_location_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ロケーションマスタ番号'),
		'comment_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'コメント番号'),
		'action_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => 'アクション日'),
		'type' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 1, 'unsigned' => false, 'comment' => 'アクションタイプ 1:いいね 2:フォロー 3:コメント 4:メッセージ 5:フォロー解除 '),
		'is_followback' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動フォローバック　１：自動フォローバック　０：なし'),
		'follow_back' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'フォロワーからのフォローバック　1:フォローバックあり 0:フォローバックなし'),
		'unfollow' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'アンフォロー　1:解除済み 0:未解除'),
		'sent' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'メッセージ　1:送信済み 0:未送信'),
		'username' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象ユーザー名', 'charset' => 'utf8'),
		'instagram_username_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'アクション対象ユーザ番号'),
		'profile_picture' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象ユーザープロフィール写真', 'charset' => 'utf8'),
		'full_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象ユーザーフルネーム', 'charset' => 'utf8'),
		'media_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象投稿写真番号', 'charset' => 'utf8'),
		'media_picture_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象投稿写真URL', 'charset' => 'utf8'),
		'media_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'アクション対象投稿URL', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $auto_comment = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動コメントPK'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'unique', 'comment' => 'インスタグラム番号（#BANG）'),
		'mtb_auto_comment_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動コメントスピードマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'UNIQUE' => array('column' => 'instagram_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動コメントを行うユーザー情報')
	);

	public $auto_follow = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動フォローPK'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'unique', 'comment' => 'インスタグラム番号（#BANG）'),
		'mtb_auto_follow_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロースピードマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'UNIQUE' => array('column' => 'instagram_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動フォローを行うユーザー情報')
	);

	public $auto_like = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動いいねPK'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'unique', 'comment' => 'インスタグラム番号（#BANG）'),
		'mtb_auto_like_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動いいねスピードマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'UNIQUE' => array('column' => 'instagram_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動いいねを行うユーザー情報')
	);

	public $auto_message = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動メッセージPK'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'index', 'comment' => 'インスタグラム番号（#BANG）'),
		'mtb_auto_message_speed_id' => array('type' => 'integer', 'null' => true, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動メッセージスピードマスタ番号'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'follower_unique' => array('column' => 'instagram_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動メッセージを行うユーザー情報')
	);

	public $auto_schedule = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動アクションスケジュール番号PK'),
		'action_time' => array('type' => 'time', 'null' => true, 'default' => null, 'key' => 'index', 'comment' => '自動アクション時間'),
		'mtb_auto_like_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動いいねスピードマスタ番号'),
		'mtb_auto_follow_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロースピードマスタ番号'),
		'mtb_auto_comment_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動コメントスピードマスタ番号'),
		'mtb_auto_unfollow_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロー解除スピードマスタ番号'),
		'mtb_auto_message_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動メッセージスピードマスタ番号'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'follower_unique' => array('column' => 'action_time', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動アクションスケジュール情報')
	);

	public $auto_settings = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'コメント番号'),
		'user_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ユーザー番号'),
		'instagram_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'auto_like' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動いいね'),
		'auto_follow' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動フォロー'),
		'auto_comment' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動コメント'),
		'auto_message' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動メッセージ'),
		'auto_unfollow' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動フォロー解除'),
		'auto_followback' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動フォローバック'),
		'min_like_count' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 4, 'unsigned' => false, 'comment' => '最小いいね数'),
		'max_like_count' => array('type' => 'integer', 'null' => false, 'default' => '150', 'length' => 4, 'unsigned' => false, 'comment' => '最大いいね数'),
		'unfollow_day' => array('type' => 'integer', 'null' => false, 'default' => '3', 'length' => 2, 'unsigned' => false, 'comment' => 'フォロー解除経過日数'),
		'auto_like_speed' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動いいねスピード 　１：ゆっくり　２；普通　３：早い'),
		'auto_follow_speed' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロースピード 　１：ゆっくり　２；普通　３：早い'),
		'auto_comment_speed' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動コメントスピード 　１：ゆっくり　２；普通　３：早い'),
		'auto_unfollow_speed' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロー解除スピード 　１：ゆっくり　２；普通　３：早い'),
		'auto_message_speed' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '自動メッセージスピード 　１：ゆっくり　２；普通　３：早い'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $auto_unfollow = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動フォロー解除PK'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'unique', 'comment' => 'インスタグラム番号（#BANG）'),
		'mtb_auto_unfollow_speed_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'unsigned' => false, 'comment' => '自動フォロー解除スピードマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'UNIQUE' => array('column' => 'instagram_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動フォロー解除を行うユーザー情報')
	);

	public $comments = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'コメント番号'),
		'instagram_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'comment' => array('type' => 'string', 'null' => false, 'collate' => 'utf8mb4_general_ci', 'comment' => 'コメント', 'charset' => 'utf8mb4'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $filters = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'フィルター番号'),
		'instagram_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'ユーザー番号'),
		'keyword' => array('type' => 'string', 'null' => false, 'collate' => 'utf8_general_ci', 'comment' => 'フィルターキーワード', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $follower_count = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'フォロワーログ番号'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'follower_count' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'フォロワーカウント'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '1日に1回フォロワー数を保存する')
	);

	public $followers = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'unsigned' => true, 'key' => 'primary', 'comment' => 'フォロワー番号'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'index', 'comment' => 'インスタグラム番号（#BANG）'),
		'username_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'フォロワー　インスタグラムユーザー番号'),
		'username' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'フォロワー　インスタグラムユーザー名', 'charset' => 'utf8'),
		'profile_picture' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'フォロワー　プロフィール写真', 'charset' => 'utf8'),
		'message_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'メッセージ　1：送信済み　0：未送信'),
		'whitelist_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'ホワイトリスト　1：ON　0：OFF'),
		'follow_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'フォローステータス　1：フォロー済み　0：未フォロー'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => 'ステータス'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'follower_unique' => array('column' => array('instagram_id', 'username_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '1日に1回フォロー数を保存する')
	);

	public $following_count = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'フォローログ番号'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'following_count' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'フォローカウント'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '1日に1回フォロー数を保存する')
	);

	public $followings = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'unsigned' => true, 'key' => 'primary', 'comment' => 'フォロー番号'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 20, 'unsigned' => false, 'key' => 'index', 'comment' => 'インスタグラム番号（#BANG）'),
		'username_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'フォロワー　インスタグラムユーザー番号'),
		'username' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'フォロワー　インスタグラムユーザー名', 'charset' => 'utf8'),
		'profile_picture' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'フォロワー　プロフィール写真', 'charset' => 'utf8'),
		'whitelist_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'ホワイトリスト　1：ON　0：OFF'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => 'ステータス'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'follower_unique' => array('column' => array('instagram_id', 'username_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'フォロー情報')
	);

	public $hashtags = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'ハッシュタグ番号'),
		'instagram_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'mtb_hashtag_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'ハッシュタグマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $instagrams = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'インスタグラム番号'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ユーザー番号'),
		'username' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'インスタグラムユーザー名', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'インスタグラムパスワード', 'charset' => 'utf8'),
		'username_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラムユーザー番号'),
		'profile_picture' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'インスタグラムプロフィール写真', 'charset' => 'utf8'),
		'following_count' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'フォロー数'),
		'follower_count' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'フォロワー数'),
		'media_count' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => '投稿数'),
		'auto_status' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => '自動アクションステータス'),
		'message_status' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => 'メッセージ送信ステータス　上限に引っかかった場合falseにする'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '1:有効 0:無効 2:停止中'),
		'instagram_auth' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => 'インスタグラム認証'),
		'instagram_auth_date' => array('type' => 'timestamp', 'null' => true, 'default' => null, 'comment' => 'インスタグラム認証実施日'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $invites = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'user_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => '招待したユーザ'),
		'invite_user_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '招待されたユーザ'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 1, 'unsigned' => false, 'comment' => '0:退会　1:無料登録　2:有料プラン'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'UNIQUE_GROUP' => array('column' => array('user_id', 'invite_user_id'), 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $locations = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'ロケーション番号'),
		'instagram_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'mtb_location_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'ロケーションマスタ番号'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $messages = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'ハッシュタグ番号'),
		'instagram_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラム番号'),
		'message' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => 'メッセージ', 'charset' => 'utf8mb4'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $mtb_auto_comment_speed = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動コメントスピード番号'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'processing_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '処理件数'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動コメントスピード　マスタテーブル')
	);

	public $mtb_auto_follow_speed = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動フォロースピード番号'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'processing_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '処理件数'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動フォロースピード　マスタテーブル')
	);

	public $mtb_auto_like_speed = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動いいねスピード番号'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'processing_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '処理件数'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動いいねスピード　マスタテーブル')
	);

	public $mtb_auto_message_speed = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動メッセージスピード番号'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'processing_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '処理件数'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動メッセージスピード　マスタテーブル')
	);

	public $mtb_auto_unfollow_speed = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => '自動フォロー解除スピード番号'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'processing_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '処理件数'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '自動フォロー解除スピード　マスタテーブル')
	);

	public $mtb_hashtag = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'ハッシュタグマスタ番号'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'ハッシュタグ名', 'charset' => 'utf8'),
		'count' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'comment' => '登録数'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'ハッシュタグ　マスタテーブル')
	);

	public $mtb_location = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'ロケーションマスタ番号'),
		'instagram_location_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラムロケーション番号'),
		'lat' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '緯度'),
		'lng' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '経度'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'ロケーション名', 'charset' => 'utf8'),
		'count' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'comment' => '登録数'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'ハッシュタグ　マスタテーブル')
	);

	public $payments = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ユーザー番号'),
		'amount' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 6, 'unsigned' => false, 'comment' => '課金金額'),
		'plan_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => 'プラン番号'),
		'payment_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '課金処理結果ステータス'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '課金処理実行ステータス'),
		'contract_start_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '契約開始日'),
		'contract_end_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '契約終了日'),
		'charge_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '課金日'),
		'payjp_cus_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP 顧客ID', 'charset' => 'utf8'),
		'payjp_car_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP カードID', 'charset' => 'utf8'),
		'payjp_ch_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP 課金ID', 'charset' => 'utf8'),
		'payjp_error_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP エラーコード', 'charset' => 'utf8'),
		'payjp_error' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP エラー', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $plans = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'comment' => 'プラン名', 'charset' => 'utf8'),
		'amount' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 5, 'unsigned' => false, 'comment' => '料金'),
		'month' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '更新月単位'),
		'day' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false, 'comment' => '更新日単位'),
		'type' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 1, 'unsigned' => false, 'comment' => 'プランタイプ'),
		'class' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'クラス', 'charset' => 'utf8'),
		'label-class' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'ラベルクラス', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => true, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $queues = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'job' => array('type' => 'string', 'null' => false, 'collate' => 'utf8_general_ci', 'comment' => 'ジョブ内容', 'charset' => 'utf8'),
		'lock' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => 'ジョブのロック'),
		'created' => array('type' => 'timestamp', 'null' => false, 'default' => null, 'comment' => 'ジョブ追加日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'ジョブキューテーブル')
	);

	public $schema_migrations = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'class' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	public $selectors = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'セレクター番号'),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '説明', 'charset' => 'utf8'),
		'key' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'セレクターキー', 'charset' => 'utf8'),
		'selector' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'セレクター', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '作成日'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '更新日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $tests = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'message' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $users = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ユーザID'),
		'status' => array('type' => 'boolean', 'null' => true, 'default' => '1', 'comment' => 'ステータス'),
		'remove' => array('type' => 'boolean', 'null' => true, 'default' => '0', 'comment' => '退会'),
		'email' => array('type' => 'string', 'null' => true, 'collate' => 'utf8_general_ci', 'comment' => 'メールアドレス', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'パスワード', 'charset' => 'utf8'),
		'batch_json' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'バッチ用JSONデータ', 'charset' => 'utf8'),
		'instagram_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'インスタグラムID（メイン）'),
		'plan_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 2, 'unsigned' => false, 'comment' => 'プラン'),
		'plan_limit_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => 'プラン有効期限'),
		'next_charge_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '次回課金日'),
		'auto_subscription_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '自動課金ステータス'),
		'cus_id' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP 顧客ID', 'charset' => 'utf8'),
		'car_id' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'PAYJP カードID', 'charset' => 'utf8'),
		'invite_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'collate' => 'utf8_general_ci', 'comment' => '招待コード', 'charset' => 'utf8'),
		'locale' => array('type' => 'string', 'null' => false, 'default' => 'jpn', 'length' => 3, 'collate' => 'utf8_general_ci', 'comment' => '３文字ロケールコード（ISO 639-2 標準準拠）', 'charset' => 'utf8'),
		'reset_password_token' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'comment' => 'パスワードリセット用トークン', 'charset' => 'utf8'),
		'reset_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => 'パスワードリセット有効期限'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public $users_users = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ユーザID'),
		'user_id' => array('type' => 'biginteger', 'null' => true, 'default' => '1', 'unsigned' => false, 'comment' => 'ユーザー番号'),
		'link_user_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '連携しているユーザー番号'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

}
