<?php

App::uses('AppModel', 'Model');
App::uses('User', 'Model');

/**
 * 友達招待 モデル
 *
 * @package		app.Controller
 */
class Invite extends AppModel {

/**
 * アソシエーション
 */
	public $belongsTo = array(
		'Instagram' => array(
			'className' => 'Instagram',
			'foreignKey' => false,
			'conditions' => array('Instagram.')
		),
	);

/**
 * 招待情報の登録
 *
 * @param string $invite_code
 * @param int $userId
 * @return void
 */
	public function addInvite($invite_code, $userId) {

		// 招待したユーザの取得
		$this->User = new User();
		$user = $this->User->find('first', 
			array(
				'fields' => array('User.id'),
				'conditions' => array('User.invite_code' => $invite_code),
				'recursive' => -1,
			)
		);

		// 招待情報を登録
		$data = array(
			'user_id' => $user['User']['id'],
			'invite_user_id' => $userId,
			'status' => 1
		);
		$this->save($data);

		// 招待したユーザーに有料プラン1カ月分を付与
		// $valid_date_ts = strtotime("+30 day",time());
		// $data = array(
		// 	'User' => array(
		// 		'id' => $user['User']['id'],
		// 		'valid_date' => date('Y-m-d', $valid_date_ts)
		// 	)
		// );
		// $userInstance->save($data);
	}

/**
 * 招待コードの生成
 *
 * @param string $email
 * @return string $code
 */
	public function createCode($email) {
		$this->User = new User();
		$code = "";

		// メールアドレスを8進数の数値に変換
		$num = intval($email, 8);

		// 重複しない招待コードが生成されるまで
		while (true) {
			$code = mb_substr(sha1($num), 0, 5);

			$user = $this->User->find('first', 
				array(
					'fields' => array('User.id'),
					'conditions' => array(
						'User.invite_code' => $code
					),
					'recursive' => -1, 
				)
			);
			if (empty($user))
				break;
			$num++; // 重複した場合はnumの値を1増やす
		}

		return $code;
	}

/**
 * 招待コードで登録した人数を取得
 */
	public function inviteCount($userId) {
		// コンポーネントでモデルクラスを読み込んで使用する
		$inviteInstance = ClassRegistry::init('Invite');

		// 1カ月プランに変更
		$count = $inviteInstance->find('count', 
			array(
				'conditions' => array('user_id' => $userId),
			)
		);
		return $count;
	}


/**
 * 招待コード経由で登録されたユーザー情報のインスタグラム情報を取得
 *
 * @param int $userId ユーザー番号
 * @return array 
 */
	public function inviteUsers($userId) {
		// JOIN設定
		$joins = array(
			array(
				'type' => 'left',
				'table' => 'users',
				'alias' => 'u',
				'conditions' => array(
					'u.id = Invite.invite_user_id'
				)
			),
			array(
				'type' => 'left',
				'table' => 'instagrams',
				'alias' => 'insta',
				'conditions' => array(
					'insta.id = u.instagram_id'
				)
			)
		);

		// 検索オプション
		$option = array(
			'fields' => array(
				'u.plan_id', 'insta.username', 'insta.profile_picture'
			),
			'joins' => $joins,
			'conditions' => array(
				'Invite.user_id' => $userId
			),
			'recursive' => -1
		);

		return $this->find('all', $option);
	}
}
