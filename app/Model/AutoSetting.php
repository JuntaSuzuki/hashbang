<?php

App::uses('AppModel', 'Model');

/**
 * 自動設定 モデル
 *
 * @package     app.Controller
 */
class AutoSetting extends AppModel {

    public $useTable = 'auto_settings';

    public $actsAs = [
        'Containable'
    ];

    public $belongsTo = [
        'Instagram',
        'User'
    ];

/**
 * 自動設定の初期化
 *
 * @param int $userId
 * @param int $instagramId
 * @return void
 */
    public function init($userId, $instagramId) {
        $data = [
            'user_id' => $userId,
            'instagram_id' => $instagramId
        ];
        try {
            $this->save($data);
        } catch(Exception $e) {}
    }

/**
 * 自動設定をOFF
 *
 * @param int $userId
 * @return void
 */
    public function off($userId) {
        $fields = array(
            'AutoSetting.auto_like'          => false,
            'AutoSetting.auto_follow'        => false,
            'AutoSetting.auto_comment'       => false,
            'AutoSetting.auto_message'       => false,
            'AutoSetting.auto_unfollow'      => false,
            'AutoSetting.auto_followback'    => false,
            'AutoSetting.auto_bulk_unfollow' => false
        );

        $conditions = [
            'AutoSetting.user_id' => $userId
        ];

        try {
            $this->updateAll($fields, $conditions);
        } catch(Exception $e) {
            $this->log($e);
        }
    }

}
