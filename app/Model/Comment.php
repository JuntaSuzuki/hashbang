<?php

App::uses('AppModel', 'Model');

/**
 * コメント モデル
 *
 * @package       app.Model
 */
class Comment extends AppModel {

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram'
    ];

/**
 * ハッシュタグの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
    public function fetchComments($instagramId) {
        $conditions = [
            'conditions' => [
                'Comment.instagram_id' => $instagramId
            ]
        ];
        return $this->find('all', $conditions);
    }
}
