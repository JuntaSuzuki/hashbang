<?php

App::uses('AppModel', 'Model');

/**
 * メッセージ モデル
 *
 * @package       app.Model
 */
class Message extends AppModel {

    public $actsAs = array('Containable');

/**
 * アソシエーション
 */
    public $belongsTo = array(
        'Instagram' => array(
        ),
    );

/**
 * メッセージの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
    public function fetchMessage($instagramId) {
        $conditions = array(
            'conditions' => array(
                'Message.instagram_id' => $instagramId
            ),
            'recuresive' => -1
        );
        return $this->find('all', $conditions);
    }
}
