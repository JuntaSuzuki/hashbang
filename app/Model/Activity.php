<?php

App::uses('AppModel', 'Model');
App::uses('Hashtag', 'Model');

/**
 * アクティビティ モデル
 *
 * @package       app.Model
 */
class Activity extends AppModel {

    public $actsAs = ['Containable'];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbHashtag',
        'MtbLocation',
        'Comment'
    ];

/**
 * アクティビティの取得
 *
 * @param int $instagramId 
 * @param int $limit 
 * @param int $offset 
 * @return array || string 
 */
    public function fetchActivities($instagramId, $limit = 20, $offset = 0) {
        $option = [
            'fields' => [
                'type', 'username', 'profile_picture',
                'media_picture_url', 'media_url', 'action_date'
            ],
            'contain' => [
                'MtbHashtag' => [
                    'fields' => ['name']
                ],
                'MtbLocation' => [
                    'fields' => ['name']
                ],
            ],
            'conditions' => [
                'Activity.instagram_id' => $instagramId
            ],
            'order' => [
                'action_date' => 'DESC'
            ],
            'limit' => $limit,
            'offset' => $offset
        ];
        return $this->find('all', $option);
    }

/**
 * アクティビティの取得オプション
 *
 * @param int $instagramId 
 * @param int $limit 
 * @param int $offset 
 * @return array || string 
 */
    public function fetchActivitiesOption($instagramId, $settings = []) {
        $default = [
            'limit' => 20,
            'offset' => 0,
            'type' => [
                LIKE, COMMENT, FOLLOW, UNFOLLOW, MESSAGE
            ],
            'conditions' => []
        ];

        $settings = array_merge($default, $settings);

        $conditions = [
            'Activity.instagram_id' => $instagramId,
            'Activity.type' => $settings['type']
        ];
        $conditions = array_merge($conditions, $settings['conditions']);

        return [
            'fields' => [
                'type', 'username', 'profile_picture',
                'mtb_hashtag_id', 'mtb_location_id',
                'media_picture_url', 'media_url', 'action_date'
            ],
            'contain' => [
                'MtbHashtag' => [
                    'fields' => ['name']
                ],
                'MtbLocation' => [
                    'fields' => ['name']
                ],
                'Comment' => [
                    'fields' => ['comment']
                ]
            ],
            'conditions' => $conditions,
            'order' => [
                'action_date' => 'DESC'
            ],
            'limit' => $settings['limit'],
            'offset' => $settings['offset']
        ];
    }

/**
 * フォローの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
    public function fetchFollowings($instagramId, $type = 'html') {
        $option = [
            'fields' => ['username'],
            'contain' => [
            ],
            'conditions' => [
                'Activity.instagram_id' => $instagramId,
                'Activity.type'         => FOLLOW
            ]
        ];
        $res = $this->find('list', $option);

        // 配列のキーを連番で振り直す
        $res = array_merge($res);
        $data = [
            'data' => $res
        ];

        if ($type == 'json') {
            return json_encode($data);
        } else {
            return $data;
        }
    }

/**
 * コメントの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
    public function fetchComments($instagramId, $type = 'html') {
        $option = [
            'fields' => ['link'],
            'contain' => [
            ],
            'conditions' => [
                'Activity.user_id' => $instagramId,
                'Activity.type'    => COMMENT
            ]
        ];
        $res = $this->find('list', $option);

        // 配列のキーを連番で振り直す
        $res = array_merge($res);
        $data = [
            'data' => $res
        ];

        if ($type == 'json') {
            return json_encode($data);
        } else {
            return $data;
        }
    }

/**
 * 本日のアクティビティを取得
 *
 * @param int $instagramId
 * @param int $type
 * @param string $findType
 * @param int $limit
 * @return array || string
 */
    public function fetchTodayActivities($instagramId, $type, $findType = 'all', $limit = 10) {
        $option = [
            'fields' => [
                'type', 'username', 'profile_picture',
                'mtb_hashtag_id', 'mtb_location_id', 'comment_id',
                'media_picture_url', 'media_url', 'action_date'
            ],
            'contain' => [
                'MtbHashtag' => [
                    'fields' => ['name']
                ],
                'MtbLocation' => [
                    'fields' => ['name']
                ],
                'Comment' => [
                    'fields' => ['comment']
                ],
            ],
            'conditions' => [
                'Activity.instagram_id' => $instagramId,
                'Activity.type' => $type,
                'Activity.action_date >' => date('Y-m-d', strtotime(date('Y-m-d') . '0 day')) . ' 00:00:00',
                'Activity.action_date <' => date('Y-m-d', strtotime(date('Y-m-d') . '+1 day')) . ' 00:00:00'
            ],
            'order' => [
                'action_date' => 'DESC'
            ],
            'limit' => $limit
        ];
        return $this->find($findType, $option);
    }

/**
 * 前日のアクティビティを取得
 *
 * @param int $instagramId
 * @param int $type
 * @param string $findType
 * @return array || string
 */
    public function fetchYesterdayActivities($instagramId, $type, $findType = 'all') {
        $option = [
            'fields' => [
                'type', 'username', 'profile_picture',
                'mtb_hashtag_id', 'mtb_location_id',
                'media_picture_url', 'media_url', 'action_date'
            ],
            'contain' => [
                'MtbHashtag' => [
                    'fields' => ['name']
                ],
                'MtbLocation' => [
                    'fields' => ['name']
                ],
            ],
            'conditions' => [
                'Activity.instagram_id' => $instagramId,
                'Activity.type' => $type,
                'Activity.action_date >' => date('Y-m-d', strtotime('-1 day')) . ' 00:00:00',
                'Activity.action_date <=' => date('Y-m-d', strtotime('-1 day')) . ' 23:59:59'
            ],
            'order' => [
                'action_date' => 'DESC'
            ]
        ];
        return $this->find($findType, $option);
    }

/**
 * ホワイトリストに登録していないフォロー情報をランダムに取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchNonWhitelistFollowing($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'instagram_username_id AS username_id'],
            'conditions' => [
                'Activity.instagram_id' => $instagramId,
                'Activity.type' => FOLLOW,
                'Activity.whitelist_status' => false,
                'Activity.unfollow' => false,
            ],
            'order' => [
                'Activity.created' => 'ASC'
            ],
            'limit' => 1
        ];

        return $this->find('first', $option);
    }

/**
 * フォロー解除対象ユーザーを取得
 *
 * フォローを行ったが、
 * 指定日数経ってもフォローバックがなく、
 * フォロー解除を行っていないフォロー
 *
 * @param int $instagramId
 * @param int $unfollowDay
 * @return array
 */
    public function fetchUnfollowFollowings($instagramId, $unfollowDay = 5) {
        $option = [
            'fields' => ['id', 'username', 'instagram_username_id AS username_id'],
            'conditions' => [
                'Activity.instagram_id' => $instagramId,
                'Activity.type' => FOLLOW,
                'Activity.whitelist_status' => false,
                'Activity.followback' => false,
                'Activity.unfollow' => false,
                'Activity.action_date <= date(current_timestamp - interval ' . $unfollowDay . ' day)',
            ],
            'order' => [
                'Activity.created' => 'ASC'
            ],
            'limit' => 1
        ];

        return $this->find('first', $option);
    }

/**
 * フォロー情報の取得オプション
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchFollowingsOption($instagramId, $settings = []) {
        $default = [
            'limit' => 20,
            'offset' => 0,
            'whitelist_status' => [true, false],
            'conditions' => []
        ];

        $settings = array_merge($default, $settings);

        return $option = [
            'fields' => ['id', 'username', 'profile_picture', 'whitelist_status'],
            'conditions' => [
                'Activity.instagram_id'     => $instagramId,
                'Activity.type'             => FOLLOW,
                'Activity.unfollow'         => false,
                'Activity.whitelist_status' => $settings['whitelist_status']
            ],
            'order' => [
                'Activity.created' => 'DESC'
            ],
            'limit' => $settings['limit'],
            'offset' => $settings['offset']
        ];
    }

/**
 * ホワイトリストのフォロー情報のリスト取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchWhitelistFollowings($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'profile_picture'],
            'conditions' => [
                'Activity.instagram_id'     => $instagramId,
                'Activity.type'             => FOLLOW,
                'Activity.whitelist_status' => true,
                'Activity.unfollow'         => false
            ],
        ];

        return $this->find('all', $option);
    }

}
