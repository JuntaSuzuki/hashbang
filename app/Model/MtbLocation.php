<?php

App::uses('AppModel', 'Model');
App::uses('Location', 'Model');

class MtbLocation extends AppModel {

/**
 * テーブル設定
 */
	public $useTable = 'mtb_location';

	public $actsAs = array('Containable');

/**
 * 登録数の更新
 *
 * @param int $id mtb_locationテーブルのid
 * @return void
 */
	public function updateCount($id) {
		$this->Location = new Location();
		$count = $this->Location->find('count', array(
			'conditions' => array(
				'mtb_location_id' => $id
			)
		));
		$this->save(array(
			'id' => $id,
			'count' => $count
		));
	}
}
