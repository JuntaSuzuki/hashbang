<?php

App::uses('AppModel', 'Model');

/**
 * お問い合わせ モデル
 *
 * @package       app.Model
 */
class Contact extends AppModel {

    public $useTable = false;

/**
 * バリデーション
 */
    public $validate = [
        'name' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'お名前を入力してください。',
            ]
        ],
        'email' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'メールアドレスを入力してください。',
            ],
            'email' => [
                'rule' => ['email', true],
                'message' => '有効なメールアドレスを入力してください。',
            ]
        ],
        'subject' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'お問い合わせタイトルを入力してください。'
            ],
        ],
        'content' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'お問い合わせ内容を入力してください。'
            ],
        ]
    ];

}
