<?php

App::uses('AppModel', 'Model');

/**
 * フォロワーカウント モデル
 *
 * @package       app.Model
 */
class FollowerCount extends AppModel {

    public $useTable = 'follower_count';

    public $actsAs = array('Containable');

/**
 * アソシエーション
 */
    public $belongsTo = array(
        'Instagram' => array(
        ),
    );

}
