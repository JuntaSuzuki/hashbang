<?php

App::uses('AppModel', 'Model');

/**
 * 自動フォロー モデル
 *
 * @package       app.Model
 */
class AutoFollow extends AppModel {

    public $useTable = 'auto_follow';

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbAutoFollowSpeed'
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoFollow.instagram_id'
            ]
        ],
        'Hashtag' => [
            'className' => 'Hashtag',
            'foreignKey' => false,
            'conditions' => [
                'Hashtag.instagram_id = AutoFollow.instagram_id'
            ]
        ],
        'Location' => [
            'className' => 'Location',
            'foreignKey' => false,
            'conditions' => [
                'Location.instagram_id = AutoFollow.instagram_id'
            ]
        ]
    ];

/**
 * 自動フォローを行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams($speedId) {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username']
                ],
                'Hashtag' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'Location' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'AutoSetting' => [
                    'auto_follow'
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.follow_api_status' => true,
                // 自動アクティビティ条件
                'OR' => [
                    'Hashtag.id IS NOT NULL',
                    'Location.id IS NOT NULL'
                ],
                'AutoSetting.auto_follow' => true,
                'AutoFollow.mtb_auto_follow_speed_id' => $speedId
            ],
            'group' => [
                'Instagram.id'
            ]
        ];
        return $this->find('all', $option);
    }

/**
 * 自動フォローを登録
 *
 * @param int $instagramId
 * @param int $speedId
 * @return boolean
 */
    public function add($instagramId, $speedId) {
        $data = [
            'instagram_id' => $instagramId,
            'mtb_auto_follow_speed_id' => $speedId
        ];
        return $this->save($data);
    }

}
