<?php

App::uses('AppModel', 'Model');

/**
 * 自動コメント モデル
 *
 * @package       app.Model
 */
class AutoComment extends AppModel {

    public $useTable = 'auto_comment';

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbAutoCommentSpeed'
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoComment.instagram_id'
            ]
        ],
        'Comment' => [
            'className' => 'Comment',
            'foreignKey' => false,
            'conditions' => [
                'Comment.instagram_id = AutoComment.instagram_id'
            ]
        ],
        'Hashtag' => [
            'className' => 'Hashtag',
            'foreignKey' => false,
            'conditions' => [
                'Hashtag.instagram_id = AutoComment.instagram_id'
            ]
        ],
        'Location' => [
            'className' => 'Location',
            'foreignKey' => false,
            'conditions' => [
                'Location.instagram_id = AutoComment.instagram_id'
            ]
        ]
    ];

/**
 * 自動コメントを行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams($speedId) {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username']
                ],
                'Comment' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'Hashtag' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'Location' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'AutoSetting' => [
                    'fields' => ['auto_comment']
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.comment_api_status' => true,
                // 自動アクティビティ条件
                'Comment.id IS NOT NULL',
                'OR' => [
                    'Hashtag.id IS NOT NULL',
                    'Location.id IS NOT NULL'
                ],
                'AutoSetting.auto_comment' => true,
                'AutoComment.mtb_auto_comment_speed_id' => $speedId
            ],
            'group' => [
                'Instagram.id'
            ]
        ];
        return $this->find('all', $option);
    }

}
