<?php

App::uses('AppModel', 'Model');

/**
 * フォロー中ユーザー モデル
 *
 * @package       app.Model
 */
class Following extends AppModel {

    public $actsAs = array('Containable');

/**
 * アソシエーション
 */
    public $belongsTo = array(
        'Instagram' => array(
        ),
    );

/**
 * フォロー情報の取得オプション
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchFollowingsOption($instagramId, $settings = []) {
        $default = [
            'limit' => 20,
            'offset' => 0,
            'status' => true,
            'whitelist_status' => [true, false],
            'conditions' => []
        ];

        $settings = array_merge($default, $settings);

        return $option = [
            'fields' => ['id', 'username', 'profile_picture', 'whitelist_status'],
            'conditions' => [
                'Following.instagram_id'     => $instagramId,
                'Following.status'           => $settings['status'],
                'Following.whitelist_status' => $settings['whitelist_status']
            ],
            'order' => [
                'Following.created' => 'DESC'
            ],
            'limit' => $settings['limit'],
            'offset' => $settings['offset']
        ];
    }

/**
 * ホワイトリストのフォロー情報の取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchWhitelistFollowings($instagramId, $limit = null) {
        $option = [
            'fields' => ['id', 'username', 'profile_picture', 'whitelist_status'],
            'conditions' => [
                'Following.instagram_id'     => $instagramId,
                'Following.whitelist_status' => true,
                'Following.status'           => true
            ],
        ];

        if (!is_null($limit)) {
            $option['limit'] = $limit;
        }

        return $this->find('all', $option);
    }

/**
 * ホワイトリストのフォロー情報のリスト取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchWhitelistFollowingsList($instagramId) {
        $option = [
            'fields' => ['id', 'username_id'],
            'conditions' => [
                'Following.instagram_id'     => $instagramId,
                'Following.whitelist_status' => true,
                'Following.status'           => true
            ],
        ];

        return $this->find('list', $option);
    }

/**
 * ホワイトリストのフォロー情報をランダムに取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchNonWhitelistFollowing($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'username_id'],
            'conditions' => [
                'Following.instagram_id'     => $instagramId,
                'Following.whitelist_status' => false,
                'Following.status'           => true,
                'Following.following_type'   => HASHBANG
            ],
            'order' => [
                'Following.id' => 'ASC'
            ]
        ];

        return $this->find('first', $option);
    }

}
