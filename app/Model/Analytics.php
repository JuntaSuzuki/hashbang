<?php

App::uses('AppModel', 'Model');
App::uses('Activity', 'Model');
App::uses('Hashtag', 'Model');

/**
 * 統計分析用 モデル
 *
 * 統計分析に必要な各メソッドをもつ
 *
 * @package       app.Model
 */
class Analytics extends AppModel {

/**
 * テーブル未使用
 *
 * @var string
 */
    public $useTable = false;

/**
 * queryメソッドが実行される前に行う前処理
 *
 * @param array オプション値 $option (since: 日付文字列 YYYY-mm-dd, until 日付文字列 YYYY-mm-dd, json: true or false)
 * @return array merged array, or Null on failure.
 */
    public function beforeQuery($option) {
        $option = array_merge(
            array(
                'since' => null, 'until' => null, 'json' => false
            ),
            (array)$option
        );

        if (strtotime($option['since']) === false) {
            return null;
        }

        if (strtotime($option['until']) === false) {
            return null;
        }

        if ( empty($option['since']) || empty($option['until']) ) {
            return null;
        }

        $option['limit'] = ( strtotime($option['until']) - strtotime($option['since']) ) / 86400;

        return $option;
    }

/**
 * アナリティクス用のアクティビティを取得する
 *
 * @param array オプション値 $option (since: 日付文字列 YYYY-mm-dd, until 日付文字列 YYYY-mm-dd, json: true or false)
 * @return array Array of records, or Null on failure.
 */
    public function activity($option) {
        $option = $this->beforeQuery($option);
        if ($option == null) {
            return false;
        }

        $sql = "
            SELECT
                day 
                , coalesce(activity_count, 0) as activity_count

            FROM 

            (
                SELECT 
                    date_format(date_add('".$option['since']."', interval t1.generate_series day), '%Y-%m-%d') as day
                FROM
                    (
                        SELECT 0 generate_series FROM DUAL WHERE (@num:=1-1)*0 UNION ALL
                        SELECT @num:=@num+1 FROM `information_schema`.COLUMNS LIMIT ".$option['limit']."
                    ) as t1
            ) as calendars 

            LEFT JOIN 

            (
                SELECT 
                    date_format(action_date, '%Y-%m-%d') as action_date_f
                    ,count(id) as activity_count 
                FROM 
                    activities 
                WHERE 
                    instagram_id = ".$option['instagram_id']." AND type = ".$option['type']."
                GROUP BY
                    action_date_f
            ) AS t2 

            ON 
                calendars.day = t2.action_date_f
        ";

        $results = $this->query($sql);

        // 値が入っているかを確認
        $status = !empty($results);
        if (!$status) {
            $error = array(
                'message' => 'データがありません',
                'code' => 404
            );
        }

        // データの整形
        $result = array();
        foreach ($results as $key => $value) {
            $result[] = array(
                'day' => date('m/d', strtotime($value['calendars']['day'])),
                'count' => (int)$value[0]['activity_count']
            );
        }

        // JSONで返す場合
        if ($option['json']) {
            // タイトル
            switch ($option['type']) {
                case LIKE:
                    $title = __('自動いいね数');
                    break;

                case FOLLOW:
                    $title = __('自動フォロー数');
                    break;

                case COMMENT:
                    $title = __('自動コメント数');
                    break;

                case UNFOLLOW:
                    $title = __('自動フォロー解除数');
                    break;
            }
            $subtitle = false;

            // errorが定義されていない場合はstatusとresultの配列になる
            return json_encode(compact('status', 'result', 'error', 'title', 'subtitle'));
        }

        return $results;
    }

/**
 * アナリティクス用のフォロワー数を取得する
 *
 * @param array オプション値 $option (since: 日付文字列 YYYY-mm-dd, until 日付文字列 YYYY-mm-dd, json: true or false)
 * @return array Array of records, or Null on failure.
 */
    public function follower($option) {
        $option = $this->beforeQuery($option);
        if ($option == null) {
            return false;
        }

        $sql = "
            SELECT 
                date_format(created, '%Y-%m-%d') as day 
                , follower_count 
            FROM 
                follower_count 
            WHERE 
                instagram_id = ".$option['instagram_id']."
            ORDER BY created DESC 
            LIMIT ".$option['limit'];

        $results = $this->query($sql);

        // 値が入っているかを確認
        $status = !empty($results);
        if (!$status) {
            $error = array(
                'message' => 'データがありません',
                'code' => 404
            );
        }

        // データの整形
        $result = array();
        foreach ($results as $key => $value) {
            $timestamp = strtotime($value[0]['day']);
            $result[$timestamp] = array(
                'day' => date('m/d', $timestamp),
                'count' => (int)$value['follower_count']['follower_count']
            );
        }

        // 日付降順にソート
        ksort($result);

        // キーを振り直す
        $result = array_merge($result);

        // JSONで返す場合
        if ($option['json']) {
            $title = __('フォロワー数');
            //$subtitle = __('<span>#BANGを開始してからの</span>フォロワー数をグラフで確認できます');
            // errorが定義されていない場合はstatusとresultの配列になる
            return json_encode(compact('status', 'result', 'error', 'title', 'subtitle'));
        }

        return $results;
    }

/**
 * アナリティクス用のフォロー数を取得する
 *
 * @param array オプション値 $option (since: 日付文字列 YYYY-mm-dd, until 日付文字列 YYYY-mm-dd, json: true or false)
 * @return array Array of records, or Null on failure.
 */
    public function following($option) {
        $option = $this->beforeQuery($option);
        if ($option == null) {
            return false;
        }

        $sql = "
            SELECT 
                date_format(created, '%Y-%m-%d') as day 
                , following_count 
            FROM 
                following_count 
            WHERE 
                instagram_id = ".$option['instagram_id']."
            ORDER BY created DESC 
            LIMIT ".$option['limit'];

        $results = $this->query($sql);

        // 値が入っているかを確認
        $status = !empty($results);
        if (!$status) {
            $error = array(
                'message' => 'データがありません',
                'code' => 404
            );
        }

        // データの整形
        $result = array();
        foreach ($results as $key => $value) {
            $timestamp = strtotime($value[0]['day']);
            $result[$timestamp] = array(
                'day' => date('m/d', $timestamp),
                'count' => (int)$value['following_count']['following_count']
            );
        }

        // 日付降順にソート
        ksort($result);

        // キーを振り直す
        $result = array_merge($result);

        // JSONで返す場合
        if ($option['json']) {
            $title = __('フォロー数');
            //$subtitle = __('<span>#BANGを開始してからの</span>フォロー数をグラフで確認できます');

            // errorが定義されていない場合はstatusとresultの配列になる
            return json_encode(compact('status', 'result', 'error', 'title' ,'subtitle'));
        }

        return $results;
    }

/**
 * ハッシュタグ解析
 *
 * @param int $instagramId インスタグラム番号
 * @param int $period 期間番号
 * @return array Array of records, or Null on failure.
 */
    public function hashtag($instagramId, $periodId) {
        $period = [
            1 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-4 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ],
            2 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-8 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ],
            3 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-31 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ],
            4 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-61 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ]
        ];

        $this->Hashtag = new Hashtag();
        $hashtags = $this->Hashtag->find('all', [
            'fields' => [
            ],
            'contain' => [
                'MtbHashtag'
            ],
            'conditions' => [
                'instagram_id' => $instagramId
            ]
        ]);

        $analytics = [];
        foreach ($hashtags as $hashtag) {
            $sql = "
                SELECT 
                    count(Activity.type = :like or null) as like_count,
                    count(Activity.type = :follow or null) as follow_count,
                    count(Activity.type = :comment or null) as comment_count,
                    count(Activity.followback = 1 or null) as followback_count
                FROM activities as Activity 
                WHERE 
                    instagram_id = :instagram_id 
                    AND 
                    mtb_hashtag_id = :mtb_hashtag_id 
                    AND 
                    action_date > :since 
                    AND 
                    action_date < :until;";
 
            $binds = [
                ':like' => LIKE,
                ':follow' => FOLLOW,
                ':comment' => COMMENT,
                ':instagram_id' => $instagramId,
                ':mtb_hashtag_id' => $hashtag['Hashtag']['mtb_hashtag_id'],
                ':since' => $period[$periodId]['since'],
                ':until' => $period[$periodId]['until']
            ];
 
            $data = $this->Hashtag->getDataSource()->query($sql, $binds);

            // データ整形
            $analytics[] = [
                'hashtag' => $hashtag['MtbHashtag']['name'],
                'like_count' => $data[0][0]['like_count'],
                'follow_count' => $data[0][0]['follow_count'],
                'comment_count' => $data[0][0]['comment_count'],
                'followback_count' => $data[0][0]['followback_count'],
            ];
        }
        return $analytics;
    }

}
