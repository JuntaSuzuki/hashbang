<?php

App::uses('AppModel', 'Model');

/**
 * フォロワー モデル
 *
 * @package       app.Model
 */
class Follower extends AppModel {

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram'
    ];

/**
 * フォロワー情報の取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchFollowers($instagramId, $limit = null) {
        $option = [
            'fields' => ['id', 'username', 'profile_picture', 'whitelist_status'],
            'conditions' => [
                'Follower.instagram_id' => $instagramId,
                'Follower.status'       => true
            ],
        ];

        if (!is_null($limit)) {
            $option['limit'] = $limit;
        }

        return $this->find('all', $option);
    }

/**
 * ホワイトリストのフォロワー情報の取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchWhitelistFollowers($instagramId, $limit = null) {
        $option = [
            'fields' => ['id', 'username', 'profile_picture', 'whitelist_status'],
            'conditions' => [
                'Follower.instagram_id'     => $instagramId,
                'Follower.whitelist_status' => true,
                'Follower.status'           => true
            ],
        ];

        if (!is_null($limit)) {
            $option['limit'] = $limit;
        }

        return $this->find('all', $option);
    }

/**
 * ホワイトリストのフォロワー情報のリスト取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchWhitelistFollowersList($instagramId) {
        $option = [
            'fields' => ['id', 'username_id'],
            'conditions' => [
                'Follower.instagram_id'     => $instagramId,
                'Follower.whitelist_status' => true,
                'Follower.status'           => true
            ],
        ];

        return $this->find('list', $option);
    }

/**
 * フォローしていないフォロワー情報を取得
 *
 * @param int $instagramId 
 * @param int $limit
 * @return array
 */
    public function fetchNoFollowBack($instagramId, $limit = 50) {
        $option = [
            'fields' => ['id', 'username', 'profile_picture', 'username_id'],
            'conditions' => [
                'Follower.instagram_id'  => $instagramId,
                'Follower.following_status' => false,
                'Follower.status'        => true
            ],
            'order' => [
                'Follower.id' => 'ASC'
            ],
            'limit' => $limit
        ];

        return $this->find('all', $option);
    }

}
