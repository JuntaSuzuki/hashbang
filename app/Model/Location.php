<?php

App::uses('AppModel', 'Model');
App::uses('MtbLocation', 'Model');

class Location extends AppModel {

	public $actsAs = array('Containable');

/**
 * アソシエーション
 */
	public $belongsTo = array(
		'Instagram' => array(
		),
		'MtbLocation' => array(
		),
	);

/**
 * ロケーションの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
	public function fetchLocations($instagramId) {
		$option = array(
			'fields' => array('id'),
			'contain' => array(
				'MtbLocation' => array(
					'fields' => array('name')
				),
			),
			'conditions' => array(
				'Location.instagram_id' => $instagramId
			)
		);
		return $this->find('all', $option);
	}

/**
 * ロケーション登録
 *
 * @param string $location
 * @param int $instagramId 
 * @return boolean 
 */
	public function add($location, $instagramId) {
		try {
			$this->MtbLocation = new MtbLocation();

			// ロケーションマスタの存在チェック
			$check = $this->MtbLocation->findByName($hashtag);

			// 存在しなければ、ロケーションマスタテーブルに追加
			if (empty($check)) {
				$data = array(
					'name' => $location
				);
				if (!$this->MtbLocation->save($data)) {
					return false;
				}
				$mtbLocationId = $this->MtbLocation->getLastInsertID();
			} else {
				$mtbLocationId = $check['MtbLocation']['id'];
			}

			// すでにハッシュタグテーブルに追加しているか確認
			$conditions = array(
				'instagram_id' => $instagramId,
				'mtb_location_id' => $mtbLocationId
			);
			if (!$this->hasAny($conditions)) {
				// ハッシュタグテーブルに追加
				$data = $conditions;
				$this->create();
				if ($this->save($data)) {
					// 登録件数の更新
					$this->MtbLocation->updateCount($mtbLocationId);

					return true;
				}
			}

			return false;
		} catch(Exception $e) {
			$this->log($e->getMessage()."\n");
			return false;
		}
	}

}
