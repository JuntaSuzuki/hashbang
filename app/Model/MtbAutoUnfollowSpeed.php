<?php

App::uses('AppModel', 'Model');

/**
 * 自動フォロー解除スピード モデル
 *
 * @package       app.Model
 */
class MtbAutoUnfollowSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_unfollow_speed';

}
