<?php

App::uses('AppModel', 'Model');

class Filter extends AppModel {

	public $actsAs = array('Containable');

/**
 * アソシエーション
 */
	public $belongsTo = array(
		'Instagram' => array(
		),
	);

/**
 * フィルターキーワードの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
	public function fetchFilters($instagramId) {
		$conditions = array(
			'conditions' => array(
				'Filter.instagram_id' => $instagramId
			)
		);
		return $this->find('all', $conditions);
	}

/**
 * フィルターキーワードの取得（フォーマット済み）
 *
 * @param int $instagramId 
 * @return array || string 
 */
	public function fetchFormattedFilters($instagramId) {
		$filters = $this->fetchFilters($instagramId);
		$formattedFilter = array();
		foreach ($filters as $filter) {
			$formattedFilter[] = $filter['Filter']['keyword'];
		}

		return $formattedFilter;
	}
}
