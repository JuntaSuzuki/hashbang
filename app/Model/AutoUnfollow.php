<?php

App::uses('AppModel', 'Model');

/**
 * 自動フォロー解除 モデル
 *
 * @package       app.Model
 */
class AutoUnfollow extends AppModel {

    public $useTable = 'auto_unfollow';

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbAutoUnfollowSpeed'
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoUnfollow.instagram_id'
            ]
        ]
    ];

/**
 * 自動フォロー解除を行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams($speedId) {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username'],
                ],
                'AutoSetting' => [
                    'auto_unfollow', 'unfollow_day'
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.unfollow_api_status' => true,
                // 自動アクティビティ条件
                'AutoSetting.auto_unfollow' => true,
                'AutoUnfollow.mtb_auto_unfollow_speed_id' => $speedId
            ],
            'group' => [
                'Instagram.id'
            ]
        ];
        return $this->find('all', $option);
    }

}
