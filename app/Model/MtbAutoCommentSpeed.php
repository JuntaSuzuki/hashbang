<?php

App::uses('AppModel', 'Model');

/**
 * 自動コメントスピード モデル
 *
 * @package       app.Model
 */
class MtbAutoCommentSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_comment_speed';

}
