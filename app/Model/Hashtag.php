<?php

App::uses('AppModel', 'Model');
App::uses('MtbHashtag', 'Model');

class Hashtag extends AppModel {

	public $actsAs = array('Containable');

/**
 * アソシエーション
 */
	public $belongsTo = array(
		'Instagram' => array(
		),
		'MtbHashtag' => array(
		),
	);

/**
 * ハッシュタグの取得
 *
 * @param int $instagramId 
 * @return array || string 
 */
	public function fetchHashtags($instagramId) {
		$option = array(
			'fields' => array('id'),
			'contain' => array(
				'MtbHashtag' => array(
					'fields' => array('name')
				),
			),
			'conditions' => array(
				'Hashtag.instagram_id' => $instagramId
			)
		);
		return $this->find('all', $option);
	}

/**
 * ハッシュタグ登録
 *
 * @param string $hashtag
 * @param int $instagramId 
 * @return boolean 
 */
	public function add($hashtag, $instagramId) {
		try {
			$this->MtbHashtag = new MtbHashtag();

			// ハッシュタグマスタの存在チェック
			$check = $this->MtbHashtag->findByName($hashtag);

			// 存在しなければ、ハッシュタグマスタテーブルに追加
			if (empty($check)) {
				$data = array(
					'name' => $hashtag
				);
				if (!$this->MtbHashtag->save($data)) {
					return false;
				}
				$mtbHashtagId = $this->MtbHashtag->getLastInsertID();
			} else {
				$mtbHashtagId = $check['MtbHashtag']['id'];
			}

			// すでにハッシュタグテーブルに追加しているか確認
			$conditions = array(
				'instagram_id' => $instagramId,
				'mtb_hashtag_id' => $mtbHashtagId
			);
			if (!$this->hasAny($conditions)) {
				// ハッシュタグテーブルに追加
				$data = $conditions;
				$this->create();
				if ($this->save($data)) {
					// 登録件数の更新
					$this->MtbHashtag->updateCount($mtbHashtagId);

					return true;
				}
			}

			return false;
		} catch(Exception $e) {
			$this->log($e->getMessage()."\n");
			return false;
		}
	}

/**
 * ハッシュタグ一括登録
 *
 * @param array $hashtags
 * @param int $instagramId 
 * @return void 
 */
	public function addAll($hashtags, $instagramId) {
		foreach ($hashtags as $hashtag) {
			$this->add($hashtag, $instagramId);
		}
	}

}
