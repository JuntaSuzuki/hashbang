<?php

App::uses('AppModel', 'Model');

/**
 * 自動いいねスピード モデル
 *
 * @package       app.Model
 */
class MtbAutoLikeSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_like_speed';

}
