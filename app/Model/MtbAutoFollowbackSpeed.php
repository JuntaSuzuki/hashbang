<?php

App::uses('AppModel', 'Model');

/**
 * 自動フォローバックスピード モデル
 *
 * @package       app.Model
 */
class MtbAutoFollowbackSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_followback_speed';

}
