<?php

App::uses('AppModel', 'Model');

/**
 * トランザクション モデル
 *
 * @package       app.Model
 */
class TransactionManager extends AppModel {

    public $useTable = false;

/**
 * トランザクション開始
 *
 * @return $dataSource
 */
    public function begin(){
        $dataSource = $this->getDataSource();
        $dataSource->begin($this);
        return $dataSource;
    }

/**
 * コミット
 *
 * @param int $_dataSource
 * @return void
 */
    public function commit($_dataSource){
        $_dataSource->commit();
    }

/**
 * ロールバック
 *
 * @param int $_dataSource
 * @return void
 */
    public function rollback($_dataSource){
        $_dataSource->rollback();
    }

}
