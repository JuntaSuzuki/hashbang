<?php

App::uses('AppModel', 'Model');

class Plan extends AppModel {

/**
 * アソシエーション
 */


/**
 * プランを取得
 *
 * @return array
 */
	public function fetchPlan() {
		// キャッシュを利用
		$result = Cache::read('plan', 'short');

		// キャッシュがなければ取得
		if (!$result) {
			$result = $this->find('all', array(
				'recursive' => -1
			));

			// キャッシュする
			Cache::write('plan', $result, 'short');
		}

		return $result;
	}


/**
 * プランを取得
 *
 * @return array
 */
	public function fetchPlanByType($type = 1) {
		// キャッシュを利用
		$result = Cache::read('plan_type' . $type, 'short');

		// キャッシュがなければ取得
		if (!$result) {
			$result = $this->find('all', array(
				'conditions' => array(
					'Plan.type' => $type
				),
				'recursive' => -1
			));

			// キャッシュする
			Cache::write('plan_type' . $type, $result, 'short');
		}

		return $result;
	}

}
