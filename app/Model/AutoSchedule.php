<?php

App::uses('AppModel', 'Model');

/**
 * 自動アクションスケジュール モデル
 *
 * @package       app.Model
 */
class AutoSchedule extends AppModel {

    public $useTable = 'auto_schedule';

    public $actsAs = [
        'Containable'
    ];

/**
 * 時間から自動いいねのスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoLikeSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_like_speed_id');
    }

/**
 * 時間から自動フォローのスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoFollowSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_follow_speed_id');
    }

/**
 * 時間から自動フォローのスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoCommentSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_comment_speed_id');
    }

/**
 * 時間から自動メッセージのスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoMessageSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_message_speed_id');
    }

/**
 * 時間から自動フォロー解除のスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoUnfollowSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_unfollow_speed_id');
    }

/**
 * 時間から自動フォローバックのスピード設定を取得
 *
 * @param string $time
 * @return int
 */
    public function fetchAutoFollowbackSpeedId($time) {
        return $this->fetchSpeedId($time, 'mtb_auto_followback_speed_id');
    }

/**
 * 時間からスピード設定を取得
 *
 * @param string $time
 * @param string $column
 * @return int
 */
    private function fetchSpeedId($time, $column) {
        $option = [
            'fields' => [$column],
            'conditions' => [
                'AutoSchedule.action_time' => $time
            ]
        ];
        $res = $this->find('first', $option);
        return (int)$res['AutoSchedule'][$column];
    }

}
