<?php

App::uses('AppModel', 'Model');

/**
 * 自動フォロースピード モデル
 *
 * @package       app.Model
 */
class MtbAutoFollowSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_follow_speed';

}
