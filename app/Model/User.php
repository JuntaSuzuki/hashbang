<?php

App::uses('AppModel', 'Model');

/**
 * ユーザー モデル
 *
 * @package       app.Model
 */
class User extends AppModel {

    public $actsAs = ['Containable'];

/**
 * アソシエーション
 */
    public $hasOne = [
        'Instagram',
        'AutoSetting'
    ];

    public $belongsTo = [
        'Plan'=> [
            'className' => 'Plan',
            'foreignKey' => '',
            'conditions' => [
                'User.plan_id = Plan.id'
            ]
        ]
    ];

    public $hasAndBelongsToMany = [
        'LinkUser' => [
          'className' => 'User',
          'join_table' => 'users_users',
          'foreignKey' => 'user_id',
          'associationForeignKey' => 'link_user_id',
          'with' => 'UsersUser',
        ]
    ];

/**
 * バリデーション
 */
    public $validate = [
        'email' => [
            'isBlank' =>[
                'rule' => 'notBlank',
                'message' => 'メールアドレスを入力してください。',
            ],
            'isUnique' =>[
                'rule' => 'isUniqueEmail', //重複禁止
                'message' => '既に使用されているメールアドレスです。',
                 'on' => 'create'
            ],
            'isEmail' =>[
                'rule' => ['email', true],
                'message' => '有効なメールアドレスを入力してください。',
            ],
            'isUniqueEmail' =>[
                'rule'    => 'isUniqueEmailNot',
                'message' => '既に使用されているメールアドレスです。',
                'on' => 'update'
            ],
        ],
        'password' => [
            'isBlank' => [
                'rule' => 'notBlank',
                'message' => 'パスワードを入力してください。'
            ],
            'isValid' => [
                'rule' => 'alphanumericsymbols',
                'message' => 'パスワードは半角英数記号にしてください。'
            ],
            'isBetween' => [
                'rule' => ['between', 8, 32],
                'message' => 'パスワードは8文字以上32文字以内にしてください。'
            ]
        ],
        'password_confirm' => [
            'password_confirm-rule-1' => [
                'rule' => 'notBlank',
                'message' => '確認パスワードを入力してください。',
            ],
            'password_confirm-rule-2' => [
                'allowEmpty' => true,
                'rule'    => ['equaltofield', 'password'],
                'message' => '上で入力されたパスワードと違います。'
            ]
        ]
    ];

    public function beforeSave($options = []) {
        $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        return true;
    }

/**
 * 英数記号のみのバリデーション
 *
 * @param array $check
 * @return boolena
 */
    public function alphanumericsymbols($check){
        $value = array_values($check);
        $value = $value[0];
        return preg_match('/^[a-zA-Z0-9\s\x21-\x2f\x3a-\x40\x5b-\x60\x7b-\x7e]+$/', $value);
    }

/**
 * メールアドレスの確認
 */
    public function isUniqueInstagramUsername($check, $on = 'create') {
        if ($on == 'create') {
            $format = "
                SELECT count(id) as count
                    FROM instagrams 
                WHERE 
                    username = '%s';";
            $sql = sprintf($format, $check['instagram_username']);
        } else {
            $format = "
                SELECT count(id) as count
                    FROM instagrams 
                WHERE 
                    username = '%s' AND 
                    username != (SELECT username FROM instagrams WHERE id = %s);";
            $sql = sprintf($format, $check['instagram_username'], $this->data['User']['id']);
        }

        $res = $this->query($sql);

        return !(bool)$res[0][0]['count'];
    }

/**
 * メールアドレスの確認
 */
    public function isUniqueEmail($check) {
        $sql = "
            SELECT count(id) as count
                FROM users 
            WHERE 
                email = :email AND status = :status
        ";

        $binds = [
            ':email' => $check['email'],
            ':status' => true
        ];
        $res = $this->query($sql, $binds, false);

        return !(bool)$res[0][0]['count'];
    }

/**
 * メールアドレスの確認
 */
    public function isUniqueEmailNot($check) {
        $sql = "
            SELECT count(id) as count
                FROM users 
            WHERE 
                email = :email AND 
                email != (SELECT email FROM users WHERE id = :id AND status = :status);";

        $binds = [
            ':email' => $check['email'],
            ':id' => $this->data['User']['id'],
            ':status' => true
        ];
        $res = $this->query($sql, $binds, false);

        return !(bool)$res[0][0]['count'];
    }

/**
 * ユーザーの存在確認
 * アカウント追加時のチェックに利用
 *
 * @param array $param
 * @return int User.id || null
 */
    public function isExistUniqueUser(array $param) {
        $sql = "
            SELECT id 
                FROM users AS User
            WHERE 
                email = :email AND 
                password = :password AND 
                email != (SELECT email FROM users WHERE id = :id);";

        $binds = [
            ':email' => $param['email'],
            ':password' => AuthComponent::password($param['password']),
            ':id' => $param['user_id']
        ];
        $res = $this->query($sql, $binds, false);

        return !empty($res) ? $res[0]['User']['id'] : null;
    }

/**
 * パスワードの確認
 */
    public function equaltofield($check, $otherfield) {
        // $check には array('password_confirm' => '入力値') が入る
        // $otherfield には 'password' が入っている
        $checkName = '';
        foreach ($check as $key => $value){
            $checkName = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$checkName];
    }

/**
 * アカウント情報の取得
 *
 * @param int $id ユーザー番号 
 * @return array
 */
    public function fetchAccount($id) {
        $option = [
            'fields' => [
                'id', 'email', 'password'
            ],
            'contain' => [
                'Instagram' => [
                    'fields' => [
                        'id', 'username', 'password'
                    ]
                ]
            ],
            'conditions' => [
                'User.id' => $id
            ]
        ];
        $res = $this->find('first', $option);

        $tmp = isset($res['Instagram'][0]) ? $res['Instagram'][0] : [];
        unset($res['Instagram']);

        $res['Instagram'] = $tmp;

        return $res;
    }

/**
 * インスタグラムアカウント情報の取得
 *
 * @param int $userId ユーザー番号 
 * @return array
 */
    public function fetchInstagramAccount($userId) {
        $option = [
            'fields' => ['id', 'email'],
            'contain' => [
                'LinkUser' => [
                    'fields' => ['id', 'email'],
                    'Instagram' => [
                        'fields' => [
                            'username'
                        ]
                    ]
                ]
            ],
            'conditions' => [
                'User.id' => $userId
            ]
        ];

        $res = $this->find('first', $option);

        // データ整形
        $instagrams = [];
        foreach ($res['LinkUser'] as $value) {
            if (!empty($value['Instagram'])) {
                $instagrams[] = $value['Instagram'];
            }
        }

        return $instagrams;
    }

/**
 * 有効期限チェック
 *
 * @param int $userId ユーザー番号 
 * @return array
 */
    public function expirationCheck($userId) {
        $res = $this->find('first', [
            'fields' => [
                'plan_limit_date', 'plan_id', 'cus_id', 'car_id', 'activate'
            ],
            'conditions' => [
                'User.id' => $userId
            ],
            'recursive' => -1
        ]);

        // 期限チェック
        if ($res['User']['plan_id'] == GOLD_PLAN) {
            $expiration = true;
        } else {
            $expiration = $res['User']['plan_limit_date'] >= date('Y-m-d');
        }

        return [
            'expiration' => $expiration,
            'plan_id'    => $res['User']['plan_id'],
            'cus_id'     => $res['User']['cus_id'],
            'car_id'     => $res['User']['car_id'],
            'activate'   => $res['User']['activate']
        ];
    }

/**
 * アカウント追加バリデーション
 *
 * @param array $param
 * @return boolean
 */
    public function accountAddValidates($param) {
        // バリデート
        $this->set([
            'email' => $param['email'],
            'password' => $param['password']
        ]);

        unset($this->validate['email']['isUnique']);
        unset($this->validate['email']['isUniqueEmail']);

        unset($this->validate['password']['isValid']);
        unset($this->validate['password']['isBetween']);

        return $this->validates();
    }

/**
 * インスタグラムアカウント情報が存在するか
 *
 * @param int $userId ユーザー番号
 * @param string $username インスタグラムユーザー名
 * @return boolean
 */
    public function hasInstagramAccount($userId, $username) {
        $option = [
            'fields' => ['id'],
            'contain' => [
                'LinkUser' => [
                    //'fields' => ['id'],
                    'Instagram' => [
                        'fields' => [
                            'username'
                        ],
                        'conditions' => [
                            'username' => $username
                        ],
                        'limit' => 1
                    ]
                ]
            ],
            'conditions' => [
                'User.id' => $userId,
            ]
        ];

        $res = $this->find('first', $option);
        if (empty($res['LinkUser'])) {
            return false;
        }

        $user = null;
        foreach ($res['LinkUser'] as $value) {
            if (!empty($value['Instagram']) && $value['Instagram']['username'] == $username) {
                $user = $value;

                // 不要データ削除
                unset($user['password']);
                unset($user['UsersUser']);
                unset($user['Instagram']);
                break;
            }
        }

        return $user;
    }


/**
 * 自動フォローバック対象のアカウントを取得
 *
 * @return array
 */
    public function fetchFollowBack() {
        $option = [
            'fields' => [
                'id'
            ],
            'contain' => [
                'Instagram' => [
                    'fields' => [
                        'id'
                    ]
                ],
                'AutoSetting' => [
                    'fields' => [
                        'id', 'auto_followback'
                    ]
                ]
            ],
            'conditions' => [
                // ユーザーアカウント条件
                'User.status' => true,
                'User.plan_limit_date >= ' => date('Y-m-d'),
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                // 自動アクティビティ条件
                'AutoSetting.auto_followback' => true
            ],
        ];

        return $this->find('all', $option);
    }

}
