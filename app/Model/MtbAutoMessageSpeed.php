<?php

App::uses('AppModel', 'Model');

/**
 * 自動メッセージスピード モデル
 *
 * @package       app.Model
 */
class MtbAutoMessageSpeed extends AppModel {

/**
 * テーブル設定
 */
    public $useTable = 'mtb_auto_message_speed';

}
