<?php

App::uses('AppModel', 'Model');
App::uses('MtbAutoLikeSpeed', 'Model');
App::uses('MtbAutoFollowSpeed', 'Model');
App::uses('MtbAutoCommentSpeed', 'Model');
App::uses('MtbAutoMessageSpeed', 'Model');
App::uses('MtbAutoUnfollowSpeed', 'Model');
App::uses('MtbAutoFollowbackSpeed', 'Model');

class Auto extends AppModel {

    public $useTable = false;

/**
 * 自動設定の回数を取得
 *
 * @return array
 */
    public function fetchCount() {
        // キャッシュを利用
        $result = Cache::read('auto_count', 'short');

        // キャッシュがなければ取得
        if (!$result) {
            $models = [
                ['MtbAutoLikeSpeed', LIKE],
                ['MtbAutoFollowSpeed', FOLLOW],
                ['MtbAutoCommentSpeed', COMMENT],
                ['MtbAutoMessageSpeed', MESSAGE],
                ['MtbAutoUnfollowSpeed', UNFOLLOW],
                ['MtbAutoFollowbackSpeed', FOLLOWBACK]
            ];

            foreach ($models as $model) {
                list($model, $type) = $model;
                $this->$model = new $model();
                $result[$type] = $this->$model->find('list', [
                    'fields' => [
                        'processing_number'
                    ]
                ]);
            }

            // キャッシュする
            Cache::write('auto_count', $result, 'short');
        }

        return $result;
    }

}