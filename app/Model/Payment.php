<?php

App::uses('AppModel', 'Model');

/**
 * 課金 モデル
 *
 * @package       app.Model
 */
class Payment extends AppModel {

    public $actsAs = ['Containable'];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Plan',
        'User'
    ];

/**
 * 課金の取得オプション
 *
 * @param int $userId 
 * @return array || string 
 */
    public function fetchOptionByUserId($userId) {
        return [
            'Payment' => [
                'fields' => [
                    'amount', 'charge_date', 'payment_status', 'status',
                    'contract_start_date', 'contract_end_date'
                ],
                'contain' => [
                    'Plan' => [
                        'fields' => [
                            'name', 'month', 'day',
                        ]
                    ],
                ],
                'conditions' => [
                    'Payment.user_id' => $userId
                ],
                'order' => [
                    'Payment.id' => 'DESC'
                ]
            ]
        ];
    }

/**
 * 次回課金情報を取得
 *
 * @param int $userId 
 * @return array || string 
 */
    public function fetchNextCharge($userId) {
        $option = [
            'contain' => [
                'Plan' => [
                    'fields' => [
                        'id', 'name', 'month', 'day',
                    ]
                ],
            ],
            'conditions' => [
                'Payment.user_id' => $userId,
                'Payment.status' => false,
                'Payment.payment_status' => false
            ],
            'order' => [
                'Payment.id' => 'DESC'
            ]
        ];

        return $this->find('first', $option);
    }

// /**
//  * プランを取得
//  *
//  * @return array
//  */
//  public function fetchPlan() {
//      // キャッシュを利用
//      $result = Cache::read('plan', 'short');

//      // キャッシュがなければ取得
//      if (!$result) {
//          $result = $this->find('all', array(
//              'recursive' => -1
//          ));

//          // キャッシュする
//          Cache::write('plan', $result, 'short');
//      }

//      return $result;
//  }


// /**
//  * プランを取得
//  *
//  * @return array
//  */
//  public function fetchPlanByType($type = 1) {
//      // キャッシュを利用
//      $result = Cache::read('plan_type' . $type, 'short');

//      // キャッシュがなければ取得
//      if (!$result) {
//          $result = $this->find('all', array(
//              'conditions' => array(
//                  'Plan.type' => $type
//              ),
//              'recursive' => -1
//          ));

//          // キャッシュする
//          Cache::write('plan_type' . $type, $result, 'short');
//      }

//      return $result;
//  }

}
