<?php

App::uses('AppModel', 'Model');
App::uses('User', 'Model');

/**
 * ユーザー連携 モデル
 *
 * @package       app.Model
 */
class UsersUser extends AppModel {

/**
 * アソシエーション
 */
    public $belongsTo = [
        'User'=> [
            'className' => 'User',
            'foreignKey' => 'user_id',
        ],
        'LinkUser'=> [
            'className' => 'User',
            'foreignKey' => 'link_user_id',
        ]
    ];

/**
 * ユーザー連携情報を登録
 *
 * @param array $param 
 * @return boolean
 */
    public function add($param) {
        // 存在チェック
        $this->User = new User();
        $userId = $this->User->isExistUniqueUser($param);
        if (!$userId) {
            return false;
        }

        // 連携済みチェック
        $conditions = [
            'user_id' => $param['user_id'],
            'link_user_id' => $userId
        ];
        if ($this->hasAny($conditions)) {
            return false;
        }

        // 連携情報を登録 
        // user_id から link_user_id と link_user_id から user_id
        $data = [
            [
                'user_id' => $param['user_id'],
                'link_user_id' => $userId
            ],
            [
                'user_id' => $userId,
                'link_user_id' => $param['user_id']
            ]
        ];
        return (bool)$this->saveMany($data);
    }
}
