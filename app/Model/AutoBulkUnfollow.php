<?php

App::uses('AppModel', 'Model');

/**
 * 自動一括フォロー解除 モデル
 *
 * @package       app.Model
 */
class AutoBulkUnfollow extends AppModel {

    public $useTable = 'auto_bulk_unfollow';

    public $actsAs = ['Containable'];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram'
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoBulkUnfollow.instagram_id'
            ]
        ]
    ];

/**
 * 自動一括フォロー解除を行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams() {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username'],
                ],
                'AutoSetting' => [
                    'auto_bulk_unfollow'
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.unfollow_api_status' => true,
                'AutoSetting.auto_bulk_unfollow' => true,
            ]
        ];
        return $this->find('all', $option);
    }

}
