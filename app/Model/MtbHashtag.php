<?php

App::uses('AppModel', 'Model');
App::uses('Hashtag', 'Model');

class MtbHashtag extends AppModel {

	public $useTable = 'mtb_hashtag';

	public $actsAs = array('Containable');

	// public $belongsTo = array(
	// 	'Hashtag' => array(
	// 		'className' => 'Hashtag',
	// 		'foreignKey' => false ,
	// 		'conditions' => array('MtbHashtag.id = Hashtag.mtb_hashtag_id')
	// 	),
	// );

/**
 * 登録数の更新
 *
 * @param int $id mtb_hashtagテーブルのid
 * @return void
 */
	public function updateCount($id) {
		$this->Hashtag = new Hashtag();
		$count = $this->Hashtag->find('count', array(
			'conditions' => array(
				'mtb_hashtag_id' => $id
			)
		));
		$this->save(array(
			'id' => $id,
			'count' => $count
		));
	}
}
