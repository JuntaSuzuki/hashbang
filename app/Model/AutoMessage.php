<?php

App::uses('AppModel', 'Model');

/**
 * 自動メッセージ モデル
 *
 * @package       app.Model
 */
class AutoMessage extends AppModel {

    public $useTable = 'auto_message';

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbAutoMessageSpeed',
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoMessage.instagram_id'
            ]
        ],
        'Message' => [
            'className' => 'Message',
            'foreignKey' => false,
            'conditions' => [
                'Message.instagram_id = AutoMessage.instagram_id'
            ]
        ],
        'Follower' => [
            'className' => 'Follower',
            'foreignKey' => false,
            'conditions' => [
                'Follower.instagram_id = AutoMessage.instagram_id'
            ]
        ]
    ];

/**
 * 自動メッセージを行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams($speedId) {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username']
                ],
                'Message' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'Follower' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'AutoSetting' => [
                    'auto_message'
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.message_api_status' => true,
                // 自動アクティビティ条件
                'Message.id IS NOT NULL',
                'Follower.message_status' => false,
                'AutoSetting.auto_message' => true,
                'AutoMessage.mtb_auto_message_speed_id' => $speedId
            ],
            'group' => [
                'Instagram.id'
            ]
        ];
        return $this->find('all', $option);
    }

}
