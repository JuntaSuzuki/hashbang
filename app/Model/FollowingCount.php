<?php

App::uses('AppModel', 'Model');

/**
 * フォローカウント モデル
 *
 * @package       app.Model
 */
class FollowingCount extends AppModel {

    public $useTable = 'following_count';

    public $actsAs = array('Containable');

/**
 * アソシエーション
 */
    public $belongsTo = array(
        'Instagram' => array(
        ),
    );

}
