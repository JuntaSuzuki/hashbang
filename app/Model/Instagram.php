<?php

App::uses('AppModel', 'Model');
App::uses('User', 'Model');

/**
 * インスタグラムアカウント モデル
 *
 * @package       app.Model
 */
class Instagram extends AppModel {

    public $actsAs = ['Containable'];

/**
 * アソシエーション
 */
    public $hasOne = [
        'AutoSetting'
    ];

    public $hasMany = [
        'Hashtag',
        'Comment',
        'Filter',
        'Location',
        'Activity',
        'Follower',
        'Message'
    ];

    public $belongsTo = [
        'User' => []
    ];

/**
 * バリデーション
 */
    public $validate = [
        'username' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'ユーザー名を入力してください。',
            ],
            'isUniqueUsername' => [
                'rule'    => 'isUniqueUsername',
                'message' => '既に登録されているユーザー名です。',
                'on' => 'create'
            ],
            'isUniqueUsername' => [
                'rule'    => 'isUniqueUsernamelNot',
                'message' => '既に登録されているユーザー名です。',
                'on' => 'update'
            ]
        ],
        'password' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'パスワードを入力してください。'
            ]
        ],
    ];

/**
 * ユーザー名の存在確認
 */
    public function isUniqueUsername($check) {
        $conditions = [
            'username' => $check['username'],
            'status' => [1, 2]
        ];

        return !(bool)$this->hasAny($conditions);
    }

/**
 * メールアドレスの確認
 */
    public function isUniqueUsernamelNot($check) {
        $sql = "
            SELECT count(id) as count
                FROM instagrams 
            WHERE 
                username = :username AND 
                username != (SELECT username FROM instagrams WHERE id = :id) AND 
                status = :status;";

        $binds = [
            ':username' => $check['username'],
            ':id' => $this->data['Instagram']['id'],
            ':status' => 1
        ];
        $res = $this->query($sql, $binds, false);

        return !(bool)$res[0][0]['count'];
    }

/**
 * インスタグラムアカウント情報の取得
 *
 * @param int $userId 
 * @return array || string 
 */
    public function fetchAccount($userId) {
        // 現在のインスタグラムアカウントIDを取得
        $instagramId = $this->User->find('first', [
            'fields' => [
                'instagram_id'
            ],
            'conditions' => [
                'User.id' => $userId
            ],
            'recursive' => -1
        ]);

        return $this->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId['User']['instagram_id']
            ],
            'recursive' => -1
        ]);
    }

/**
 * インスタグラムアカウント情報の取得
 *
 * @param int $userId 
 * @return array || string 
 */
    public function fetchInstagramInfo($instagramId) {
        return $this->find('first', [
            'contain' => [
                'AutoSetting'
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            //'recursive' => -1
        ]);
    }

/**
 * フォロワー数とフォロー数の取得
 *
 * @param int $userId 
 * @return array
 */
    public function fetchFollowAndFollowBy($instagramId) {
        $option = [
            'fields' => ['follows_count', 'followed_by_count'],
            'contain' => [
            ],
            'conditions' => [
                'instagramId' => $instagramId
            ]
        ];
        $res = $this->find('first', $option);

        if (empty($res)) {
            return [
                'follows_count' => 0,
                'followed_by_count' => 0
            ];
        } else {
            return $res['Instagram'];
        }
    }

/**
 * ハッシュタグ情報を取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchHashtags($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'password'],
            'contain' =>[
                'Hashtag' => [
                    'fields' => ['id'],
                    'MtbHashtag' => [
                        'fields' => ['id', 'name'],
                    ],
                ],
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ]
        ];
        return $this->find('first', $option);
    }

/**
 * ハッシュタグ情報を整形
 *
 * @param array $hashtags 
 * @return array
 */
    public function formatHashtags($hashtags) {
        $formattedHashtags = [];
        foreach ($hashtags as $hashtag) {
            $formattedHashtags[] = [
                'id' => $hashtag['MtbHashtag']['id'],
                'name' => $hashtag['MtbHashtag']['name']
            ];
        }
        return $formattedHashtags;
    }

/**
 * コメント情報を取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchComments($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'password'],
            'contain' =>[
                'Comment' => [
                    'fields' => ['id', 'comment'],
                ],
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ]
        ];
        return $this->find('first', $option);
    }

/**
 * コメント情報を整形
 *
 * @param array $comments 
 * @return array
 */
    public function formatComments($comments) {
        $formattedComments = [];
        foreach ($comments as $comment) {
            $formattedComments[] = [
                'id' => $comment['id'],
                'comment' => $comment['comment']
            ];
        }
        return $formattedComments;
    }

/**
 * ロケーション情報を取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchLocations($instagramId) {
        $option = [
            'fields' => ['id', 'username', 'password'],
            'contain' =>[
                'Location' => [
                    'fields' => ['id'],
                    'MtbLocation' => [
                        'fields' => ['id', 'instagram_location_id'],
                    ],
                ],
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ]
        ];
        return $this->find('first', $option);
    }

/**
 * ロケーション情報を整形
 *
 * @param array $locations 
 * @return array
 */
    public function formatLocations($locations) {
        $formattedLocations = [];
        foreach ($locations as $location) {
            $formattedLocations[] = [
                'id' => $location['MtbLocation']['id'],
                'location_id' => $location['MtbLocation']['instagram_location_id']
            ];
        }
        return $formattedLocations;
    }

/**
 * フォロー解除対象ユーザーを取得
 *
 * フォローを行ったが、
 * 指定日数経ってもフォローバックがなく、
 * フォロー解除を行っていないフォロー
 *
 * @param int $instagramId
 * @param array $whitelistFollower
 * @param int $unfollowDay
 * @return array
 */
    public function fetchUnfollowFollowings($instagramId, $whitelistFollower, $unfollowDay = 5) {
        $option = [
            'fields' => ['id', 'username', 'password'],
            'contain' =>[
                'Activity' => [
                    'fields' => ['id', 'username', 'instagram_username_id', 'action_date'],
                    'conditions' => [
                        'Activity.type' => FOLLOW,
                        'Activity.followback' => false,
                        'Activity.unfollow' => false,
                        'Activity.action_date <= date(current_timestamp - interval ' . $unfollowDay . ' day)',
                        'NOT' => [
                            'Activity.instagram_username_id' => $whitelistFollower
                        ]
                    ],
                    'order' => [
                        'Activity.action_date' => 'DESC'
                    ],
                    'limit' => 1
                ],
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
        ];

        return $this->find('first', $option);
    }


/**
 * メッセージ送信対象フォロワーを取得
 *
 * 新規フォロワーでメッセージを送信していないユーザー
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchUnsentMessage($instagramId, $limit = null) {
        $option = [
            'fields' => ['id', 'username', 'password'],
            'contain' =>[
                'Follower' => [
                    'fields' => ['id', 'username', 'username_id'],
                    'conditions' => [
                        'Follower.message_status' => false,
                    ],
                    'order' => [
                        'Follower.created' => 'DESC'
                    ]
                ],
                'Message' => [
                    'fields' => ['id', 'message']
                ]
            ],
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
        ];

        if (!is_null($limit)) {
            $option['contain']['Follower']['limit'] = [
                'limit' => $limit
            ];
        }

        return $this->find('first', $option);
    }


/**
 * 自動フォローバック対象のアカウントを取得
 *
 * @param int $instagramId 
 * @return array
 */
    public function fetchFollowBack($instagramId) {
        $option = [
            'fields' => [
                'username', 'password'
            ],
            'contain' => [
                'AutoSetting' => [
                    'fields' => [
                        'id'
                    ]
                ]
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.id' => $instagramId,
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                // 自動アクティビティ条件
                'AutoSetting.auto_followback' => true
            ],
        ];

        return $this->find('first', $option);
    }

/**
 * 有効な自動設定をONにしているユーザーを取得
 *
 * @return array
 */
    public function fetchValidAccounts() {
        $option = [
            'fields' => ['id'],
            'contain' => [
                'AutoSetting'
            ],
            'conditions' => [
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'OR' => [
                    'AutoSetting.auto_like' => true,
                    'AutoSetting.auto_follow' => true,
                    'AutoSetting.auto_comment' => true,
                    'AutoSetting.auto_message' => true,
                    'AutoSetting.auto_unfollow' => true,
                    'AutoSetting.auto_followback' => true,
                    'AutoSetting.auto_bulk_unfollow' => true,
                ]
            ]
        ];

        return $this->find('all', $option);
    }

/**
 * いいねAPI制限解除ユーザーを取得
 *
 * @return array
 */
    public function fetchLikeApiRestrictRelease() {
        $this->virtualFields = [
            'like_restrict_time_diff' => 'TIMESTAMPDIFF(MINUTE, like_api_restrict_date, now())'
        ];

        $option = [
            'fields' => [
                'id', 'like_api_status','like_restrict_time_diff'
            ],
            'conditions' => [
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.like_api_status' => false,
                'Instagram.like_restrict_time_diff > ' => 0
            ],
            'recursive' => -1
        ];

        return $this->find('all', $option);
    }

/**
 * コメントAPI制限解除ユーザーを取得
 *
 * @return array
 */
    public function fetchCommentApiRestrictRelease() {
        $this->virtualFields = [
            'comment_restrict_time_diff' => 'TIMESTAMPDIFF(MINUTE, comment_api_restrict_date, now())'
        ];

        $option = [
            'fields' => [
                'id', 'comment_api_status', 'comment_restrict_time_diff'
            ],
            'conditions' => [
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.comment_api_status' => false,
                'Instagram.comment_restrict_time_diff > ' => 0
            ],
            'recursive' => -1
        ];

        return $this->find('all', $option);
    }

/**
 * メッセージAPI制限解除ユーザーを取得
 *
 * @return array
 */
    public function fetchMessageApiRestrictRelease() {
        $this->virtualFields = [
            'message_restrict_time_diff' => 'TIMESTAMPDIFF(MINUTE, message_api_restrict_date, now())'
        ];

        $option = [
            'fields' => [
                'id', 'message_api_status', 'message_restrict_time_diff'
            ],
            'conditions' => [
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.message_api_status' => false,
                'Instagram.message_restrict_time_diff > ' => 0
            ],
            'recursive' => -1
        ];

        return $this->find('all', $option);
    }

}
