<?php

App::uses('AppModel', 'Model');

/**
 * 自動いいね モデル
 *
 * @package       app.Model
 */
class AutoLike extends AppModel {

    public $useTable = 'auto_like';

    public $actsAs = [
        'Containable'
    ];

/**
 * アソシエーション
 */
    public $belongsTo = [
        'Instagram',
        'MtbAutoLikeSpeed'
    ];

    public $hasOne = [
        'AutoSetting' => [
            'className' => 'AutoSetting',
            'foreignKey' => false,
            'conditions' => [
                'AutoSetting.instagram_id = AutoLike.instagram_id'
            ]
        ],
        'Hashtag' => [
            'className' => 'Hashtag',
            'foreignKey' => false,
            'conditions' => [
                'Hashtag.instagram_id = AutoLike.instagram_id'
            ]
        ],
        'Location' => [
            'className' => 'Location',
            'foreignKey' => false,
            'conditions' => [
                'Location.instagram_id = AutoLike.instagram_id'
            ]
        ]
    ];

/**
 * 自動いいねを行うインスタグラムアカウントを取得
 *
 * インスタグラムアカウントが有効かつ
 * 自動アクションステータスが有効なものに限る
 *
 * @param int $speedId
 * @return array
 */
    public function fetchInstagrams($speedId) {
        $option = [
            'fields' => ['id'],
            'contain' =>[
                'Instagram' => [
                    'fields' => ['id', 'username']
                ],
                'Hashtag' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'Location' => [
                    'fields' => ['id'],
                    'limit' => 1
                ],
                'AutoSetting' => [
                    'auto_like'
                ],
            ],
            'conditions' => [
                // インスタグラムアカウント条件
                'Instagram.status' => true,
                'Instagram.instagram_auth' => true,
                'Instagram.like_api_status' => true,
                // 自動アクティビティ条件
                'OR' => [
                    'Hashtag.id IS NOT NULL',
                    'Location.id IS NOT NULL'
                ],
                'AutoSetting.auto_like' => true,
                'AutoLike.mtb_auto_like_speed_id' => $speedId
            ],
            'group' => [
                'Instagram.id'
            ]
        ];
        return $this->find('all', $option);
    }

/**
 * 自動いいねを登録
 *
 * @param int $instagramId
 * @param int $speedId
 * @return boolean
 */
    public function add($instagramId, $speedId) {
        $data = [
            'instagram_id' => $instagramId,
            'mtb_auto_like_speed_id' => $speedId
        ];
        return $this->save($data);
    }
}
