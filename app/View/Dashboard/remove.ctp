
<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- REMOVE -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span> <?php echo __('退会手続き'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('退会手続き'); ?></h3>
          </div>
          <div class="boxs-body">
            <div class="row row-container">
              <div class="col-md-12">
                <p>
                  <?php echo __('#BANGから退会するには、現在のパスワードを入力して「退会する」をクリックしてください。<br>
                  <b class="text-red">退会されますと、すべてのデータが削除されますのでご注意ください。<br>
                  また、この操作は取り消すことができません。</b>'); ?>
                </p>
              </div>
              <div class="col-md-6">
                <?php 
                  echo $this->Form->create('User', array(
                    'novalidate' => true,
                    'onsubmit' => "return confirm('" . __('退会処理を実行します。よろしいですか？') . "')"
                  ));
                ?>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('password', array(
                        'label' => __('パスワード'),
                        'class' => 'form-control',
                        'value' => false,
                      ));
                    ?>
                    <p class="js-error" style="display: none;"><?php echo __('パスワードが入力されていません'); ?></p>
                  </div>
                  <button type="submit" class="btn btn-info" id="account"><?php echo __('退会する'); ?></button>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ REMOVE -->
