
<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- INVITE -->
<section id="content">
  <div class="page page-forms-common">

    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-users"></i> <?php echo __('友達招待でお得!'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('友達やブログの読者を招待する'); ?></h3>
          </div>
          <div class="boxs-body" id="subscription">
            <div class="container-fluid">

              <div class="invite-section">
                <p class="invite-lead">招待コードURLをお友達に伝えたり、SNS・ブログ・ツイッターの読者などに広めてください。
                  <br>招待コードURLを経由して、お友達やブログの読者が#BANGに登録してプランを購入すると、
                </p>
                <h2>あなたのプランがずーっと無料になります♪</h2>

                <hr>

                <div class="invite-code">
                  <div class="row invite-code-form">
                    <div class="col-md-3">
                      <div class="invite-code-text">あなたの招待コードURL</div>
                    </div>
                    <div class="col-md-6">
                      <div>
                        <input type="text" value="http://hashbang.jp/invites/code/<?php echo h($invite_code); ?>" class="form-control" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="row invite-code-form">
                    <div class="col-md-3">
                      <div class="invite-code-text">あなたの招待コード</div>
                    </div>
                    <div class="col-md-6">
                      <div>
                        <input type="text" value="<?php echo h($invite_code); ?>" class="form-control" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <p class="text-center">あなたの招待コードで <?php echo h(count($invites)); ?> 人が登録しました。</p>

                  <p class="invite-code-attention">SNSやLINEで伝えよう！</p>

                  <ul class="sns_btn">
                    <li>
                      <img src="/images/btn_facebook.png" alt="Facebook">
                      <p>Facebook</p>
                    </li>
                    <li>
                      <img src="/images/btn_line.png" alt="LINE">
                      <p>LINE</p>
                    </li>
                    <li>
                      <img src="/images/btn_twitter.png" alt="Twitter">
                      <p>Twitter</p>
                    </li>
                    <li>
                      <img src="/images/btn_e-mail.png" alt="E-mail">
                      <p>E-mail</p>
                    </li>
                  </ul>

                  <p>※ 招待コードから個人が特定されることはありませんので、不特定多数の方に広めていただいても安心です。</p>
                </div>

                <hr>

                <h4>【招待コードのご利用について】</h4>
                <p>■<strong>特典内容</strong></p>
                <ul>
                  <li>あなたが招待したユーザーが#BANGでプランを購入すると、あなたのプランが無料になります。</li>
                </ul>
                <p>■<strong>注意事項</strong></p>
                <ul>
                  <li>自分で発行した招待コードを自分で使用することはできません。</li>
                  <li>何人でも招待することができます。</li>
                  <li>プランが無料になるためには、最低3人のユーザーがプランを購入していること（アクティブであること）が条件です。</li>
                  <li>事前の予告なく招待コードの特典の内容を変更したり、中止することがあります。</li>
                </ul>
              </div>

            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('あなたの招待コード経由で登録したユーザー'); ?></h3>
          </div>
          <div class="boxs-body" id="invite">

            <?php if (count($invites) == 0): ?>
              <?php echo __('招待コード経由で登録したユーザーはまだいません'); ?>
            <?php else: ?>
              <ul class="list-unstyled">
                <?php foreach ($invites as $invite): ?>
                <li class="p-10 b-b">
                  <div class="media">
                    <div class="pull-left thumb thumb-md">
                      <img class="media-object br-5" src="<?php echo h($invite['insta']['profile_picture']); ?>" alt="">
                    </div>
                    <div class="media-body">
                      <h5 class="media-heading mt-10"><?php echo h($invite['insta']['username']); ?> </h5>

                      <?php if ($invite['u']['plan'] == 0): ?>
                        <small class="text-cyan"><?php echo __('お試しプラン'); ?></small>
                      <?php else: ?>
                        <small class="text-lightred"><?php echo __('アクティブ'); ?></small>
                      <?php endif; ?>
                    </div>
                  </div>
                </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

          </div><!-- /boxs-body -->
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ INVITE -->
