
<?php echo $this->Html->script('/assets/js/activity', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- ACTIVITY -->
<section id="content">
  <div class="page page-timeline">

    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-file-text"></i> <?php echo __('アクティビティ'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <!-- page content -->
    <div class="pagecontent">
      <div class="row">
        <div class="col-md-12">
          <section class="boxs-simple no-bg">
            <?php if (count($activities) == 0): ?>
              <div class="boxs-body p-0">
                <?php echo __('まだアクティビティはありません'); ?>
            <?php else: ?>
              <div class="boxs-body streamline timeline p-0">
                <ul id="timeline_item">
                  <li class="heading"></li>
                  <?php echo $this->element('/Dashboard/activity_timeline'); ?>
                </ul>
                <?php if ((int)$this->paginator->counter('%count%') > Configure::read('activity_limit')): ?>
                  <button type="button" class="btn btn-default btn-lg btn-block" id="moreActivity"><?php echo __('もっと見る'); ?></button>
                <?php endif; ?>
              </div><!-- /boxs body -->
            <?php endif; ?>
          </section><!-- /boxs -->
        </div><!-- /col -->
      </div><!-- /row -->
    </div>
    <!-- /page content -->

  </div>
</section>
<!--/ ACTIVITY -->
