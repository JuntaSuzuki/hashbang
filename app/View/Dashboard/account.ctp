
<?php echo $this->Html->script('/assets/js/account', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- ACCOUNT -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('アカウント設定'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('アカウント設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <div class="row row-container">
              <div class="col-md-6">
                <?php 
                  echo $this->Form->create('User', [
                    'novalidate' => true
                  ]);
                ?>
                  <input type="password" name="dummy" style="display:none">
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('email', array(
                        'label' => __('メールアドレス'),
                        'class' => 'form-control'
                      ));
                    ?>
                    <p class="js-error" style="display: none;"><?php echo __('メールアドレスが入力されていません'); ?></p>
                  </div>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('password', array(
                        'label' => __('パスワード'),
                        'class' => 'form-control',
                        'value' => false,
                        'placeholder' => __('変更する場合に入力してください')
                      ));
                    ?>
                    <p class="js-error" style="display: none;"><?php echo __('パスワードが入力されていません'); ?></p>
                  </div>
                  <button type="button" class="btn btn-info" id="account">更新する</button>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>

        </section>
        <p class="remove-account">退会をご希望の場合は、<a href="/dashboard/remove">退会手続きページ</a>へお進みください。</p>
      </div>
    </div>
    <!-- end row -->
  </div>
</section>
<!--/ ACCOUNT -->
