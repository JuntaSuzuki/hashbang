
<?php echo $this->Html->script('/assets/js/whitelist', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- WHITELIST -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-address-card-o"></i> <?php echo __('ホワイトリスト'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('ホワイトリスト'); ?></h3>
          </div>
          <div class="boxs-body">
            <p>
              <?php echo __('ホワイトリストに追加したフォローユーザーは、自動でフォロー解除されることはありません。'); ?><br>
              <b class="text-danger"><?php echo __('※フォローユーザーリストに表示されているユーザーは、#BANGが自動でフォローしたユーザーです。'); ?></b>
            </p>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('フォローユーザーリスト'); ?></h3>
          </div>
          <div class="boxs-body">
            <div role="tabpanel">

              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                  <div class="wrap-reset whitelist">
                    <?php if (count($followings) > 0): ?>
                      <?php foreach ($followings as $following): ?>
                        <div class="media">
                          <div class="media-image">
                            <?php echo $this->Dashboard->followingMediaFormat($following['Activity']); ?>
                          </div>
                          <div class="media-text">
                            <?php echo h($following['Activity']['username']); ?>
                          </div>
                          <div class="media-button">
                            <?php echo $this->Dashboard->followingButtonFormat($following['Activity']); ?>
                          </div>
                        </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php echo __('フォローユーザーの取得が完了していません。<br><b>[フォローユーザーを更新]</b>ボタンをクリックして、しばらくお待ち下さい。'); ?>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- end tabpanel -->
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('ホワイトリスト登録中のフォローユーザー'); ?></h3>
          </div>
          <div class="boxs-body whitelist-box">
            <?php echo $this->Dashboard->whitelistList($whitelists); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ WHITELIST -->
