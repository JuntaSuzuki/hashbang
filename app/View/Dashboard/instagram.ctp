
<?php echo $this->Html->script('/assets/js/instagram', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- CONTENT -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('インスタグラム設定'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <?php if ($auth_error): ?>
          <div class="alert alert-big alert-lightred alert-dismissable fade in br-5">
            <h4><b><?php echo __('インスタグラムとのコネクトが切断しています'); ?></b></h4>
            <strong><?php echo __('下記の原因が考えられますので、１〜３の方法をお試しください。'); ?></strong><br><br>
            <strong><?php echo __('原因１：<br>インスタグラムのユーザー名またはパスワードを変更している。<br>【対応方法】<br>正しいインスタグラムのユーザー名とパスワードを入力し、『アカウントを更新する』をクリックしてください。'); ?></strong><br><br>
            <strong><?php echo __('原因２：<br>インスタグラム側でアカウントの認証を行う必要がある。<br>【対応方法】<br>インスタグラムアプリにログインし、「アカウントの認証を実行」画面が表示される場合、インスタグラム側の手順に従って認証を行う必要があります。<br>認証完了後、下記フォームにインスタグラムのユーザー名とパスワードを入力し、『アカウントを更新する』をクリックしてください。'); ?></strong><br><br>
            <strong><?php echo __('原因３：<br>二段階認証がオンになっている。<br>【対応方法】<br>インスタグラムアプリにログインし、設定画面から二段階認証をオフにします。<br>二段階認証をオフにした後、下記フォームにインスタグラムのユーザー名とパスワードを入力し、『アカウントを更新する』をクリックしてください。'); ?></strong>
          </div>
        <?php endif; ?>
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('インスタグラム設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <div class="row row-container">
              <div class="col-md-6">
                <?php 
                  echo $this->Form->create('Instagram', [
                    'novalidate' => true
                  ]);
                ?>
                  <input type="password" name="dummy" style="display:none">
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('username', array(
                        'label' => __('インスタグラムユーザー名'),
                        'id' => 'InstagramUsername',
                        'class' => 'form-control'
                      ));
                    ?>
                    <p class="js-error" style="display: none;">インスタグラムユーザーが入力されていません</p>
                  </div>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('password', array(
                        'label' => __('インスタグラムパスワード'),
                        'id' => 'InstagramPassword',
                        'class' => 'form-control',
                        'value' => false
                      ));
                    ?>
                    <p class="js-error" style="display: none;">インスタグラムパスワードが入力されていません</p>
                  </div>
                  <button type="button" class="btn btn-info" id="instagramAccount"><?php echo __('アカウントを更新する'); ?></button>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ CONTENT -->
