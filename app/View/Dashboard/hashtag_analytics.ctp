
<?php echo $this->Html->script('/assets/js/hashtag_analytics', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- HASHTAG ANALYTICS -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-bar-chart"></i> <?php echo __('ハッシュタグ解析'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('ハッシュタグ解析'); ?></h3>
          </div>
          <?php if (empty($analytics)): ?>
            <div class="boxs-widget">
              <p><?php echo __('表示するデータがまだありません'); ?></p>
            </div>
          <?php else: ?>
            <div class="boxs-widget">
              <select class="input-md form-control inline w-sm" id="period">
                <option value="1"><?php echo __('過去3日間'); ?></option>
                <option value="2"><?php echo __('過去7日間'); ?></option>
                <option value="3"><?php echo __('過去30日間'); ?></option>
                <option value="4"><?php echo __('過去60日間'); ?></option>
              </select>
            </div>
            <div class="boxs-body">
              <div class="table-responsive">
                <table class="table table-bordered mb-0">
                  <thead>
                    <tr>
                      <th><?php echo __('ハッシュタグ'); ?></th>
                      <th class="text-center"><?php echo __('自動いいね（回）'); ?></th>
                      <th class="text-center"><?php echo __('自動コメント（回）'); ?></th>
                      <th class="text-center"><?php echo __('自動フォロー（人）'); ?></th>
                      <th class="text-center"><?php echo __('フォロー獲得（人）'); ?></th>
                      <th class="text-center"><?php echo __('獲得率（％）'); ?></th>
                    </tr>
                  </thead>
                  <tbody id="result">
                    <?php foreach ($analytics as $analy): ?>
                    <tr>
                      <td><?php echo h($analy['hashtag']); ?></td>
                      <td class="text-center"><?php echo h($analy['like_count']); ?></td>
                      <td class="text-center"><?php echo h($analy['comment_count']); ?></td>
                      <td class="text-center"><?php echo h($analy['follow_count']); ?></td>
                      <td class="text-center"><?php echo h($analy['followback_count']); ?></td>
                      <td class="text-center"><?php echo $this->Dashboard->calcObtainRate($analy); ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php endif; ?>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ HASHTAG ANALYTICS -->
