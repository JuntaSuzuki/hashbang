
<?php echo $this->Html->script('/assets/js/subscription', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- SUBSCRIPTION -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('契約内容変更'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('契約内容変更'); ?></h3>
          </div>
          <div class="boxs-body">
            <?php if ($expiration): ?>
              <p>
                次回契約は、
                <span class="text-danger"><?php echo h(date('Y/m/d', strtotime($nextCharge['Payment']['contract_start_date']))); ?></span> 
                より適用されます。契約内容の変更は、
                <span class="text-danger"><?php echo h(date('Y/m/d', strtotime($nextCharge['Payment']['contract_start_date']))); ?></span> 
                までにお願いします。
              </p>
            <?php else: ?>
              <p>
                契約は、本日
                <span class="text-danger"><?php echo date('Y/m/d'); ?></span> 
                より適用されます。
              </p>
            <?php endif; ?>
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <table class="table table-bordered plan-table">
                  <tbody>
                    <tr>
                      <th><?php echo __('プラン'); ?></th>
                      <td>
                        <?php 
                          echo $this->Form->input('plan_id', array(
                            'type' => 'select',
                            'label' => false,
                            'class' => 'form-control',
                            'options' => Configure::read('plan'),
                            'value' => $nextCharge['Plan']['id']
                          ));
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <th><?php echo __('契約期間'); ?></th>
                      <td>
                        <?php 
                          echo $this->Form->input('month', array(
                            'type' => 'select',
                            'label' => false,
                            'class' => 'form-control',
                            'options' => Configure::read('month'),
                            'value' => $nextCharge['Plan']['month']
                          ));
                        ?>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <table class="table table-bordered plan-table mt-20 mb-20">
                  <tbody>
                    <tr>
                      <th><?php echo __('契約開始日'); ?></th>
                      <td>
                        <?php if ($expiration): ?>
                          <span id="contract_start_date">
                            <?php echo h(date('Y/m/d', strtotime($nextCharge['Payment']['contract_start_date']))); ?>
                          <span>
                        <?php else: ?>
                          <span id="contract_start_date">
                            <?php echo date('Y/m/d'); ?>
                          <span>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <tr>
                      <th><?php echo __('契約終了日'); ?></th>
                      <td>
                        <?php if ($expiration): ?>
                          <span id="contract_end_date">
                            <?php echo h(date('Y/m/d', strtotime($nextCharge['Payment']['contract_end_date']))); ?>
                          <span>
                        <?php else: ?>
                          <span id="contract_end_date">
                            <?php echo date('Y/m/d', strtotime('+' . ($plan['Plan']['day'] - 1). ' day')); ?>
                          <span>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <tr>
                      <th><?php echo __('ご請求額'); ?></th>
                      <td>
                        <span id="amount"><?php echo h(number_format($nextCharge['Payment']['amount'])); ?></span>円(税込)
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <?php 
              echo $this->Form->create('Payment');
            ?>
            <button type="submit" class="btn btn-sm btn-primary">契約変更</button>
            <?php
              echo $this->Html->link(
                'キャンセル',
                [
                  'action' => 'subscription',
                ],
                ['class' => 'btn btn-sm btn-default']
              );
            ?>
            <?php echo $this->Form->input('plan_id', array('type' => 'hidden', 'value' => $nextCharge['Payment']['plan_id'])); ?>
            <?php echo $this->Form->end(); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ SUBSCRIPTION -->

<?php echo $this->Form->create(false, array(
  'url' => array('controller' => 'subscription', 'action' => 'pay'),
  'novalidate' => true)); ?>
  <input type="hidden" name="payjp-plan-id" value="" id="plan_id">
  <script
    type="text/javascript"
    src="https://checkout.pay.jp/"
    class="payjp-button"
    data-key="<?php echo h(Configure::read('payjp-public')); ?>"
    data-text="このプランを購入する"
    data-submit-text="このプランを購入する">
  </script>
<?php echo $this->Form->end(); ?>
