
<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- SUBSCRIPTION -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('ご利用プラン'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <?php if (!$expiration): ?>
          <div class="alert alert-big alert-lightred alert-dismissable fade in br-5">
            <strong><?php echo __('プランの有効期間が終了しました。<br>引き続きサービスをご利用いただくにはプランを購入してください。'); ?></strong>
          </div>
        <?php endif; ?>
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('現在のプラン'); ?></h3>
          </div>
          <div class="boxs-body">
            <table class="table table-bordered plan-table">
              <tbody>
                <?php if ($plan_id == FREE_PLAN): ?>
                  <tr>
                    <th><?php echo __('現在のプラン'); ?></th>
                    <td>
                      <?php echo h($plan['Plan']['name']); ?>
                    </td>
                  </tr>
                  <tr>
                    <th><?php echo __('プランの有効期間'); ?></th>
                    <td><?php echo h(date('Y/m/d', strtotime($plan['User']['plan_limit_date']))); ?> まで</td>
                  </tr>
                <?php elseif ($plan_id == GOLD_PLAN): ?>
                  <tr>
                    <th><?php echo __('現在のプラン'); ?></th>
                    <td>
                      <?php echo h($plan['Plan']['name']); ?>
                    </td>
                  </tr>
                  <tr>
                    <th><?php echo __('プランの有効期間'); ?></th>
                    <td>無期限</td>
                  </tr>
                <?php else: ?>
                  <tr>
                    <th><?php echo __('現在のプラン'); ?></th>
                    <td><?php echo h($plan['Plan']['name']); ?> (<?php echo h($plan['Plan']['month']); ?>ヵ月プラン)</td>
                  </tr>
                  <tr>
                    <th><?php echo __('プラン有効期間'); ?></th>
                    <td><?php echo h(date('Y/m/d', strtotime($plan['User']['plan_limit_date']))); ?> まで</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>

            <?php if (!in_array($plan_id, [FREE_PLAN, GOLD_PLAN]) && $expiration): ?>
              <p class="mt-10">契約変更を行う場合は、『契約変更』をクリックしてください。</p>
              <?php
                echo $this->Html->link(
                  '契約変更',
                  [
                    'action' => 'subscriptionChange',
                  ],
                  ['class' => 'btn btn-sm btn-primary']
                );
              ?>
            <?php endif; ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <?php if (!in_array($plan_id, [FREE_PLAN, GOLD_PLAN]) && $expiration): ?>
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動更新設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p>
              <?php echo __('契約更新を自動で行うことができます。<br>
              お支払い忘れでシステムが使えなくなることを防ぐことができます。'); ?>
            </p>
            <table class="table table-bordered plan-table">
              <tbody>
                <tr>
                  <th><?php echo __('状態'); ?></th>
                  <td>
                    <?php if ($plan['User']['auto_subscription_status']): ?>
                      <?php echo __('自動更新設定中'); ?>
                    <?php else: ?>
                      <?php echo __('自動更新未設定'); ?>
                    <?php endif; ?>
                  </td>
                </tr>
                <?php if ($plan['User']['auto_subscription_status']): ?>
                <tr>
                  <th><?php echo __('次回請求日'); ?></th>
                  <td><?php echo h(date('Y/m/d', strtotime($plan['User']['next_charge_date']))); ?></td>
                </tr>
                <?php endif; ?>
              </tbody>
            </table>
            <?php if ($plan['User']['auto_subscription_status']): ?>
              <p class="mt-10">自動更新を解除する場合は、『解除』をクリックしてください。</p>
              <?php
                echo $this->Form->postLink(
                  '解除',
                  [
                    'controller' => 'subscription',
                    'action' => 'auto',
                    0
                  ],
                  ['class' => 'btn btn-sm btn-danger'],
                  '自動更新設定を解除します。よろしいですか?'
                );
              ?>
            <?php else: ?>
              <p class="mt-10">自動更新を設定する場合は、『設定』をクリックしてください。</p>
              <?php
                echo $this->Form->postLink(
                  '設定',
                  [
                    'controller' => 'subscription',
                    'action' => 'auto',
                    1
                  ],
                  ['class' => 'btn btn-sm btn-success'],
                  '自動更新設定を設定します。よろしいですか?'
                );
              ?>
            <?php endif; ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
    <?php endif; ?>

    <?php if (empty($cus_id) && $plan_id != GOLD_PLAN): ?>
      <div class="row">
        <div class="col-md-12">
          <section class="boxs">
            <div class="boxs-header dvd dvd-btm">
              <h3>プランの選択</h3>
            </div>
            <div class="boxs-body" id="subscription">
              <div class="container-fluid">
                <p>
                  ご希望のプランを選択してください。<br>
                  より長くお使いいただくことで、インスタグラムのフォロワーをたくさん増やすことができます。
                </p>
                <div class="row pricing-section">
                  <?php foreach ($planType1 as $plan): ?>
                  <div class="col-sm-6 col-lg-4">
                    <div class="pricing-box <?php echo $plan['Plan']['class']; ?>">
                      <div class="pricing-header">
                        <div class="plan-title"><?php echo $plan['Plan']['name']; ?></div>
                        <div class="plan-price"><span><small>税込 </small></span><?php echo number_format($plan['Plan']['amount']); ?><span>円</span></div>
                      </div>
                      <ul class="list-unstyled plan-features">
                        <li><?php echo $plan['Plan']['description']; ?></li>
                      </ul>
                      <p class="auto-update"><?php echo $plan['Plan']['day']; ?>日ごとに自動更新</p>
                      <div class="pricing-button pay-btn" data-plan-id="<?php echo $plan['Plan']['id']; ?>">
                        <a href="#" class="btn btn-custom">このプランを購入する</a>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>

                <p>セットプランの長期割だとさらにお得！</p>

                <div class="row pricing-section">
                  <?php foreach ($planType2 as $plan): ?>
                  <div class="col-sm-6 col-lg-4">
                    <div class="pricing-box <?php echo $plan['Plan']['class']; ?>">
                      <div class="pricing-header">
                        <div class="plan-title"><?php echo $plan['Plan']['name']; ?></div>
                        <div class="plan-price"><span><small>税込 </small></span><?php echo number_format($plan['Plan']['amount']); ?><span>円</span></div>
                      </div>
                      <ul class="list-unstyled plan-features">
                        <li><?php echo $plan['Plan']['description']; ?></li>
                      </ul>
                      <p class="auto-update"><?php echo $plan['Plan']['day']; ?>日ごとに自動更新</p>
                      <div class="pricing-button pay-btn" data-plan-id="<?php echo $plan['Plan']['id']; ?>">
                        <a href="#" class="btn btn-custom">このプランを購入する</a>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>

              </div>
            </div>
          </section>
        </div>
      </div>
      <!-- end row -->
    <?php endif; ?>

    <?php if (!in_array($plan_id, [FREE_PLAN, GOLD_PLAN]) && !$expiration): ?>
      <div class="row">
        <div class="col-md-12">
          <section class="boxs">
            <div class="boxs-header dvd dvd-btm">
              <h3>プランの選択</h3>
            </div>
            <div class="boxs-body" id="subscription">
              <div class="container-fluid">
                <p>
                  ご希望のプランを選択してください。<br>
                  より長くお使いいただくことで、インスタグラムのフォロワーをたくさん増やすことができます。
                </p>
                <div class="row pricing-section">
                  <?php foreach ($planType1 as $plan): ?>
                  <div class="col-sm-6 col-lg-4">
                    <div class="pricing-box <?php echo $plan['Plan']['class']; ?>">
                      <div class="pricing-header">
                        <div class="plan-title"><?php echo $plan['Plan']['name']; ?></div>
                        <div class="plan-price"><?php echo number_format($plan['Plan']['amount']); ?><span>円</span></div>
                      </div>
                      <ul class="list-unstyled plan-features">
                        <li><?php echo $plan['Plan']['description']; ?></li>
                      </ul>
                      <p class="auto-update"><?php echo $plan['Plan']['day']; ?>日ごとに自動更新</p>
                      <div class="pricing-button pay-btn">
                        <?php
                          echo $this->Form->postLink(
                            'このプランを購入する',
                            [
                              'controller' => 'subscription',
                              'action' => 'payPlan',
                              $plan['Plan']['id']
                            ],
                            ['class' => 'btn btn-custom'],
                            'このプランを購入します。よろしいですか?'
                          );
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>

                <p>セットプランの長期割だとさらにお得！</p>

                <div class="row pricing-section">
                  <?php foreach ($planType2 as $plan): ?>
                  <div class="col-sm-6 col-lg-4">
                    <div class="pricing-box <?php echo $plan['Plan']['class']; ?>">
                      <div class="pricing-header">
                        <div class="plan-title"><?php echo $plan['Plan']['name']; ?></div>
                        <div class="plan-price"><?php echo number_format($plan['Plan']['amount']); ?><span>円</span></div>
                      </div>
                      <ul class="list-unstyled plan-features">
                        <li><?php echo $plan['Plan']['description']; ?></li>
                      </ul>
                      <p class="auto-update"><?php echo $plan['Plan']['day']; ?>日ごとに自動更新</p>
                      <div class="pricing-button pay-btn">
                        <?php
                          echo $this->Form->postLink(
                            'このプランを購入する',
                            [
                              'controller' => 'subscription',
                              'action' => 'payPlan',
                              $plan['Plan']['id']
                            ],
                            ['class' => 'btn btn-custom'],
                            'このプランを購入します。よろしいですか?'
                          );
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>

              </div>
            </div>
          </section>
        </div>
      </div>
      <!-- end row -->
    <?php endif; ?>

    <?php if (!in_array($plan_id, [FREE_PLAN, GOLD_PLAN])): ?>
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('契約内容一覧'); ?></h3>
          </div>
          <div class="boxs-body">
            <?php if (count($payments) == 0 ): ?>
              <?php echo __('まだ契約内容はありません'); ?>
            <?php else: ?>
              <p>
                一番上に表示されている契約内容は次回の更新予定です。<br>
                自動更新を設定している場合、こちらの内容で更新されます。<br>
                <b class="text-danger">※自動更新を設定していない場合、更新されることはありません。</b>
              </p>
              <div class="table-responsive">
                <table class="table table-bordered plan-table text-center">
                  <tbody>
                    <tr>
                      <th><?php echo __('契約期間'); ?></th>
                      <th><?php echo __('契約開始日'); ?></th>
                      <th><?php echo __('契約終了日'); ?></th>
                      <th><?php echo __('請求金額'); ?></th>
                      <th><?php echo __('請求日'); ?></th>
                      <th><?php echo __('請求状況'); ?></th>
                      <th><?php echo __('請求内容'); ?></th>
                    </tr>
                    <?php foreach($payments as $payment): ?>
                      <tr <?php echo $this->Dashboard->paymentStatus($payment) ?>>
                        <td><?php echo h($payment['Plan']['month']); ?>ヵ月（<?php echo h($payment['Plan']['day']); ?>日）</td>
                        <td><?php echo h(date('Y/m/d', strtotime($payment['Payment']['contract_start_date']))); ?></td>
                        <td><?php echo h(date('Y/m/d', strtotime($payment['Payment']['contract_end_date']))); ?></td>
                        <td><?php echo h(number_format($payment['Payment']['amount'])); ?>円</td>
                        <td><?php echo h(date('Y/m/d', strtotime($payment['Payment']['charge_date']))); ?></td>
                        <td><?php echo Configure::read('payment_status.' . (int)$payment['Payment']['payment_status']); ?></td>
                        <td><?php echo h($payment['Plan']['name']); ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            <?php endif; ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
    <?php endif; ?>
  </div>
</section>
<!--/ SUBSCRIPTION -->

<?php if (empty($cus_id)): ?>
<?php 
  echo $this->Form->create(false, [
    'url' => [
      'controller' => 'subscription',
      'action' => 'pay'
    ],
    'novalidate' => true
  ]);
?>
  <input type="hidden" name="payjp-plan-id" value="" id="plan_id">
  <script
    type="text/javascript"
    src="https://checkout.pay.jp/"
    class="payjp-button"
    data-key="<?php echo h(Configure::read('payjp-public')); ?>"
    data-text="このプランを購入する"
    data-submit-text="このプランを購入する">
  </script>
<?php echo $this->Form->end(); ?>
<?php endif; ?>

