
<?php echo $this->Html->script('/assets/js/filter', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- FILTER -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-filter"></i> <?php echo __('フィルター管理'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('フィルター設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('このキーワードを含む投稿に対して自動いいね、自動フォロー、自動コメントは行いません。'); ?>
            </p>
            <form class="form-inline" role="form">
              <div class="form-group">
                <label for="filterValue" class="sr-only control-label"><?php echo __('キーワードを入力'); ?></label>
                <input type="text" class="form-control" id="filterValue" size="40" placeholder="<?php echo __('登録したいキーワード'); ?>">
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-cyan" id="filterAdd"><?php echo __('登録'); ?></button>
              </div>
            </form>

            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0" role="alert" style="display: none;">
              <?php echo __('<strong>これ以上キーワードを登録できません！</strong> 不要なキーワードを削除してから登録してください。'); ?>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('登録中のキーワード'); ?><?php echo $this->Dashboard->filterMsg(count($filters)); ?></h3>
          </div>
          <div class="boxs-body filter-box">
            <?php echo $this->Dashboard->filterList($filters); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ FILTER -->
