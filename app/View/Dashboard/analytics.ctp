
<?php echo $this->Html->script('https://code.highcharts.com/highcharts.js', array("inline" => false)); ?>
<?php echo $this->Html->script('/assets/js/analytics', array("inline" => false)); ?>
<?php echo $this->Html->script('/assets/js/analytics', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<style type="text/css">
#container {
  min-width: 310px;
  max-width: 800px;
  height: 400px;
  margin: 0 auto
}
</style>

<!-- ANALYTICS -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-line-chart"></i> <?php echo __('レポート'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('レポート'); ?></h3>
          </div>
          <div class="boxs-widget">
            <select class="input-md form-control inline w-sm" id="period">
              <option value="2"><?php echo __('過去7日間'); ?></option>
              <option value="3"><?php echo __('過去30日間'); ?></option>
            </select>
          </div>
          <div class="boxs-body">
            <div id="container_follower" class="mb-40"></div>
            <div id="container_following" class="mb-40"></div>
            <div id="container_like" class="mb-40"></div>
            <div id="container_follow" class="mb-40"></div>
            <div id="container_comment" class="mb-40"></div>
            <div id="container_unfollow"></div>
          </div>
        </section>
      </div>
    </div>
    <!-- row -->
  </div>
</section>
<!--/ ANALYTICS -->
