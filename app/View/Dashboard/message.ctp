
<?php echo $this->Html->script('/assets/js/message', ["inline" => false]); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- MESSAGE -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-comment fa-paper-plane"></i> <?php echo __('メッセージ管理'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('メッセージ設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('新規フォロワーに対して自動メッセージを行います。<br>'); ?>
              <?php echo __('フォローのお礼などを自動化することが可能です。<br>'); ?>
              <?php echo __('メッセージに使用される文章は、登録されているメッセージからランダムに選ばれます。<br>'); ?>
              <?php echo __('例: フォローありがとうございます！よろしくお願いします！！<br><br>'); ?>
              <b class="text-danger"><?php echo __('※ビジネスの勧誘メッセージなどを登録されますと、インスタグラムの利用制限を受けやすくなりますのでご注意ください。'); ?></b>
            </p>
            <?php 
              echo $this->Form->create('Message', [
                'novalidate' => true
              ]);
            ?>
              <div class="form-group">
                <?php 
                  echo $this->Form->textarea('message', [
                    'label' => false,
                    'class' => 'form-control',
                    'id' => 'messageValue',
                    'rows' => 8,
                    'placeholder' => __('登録したいメッセージ')
                  ]);
                ?>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-cyan" id="messageAdd"><?php echo __('登録'); ?></button>
                <button type="button" class="btn btn-default" id="messageCancel" style="display: none;"><?php echo __('キャンセル'); ?></button>
              </div>
              <?php echo $this->Form->input('id', ['type' => 'hidden']); ?>
            <?php echo $this->Form->end(); ?>

            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0" role="alert" style="display: none;">
              <?php echo __('<strong>これ以上メッセージを登録できません！</strong> 不要なメッセージを削除してから登録してください。'); ?>
            </div>
          </div>
        </section>
      </div>

      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('登録中のメッセージ'); ?><?php echo $this->Dashboard->messageMsg(count($messages)); ?></h3>
          </div>
          <div class="boxs-body message-box">
            <?php echo $this->Dashboard->messageList($messages); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
  </div>
</section>
<!--/ MESSAGE -->
