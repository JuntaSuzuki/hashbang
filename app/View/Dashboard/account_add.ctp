
<?php echo $this->Html->script('/assets/js/instagram', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- ACCOUNT ADD -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('アカウント追加'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('アカウント追加'); ?></h3>
          </div>
          <div class="boxs-body">
            <p>
              <?php echo __('アカウントを追加すると、都度ログアウトとログインを繰り返さず、複数の#BANGアカウントを切り替えられるようになります。<br>追加後、上部にあるユーザー名の部分をリスト展開した際にアカウントが表示されるようになり、ここからアカウント切り替えが可能です。<br>
              <b class="text-danger">※別の#BANGアカウントをお持ちでない方は、まずは新規登録を行い、アカウントを追加してください。</b>'); ?>
            </p>
            <a href="/users/sign_up">
              <button type="submit" class="btn btn-sm btn-success mb-25"><?php echo __('#BANGアカウントを新規登録する'); ?></button>
            </a>
            <div class="row row-container">
              <div class="col-md-6">
                <?php 
                  echo $this->Form->create('User', [
                    'novalidate' => true
                  ]);
                ?>
                  <input type="password" name="dummy" style="display:none">
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('email', array(
                        'label' => __('#BANGメールアドレス'),
                        'class' => 'form-control',
                        'placeholder' => __('メールアドレス')
                      ));
                    ?>
                  </div>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('password', array(
                        'label' => __('#BANGパスワード'),
                        'class' => 'form-control',
                        'placeholder' => __('パスワード'),
                        'value' => false
                      ));
                    ?>
                  </div>
                  <button type="submit" class="btn btn-info"><?php echo __('アカウントを追加する'); ?></button>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ ACCOUNT ADD -->
