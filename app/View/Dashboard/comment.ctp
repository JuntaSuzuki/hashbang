
<?php echo $this->Html->script('/assets/js/comment', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- CONTENT -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-comment fa-flip-horizontal"></i> <?php echo __('コメント管理'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('コメント設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('コメントに使用される文章は、登録されているコメントからランダムに選ばれます。<br>最低でも10個の当たり障りのない、汎用的なコメントを登録してください。<br>
              例: いいね, はじめまして, 素敵ですね, また見に来ますね!!, 次の投稿も楽しみにしています!'); ?>
            </p>
            <form class="form-inline" role="form">
              <div class="form-group">
                <label for="commentValue" class="sr-only control-label "><?php echo __('コメントを入力：'); ?></label>
                <input type="text" class="form-control" id="commentValue" size="40" placeholder="<?php echo __('登録したいコメント'); ?>">
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-cyan" id="commentAdd"><?php echo __('登録'); ?></button>
              </div>
            </form>

            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0" role="alert" style="display: none;">
              <?php echo __('<strong>これ以上コメントを登録できません！</strong> 不要なコメントを削除してから登録してください。'); ?>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('登録中のコメント'); ?><?php echo $this->Dashboard->commentMsg(count($comments)); ?></h3>
          </div>
          <div class="boxs-body comment-box">
            <?php echo $this->Dashboard->commentList($comments); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
  </div>
</section>
<!--/ CONTENT -->
