
<?php echo $this->Html->script('/assets/js/hashtag', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- HASHTAG -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-hashtag"></i> <?php echo __('ハッシュタグ管理'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('ハッシュタグ設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15"><?php echo __('登録したハッシュタグに一致する投稿に対して自動いいね、自動フォロー、自動コメントを行います。<br>※自動処理開始まで1時間程度かかる場合がございます。'); ?></p>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group mb-0">
                  <div class="col-sm-12">
                    <label for="tagValue" class="control-label"><?php echo __('ハッシュタグを入力'); ?></label>
                    <input type="text" class="form-control" id="tagValue" placeholder="<?php echo __('登録したいハッシュタグ名'); ?>">
                    <div id="hashtagSearch">
                      <i class="fa fa-spinner fa-spin fa-fw"></i><?php echo __('「<span id="hashtagSearchText"></span>」を検索中'); ?>
                    </div>
                    <div class="hash-search">
                      <div class="hash-search-box"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0" role="alert" style="display: none;">
              <?php echo __('<strong>これ以上ハッシュタグを登録できません！</strong> 不要なハッシュタグを削除してから登録してください。'); ?>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('登録中のハッシュタグ'); ?> <?php echo $this->Dashboard->hashtagMsg(count($hashtags)); ?></h3>
          </div>
          <div class="boxs-body hashtag-box">
            <?php echo $this->Dashboard->hashtagList($hashtags); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ HASHTAG -->
