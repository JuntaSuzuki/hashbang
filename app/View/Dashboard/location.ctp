
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?language=ja&key=AIzaSyBtmeO2T68NlqM8Xduh5Rw_2fa_CuF0SNg', array("inline" => false)); ?>

<?php echo $this->Html->script('/assets/js/location', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- LOCATION -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-map-marker"></i> <?php echo __('エリア管理'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('エリア設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('登録したエリア情報に一致する投稿に対して自動いいね、自動フォロー、自動コメントを行います。<br>※自動処理開始まで1時間程度かかる場合がございます。'); ?>
            </p>
            <div class="row">
              <div class="col-lg-4 col-md-5 pl-15 pr-15">
                <div class="form-group mb-0">
                  <label for="locationValue" class="control-label"><?php echo __('エリアを探す'); ?></label>
                  <input type="text" class="form-control" id="locationValue" placeholder="<?php echo __('住所またはエリア名を検索'); ?>">
                  <div id="locationSearch">
                    <i class="fa fa-spinner fa-spin fa-fw"></i><?php echo __('「<span id="locationSearchText"></span>」を検索中'); ?>
                  </div>
                </div>
              </div>
            </div>

            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0 alert-fail" role="alert" style="display: none;">
              <?php echo __('<strong>エリアの取得に失敗しました</strong> エリア名に間違いがないか確認してください。'); ?>
            </div>
            <div class="alert alert-danger alert-dismissible fade in mt-10 mb-0 alert-nomore" role="alert" style="display: none;">
              <?php echo __('<strong>これ以上エリアを登録できません！</strong> 不要なエリアを削除してから登録してください。'); ?>
            </div>

            <div id="map-canvas"></div>
            <div class="row location-list">
              <div class="col-md-12 pl-15">
                <label class="control-label mt-10"><?php echo __('エリア一覧'); ?></label>
                <div class="location-list-result"></div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('登録中のエリア'); ?><?php echo $this->Dashboard->locationMsg(count($locations)); ?></h3>
          </div>
          <div class="boxs-body location-box">
            <?php echo $this->Dashboard->locationList($locations); ?>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ LOCATION -->
