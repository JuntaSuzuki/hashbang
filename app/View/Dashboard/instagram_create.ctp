
<?php echo $this->Html->script('/assets/js/instagram', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- INSTAGRAM CREATE -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><a href="#" class="active"><?php echo __('新規アカウントの追加'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('新規アカウントの追加'); ?></h3>
          </div>
          <div class="boxs-body">
            <div class="row row-container">
              <div class="col-md-6">
                <?php echo $this->Form->create('Instagram'); ?>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('username', array(
                        'label' => __('インスタグラムユーザーネーム'),
                        'class' => 'form-control',
                        'placeholder' => __('インスタグラムユーザーネーム')
                      ));
                    ?>
                    <p class="js-error" style="display: none;"><?php echo __('インスタグラムユーザーが入力されていません'); ?></p>
                  </div>
                  <div class="form-group">
                    <?php 
                      echo $this->Form->input('password', array(
                        'label' => __('インスタグラムパスワード'),
                        'class' => 'form-control',
                        'placeholder' => __('インスタグラムパスワード'),
                        'value' => false
                      ));
                    ?>
                    <p class="js-error" style="display: none;"><?php echo __('インスタグラムパスワードが入力されていません'); ?></p>
                  </div>
                  <button type="button" class="btn btn-info" id="instagramAccountAdd"><?php echo __('アカウントを追加する'); ?></button>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

  </div>
</section>
<!--/ INSTAGRAM CREATE -->
