
<?php echo $this->Html->script('/assets/js/vendor/bootstrap/jquery-bootstrap-modal-steps', array("inline" => false)); ?>
<?php echo $this->Html->script('/assets/js/today_activity', array("inline" => false)); ?>

<?php if (!$activate): ?>
<?php $this->Html->scriptStart(array('inline' => false)); ?>
$(function() {
  $('#modal_intro').modalSteps({
    btnPreviousHtml: '<?php echo __('前へ'); ?>',
    btnNextHtml: '<?php echo __('次へ'); ?>',
    btnLastStepHtml: '<?php echo __('始める'); ?>',
    completeCallback: function() {
      var option = {
        url : '/dashboard/intro'
      }
      var data = {
        status : true,
      }
      app.global.ajaxPost(option, data, 'json')
        .done(function(res) {
        });
    }
  });
});

$(window).load(function(){
  $('.modal-intro').click();
});
<?php $this->Html->scriptEnd(); ?>
<?php endif; ?>
<style>
.alert.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
</style>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- CONTENT -->
<section id="content">
  <div class="page page-dashboard">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/dashboard"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-dashboard"></i> <?php echo __('ダッシュボード'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <!-- お試しプラン -->
    <?php if ($instagramAccount['Instagram']['media_count'] < MEDIA_COUNT): ?>
    <div class="alert alert-big alert-lightred alert-dismissable fade in br-5">
      <strong><?php echo __('最低でも５枚ほど写真を投稿してください。<br>それより少ない場合、インスタグラムの利用制限を受けやすくなります。'); ?></strong>
    </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12" data-intro='Hello step one!' data-step='1'>
        <section class="boxs boxs-simple br-5 relative">
          <!-- boxs widget -->
          <div class="boxs-widget p-30 pb-0">
            <div class="row">
              <div class="col-xs-4">
                <?php if (empty($instagramAccount)): ?>
                  <div class="thumb thumb-xl">
                    <img class="br-5" src="/img/noimage.png" alt="">
                  </div>
                <?php else: ?>
                  <div class="thumb thumb-lg">
                    <img class="br-5 mb-5" src="<?php echo h(str_replace('http://', 'https://', $instagramAccount['Instagram']['profile_picture'])); ?>" width="100%">
                  </div>
                <?php endif; ?>
              </div>
              <div class="col-xs-8 pr-0">
                <?php if (empty($instagramAccount)): ?>
                  <h4 class="mb-0 pl-10 pt-10"><?php echo __('アカウント未設定'); ?></h4>
                <?php else: ?>
                  <h4 class="mb-0 pl-10 pt-10"><?php echo h($instagramAccount['Instagram']['username']); ?></h4>
                <?php endif; ?>
                <ul class="list-inline tbox text-center m-0">
                  <li class="tcol p-5">
                    <h2 class="m-0 text-2lg"><?php echo $this->Dashboard->statusCountFormat($status['follower_count']); ?><span class="status-unit">人</span></h2>
                    <span class="dker"><?php echo __('フォロワー'); ?></span>
                  </li>
                  <li class="tcol dker p-5">
                    <h2 class="m-0 text-2lg"><?php echo $this->Dashboard->statusCountFormat($status['following_count']); ?><span class="status-unit">人</span></h2>
                    <span class="dker"><?php echo __('フォロー中'); ?></span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- /boxs widget -->
          <!-- boxs body -->
          <div class="boxs-body text-right">
            <a href="/dashboard/instagram"><button class="btn btn-sm btn-default"><i class="fa fa-instagram"></i> <?php echo __('インスタグラム設定'); ?></button></a>
            <a href="/dashboard/subscription"><button class="btn btn-sm btn-success"><?php echo __('プラン変更'); ?></button></a>
          </div>
          <span class="label plan-label <?php echo h($plan['Plan']['label-class']); ?>">
            <?php echo h($plan['Plan']['name']); ?>
          </span>
          <!-- /boxs body -->
        </section>
        <!-- /boxs -->
      </div>
      <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
      <div class="col-lg-2 col-sm-6 col-sm-12">
        <section class="boxs boxs-simple br-5">
          <div class="boxs-widget bg-blue text-center p-25 br-5-t"> <i class="fa fa-heart fa-2x"></i> </div>
          <div class="boxs-body text-center">
            <p class="text-elg text-strong mb-0"><?php echo $this->Dashboard->statusCountFormat($status['like_count']); ?><span class="status-unit">回</span></p>
            <span class="text-muted"><?php echo __('自動いいね数'); ?></span>
            <div class="status-class">
              <i class="fa fa-line-chart" aria-hidden="true"></i><?php echo __('前日'); ?>
            </div>
          </div>
        </section>
      </div>
      <!-- end col-lg-2 -->

      <div class="col-lg-2 col-sm-6 col-sm-12">
        <section class="boxs boxs-simple br-5">
          <div class="boxs-widget bg-greensea text-center p-25 br-5-t"> <i class="fa fa-user-plus fa-2x"></i> </div>
          <div class="boxs-body text-center">
            <p class="text-elg text-strong mb-0"><?php echo $this->Dashboard->statusCountFormat($status['follow_count']); ?><span class="status-unit">人</span></p>
            <span class="text-muted"><?php echo __('自動フォロー数'); ?></span>
            <div class="status-class">
              <i class="fa fa-line-chart" aria-hidden="true"></i><?php echo __('前日'); ?>
            </div>
          </div>
        </section>
      </div>
      <!-- end col-lg-2 -->

      <div class="col-lg-2 col-sm-6 col-sm-12">
        <section class="boxs boxs-simple br-5">
          <div class="boxs-widget bg-warning text-center p-25 br-5-t"> <i class="fa fa-comment fa-flip-horizontal fa-2x"></i> </div>
          <div class="boxs-body text-center">
            <p class="text-elg text-strong mb-0"><?php echo $this->Dashboard->statusCountFormat($status['comment_count']); ?><span class="status-unit">回</span></p>
            <span class="text-muted"><?php echo __('自動コメント数'); ?></span>
            <div class="status-class">
              <i class="fa fa-line-chart" aria-hidden="true"></i><?php echo __('前日'); ?>
            </div>
          </div>
        </section>
      </div>
      <!-- end col-lg-2 -->
      <?php endif; ?>

      <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
      <div class="col-lg-2 col-sm-6 col-sm-12">
        <section class="boxs boxs-simple br-5">
          <div class="boxs-widget bg-dutch text-center p-25 br-5-t"> <i class="fa fa-paper-plane fa-2x"></i> </div>
          <div class="boxs-body text-center">
            <p class="text-elg text-strong mb-0"><?php echo $this->Dashboard->statusCountFormat($status['message_count']); ?><span class="status-unit">回</span></p>
            <span class="text-muted"><?php echo __('自動メッセージ'); ?></span>
            <div class="status-class">
              <i class="fa fa-line-chart" aria-hidden="true"></i><?php echo __('前日'); ?>
            </div>
          </div>
        </section>
      </div>
      <!-- end col-lg-2 -->
      <?php endif; ?>
    </div>


    <div class="row">
      <div class="col-xs-12">
        <section class="boxs today-activity">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('本日のアクティビティ'); ?></h3>
          </div>
          <div class="boxs-body">
            <div role="tabpanel">
              <ul class="nav nav-tabs tabs-dark" role="tablist">
                <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
                <li role="presentation" class="active">
                  <a href="#activity_like" aria-controls="like" role="tab" data-toggle="tab">
                    <i class="fa fa-heart" aria-hidden="true"></i> <span class="hidden-xs"><?php echo __('いいね'); ?></span>
                  </a>
                </li>
                <li role="presentation">
                  <a href="#activity_follow" aria-controls="follow" role="tab" data-toggle="tab">
                    <i class="fa fa-user-plus" aria-hidden="true"></i> <span class="hidden-xs"><?php echo __('フォロー'); ?></span>
                  </a>
                </li>
                <li role="presentation">
                  <a href="#activity_comment" aria-controls="comment" role="tab" data-toggle="tab">
                    <i class="fa fa-comment fa-flip-horizontal" aria-hidden="true"></i> <span class="hidden-xs"><?php echo __('コメント'); ?></span>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12])): ?>
                <li role="presentation">
                  <a href="#activity_message" aria-controls="message" role="tab" data-toggle="tab">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i> <span class="hidden-xs"><?php echo __('メッセージ'); ?></span>
                  </a>
                </li>
                <?php endif; ?>
              </ul>
              <!-- end nav-tabs -->

              <div class="tab-content">
                <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
                <div role="tabpanel" class="tab-pane active" id="activity_like">
                  <div class="wrap-reset" style="max-height: 245px;overflow:auto;">
                    <?php if (count($likes) > 0): ?>
                      <?php foreach ($likes as $like): ?>
                      <div class="media">
                        <div class="thumb">
                          <?php echo $this->Dashboard->activityTodayMediaFormat($like['Activity']); ?>
                        </div>
                        <div class="media-info">
                          <p class="media-time">
                            <i class="fa fa-clock-o"></i> <?php echo $this->Dashboard->activityTimeFormat($like['Activity']['action_date']); ?>
                          </p>
                        </div>
                        <div class="media-body">
                          <p class="media-heading mb-0"><?php echo $this->Dashboard->activityTypeFormat($like); ?></p>
                          <?php echo $this->Dashboard->activityLabelFormat($like); ?>
                        </div>
                      </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php echo __('いいねのアクティビティがありません'); ?>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- end tabpanel -->

                <div role="tabpanel" class="tab-pane" id="activity_follow">
                  <div class="wrap-reset" style="max-height: 245px;overflow:auto;">
                    <?php if (count($follows) > 0): ?>
                      <?php foreach ($follows as $follow): ?>
                      <div class="media">
                        <div class="thumb">
                          <?php echo $this->Dashboard->activityTodayMediaFormat($follow['Activity']); ?>
                        </div>
                        <div class="media-info">
                          <p class="media-time">
                            <i class="fa fa-clock-o"></i> <?php echo $this->Dashboard->activityTimeFormat($follow['Activity']['action_date']); ?>
                          </p>
                        </div>
                        <div class="media-body">
                          <p class="media-heading mb-0"><?php echo $this->Dashboard->activityTypeFormat($follow); ?></p>
                          <?php echo $this->Dashboard->activityLabelFormat($follow); ?>
                        </div>
                      </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php echo __('フォローのアクティビティがありません'); ?>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- end tabpanel -->

                <div role="tabpanel" class="tab-pane" id="activity_comment">
                  <div class="wrap-reset" style="max-height: 245px;overflow:auto;">
                    <?php if (count($comments) > 0): ?>
                      <?php foreach ($comments as $comment): ?>
                      <div class="media">
                        <div class="thumb">
                          <?php echo $this->Dashboard->activityTodayMediaFormat($comment['Activity']); ?>
                        </div>
                        <div class="media-info">
                          <p class="media-time">
                            <i class="fa fa-clock-o"></i> <?php echo $this->Dashboard->activityTimeFormat($comment['Activity']['action_date']); ?>
                          </p>
                        </div>
                        <div class="media-body">
                          <p class="media-heading mb-0"><?php echo $this->Dashboard->activityTypeFormat($comment); ?></p>
                          <?php echo $this->Dashboard->activityLabelFormat($comment); ?>
                        </div>
                      </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php echo __('コメントのアクティビティがありません'); ?>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- end tabpanel -->
                <?php endif; ?>

                <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
                <div role="tabpanel" class="tab-pane <?php echo $plan_id == MESSAGE_PLAN? 'active' : ''; ?>" id="activity_message">
                  <div class="wrap-reset" style="max-height: 245px;overflow:auto;">
                    <?php if (count($messages) > 0): ?>
                      <?php foreach ($messages as $message): ?>
                      <div class="media">
                        <div class="thumb">
                          <?php echo $this->Dashboard->activityTodayMediaFormat($message['Activity']); ?>
                        </div>
                        <div class="media-info">
                          <p class="media-time">
                            <i class="fa fa-clock-o"></i> <?php echo $this->Dashboard->activityTimeFormat($message['Activity']['action_date']); ?>
                          </p>
                        </div>
                        <div class="media-body">
                          <p class="media-heading mb-0">
                            <?php echo $this->Dashboard->activityTypeFormat($message); ?>
                          </p>
                        </div>
                      </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php echo __('メッセージのアクティビティがありません'); ?>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- end tabpanel -->
                <?php endif; ?>
              </div>
              <!-- end tab-content -->
            </div>
          </div>
          <!-- /boxs body -->
        </section>
      </div>
    </div>
  </div>
  <!-- end page -->
</section>
<!--/ CONTENT -->

<?php if (!$activate): ?>
<span class="modal-intro" data-toggle="modal" data-target="#modal_intro"></span>
<div id="modal_intro" class="modal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="js-title-step"></h4>
      </div>
      <div class="modal-body">
        <div class="row hide" data-step="1" data-title="<?php echo __('#BANGへようこそ'); ?>">
          <div class="col-md-12">
            <div class="col-md-12">
              <p>#BANGがあなたに代わっていいね、コメント、フォロー、メッセージを自動で行い、フォロワーを効率的に増やすお手伝いをします。</p>
              <img src="/img/intro1.png" class="img-responsive m-auto">
            </div>
          </div>
        </div>
        <div class="row hide" data-step="2" data-title="ハッシュタグを登録する">
          <div class="col-md-12">
            <div class="col-md-12">
              <p><?php echo __('例えば「パンケーキ」のハッシュタグを登録した場合、「パンケーキ」のハッシュタグが付いた投稿に対して自動いいね、自動コメントを行います。<br>ユーザーに対しては自動フォローを行います。'); ?></p>
              <img src="/img/intro2.png" class="img-responsive m-auto">
              <?php if ($this->Session->read('hashtag_count') > 0): ?>
                <div class="alert alert-big alert-info alert-dismissable fade in br-5 mt-20 mb-5">
                  <strong><?php echo __('あなたの投稿でよく使われているハッシュタグを『%s個』登録しています。<br>ハッシュタグ管理から任意のハッシュタグに変更することができます。', $this->Session->read('hashtag_count')); ?></strong>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="row hide" data-step="3" data-title="自動設定をオンにする">
          <div class="col-md-12">
            <div class="col-md-12">
              <p>ハッシュタグを登録しただけでは、自動システムは動き始めません。<br>自動で行いたい機能のスイッチをオンにすることで、システムが動作し始めます。<br>スピード調整など、より細かい条件も設定できます。</p>
              <img src="/img/intro3.png" class="img-responsive m-auto">
              <?php if ($this->Session->read('hashtag_count') > 0): ?>
                <div class="alert alert-big alert-info alert-dismissable fade in br-5 mt-20 mb-5">
                  <strong><?php echo __('自動いいねと自動フォローの設定をONにしています。<br>自動設定から自分の好きな設定に変更することができます。'); ?></strong>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="row hide" data-step="4" data-title="他の機能も使ってみる">
          <div class="col-md-12">
            <div class="col-md-12">
              <p>#BANGにはメッセージ機能やエリア機能など、他の機能がまだまだあります!<br>
              お試しプランではすべての機能をご利用できますので、是非他の機能もお試しください!<br>わからない点がありましたら、サポートまでお問い合わせください。<br>是非素敵なインスタLIFEを!!</p>
              <img src="/img/intro4.png" class="img-responsive m-auto">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous">前へ</button>
        <button type="button" class="btn btn-success js-btn-step" data-orientation="next">次へ</button>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
