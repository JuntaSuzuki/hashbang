
<?php echo $this->Html->script('/assets/js/vendor/bootstrap/bootstrap-slider.min', array("inline" => false)); ?>

<?php echo $this->element('/Dashboard/header'); ?>

<?php echo $this->element('/Dashboard/controls'); ?>

<!-- CONTENT -->
<section id="content">
  <div class="page page-forms-common">
    <div class="row">
      <div class="col-md-12">
        <div class="pageheader clearfix">
          <div class="page-bar pull-left">
            <ul class="page-breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i> <?php echo __('ホーム'); ?></a></li>
              <li><span><i class="fa fa-magic"></i> <?php echo __('自動設定'); ?></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <!-- API制限 -->
    <?php echo $this->Dashboard->isEnabledApi($instagramAccount); ?>

    <!-- お試しプラン -->
    <?php if ($this->Dashboard->isSlowStartPeriod($user)): ?>
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-big alert-lightred alert-dismissable fade in br-5">
          <strong>
            <?php echo __('#BANGを使い始めると急激にいいねやフォローなどのアクティビティの数が増えてしまい、<br>
              インスタグラムの制限に引っかかる可能性があります。<br><br>
              そのため、#BANGでは5日間のスロースタート期間を設けており、<br>
              スロースタート期間中はスピード調整ができなくなっております。<br><br>
          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>プラン購入後もスロースタート期間中は、スピード調整ができませんのでご注意ください。'); ?>
          </strong>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動いいね設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('登録したハッシュタグに一致する投稿に対して自動的にいいねします。'); ?><br>
              <b><?php echo __('※いいね開始まで1時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoLikeSwitch" id="autoLike1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_like']); ?>>
              <label class="onoffswitch-label" for="autoLike1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動いいねする投稿のいいね数'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('設定した数値のいいね数の投稿に自動いいねします。'); ?><br>
              <b><?php echo __('※いいねが少ないスパム投稿を除外できます。'); ?></b>
            </p>
            <div style="height: 30px;">
              <input id="slider" type="text" 
                data-slider-value="[<?php echo h($auto['AutoSetting']['min_like_count']); ?>, <?php echo h($auto['AutoSetting']['max_like_count']); ?>]" style="display: none; width: 97%;">
            </div>
            <div class="slider_desc">
              <span id="min_like_count"><?php echo h($auto['AutoSetting']['min_like_count']); ?></span>
              <span>〜</span>
              <span id="max_like_count"><?php echo h($auto['AutoSetting']['max_like_count']); ?></span>
              <span> <?php echo __('件のいいねが付いている投稿を対象に自動いいねします。'); ?></span>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動いいねのスピード調整'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-5">
              <?php echo __('自動でいいねする機能のスピードを変更できます。（スピードよりも各項目の設定条件が優先されます。）'); ?><br>
              <b class="text-danger"><?php echo __('※インスタグラムを開設したばかりのアカウントは、スピードが早すぎると利用制限を受ける可能性がありますのでご注意ください。'); ?></b>
            </p>
            <ul class="mb-15 pl-25">
              <li><?php echo __('安全に運用するなら「ゆっくり」（1日：最大'); ?><?php echo $autoCount[LIKE][1]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('通常に運用するなら「普通」（1日：最大'); ?><?php echo $autoCount[LIKE][2]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('早めにするなら「早い」（1日：最大'); ?><?php echo $autoCount[LIKE][3]; ?><?php echo __('回まで）'); ?></li>
            </ul>
            <div class="btn-group" id="autoLikeSpeed">
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_like_speed'], 1); ?>" data-toggle="active" data-type="radio" data-id="1" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('ゆっくり'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_like_speed'], 2); ?>" data-toggle="active" data-type="radio" data-id="2" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('普通'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_like_speed'], 3); ?>" data-toggle="active" data-type="radio" data-id="3" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('早い'); ?></label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動フォロー設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('登録したハッシュタグを含む写真を投稿したユーザーを自動でフォローします。'); ?><br>
              <b class="text-danger"><?php echo __('※フォロー開始まで1時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoFollowSwitch" id="autoFollow1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_follow']); ?>>
              <label class="onoffswitch-label" for="autoFollow1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動フォローのスピード調整'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-5">
              <?php echo __('自動でフォローする機能のスピードを変更できます。（スピードよりも各項目の設定条件が優先されます。）'); ?><br>
              <b class="text-danger"><?php echo __('※インスタグラムを開設したばかりのアカウントは、スピードが早すぎると利用制限を受ける可能性がありますのでご注意ください。'); ?></b>
            </p>
            <ul class="mb-15 pl-25">
              <li><?php echo __('安全に運用するなら「ゆっくり」（1日：最大'); ?><?php echo $autoCount[FOLLOW][1]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('通常に運用するなら「普通」（1日：最大'); ?><?php echo $autoCount[FOLLOW][2]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('早めにするなら「早い」（1日：最大'); ?><?php echo $autoCount[FOLLOW][3]; ?><?php echo __('回まで）'); ?></li>
            </ul>
            <div class="btn-group" id="autoFollowSpeed">
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_follow_speed'], 1); ?>" data-toggle="active" data-type="radio" data-id="1" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('ゆっくり'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_follow_speed'], 2); ?>" data-toggle="active" data-type="radio" data-id="2" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('普通'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_follow_speed'], 3); ?>" data-toggle="active" data-type="radio" data-id="3" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('早い'); ?></label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動コメント設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('登録したハッシュタグに一致する投稿に対して自動的にコメントします。'); ?><br>
              <b><?php echo __('※コメント開始まで1時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoCommentSwitch" id="autoComment1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_comment']); ?>>
              <label class="onoffswitch-label" for="autoComment1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動コメントのスピード調整'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-5">
              <?php echo __('自動でコメントする機能のスピードを変更できます。（スピードよりも各項目の設定条件が優先されます。）'); ?><br>
              <b class="text-danger"><?php echo __('※インスタグラムを開設したばかりのアカウントは、スピードが早すぎると利用制限を受ける可能性がありますのでご注意ください。'); ?></b>
            </p>
            <ul class="mb-15 pl-25">
              <li><?php echo __('安全に運用するなら「ゆっくり」（1日：最大'); ?><?php echo $autoCount[COMMENT][1]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('通常に運用するなら「普通」（1日：最大'); ?><?php echo $autoCount[COMMENT][2]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('早めにするなら「早い」（1日：最大'); ?><?php echo $autoCount[COMMENT][3]; ?><?php echo __('回まで）'); ?></li>
            </ul>
            <div class="btn-group" id="autoCommentSpeed">
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_comment_speed'], 1); ?>" data-toggle="active" data-type="radio" data-id="1" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('ゆっくり'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_comment_speed'], 2); ?>" data-toggle="active" data-type="radio" data-id="2" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('普通'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_comment_speed'], 3); ?>" data-toggle="active" data-type="radio" data-id="3" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('早い'); ?></label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
    <?php endif; ?>

    <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動メッセージ設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('新規フォロワーに自動でメッセージを送信します。'); ?><br>
              <b><?php echo __('※メッセージ開始まで3時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoMessageSwitch" id="autoMessage1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_message']); ?>>
              <label class="onoffswitch-label" for="autoMessage1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動メッセージのスピード調整'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-5">
              <?php echo __('自動メッセージする機能のスピードを変更できます。（スピードよりも各項目の設定条件が優先されます。）'); ?><br>
              <b class="text-danger"><?php echo __('※インスタグラムを開設したばかりのアカウントは、スピードが早すぎると利用制限を受ける可能性がありますのでご注意ください。'); ?></b>
            </p>
            <ul class="mb-15 pl-25">
              <li><?php echo __('安全に運用するなら「ゆっくり」（1日：最大'); ?><?php echo $autoCount[MESSAGE][1]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('通常に運用するなら「普通」（1日：最大'); ?><?php echo $autoCount[MESSAGE][2]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('早めにするなら「早い」（1日：最大'); ?><?php echo $autoCount[MESSAGE][3]; ?><?php echo __('回まで）'); ?></li>
            </ul>
            <div class="btn-group" id="autoMessageSpeed">
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_message_speed'], 1); ?>" data-toggle="active" data-type="radio" data-id="1" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('ゆっくり'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_message_speed'], 2); ?>" data-toggle="active" data-type="radio" data-id="2" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('普通'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_message_speed'], 3); ?>" data-toggle="active" data-type="radio" data-id="3" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('早い'); ?></label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
    <?php endif; ?>

    <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動フォロー解除設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('フォローを返さないユーザーを自動でフォロー解除します。'); ?><br>
              <b><?php echo __('※フォロー解除開始まで3時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoUnfollowSwitch" id="autoUnfollow1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_unfollow']); ?>>
              <label class="onoffswitch-label" for="autoUnfollow1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('フォロー解除を開始する経過日数'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('フォローしてから設定された日数を待った後、フォロー解除を開始します（最大 30日まで）'); ?><br>
            </p>
            <div style="height: 30px;">
              <input id="sliderUnfollow" type="text" 
                data-slider-value="<?php echo h($auto['AutoSetting']['unfollow_day']); ?>" style="display: none; width: 97%;">
            </div>
            <div class="slider_desc">
              <span id="unfollow_day"><?php echo h($auto['AutoSetting']['unfollow_day']); ?></span>
              <span> <?php echo __('日経過してもフォローを返さないユーザーを自動フォロー解除します。'); ?></span>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動フォロー解除のスピード調整'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-5">
              <?php echo __('自動フォロー解除する機能のスピードを変更できます。（スピードよりも各項目の設定条件が優先されます。）'); ?><br>
              <b class="text-danger"><?php echo __('※インスタグラムを開設したばかりのアカウントは、スピードが早すぎると利用制限を受ける可能性がありますのでご注意ください。'); ?></b>
            </p>
            <ul class="mb-15 pl-25">
              <li><?php echo __('安全に運用するなら「ゆっくり」（1日：最大'); ?><?php echo $autoCount[UNFOLLOW][1]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('通常に運用するなら「普通」（1日：最大'); ?><?php echo $autoCount[UNFOLLOW][2]; ?><?php echo __('回まで）'); ?></li>
              <li><?php echo __('早めにするなら「早い」（1日：最大'); ?><?php echo $autoCount[UNFOLLOW][3]; ?><?php echo __('回まで）'); ?></li>
            </ul>
            <div class="btn-group" id="autoUnfollowSpeed">
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_unfollow_speed'], 1); ?>" data-toggle="active" data-type="radio" data-id="1" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('ゆっくり'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_unfollow_speed'], 2); ?>" data-toggle="active" data-type="radio" data-id="2" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('普通'); ?></label>
              <label class="btn btn-default toggle-class <?php echo $this->Dashboard->activeAutoSpeedSetting($auto['AutoSetting']['auto_unfollow_speed'], 3); ?>" data-toggle="active" data-type="radio" data-id="3" <?php echo $this->Dashboard->isEnabled($user); ?>><?php echo __('早い'); ?></label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動一括フォロー解除設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('ハッシュバンを使って増えたユーザーに対して一括フォロー解除できます。<br>フォロー解除は、古いフォローユーザーから順番に解除していきます。'); ?><br>
              <?php echo __('あなたが自分でフォローしたユーザーだけ残したい場合にこの機能を使用してください。'); ?><br>
              <b class="text-danger"><?php echo __('※自動一括フォロー解除をONにしている場合、自動フォローと自動フォロー解除機能がストップしますのでご注意ください。'); ?><br><?php echo __('　解除したくないユーザーは『ホワイトリスト』に設定してください。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoBulkUnfollowSwitch" id="autoBulkUnfollow1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_bulk_unfollow']); ?>>
              <label class="onoffswitch-label" for="autoBulkUnfollow1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-12">
        <section class="boxs">
          <div class="boxs-header dvd dvd-btm">
            <h3><?php echo __('自動フォローバック設定'); ?></h3>
          </div>
          <div class="boxs-body">
            <p class="mb-15">
              <?php echo __('フォローしてくれたユーザーに自動でフォローを返します。'); ?><br>
              <b><?php echo __('※フォローバック開始まで3時間程度かかる場合がございます。'); ?></b>
            </p>
            <div class="onoffswitch labeled blue inline-block">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoFollowBackSwitch" id="autoFollowBack1" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_followback']); ?>>
              <label class="onoffswitch-label" for="autoFollowBack1"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- end row -->
    <?php endif; ?>

  </div>
</section>
<!--/ CONTENT -->
