<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <?php echo $this->Html->charset(); ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title><?php echo $this->fetch('title'); ?></title>
  <meta name="description" content="#BANG（ハッシュバン）はインスタグラマーの為のサポートツールです。忙しいあなたに代わっていいね、コメント、フォロー、メッセージを自動で行い、フォロワーを効率的に増やすお手伝いをします。" />
  <meta name="author" content="#BANG" />
  <meta name="keywords" content="インスタグラム,フォロワー,フォロー,いいね,コメント,メッセージ,ハッシュタグ,増やす,#BANG,ハッシュバン" />

  <meta property="og:title" content="#BANG [ハッシュバン]" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://hashbang.jp" />
  <meta property="og:image" content="https://hashbang.jp/img/fb.png" />
  <meta property="og:site_name" content="#BANG [ハッシュバン]" />
  <meta property="og:description" content="#BANG（ハッシュバン）は忙しいあなたに代わっていいね、コメント、フォロー、メッセージを自動で行い、フォロワーを効率的に増やすお手伝いをします。" />

  <?php if (Configure::read('nofollow')): ?>
    <?php echo $this->Html->meta(['name' => 'robots', 'content' => 'noindex,nofollow'], null, ['inline' => false]); ?>
  <?php endif; ?>

  <link href="/favicon.ico" type="image/x-icon" rel="icon" />
  <link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Crete+Round:400i%7COpen+Sans:400,600,700" rel="stylesheet">

  <!-- Bootstrap CSS -->
  <?php echo $this->Html->css('bootstrap.min'); ?>

  <!-- Icon CSS -->
  <?php echo $this->Html->css('themify-icons'); ?>

  <!-- Magnific-popup -->
  <?php echo $this->Html->css('magnific-popup'); ?>

  <!-- Custom styles for this template -->
  <?php echo $this->Html->css('landing'); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

  <?php echo $this->Html->script('jquery-2.1.4.min'); ?>
  <?php echo $this->Html->script('/assets/js/vendor/jquery/jquery.mobile-events.min'); ?>

  <!-- PHPからJavaScriptにデータを渡す -->
  <?php echo $this->element('Common/set_js_vars'); ?>

  <?php echo $this->element('Common/analyticstracking'); ?>
</head>
<body data-spy="scroll" data-target="#data-scroll">
  <?php echo $this->Session->flash(); ?>

  <?php echo $this->fetch('content'); ?>

  <!-- Sticky Header -->
  <?php echo $this->Html->script('jquery.sticky'); ?>

  <!-- Jquery easing -->
  <?php echo $this->Html->script('jquery.easing.1.3.min'); ?>

  <!-- Owl Carousel -->
  <?php echo $this->Html->script('owl.carousel.min'); ?>

  <!-- Magnific Popup -->
  <?php echo $this->Html->script('jquery.magnific-popup.min'); ?>

  <!-- parsley - form validation -->
  <?php echo $this->Html->script('parsley.min'); ?>

  <!-- Custom js -->
  <?php echo $this->Html->script('app'); ?>

  <?php echo $this->Html->script('/assets/js/overlay'); ?>
  <?php echo $this->Html->script('main'); ?>

  <?php echo $this->fetch('script'); ?>
  <?php echo $this->element('sql_dump'); ?>
</body>
</html>