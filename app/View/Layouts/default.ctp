<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
  
  // オーバーレイ時の固定用クラス判定
  $fixed = '';
  if (((!$expiration_off && !$expiration)) || ($expiration && !$auth_error_off && $auth_error)) {
    $fixed = 'contents-fixed';
  }
?>
<!DOCTYPE html>
<html class="<?php echo $fixed; ?> ">
<head>
  <?php echo $this->Html->charset(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title><?php echo $this->fetch('title'); ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <?php if (Configure::read('nofollow')): ?>
    <?php echo $this->Html->meta(['name' => 'robots', 'content' => 'noindex,nofollow'], null, ['inline' => false]); ?>
  <?php endif; ?>

  <link href="/favicon.ico" type="image/x-icon" rel="icon" />
  <link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />

  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script>window.html5 || document.write('<script src="/js/html5shiv.js"><\/script>')</script>
  <![endif]-->

  <!-- vendor css files -->
  <?php echo $this->Html->css('/assets/css/vendor/bootstrap.min'); ?>
  <?php echo $this->Html->css('/assets/css/vendor/animate'); ?>
  <?php echo $this->Html->css('/assets/css/vendor/font-awesome.min'); ?>
  <?php echo $this->Html->css('/assets/js/vendor/animsition/css/animsition.min'); ?>
  <?php echo $this->Html->css('/assets/js/vendor/toastr/toastr'); ?>

  <!-- project main css files -->
  <?php echo $this->Html->css('/assets/css/main'); ?>

  <!--Modernizr-->
  <?php echo $this->Html->script('/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min'); ?>
  <?php echo $this->Html->script('/assets/js/vendor/jquery/jquery-1.11.2.min'); ?>
  <?php echo $this->Html->script('/assets/js/vendor/jquery/jquery.mobile-events.min'); ?>

  <!-- PHPからJavaScriptにデータを渡す -->
  <?php echo $this->element('Common/set_js_vars'); ?>

  <?php echo $this->element('Common/analyticstracking'); ?>
</head>
<body id="yatri" class="appWrapper <?php echo $fixed; ?>">
<?php
// A8用 imgタグ
if ($this->Session->check('a8_code') && $this->Session->check('a8_order_id')): ?>
<img src="https://px.a8.net/cgi-bin/a8fly/a8flyec?F=S&a8=<?php echo $this->Session->read('a8_code'); ?>&pid=s00000017647001&so=<?php echo $this->Session->read('a8_order_id'); ?>&si=1000.1.1000.a8" width="1" height="1">
<?php 
endif;

// セッションから削除
unset($_SESSION['a8_code']);
unset($_SESSION['a8_order_id']);
?>
<!-- Application Content -->
<div id="wrap" class="animsition">
  <?php echo $this->Session->flash(); ?>

  <?php 
    // 有効期間チェック
    if (!$expiration_off && !$expiration): ?>
  <div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert">
    <div class="toast toast-error">
      <div class="toast-message">
        <?php if ($plan_id == FREE_PLAN): ?>
          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php echo __('無料お試し期間が終了しました。<br>
          引き続きサービスをご利用いただくには、<br>
          <a href="/dashboard/subscription" class="underline">こちらからプランの購入を行ってください。</a>'); ?>
        <?php else: ?>
          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php echo __('プランの有効期間が終了しました。<br>
          引き続きサービスをご利用いただくには、<br>
          <a href="/dashboard/subscription" class="underline">こちらからプランの購入を行ってください。</a>'); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php 
    // 認証エラーチェック
    if ($expiration && !$auth_error_off && $auth_error): ?>
  <div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert">
    <div class="toast toast-error">
      <div class="toast-message">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php echo $instagramAccount['Instagram']['username']; ?> <?php echo __('はインスタグラムとのコネクトが切断されているため、<br>
        現在全てのアクティビティを停止しています。<br><br>
        引き続きサービスをご利用いただくには、<br>
        <a href="/dashboard/instagram" class="underline">インスタグラムアカウントの再コネクトを行ってください。</a>'); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php echo $this->fetch('content'); ?>
</div>

<!-- Vendor JavaScripts --> 
<?php echo $this->Html->script('/assets/js/vendor/bootstrap/bootstrap.min'); ?>
<?php echo $this->Html->script('/assets/js/vendor/jRespond/jRespond.min'); ?>
<?php echo $this->Html->script('/assets/js/vendor/slimscroll/jquery.slimscroll.min'); ?>
<?php echo $this->Html->script('/assets/js/vendor/animsition/js/jquery.animsition.min'); ?>
<!--/ vendor javascripts -->

<!-- Custom JavaScripts -->
<?php echo $this->Html->script('/assets/js/main'); ?>
<?php echo $this->Html->script('/assets/js/app'); ?>
<?php echo $this->Html->script('/assets/js/auto'); ?>
<?php echo $this->Html->script('/assets/js/overlay'); ?>
<?php echo $this->Html->script('/assets/js/sprintf'); ?>
<!--/ custom javascripts -->

<?php echo $this->fetch('script'); ?>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>