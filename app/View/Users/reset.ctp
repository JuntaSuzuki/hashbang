
<?php echo $this->element('/Pages/header'); ?>

<!-- RESET -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="home-wrapper home-intro">
          <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <?php 
              echo $this->Form->create('User', array(
                'class' => 'intro-form',
                'novalidate' => true
              ));
            ?>
              <h5><?php echo __('パスワードの再設定'); ?></h5>
              <?php foreach ($validationErrors as $error): ?>
                <p class="error-message"><?php echo $error[0]; ?></p>
              <?php endforeach; ?>
              <?php 
                echo $this->Form->input('password', array(
                  'type' => 'password',
                  'label' => false,
                  'error' => false,
                  'placeholder' => __('パスワード'),
                  'default' => ''
                ));
              ?>

              <?php 
                echo $this->Form->input('password_confirm', array(
                  'type' => 'password',
                  'label' => false,
                  'error' => false,
                  'placeholder' => __('パスワード（確認）'),
                  'default' => ''
                ));
              ?>

              <button type="submit" class="btn btn-custom btn-block"><?php echo __('設定する'); ?></button>
            <?php echo $this->Form->end(); ?>

          </div>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END RESET -->
