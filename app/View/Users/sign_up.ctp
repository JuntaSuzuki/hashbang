
<?php echo $this->Html->script('sign', array("inline" => false)); ?>

<?php echo $this->element('/Pages/header'); ?>

<!-- SIGN IN -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-4 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ1'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('会員情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ2'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('インスタグラム情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ3'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('登録完了'); ?></div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="home-wrapper home-intro">
          <?php 
            echo $this->Form->create('User', array(
              'class' => 'intro-form',
              'novalidate' => true
            ));
          ?>
            <h5><?php echo __('新規登録'); ?></h5>
            <?php 
              echo $this->Form->input('instagram_username', array(
                'label' => false,
                'placeholder' => __('インスタグラムユーザーネーム')
              ));
            ?>
            <?php 
              echo $this->Form->input('instagram_password', array(
                'type' => 'password',
                'label' => false,
                'placeholder' => __('インスタグラムパスワード')
              ));
            ?>
            <?php 
              echo $this->Form->input('email', array(
                'label' => false,
                'class' => 'fname',
                'placeholder' => __('#BANG メールアドレス'),
              ));
            ?>
            <?php 
              echo $this->Form->input('password', array(
                'label' => false,
                'placeholder' => __('#BANG パスワード')
              ));
            ?>
            <p class="term-privacy">
              <?php echo __('※<a href="/pages/term">利用規約</a>に同意したうえでご登録ください。'); ?>
            </p>
            <button type="button" class="btn btn-custom btn-block" id="signUp"><?php echo __('会員登録'); ?></button>
            <div class="text-center">
              <a href="/users/sign_in"><?php echo __('すでに会員の方はこちら'); ?></a>
            </div>
          <?php echo $this->Form->end(); ?>
          <span id="result"></span>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END SIGN IN -->

<?php //echo $this->element('/Landing/copyright'); ?>
