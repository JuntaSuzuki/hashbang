
<?php echo $this->element('/Pages/header'); ?>

<!-- FORGOT -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-12">
        <div class="home-wrapper home-intro">
          <?php 
            echo $this->Form->create('User', array(
              'class' => 'intro-form',
              'novalidate' => true
            ));
          ?>
            <h5><?php echo __('パスワードの再設定'); ?></h5>
            <p><?php echo __('登録されているメールアドレスをご入力ください。<br>再設定用のURLをメールにて送信させていただきます。'); ?></p>
            <?php if (isset($error)): ?>
              <?php 
                echo $this->Form->input('email', array(
                  'label' => false,
                  'error' => false,
                  'class' => 'form-error',
                  'placeholder' => __('メールアドレス')
                ));
              ?>
            <?php else: ?>
              <?php 
                echo $this->Form->input('email', array(
                  'label' => false,
                  'error' => false,
                  'class' => 'fname',
                  'placeholder' => __('メールアドレス')
                ));
              ?>
            <?php endif; ?>
            <button type="submit" class="btn btn-custom btn-block"><?php echo __('再設定手続きをする'); ?></button>
            <div class="text-center">
              <a href="/users/sign_in"><?php echo __('すでに会員の方はこちら'); ?></a>
            </div>
            <div class="text-center">
              <a href="/users/sign_up"><?php echo __('新規登録'); ?></a>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- FORGOT -->
