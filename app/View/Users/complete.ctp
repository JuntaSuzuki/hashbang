
<?php echo $this->Html->script('sign', array("inline" => false)); ?>

<?php echo $this->element('/Pages/header'); ?>

<!-- SIGN IN -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-4 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ1'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('会員情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ2'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('インスタグラム情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ3'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('登録完了'); ?></div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="home-wrapper home-intro">
          <div class="intro-form text-center mt25">
            <h5><?php echo __('登録完了'); ?></h5>
            <div><?php echo __('アカウント登録が完了しました。<br>ログインをクリックしてください。'); ?></div>
            <button type="button" class="btn btn-custom btn-block mt20" onclick="location.href='/dashboard'">
              <?php echo __('ログイン'); ?>
            </button>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END SIGN IN -->
