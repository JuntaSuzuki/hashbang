
<?php echo $this->Html->script('sign', array("inline" => false)); ?>

<?php echo $this->element('/Pages/header'); ?>

<!-- SIGN IN -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-4 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ1'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="/users/sign_up" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('会員情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step disabled">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ2'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('インスタグラム情報'); ?></div>
          </div>
          
          <div class="col-xs-4 bs-wizard-step disabled">
            <div class="text-center bs-wizard-stepnum"><?php echo __('ステップ3'); ?></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"><?php echo __('登録完了'); ?></div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="home-wrapper home-intro">
          <?php 
            echo $this->Form->create('User', array(
              'class' => 'intro-form',
              'novalidate' => true
            ));
          ?>
            <input type="password" name="dummy" style="display:none">
            <h5><?php echo __('新規登録'); ?></h5>
            <?php 
              echo $this->Form->input('email', array(
                'label' => false,
                'class' => 'fname',
                'placeholder' => __('メールアドレス'),
              ));
            ?>
            <?php 
              echo $this->Form->input('password', array(
                'label' => false,
                'placeholder' => __('パスワード'),
              ));
            ?>
            <p class="term-privacy">
              <?php echo __('※<a href="/pages/term">利用規約</a>に同意したうえでご登録ください。'); ?>
            </p>
            <button type="submit" class="btn btn-custom btn-block"><?php echo __('次へ'); ?></button>
            <div class="text-center">
              <a href="/users/sign_in"><?php echo __('すでに会員の方はこちら'); ?></a>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END SIGN IN -->

<?php //echo $this->element('/Landing/copyright'); ?>
