
<?php echo $this->element('/Pages/header'); ?>

<!-- SIGN IN -->
<section class="sign bg-lightgray">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-12">
        <div class="home-wrapper home-intro">
          <?php 
            echo $this->Form->create('User', array(
              'class' => 'intro-form',
              'novalidate' => true
            ));
          ?>
            <input type="password" name="dummy" style="display:none">
            <h5><?php echo __('ログイン'); ?></h5>
            <?php 
              echo $this->Form->input('email', array(
                'label' => false,
                'placeholder' => __('メールアドレス'),
                'default' => ''
              ));
            ?>
            <?php 
              echo $this->Form->input('password', array(
                'label' => false,
                'placeholder' => __('パスワード'),
                'default' => ''
              ));
            ?>
            <button type="submit" class="btn btn-custom btn-lg btn-block"><?php echo __('ログイン'); ?></button>
            <div class="text-center">
              <a href="/users/sign_up"><?php echo __('新規登録'); ?></a>
            </div>
            <div class="text-center">
              <a href="/users/forgot"><?php echo __('パスワードをお忘れですか？'); ?></a>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END SIGN IN -->
