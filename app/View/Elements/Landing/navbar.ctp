
<!-- Navbar -->
<div class="navbar navbar-custom sticky" role="navigation">
  <div class="container">
    <!-- Navbar-header -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <i class="ti-menu"></i>
      </button>
      <a class="navbar-brand logo" href="/">
        <img src="/img/logo.png" alt="logo" height="35"/>
      </a>
    </div>
    <!-- end navbar-header -->

    <!-- menu -->
    <div class="navbar-collapse collapse" id="data-scroll">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="<?php echo ($active != '') ? '/' : ''; ?>#services"><?php echo __('#BANGとは'); ?></a>
        </li>
        <li>
          <a href="<?php echo ($active != '') ? '/' : ''; ?>#features"><?php echo __('機能紹介'); ?></a>
        </li>
        <li>
          <a href="<?php echo ($active != '') ? '/' : ''; ?>#pricing"><?php echo __('料金プラン'); ?></a>
        </li>
        <li>
          <a href="<?php echo ($active != '') ? '/' : ''; ?>#contact"><?php echo __('お問い合わせ'); ?></a>
        </li>
        <li>
          <a href="/users/sign_in"><?php echo __('ログイン'); ?></a>
        </li>
      </ul>
    </div>
    <!--/Menu -->
  </div>
  <!-- end container -->
</div>
