
<!-- FOOTER -->
<footer class="bg-lightgray footer bg-pattern">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <a href="/"><img src="/img/logo.png" alt="logo" height="24" /></a>
        <p class="margin-t-20">
          <?php echo __('#BANGが、忙しいあなたに代わって自動システムがいいね、コメント、フォロー、メッセージを自動で行い、フォロワーを効率的に増やすお手伝いをします。'); ?>
        </p>
      </div>
      <div class="col-md-3 col-sm-6 col-md-offset-2">
        <h5>About</h5>
        <ul class="list-unstyled footer-list">
          <li>
            <a href="/pages/term"><?php echo __('利用規約'); ?></a>
          </li>
          <li>
            <a href="/pages/privacy"><?php echo __('プライバシーポリシー'); ?></a>
          </li>
          <li>
            <a href="/pages/legal"><?php echo __('特定商取引法に基づく表示'); ?></a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6">
        <h5>Information</h5>
        <ul class="list-unstyled footer-list">
          <li>
            <a href="/pages/corporate"><?php echo __('運営会社'); ?></a>
          </li>
          <li>
            <a href="/pages/contact"><?php echo __('お問い合わせ'); ?></a>
          </li>
        </ul>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</footer>
<!-- end FOOTER -->
