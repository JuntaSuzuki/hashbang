
<!-- PRICING -->
<section class="section" id="pricing">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2>料金プラン</h2>
        </div>
      </div>
    </div>
    <!-- end row -->
    <div class="row">
      <div class="col-md-12">
        <div class="row pricing-section">
          <div class="col-sm-4">
            <div class="pricing-box">
              <div class="pricing-header">
                <div class="plan-title"><?php echo __('ベーシックプラン'); ?></div>
                <div class="plan-price"><?php echo __('<small>月額</small> 2,000<span>円</span>'); ?></div>
              </div>
              <p><?php echo __('まずはお試しという方はこちら'); ?></p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="pricing-box">
              <div class="pricing-header">
                <div class="plan-title"><?php echo __('メッセージプラン'); ?></div>
                <div class="plan-price"><?php echo __('<small>月額</small> 2,000<span>円</span>'); ?></div>
              </div>
              <p><?php echo __('メッセージを自動化させたい！'); ?></p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="pricing-box active">
              <div class="pricing-header">
                <div class="plan-title"><?php echo __('セットプラン'); ?></div>
                <div class="plan-price"><?php echo __('<small>月額</small> 3,500<span>円</span>'); ?></div>
              </div>
              <p><?php echo __('#BANGの全機能を使いたいはこちら'); ?></p>
            </div>
          </div>
        </div>
        <a href="/pages/plan" class="btn btn-link margin-t-30 pull-right plan-detail-btn"><?php echo __('詳しい料金プランを見る <i class="ti-angle-double-right"></i>'); ?></a>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->

    <div class="row text-center">
      <div class="col-sm-12">
        <a href="/users/sign_up" class="btn btn-custom btn-lg"><?php echo __('7日間 全機能無料で使ってみる!'); ?></a>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end PRICING -->
