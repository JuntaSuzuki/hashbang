
<!-- VOICE -->
<section class="section bg-testimonials bg-pattern" id="voice">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2>利用者の声</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <div class="testimonial-box text-center">
          <h4>ショップの宣伝に大きく役立ちました!</h4>
          <p>すぐに反響があり、即効性を感じました。<br>いまは1000人を超えるフォロワーを獲得でき大変満足しております!</p>
        </div>
      </div>
      <!-- col -->
    </div>
    <!-- row -->
  </div>
  <!-- end container -->
</section>
<!-- end VOICE -->
