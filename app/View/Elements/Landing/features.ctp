
<!-- FEATURES -->
<section class="section" id="features">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <div class="title-box text-center">
              <h2><?php echo __('#BANGの機能'); ?></h2>
            </div>
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8">
            <div class="features-box">
              <div class="number">1 </div>
              <h3><?php echo __('自動いいね'); ?></h3>
              <p>
                <?php echo __('登録したハッシュタグに関連するユーザの写真に自動でいいねを行います。'); ?><br>
                <?php echo __('いいねを受け取ったユーザーはあなたのことが気になり、あなたがインスタグラムに投稿している写真を見に来てくれます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/like-icon.png" width="200px" alt="<?php echo __('自動いいね'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">2 </div>
              <h3><?php echo __('自動フォロー'); ?></h3>
              <p>
                <?php echo __('登録したハッシュタグに関連するユーザを自動でフォローします。'); ?><br>
                <?php echo __('フォローをしてもらったユーザーはあなたのことが気になり、あなたがインスタグラムに投稿している写真を見に来てくれます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/follow-icon.png" width="200px" alt="<?php echo __('自動フォロー'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8">
            <div class="features-box">
              <div class="number">3 </div>
              <h3><?php echo __('自動コメント'); ?></h3>
              <p>
                <?php echo __('登録したハッシュタグに関連するユーザに自動でコメントします。'); ?><br>
                <?php echo __('コメントを受け取ったユーザーはあなたのことが気になり、あなたがインスタグラムに投稿している写真を見に来てくれます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/comment-icon.png" width="200px" alt="<?php echo __('自動コメント'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">4 </div>
              <h3><?php echo __('エリア選択'); ?></h3>
              <p>
                <?php echo __('設定したエリアの人を狙って自動いいね、フォロー、コメントをします。'); ?><br>
                <?php echo __('狙った地域のフォロワーを獲得することができます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/area-icon.png" width="200px" alt="<?php echo __('エリア選択'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8">
            <div class="features-box">
              <div class="number">5 </div>
              <h3><?php echo __('スピード調整機能'); ?></h3>
              <p>
                <?php echo __('いいね、コメント、フォロー、メッセージをどのくらいのペースで行うか自分で設定出来ます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/adjust-icon.png" width="200px" alt="<?php echo __('スピード調整機能'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">6 </div>
              <h3><?php echo __('自動メッセージ <small>（メッセージプランとセットプランの契約者のみ使用可能）</small>'); ?></h3>
              <p>
                <?php echo __('あなたをフォローしてくれたユーザーに自動でメッセージを送信します。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/message-icon.png" width="200px" alt="<?php echo __('自動メッセージ'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8">
            <div class="features-box">
              <div class="number">7 </div>
              <h3><?php echo __('自動フォロー解除'); ?></h3>
              <p>
                <?php echo __('フォローを返してくれないユーザーを自動で解除します。'); ?><br>
                <?php echo __('システムを使って増えたフォロワーを自動で解除する機能もあります。'); ?><br>
                <?php echo __('解除したくないユーザーを設定することも可能です。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/unfollow-icon.png" width="200px" alt="<?php echo __('自動メッセージ'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">8 </div>
              <h3><?php echo __('フィルター機能'); ?></h3>
              <p>
                <?php echo __('投稿やプロフィールに指定したキーワードを含むユーザ対して、自動いいね、自動フォロー、自動コメントは行わないようにできます。'); ?><br>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/filter-icon.png" width="200px" alt="<?php echo __('フィルター機能'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8">
            <div class="features-box">
              <div class="number">9 </div>
              <h3><?php echo __('タグ解析'); ?></h3>
              <p>
                <?php echo __('設定したハッシュタグからどのくらいフォロワーが増えているか分析します。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/analytics-icon.png" width="200px" alt="<?php echo __('タグ解析'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">10 </div>
              <h3><?php echo __('アクティビティログ'); ?></h3>
              <p>
                <?php echo __('自動でいいね、フォロー、コメント、メッセージした内容を確認できます。'); ?><br>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/actionlog-icon.png" width="200px" alt="<?php echo __('アクティビティログ'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-sm-8">
            <div class="features-box">
              <div class="number">11 </div>
              <h3><?php echo __('レポート機能'); ?></h3>
              <p>
                <?php echo __('毎日のフォロワーの増加をレポートとして確認できます。'); ?>
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img src="/img/report-icon.png" width="200px" alt="<?php echo __('フォロワーレポート'); ?>">
          </div>
        </div>
        <!-- end row -->

        <div class="row vertical-content features">
          <div class="col-md-8 col-md-push-4">
            <div class="features-box">
              <div class="number">12 </div>
              <h3><?php echo __('アカウント一括管理'); ?></h3>
              <p>
                <?php echo __('複数のアカウントをボタン一つで管理出来ます。<br>※アカウント毎に登録は必要です。'); ?><br>
              </p>
            </div>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/img/account-bulk-icon.png" width="200px" alt="<?php echo __('アカウント一括管理'); ?>">
          </div>
        </div>
        <!-- end row -->

      </div>
    </div>
  </div>
  <!-- end container -->
</section>
<!-- end FEATURES -->
