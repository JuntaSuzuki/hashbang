
<?php echo $this->Html->script('contact', array("inline" => false)); ?>

<!-- CONTACT -->
<section class="section" id="contact">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2><?php echo __('お問い合わせ'); ?></h2>
        </div>
        <p class="text-center"><?php echo __('#BANGについてご不明な点や質問などございましたら、お問い合わせください。'); ?></p>
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <!--contact form start-->
      <div class="col-sm-12">
        <?php 
          echo $this->Form->create('Contact', [
            'class' => 'contact-form margin-t-20',
            'id' => 'contactForm',
            'novalidate' => true
          ]);
        ?>
          <div class="row">
            <div class="col-sm-4">
              <?php 
                echo $this->Form->input('name', [
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('お名前')
                ]);
              ?>
            </div>
            <div class="col-sm-4">
              <?php 
                echo $this->Form->input('email', [
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('メールアドレス')
                ]);
              ?>
            </div>
            <div class="col-sm-4">
              <?php 
                echo $this->Form->input('subject', [
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('お問い合わせタイトル')
                ]);
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <?php 
                echo $this->Form->textarea('content', [
                  'label' => false,
                  'class' => 'form-control',
                  'rows' => 5,
                  'placeholder' => __('お問い合わせ内容')
                ]);
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 contact-btn-outer">
              <button type="button" class="btn btn-custom btn-lg" id="contactBtn"><?php echo __('送信する'); ?></button>
            </div>
          </div>
        <?php echo $this->Form->end(); ?>

        <div class="line-box">
          <a href="http://line.me/ti/p/%40mfy6882w"><img height="36" border="0" alt="'stats.label.addfriend' (MISSING TRANSLATION)" src="https://biz.line.naver.jp/line_business/img/btn/addfriends_ja.png"></a>
          <div>お問い合わせはLINEでも受け付けています。友達追加の上ご連絡ください。</div>
        </div>
      </div>
      <!--contact form end-->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end CONTACT -->
