
<!-- CTA -->
<section class="section bg-lightgray bg-img bg-img-3 bg-overlay">
  <div class="container">
    <div class="row text-center">
      <div class="col-sm-12">
        <div class="cta-section">
          <h1><?php echo __('まずは全機能を７日間無料でお試しくださいませ！'); ?></h1>
          <h4><?php echo __('＊無料お試しにはクレジットカード等のご登録は必要ありません。<br>７日終了後に#BANGのアカウントは自然に停止し、料金が発生することはございませんので、ご安心ください。'); ?></h4>
          <a href="/users/sign_up" class="btn btn-custom btn-lg"><?php echo __('7日間 全機能無料で使ってみる!'); ?></a>
        </div>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end CTA -->
