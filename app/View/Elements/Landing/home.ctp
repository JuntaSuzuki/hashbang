
<!-- HOME -->
<section class="home bg-img-2 home-small" id="home">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="home-wrapper home-intro">
          <h1><?php echo __('#BANGへようこそ!!'); ?></h1>
          <h4>
            <?php echo __('忙しいあなたに代わって自動システムがいいね、コメント、フォロー、メッセージを自動で行い、フォロワーを効率的に増やすお手伝いをします。'); ?>
          </h4>

          <a href="/users/sign_up">
            <button type="button" class="btn btn-custom btn-signup"><?php echo __('7日間 全機能無料で使ってみる!'); ?></button>
          </a>
          <h5><?php echo __('すべての機能を7日間無料でお試しいただけます。<br>＊無料お試しにはクレジットカード等のご登録は必要ありません。<br>
7日終了後に#BANGのアカウントは自然に停止し、料金が発生することはございませんので、ご安心ください。'); ?></span></h5>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- END HOME -->
