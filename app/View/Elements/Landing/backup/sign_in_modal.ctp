
<!-- Login Modal -->
<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-modal" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <?php echo $this->Html->image('logo.png'); ?>
      </div>
      <div class="modal-body">
        <?php echo $this->Form->create('User'); ?>
          <fieldset>
            <div class="form-group">
              <?php echo $this->Form->input('email', array('label' => 'メールアドレス','class' => 'form-control', 'placeholder' => 'example@email.com')); ?>
            </div>

            <div class="form-group">
              <?php echo $this->Form->input('password', array('label' => 'パスワード', 'class' => 'form-control')); ?>
              <p><a href="#" id="openPasswordModal">パスワードを忘れた場合はこちら</a></p>
            </div>

            <div class="btn-area">
              <input type="button" value="ログイン" class="btn btn-lg btn-primary btn-block login-button" id="signIn">
            </div>
          </fieldset>
        <?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <p><a href="#" class="register-btn" id="openSignUpModal">新規登録はこちら</a></p>
      </div>
    </div>
  </div>
</div>