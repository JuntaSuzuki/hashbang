
<!-- Password Modal -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-modal" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <?php echo $this->Html->image('logo.png'); ?>
      </div>
      <div class="modal-body">
        <?php echo $this->Form->create('User'); ?>
          <fieldset>
            <div class="form-group">
              <?php echo $this->Form->input('email', array('label' => 'メールアドレス','class' => 'form-control', 'placeholder' => 'example@email.com')); ?>
            </div>

            <div class="btn-area">
              <input type="button" name="commit" value="パスワード再発行メールを送信" class="btn btn-lg btn-primary btn-block login-button">
            </div>
          </fieldset>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>