
<!-- Landing Start -->
<div class="start-area row" style="">
  <h2 style="font-size: 50px;color: #ffffff;line-height: 1.1;text-align: center" class="vc_custom_heading eltdf-ch-class vc_custom_1477999914352">START RIGHT NOW!</h2>
  <h6 style="font-size: 18px;color: #ffffff;line-height: 1.5;text-align: center;font-family:Droid Serif;font-weight:400;font-style:italic" class="vc_custom_heading vc_custom_1477485591659">Get Blu today and have a fully functional business website set up in no time thanks to our collection of beautiful predesigned pages, carefully created elements, and powerful options. </h6>
  <a itemprop="url" href="https://themeforest.net/item/blu-a-beautiful-theme-for-businesses-and-individuals/18217358?ref=Elated-Themes" target="_blank" style="color: #25abd1;background-color: #ffffff;border-color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-solid">
    <span class="eltdf-btn-text">BUY THE THEME</span>
  </a>
</div>

<div class="vc_row wpb_row vc_row-fluid vc_custom_1476694819173 vc_row-has-fill eltdf-content-aligment-center eltdf-row-type-row" style="">
  <div class="wpb_column vc_column_container vc_col-sm-12">
    <div class="vc_column-inner vc_custom_1476695136073">
      <div class="wpb_wrapper">
        <div class="eltdf-row-grid-section">
          <div class="vc_row wpb_row vc_inner vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
              <div class="vc_column-inner vc_custom_1476695141512">
                <div class="wpb_wrapper">
                  <div class="eltdf-elements-holder eltdf-responsive-mode-768">
                    <div class="eltdf-eh-item " data-item-class="eltdf-eh-custom-534438" data-768-1024="10% 11% 10% 11%" data-600-768="10% 3% 10% 3%" data-480="41% 3% 43% 3%">
                      <div class="eltdf-eh-item-inner">
                        <div class="eltdf-eh-item-content eltdf-eh-custom-534438" style="padding: 10% 20% 10% 20% ">
                          <div class="eltdf-animation-holder eltdf-element-from-bottom eltdf-element-from-bottom-on" data-animation="eltdf-element-from-bottom" data-animation-delay="100">
                            <div class="eltdf-animation-inner">
                              <h2 style="font-size: 50px;color: #ffffff;line-height: 1.1;text-align: center" class="vc_custom_heading eltdf-ch-class vc_custom_1477999914352">START RIGHT NOW!</h2>
                            </div>
                          </div>
                          <div class="vc_empty_space" style="height: 7px">
                            <span class="vc_empty_space_inner"></span>
                          </div>
                          <div class="eltdf-animation-holder eltdf-element-from-bottom eltdf-element-from-bottom-on" data-animation="eltdf-element-from-bottom" data-animation-delay="200">
                            <div class="eltdf-animation-inner">
                              <h6 style="font-size: 18px;color: #ffffff;line-height: 1.5;text-align: center;font-family:Droid Serif;font-weight:400;font-style:italic" class="vc_custom_heading vc_custom_1477485591659">Get Blu today and have a fully functional business website set up in no time thanks to our collection of beautiful predesigned pages, carefully created elements, and powerful options. </h6>
                            </div>
                          </div>
                          <div class="vc_empty_space" style="height: 23px">
                            <span class="vc_empty_space_inner"></span>
                          </div>
                          <div class="eltdf-animation-holder eltdf-element-from-bottom eltdf-element-from-bottom-on" data-animation="eltdf-element-from-bottom" data-animation-delay="300">
                            <div class="eltdf-animation-inner">
                              <a itemprop="url" href="https://themeforest.net/item/blu-a-beautiful-theme-for-businesses-and-individuals/18217358?ref=Elated-Themes" target="_blank" style="color: #25abd1;background-color: #ffffff;border-color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-solid">
                                <span class="eltdf-btn-text">BUY THE THEME</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>