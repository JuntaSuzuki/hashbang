
<!-- FUN-FACTS -->
<section class="section bg-lightgray funfacts">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2><?php echo __('一ヶ月で3000人の実績'); ?></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="testimonial-box text-center">
          <h4><?php echo __('＃BANGを利用しているユーザー様は、すでに一ヶ月に3000人のフォロワーの獲得を実現しています。<br>#BANGでフォロワーを効率よく集めましょう！</h1>'); ?></h4>
        </div>
      </div>
    </div>
    <!-- row -->
  </div>
  <!-- end container -->
</section>
<!-- end FUN-FACTS -->
