
<!-- SERVICES -->
<section class="section bg-testimonials" id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2><?php echo __('#BANGとは？'); ?></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="testimonial-box text-center">
          <h4>
            <?php echo __('#BANGの自動システムからいいねやコメントを受け取った人は、<br>
あなたのことが気になり、あなたがインスタグラムに投稿している写真を見に来ます。<br><br>
あなたのインスタグラムを訪れた人を、<br>
あなたの写真で魅了出来れば、いいねやコメント、フォローをしてくれます。<br><br>
フォローしてくれた方に対して、あなたが設定したメッセージを自動送信する事も出来ます。<br>
自動メッセージ機能により、フォローのお礼も自動ですることが可能になります。<br><br>
また、エリア選択機能やハッシュタグ設定により、あなたが集めたいフォロワーを集める事が出来ます。<br><br>
複数アカウントもボタン一つで管理できるのでとても便利です。<br><br>
＃BANGはインスタグラマーの為のサポートツールです。<br>
あなたは素敵な写真を撮ることや、他の作業に時間を使えるようになります。<br>
是非素敵なインスタLIFEを!!'); ?>
          </h4>
        </div>
      </div>
    </div>
    <!-- row -->
  </div>
  <!-- end container -->
</section>
<!-- end SERVICES -->
