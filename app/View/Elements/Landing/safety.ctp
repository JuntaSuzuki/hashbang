
<!-- SAFETY -->
<section class="section bg-testimonials bg-pattern" id="safety">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2><?php echo __('安心安全'); ?></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="testimonial-box text-center">
          <h4><?php echo __('＃BANGはフォロワーを購入する従来の方法とは全く異なります。<br>繰り返し行われたテストから導き出された適切なペースでフォロワーを増やすので<br>アカウントの凍結などの心配もありません!'); ?></h4>
        </div>
      </div>
    </div>
    <!-- row -->
  </div>
  <!-- end container -->
</section>
<!-- end SAFETY -->
