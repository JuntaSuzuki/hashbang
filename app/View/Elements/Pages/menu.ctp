
<div id="js-menu-fixed" class="menu">
  <div class="row">
    <ul class="clearfix">
      <li>
        <a href="/pages/term"><?php echo __('利用規約'); ?></a>
      </li>
      <li>
        <a href="/pages/privacy"><?php echo __('プライバシーポリシー'); ?></a>
      </li>
      <li>
        <a href="/pages/legal"><?php echo __('特定商取引法に基づく表記'); ?></a>
      </li>
      <li>
        <a href="/pages/corporate"><?php echo __('運営会社'); ?></a>
      </li>
      <li>
        <a href="/pages/contact">
          <?php echo __('お問い合わせ'); ?>
        </a>
      </li>
    </ul>
  </div>
</div>