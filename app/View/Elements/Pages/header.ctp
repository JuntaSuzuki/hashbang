
<!-- Navbar -->
<div class="navbar navbar-custom sticky" role="navigation">
  <div class="container">
    <!-- Navbar-header -->
    <div class="navbar-header page-navbar-header">
      <a class="navbar-brand logo" href="/">
        <img src="/img/logo.png" alt="logo" height="35"/>
      </a>
    </div>
  </div>
</div>
