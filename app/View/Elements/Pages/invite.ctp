
<!-- PRICING -->
<section class="section" id="pricing">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h2>友達やブログ読者への紹介で、有料プラン1カ月分を無料でプレゼント♪</h2>
        </div>
      </div>
    </div>
    <!-- end row -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="invite-section">
          <p class="invite-lead">招待コードURLをお友達に伝えたり、SNS・ブログ・ツイッターの読者などに広めてください。
            <br>招待コードURLを経由してお友達やブログの読者が#BANGに無料登録をすると、
          </p>
          <h2>あなたに有料プラン1カ月分を無料でプレゼントいたします♪</h2>
          <p class="text-center">招待コードはログインをして画面右上の「友達招待でお得！」のページからご利用下さい。</p>
          <ul class="sns_btn">
            <li>
              <img src="/images/btn_facebook.png" alt="Facebook">
              <p>Facebook</p>
            </li>
            <li>
              <img src="/images/btn_line.png" alt="LINE">
              <p>LINE</p>
            </li>
            <li>
              <img src="/images/btn_twitter.png" alt="Twitter">
              <p>Twitter</p>
            </li>
            <li>
              <img src="/images/btn_e-mail.png" alt="E-mail">
              <p>E-mail</p>
            </li>
          </ul>

          <div class="invite-code">
            <p class="text-center">（例）※下記の招待コードは例のため、実際にご利用になることはできません。</p>
            <table>
              <tbody>
                <tr>
                  <th>あなたの招待コードURL</th>
                  <td>
                    <input type="text" value="http://hashbang.jp/invite/xxxxx" class="invite_box">
                  </td>
                </tr>
                <tr>
                  <th>あなたの招待コード</th>
                  <td>
                    <input type="text" value="xxxxx" class="invite_box">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <p>※ 招待コードから個人が特定されることはありませんので、不特定多数の方に広めていただいても安心です。</p>

          <h4>【招待コードのご利用について】</h4>
          <p>■<strong>特典内容</strong></p>
          <ul>
            <li>あなたが招待した新規ユーザーが#BANGに無料登録をすると、あなたに有料プラン1カ月分が提供されます。</li>
          </ul>
          <p>■<strong>注意事項</strong></p>
          <ul>
            <li>自分で発行した招待コードを自分で使用することはできません。</li>
            <li>何人でも招待することができます。例えば、10人を招待したとすると有料会員10カ月分を得ることができます。</li>
            <li>事前の予告なく招待コードの特典の内容を変更したり、中止することがあります。</li>
          </ul>
        </div>

      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end PRICING -->
