
<section class="subscription">
  <div class="container">
    <h2 class="subscription__title">料金プラン</h2>
    <div class="row">
      <!-- Pricing Table Area -->
      <!-- Single Table -->
      <div class="col-sm-4">
        <div class="pricing-table text-center clearfix">
          <!-- Price -->
          <div class="price">
            <span>2,580</span>
            <span class="sub">円</span>
          </div>
          <!-- Heading -->
          <div class="pricing-table-heading">
            <h2>1ヶ月プラン</h2>
            <p>まずはお試しの方はこちら</p>
          </div>
          <!-- Button -->
          <div class="pricing-button pay-btn" data-plan-id="plan1">
            <a href="" class="btn btn-pricing">無料で始める</a>
          </div>
        </div>
      </div>
      <!-- Single Table -->
      <div class="col-sm-4">
        <div class="pricing-table text-center clearfix">
          <!-- Price -->
          <div class="price">
            <span>6,840</span>
            <span class="sub">円</span>
          </div>
          <!-- Heading -->
          <div class="pricing-table-heading">
            <h2>3ヶ月プラン</h2>
            <p>一番人気のお気軽＆お手軽プラン！</p>
          </div>
          <!-- Button -->
          <div class="pricing-button pay-btn" data-plan-id="plan2">
            <a href="#" class="btn btn-pricing">無料で始める</a>
          </div>
        </div>
      </div>
      <!-- Single Table -->
      <div class="col-sm-4">
        <div class="pricing-table text-center clearfix">
          <!-- Price -->
          <div class="price">
            <span>11,800</span>
            <span class="sub">円</span>
          </div>
          <!-- Heading -->
          <div class="pricing-table-heading">
            <h2>6ヶ月プラン</h2>
            <p>しっかりフォロワーを獲得したい</p>
          </div>
          <!-- Button -->
          <div class="pricing-button pay-btn" data-plan-id="plan3">
            <a href="#" class="btn btn-pricing">無料で始める</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>