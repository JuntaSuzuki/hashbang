
<header class="eltdf-page-header">
  <div class="eltdf-fixed-wrapper fixed" style="top: 0px;">
    <div class="eltdf-menu-area eltdf-menu-center" style="height: 60px;">
      <div class="eltdf-grid">

        <div class="eltdf-vertical-align-containers">
          <div class="eltdf-position-left">
            <div class="eltdf-position-left-inner">
              <div class="eltdf-logo-wrapper">
                <a itemprop="url" href="#" style="height: 24px;">
                  LOGO
                  <!-- <img itemprop="image" class="eltdf-normal-logo" src="http://blu.elated-themes.com/wp-content/uploads/2016/10/logo-blu.png" width="185" height="48" alt="logo">
                  <img itemprop="image" class="eltdf-dark-logo" src="http://blu.elated-themes.com/wp-content/uploads/2016/10/logo-blu-dark.png" width="185" height="48" alt="dark logo">
                  <img itemprop="image" class="eltdf-light-logo" src="http://blu.elated-themes.com/wp-content/uploads/2016/10/logo-blu-light.png" width="185" height="48" alt="light logo"> -->
                </a>
              </div>
            </div>
          </div>
          <div class="eltdf-position-center">
            <div class="eltdf-position-center-inner">
              <nav class="eltdf-main-menu eltdf-drop-down eltdf-default-nav">
                <ul id="menu-main-menu" class="clearfix">
                  <li id="nav-menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children eltdf-active-item has_sub wide">
                    <a href="#" class=" current ">
                      <span class="item_outer">
                        <span class="item_text">〇〇とは</span>
                      </span>
                    </a>
                  </li>
                  <li id="nav-menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                    <a href="#" class="">
                      <span class="item_outer">
                        <span class="item_text">機能紹介</span>
                      </span>
                    </a>
                  </li>
                  <li id="nav-menu-item-17" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide">
                    <a href="#" class="">
                      <span class="item_outer">
                        <span class="item_text">ご利用料金</span>
                      </span>
                    </a>
                  </li>
                  <li id="nav-menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide">
                    <a href="#" class="">
                      <span class="item_outer">
                        <span class="item_text">利用者の声</span>
                      </span>
                    </a>
                  </li>
                  <li id="nav-menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide">
                    <a href="#" class="">
                      <span class="item_outer">
                        <span class="item_text">よくある質問</span>
                      </span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>

          

          <div class="eltdf-position-right">
            <div class="eltdf-position-right-inner">
              <ul class="login-btn clearfix">
                <li>
                  <a class="sign-up-btn visible-md-inline visible-lg-inline" href="#" data-toggle="modal" data-target="#signUpModal">新規登録</a>
                </li>
                <li>
                  <a class="sign-in-btn visible-md-inline visible-lg-inline" href="#" data-toggle="modal" data-target="#signInModal">ログイン</a>
                </li>
              </ul>
              <a class="eltdf-side-menu-button-opener visible-xs-block visible-sm-block" href="javascript:void(0)">
                <span aria-hidden="true" class="edgtf-icon-font-elegant icon_menu eltdf-side-menu-icon"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>