
<footer class="footer">

  <div class="footer__top">
    <div class="container">
      <!-- <h3 class="big">21, Ampitheatre Parkway, Mountain View, CA</h3> -->
      <ul class="footer__top-list">
        <li class="footer__top-list-item" >
          <a href="#" target="_blank">
            <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i>
          </a>
        </li>
        <li class="footer__top-list-item" >
          <a href="#" target="_blank">
            <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
          </a>
        </li>
        <li class="footer__top-list-item" >
          <a href="#" target="_blank">
            <i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
          </a>
        </li>
      </ul>
    </div>
  </div>

  <div class="footer__bottom">
    <div class="container">
      <nav class="footer__bottom-menu">
        <ul class="footer__bottom-list">
          <li class="footer__bottom-list-item">
            <a href="#">利用規約</a>
          </li>
          <li class="footer__bottom-list-item">
            <a href="#">プライバシーポリシー</a>
          </li>
          <li class="footer__bottom-list-item">
            <a href="#">特定商取引法に基づく表示</a>
          </li>
        </ul>
      </nav>
      <div class="footer__bottom-text">Copyright &#169; 2017 <span>〇〇</span></div>
      </div>
    </div>
  </div>
</footer>
