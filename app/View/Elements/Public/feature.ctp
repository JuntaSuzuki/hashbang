<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <img src="http://portfoliotheme.org/fusion/wp-content/themes/fusion/images/sliders/iphone-gold-slider-stage.png" alt="iphone Slider">
    </div>
    <div class="col-md-9">
      <div class="text-content">
        <script type="text/javascript">
      jQuery(window).load(function () {jQuery('#features-slider.tab-slider-container .flexslider').flexslider({animation: "slide",slideshowSpeed: 5000,animationSpeed: 600,namespace: "flex-",controlNav: true,directionNav: false,smoothHeight: false,animationLoop: true,slideshow: true,easing: "swing",sync: "#features-iphone-slider.flex-slider-container .flexslider",manualControls: "#features-slider .tab-list li a",controlsContainer: "#features-slider .tab-list"})});
      </script>
        <div id="features-slider" class="tab-slider-container">
          <ul class="tab-list">
            <li>
              <a href="#0" class="flex-active">
                <i class="icon-uniBF"></i>
                <span class="tab-title">Pin Location</span>
              </a>
            </li>
            <li>
              <a href="#1" class="">
                <i class="icon-uniE10B"></i>
                <span class="tab-title">Contacts View</span>
              </a>
            </li>
            <li>
              <a href="#2" class="">
                <i class="icon-uniEF"></i>
                <span class="tab-title">Print Messages</span>
              </a>
            </li>
            <li>
              <a href="#3" class="">
                <i class="icon-uniE103"></i>
                <span class="tab-title">Hash Tags</span>
              </a>
            </li>
          </ul>
          <div class="flexslider">
            <!-- .slides -->
            <div class="flex-viewport" style="overflow: hidden; position: relative;">
              <ul class="slides" style="width: 1200%; transition-duration: 0s; transform: translate3d(-680px, 0px, 0px);">
                <li class="slide  clone" aria-hidden="true" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Great attention to detail that is reflected in each and every pixel. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
                <li class="slide flex-active-slide" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Designed to pixel perfection for great usability and aesthetics. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
                <!-- .slide -->
                <li class="slide" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Support is just a click away for registered users and purchasers. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
                <!-- .slide -->
                <li class="slide" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Theme is guaranteed to receive lifetime updates and bug fixes. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
                <!-- .slide -->
                <li class="slide" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Great attention to detail that is reflected in each and every pixel. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
                <!-- .slide -->
                <li class="slide  clone" aria-hidden="true" style="width: 680px; float: left; display: block;">
                  <div class="sixcol">
                    <div class="big">Designed to pixel perfection for great usability and aesthetics. Phasellus iaculis iusto nobiuse nullam illo praesent quaerat.</div>
                  </div>
                  <div class="sixcol last">Vut sagittis a, magna ridiculus scelerisque parturient! Cum duis nunc in dignissim, porta porta enim, proin turpis egestas. Mauris dapibus sed integer placerat ornare do torquent fuga quas pariatur. </div>
                  <span class="clear"></span>
                </li>
              </ul>
            </div>
          </div>
          <!-- .flexslider -->
        </div>
        <!-- .tab-slider-container -->
      </div>
    </div>
  </div>
</div>