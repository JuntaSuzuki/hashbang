
<div class="banner">
  <div class="container">

    <div class="banner__login-btn">
      <a href="/users/sign_in">ログイン</a>
    </div>

    <div class="banner__main">
      <h1 class="banner__logo">AutoGram</h1>
      <p class="banner__logo-copy">自動化の決定版</p>
      <h2 class="banner__copy">インスタライフを<br>もっと快適に!! </h2>
      <div class="banner__btn-link">
        <a class="fancybox-media" href="#">はじめての方へ</a>
      </div>
    </div>

    <div class="banner__sub">
      <form name="form" class="banner__form">
        <div class="banner__form-email">
          <i class="fa fa-envelope fa-2x banner__form-icon"></i>
          <input type="text" name="email" class="banner__form-input" required="" placeholder="メールアドレス">
        </div>
        <div class="banner__form-password">
          <i class="fa fa-key fa-2x banner__form-icon"></i>
          <input type="password" name="password" class="banner__form-input"placeholder="ご希望のパスワード（8文字以上）">
        </div>
        <button type="submit" class="btn banner__btn">5日間無料で試してみる！ </button>
      </form>
      <div class="banner__other">
        <!-- <div class="banner__facebook">
          <div class="banner__facebook-link">
            <a href="">Facebookではじめる</a>
          </div>
        </div> -->
        <div class="banner__terms">
          <a href="/terms">利用規約<span>はこちら</span></a>
        </div>
      </div>
    </div>

  </div>
</div>
