<?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動いいね'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoLikeSwitch" id="autoLike" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_like']); ?>>
        <label class="onoffswitch-label" for="autoLike">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->

<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動フォロー'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoFollowSwitch" id="autoFollow" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_follow']); ?>>
        <label class="onoffswitch-label" for="autoFollow">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->

<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動コメント'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoCommentSwitch" id="autoComment" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_comment']); ?>>
        <label class="onoffswitch-label" for="autoComment">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->
<?php endif; ?>

<?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動メッセージ'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoMessageSwitch" id="autoMessage" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_message']); ?>>
        <label class="onoffswitch-label" for="autoMessage">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->
<?php endif; ?>

<?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動フォロー解除'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoUnfollowSwitch" id="autoUnfollow" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_unfollow']); ?>>
        <label class="onoffswitch-label" for="autoUnfollow">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->

<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動一括フォロー解除'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoBulkUnfollowSwitch" id="autoBulkUnfollow" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_bulk_unfollow']); ?>>
        <label class="onoffswitch-label" for="autoBulkUnfollow">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->

<div class="form-group">
  <div class="row">
    <label class="col-xs-8 control-label"><?php echo __('自動フォローバック'); ?></label>
    <div class="col-xs-4 control-label">
      <div class="onoffswitch labeled blue">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox autoFollowBackSwitch" id="autoFollowBack" <?php echo $this->Dashboard->activeAutoSetting($auto['AutoSetting']['auto_followback']); ?>>
        <label class="onoffswitch-label" for="autoFollowBack">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- end form-group -->
<?php endif; ?>
