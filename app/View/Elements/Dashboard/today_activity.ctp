
<?php foreach ($activities as $activity): ?>
  <div class="media">
    <div class="thumb">
      <?php echo $this->Dashboard->activityTodayMediaFormat($activity['Activity']); ?>
    </div>
    <div class="media-info">
      <p class="media-time">
        <i class="fa fa-clock-o"></i> <?php echo $this->Dashboard->activityTimeFormat($activity['Activity']['action_date']); ?>
      </p>
    </div>
    <div class="media-body">
      <p class="media-heading mb-0"><?php echo $this->Dashboard->activityTypeFormat($activity); ?></p>
      <?php echo $this->Dashboard->activityLabelFormat($activity); ?>
    </div>
  </div>
<?php endforeach; ?>

