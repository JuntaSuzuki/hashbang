
<!-- ACTIVITY TIMELINE -->
<?php foreach ($activities as $activity): ?>
<li class="timeline-post">
  <?php echo $this->Dashboard->activityIconFormat($activity['Activity']); ?>
  <div class="post-container">
    <div class="panel panel-default br-5 b-0">
      <span class="text-muted time">
        <i class="fa fa-clock-o"></i><?php echo $this->Dashboard->activityTimeFormat($activity['Activity']['action_date']); ?>
        <?php echo $this->Dashboard->activityMediaFormat($activity['Activity']); ?>
      </span>
      <div class="activity-meta">
        <?php echo $this->Dashboard->activityProfileFormat($activity['Activity']); ?>
        <h5><?php echo h($activity['Activity']['username']); ?></h5>
      </div>
      <p><?php echo $this->Dashboard->activityLabelFormat($activity); ?></p>
      <?php echo $this->Dashboard->activityTypeFormat($activity); ?>
    </div>
  </div>
</li>
<?php endforeach; ?>
