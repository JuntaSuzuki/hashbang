
<?php foreach ($followings as $following): ?>
  <div class="media">
    <div class="media-image">
      <?php echo $this->Dashboard->followingMediaFormat($following['Activity']); ?>
    </div>
    <div class="media-text">
      <?php echo h($following['Activity']['username']); ?>
    </div>
    <div class="media-button">
      <?php echo $this->Dashboard->followingButtonFormat($following['Activity']); ?>
    </div>
  </div>
<?php endforeach; ?>
