
<div class="panel panel-default">
  <div class="panel-heading" role="tab">
    <h4 class="panel-title">
      <a data-toggle="collapse" href="#sidebarNav"><?php echo __('メニュー'); ?> <i class="fa fa-angle-up"></i>
      </a>
    </h4>
  </div>
  <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
    <div class="panel-body">
      <!--  NAVIGATION Content -->
      <ul id="navigation">
        <li <?php echo $this->Dashboard->activeMenu($active, 'home'); ?>>
          <a href="/dashboard">
            <i class="fa fa-dashboard"></i>
            <span><?php echo __('ダッシュボード'); ?></span>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'hashtag'); ?>>
          <a href="/dashboard/hashtag">
            <i class="fa fa-hashtag"></i>
            <span><?php echo __('ハッシュタグ管理'); ?></span>
          </a>
        </li>
        <li <?php echo $this->Dashboard->activeMenu($active, 'comment'); ?>>
          <a href="/dashboard/comment">
            <i class="fa fa-comment fa-flip-horizontal"></i>
            <span><?php echo __('コメント管理'); ?></span>
          </a>
        </li>
        <?php endif; ?>
        <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'message'); ?>>
          <a href="/dashboard/message">
            <i class="fa fa-paper-plane"></i>
            <span><?php echo __('メッセージ管理'); ?></span>
          </a>
        </li>
        <?php endif; ?>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'filter'); ?>>
          <a href="/dashboard/filter">
            <i class="fa fa-fw fa-filter"></i>
            <span><?php echo __('フィルター管理'); ?></span>
          </a>
        </li>
        <li <?php echo $this->Dashboard->activeMenu($active, 'location'); ?>>
          <a href="/dashboard/location">
            <i class="fa fa-fw fa-map-marker"></i>
            <span><?php echo __('エリア管理'); ?></span>
          </a>
        </li>
        <?php endif; ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'activity'); ?>>
          <a href="/dashboard/activity">
            <i class="fa fa-file-text-o"></i>
            <span><?php echo __('アクティビティ'); ?></span>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'whitelist'); ?>>
          <a href="/dashboard/whitelist">
            <i class="fa fa-address-card-o"></i>
            <span><?php echo __('ホワイトリスト'); ?></span>
          </a>
        </li>
        <?php endif; ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'auto'); ?>>
          <a href="/dashboard/auto">
            <i class="fa fa-magic"></i>
            <span><?php echo __('自動設定'); ?></span>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li <?php echo $this->Dashboard->activeMenu($active, 'analytics'); ?>>
          <a href="/dashboard/analytics">
            <i class="fa fa-line-chart"></i>
            <span><?php echo __('レポート'); ?></span>
          </a>
        </li>
        <li <?php echo $this->Dashboard->activeMenu($active, 'hashtag_analytics'); ?>>
          <a href="/dashboard/hashtag_analytics">
            <i class="fa fa-bar-chart-o"></i>
            <span><?php echo __('ハッシュタグ解析'); ?></span>
          </a>
        </li>
        <?php endif; ?>
      </ul>
      <!--/ NAVIGATION Content -->

    </div>
  </div>
</div>
