
<?php foreach ($analytics as $analy): ?>
  <tr>
    <td><?php echo h($analy['hashtag']); ?></td>
    <td class="text-center"><?php echo h($analy['like_count']); ?></td>
    <td class="text-center"><?php echo h($analy['comment_count']); ?></td>
    <td class="text-center"><?php echo h($analy['follow_count']); ?></td>
    <td class="text-center"><?php echo h($analy['followback_count']); ?></td>
    <td class="text-center"><?php echo $this->Dashboard->calcObtainRate($analy); ?></td>
  </tr>
<?php endforeach; ?>
