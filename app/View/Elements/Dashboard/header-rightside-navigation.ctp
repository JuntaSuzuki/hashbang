
<!-- Header navigation -->
<ul class="nav-right pull-right list-inline mr-50">
  <li class="dropdown nav-profile">
    <a href class="dropdown-toggle" data-toggle="dropdown">
      <?php if (empty($instagramAccount)): ?>
        <img src="/img/noimage.png" class=" size-30x30">
        <span>アカウント未設定 
          <i class="fa fa-angle-down"></i>
        </span>
      <?php else: ?>
        <img src="<?php echo h(str_replace('http://', 'https://', $instagramAccount['Instagram']['profile_picture'])); ?>" class="br-5 size-30x30">
        <span>
          <span><?php echo h($instagramAccount['Instagram']['username']); ?></span> 
          <i class="fa fa-angle-down"></i>
        </span>
      <?php endif; ?>
    </a>
    <ul class="dropdown-menu animated littleFadeInDown " role="menu">
      <?php foreach ($instagrams as $instagram): ?>
      <li>
        <a href="/dashboard/accountChange/<?php echo $instagram['username']; ?>" role="button" tabindex="0">
          <i class="fa fa-fw fa-instagram"></i><?php echo $instagram['username']; ?>
        </a>
      </li>
      <?php endforeach; ?>
      <?php if (count($instagrams) > 0): ?>
      <li class="divider"></li>
      <?php endif; ?>
      <li>
        <a href="/dashboard/instagram" role="button" tabindex="0">
          <i class="fa fa-fw fa-user-circle"></i>インスタグラム設定
        </a>
      </li>
      <li>
        <a href="/dashboard/account" role="button" tabindex="0">
          <i class="fa fa-fw fa-user"></i>アカウント設定
        </a>
      </li>
      <li>
        <a href="/dashboard/subscription" role="button" tabindex="0">
          <i class="fa fa-fw fa-check"></i>プラン変更
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="/dashboard/accountAdd" role="button" tabindex="0">
          <i class="fa fa-fw fa-user-plus"></i>アカウント追加
        </a>
      </li>

      <li class="divider"></li>
      <li>
        <a href="/users/logout" role="button" tabindex="0">
          <i class="fa fa-fw fa-sign-out"></i>ログアウト
        </a>
      </li>
    </ul>
  </li>

  <li class="dropdown menu">
    <a href class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-bars"></i>
    </a>
    <div class="dropdown-menu pull-right with-arrow panel panel-default animated littleFadeInUp ">
      <div class="panel-heading"><?php echo __('メニュー'); ?></div>
      <ul class="list-group">
        <li class="list-group-item">
          <a href="/dashboard" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-dashboard"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('ダッシュボード'); ?></span>
            </div>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li class="list-group-item">
          <a href="/dashboard/hashtag" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-hashtag"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('ハッシュタグ管理'); ?></span>
            </div>
          </a>
        </li>
        <li class="list-group-item">
          <a href="/dashboard/comment" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-comment fa-flip-horizontal"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('コメント管理'); ?></span>
            </div>
          </a>
        </li>
        <?php endif; ?>
        <?php if (in_array($plan_id, [FREE_PLAN, MESSAGE_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li class="list-group-item">
          <a href="/dashboard/message" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-paper-plane"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('メッセージ管理'); ?></span>
            </div>
          </a>
        </li>
        <?php endif; ?>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li class="list-group-item">
          <a href="/dashboard/filter" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-fw fa-filter"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('フィルター管理'); ?></span>
            </div>
          </a>
        </li>
        <li class="list-group-item">
          <a href="/dashboard/location" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-fw fa-map-marker"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('エリア管理'); ?></span>
            </div>
          </a>
        </li>
        <?php endif; ?>
        <li class="list-group-item">
          <a href="/dashboard/activity" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-fw fa-file-text-o"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('アクティビティ'); ?></span>
            </div>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li class="list-group-item">
          <a href="/dashboard/whitelist" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-address-card-o"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('ホワイトリスト'); ?></span>
            </div>
          </a>
        </li>
        <?php endif; ?>
        <li class="list-group-item">
          <a href="/dashboard/auto" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-magic"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('自動設定'); ?></span>
            </div>
          </a>
        </li>
        <?php if (in_array($plan_id, [FREE_PLAN, BASIC_PLAN, SET_PLAN1, SET_PLAN3, SET_PLAN6, SET_PLAN12, GOLD_PLAN])): ?>
        <li class="list-group-item">
          <a href="/dashboard/analytics" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-line-chart"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('レポート'); ?></span>
            </div>
          </a>
        </li>
        <li class="list-group-item">
          <a href="/dashboard/hashtag_analytics" class="media">
            <span class="pull-left media-object media-icon">
              <i class="fa fa-bar-chart-o"></i>
            </span>
            <div class="media-body">
              <span class="block"><?php echo __('ハッシュタグ解析'); ?></span>
            </div>
          </a>
        </li>
        <?php endif; ?>
      </ul>
    </div>
  </li>

  <li class="toggle-right-sidebar">
    <a role="button" tabindex="0">
      <i class="fa fa-cog"></i>
    </a>
  </li>
</ul>
<!-- Header navigation end -->
