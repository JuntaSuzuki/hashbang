
<!--  GENERAL SETTING Content -->
<div class="panel settings panel-default">
  <div class="panel-heading" role="tab">
    <h4 class="panel-title">
      <a data-toggle="collapse" href="#sidebarControls"><?php echo __('自動設定'); ?> 
        <i class="fa fa-angle-up"></i>
      </a>
    </h4>
  </div>
  <div id="sidebarControls" class="panel-collapse collapse in" role="tabpanel">
    <div class="panel-body">
      <?php echo $this->element('/Dashboard/general-auto-setting'); ?>
    </div>
  </div>
</div>
<!--/ GENERAL SETTING Content -->
