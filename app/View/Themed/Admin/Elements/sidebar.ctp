
<!--SIDEBAR Content -->
<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper">
    <a href="#" class="left-sidebar-toggle"><?php echo __('ナビゲーション'); ?></a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
            <li <?php echo $this->Util->activeClass($active, 'user'); ?>>
              <a href="/admins/user">
                <span><?php echo __('顧客管理'); ?></span>
              </a>
            </li>
            <li <?php echo $this->Util->activeClass($active, 'sale'); ?>>
              <a href="/admins/sale">
                <span><?php echo __('売上管理'); ?></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ SIDEBAR Content -->
