<script>
// 名前空間
var app = {};

// そのメソッドに渡す引数オブジェクト。
app.vars = $.parseJSON('<?php echo json_encode($js_vars); ?>');

// // 関数格納用
// App.func = new Array;

// // イベント格納用
// App.event = {};

// // イベントの格納
// App.addEvent = function(event) {
// 	if (!event) {
// 		event = {};
// 	}
// 	for (var attrname in event) {
// 		if (event.hasOwnProperty(attrname)) {
// 			App.event[attrname] = event[attrname];
// 		}
// 	}
// }
</script>
