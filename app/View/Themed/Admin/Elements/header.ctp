
<!-- HEADER Content -->
<nav class="navbar navbar-default navbar-fixed-top be-top-header">
  <div class="container-fluid">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">
        <?php echo __('#BANG<br>管理画面'); ?>
      </a>
    </div>
    <div class="be-right-navbar">
      <ul class="nav navbar-nav navbar-right be-user-nav">
        <li class="dropdown">
          <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
            <span class="icon mdi mdi-account"></span>
          </a>
          <ul role="menu" class="dropdown-menu">
            <li>
              <div class="user-info">
                <div class="user-name">管理者</div>
              </div>
            </li>
            <li>
              <a href="/admins/logout">
                <span class="icon mdi mdi-power"></span>ログアウト
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!--/ HEADER Content  -->
