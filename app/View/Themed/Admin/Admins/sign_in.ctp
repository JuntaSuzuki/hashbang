
<?php echo $this->element('header'); ?>

<!-- SIGN IN -->
<div class="be-content">
  <div class="main-content container-fluid">
    <div class="splash-container">
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading">
          <?php echo __('ログイン'); ?>
          <span class="splash-description"><?php echo __('メールアドレスとパスワードでログイン'); ?></span>
        </div>
        <div class="panel-body">
          <?php 
            echo $this->Form->create('Admin', array(
              'novalidate' => true
            ));
          ?>
            <div class="form-group">
              <?php 
                echo $this->Form->input('email', array(
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('メールアドレス'),
                  'autocomplete' => 'off'
                ));
              ?>
            </div>
            <div class="form-group">
              <?php 
                echo $this->Form->input('password', array(
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('パスワード'),
                  'autocomplete' => 'off'
                ));
              ?>
            </div>
            <div class="form-group row login-tools">
              <div class="col-xs-12 login-forgot-password">
                <a href="/admins/forgot"><?php echo __('パスワードをお忘れの方はこちら'); ?></a>
              </div>
            </div>
            <div class="form-group login-submit">
              <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">
                <?php echo __('ログイン'); ?>
              </button>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END SIGN IN -->
