
<?php echo $this->element('/header'); ?>

<?php echo $this->element('/sidebar'); ?>

<div class="be-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="page-head sm-mb-20">
        <h2 class="page-head-title pull-left">
          <?php echo __('顧客一覧'); ?>
        </h2>
      </div>
    </div>
  </div>
  <div class="main-content container-fluid xs-pt-10">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
          <div class="panel-heading panel-heading-divider">
            <?php echo __('検索絞込'); ?>
          </div>
          <div class="panel-body">
            <?php 
              echo $this->Form->create('Worker', array(
                'url' => Configure::read('url') . 'admins/worker',
                'novalidate' => true
              ));
            ?>
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group xs-mt-10">
                    <?php 
                      echo $this->Form->input('coc', array(
                        'label' => __('インスタグラムユーザー名'),
                        'class' => 'form-control',
                        'placeholder' => __('インスタグラムユーザー名')
                      ));
                    ?>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group xs-mt-10">
                    <?php 
                      echo $this->Form->input('coc', array(
                        'label' => __('メールアドレス'),
                        'class' => 'form-control',
                        'placeholder' => __('メールアドレス')
                      ));
                    ?>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-space btn-success btn-lg hover">
                      <?php echo __('検索'); ?>
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default panel-table">
          <div class="panel-body">
            <div id="table1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
              <div class="row be-datatable-body">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table id="table1" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                      <thead>
                        <tr role="row">
                          <th><?php echo __('No.'); ?></th>
                          <th><?php echo __('メールアドレス'); ?></th>
                          <th><?php echo __('金額'); ?></th>
                          <th><?php echo __('プラン'); ?></th>
                          <th width="28%" class="text-center"><?php echo __('課金日時'); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($users as $user): ?>
                        <tr class="gradeA odd" role="row">
                          <td>
                            <?php echo h($user['User']['id']); ?>
                          </td>
                          <td>
                            <?php echo h($user['User']['email']); ?>
                          </td>
                          <td>

                          </td>
                          <td>
                            
                          </td>
                          <td class="text-center">
                            
                          </td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.row -->

              <div class="row be-datatable-footer">
                <div class="col-sm-5">
                  <div class="dataTables_info" role="status" aria-live="polite">
                    <?php 
                      echo $this->Paginator->counter(array(
                        'model' => 'User',
                        'format' => '{:count}件中&nbsp;{:start}～{:end}件を表示'
                      ));
                    ?>
                  </div>
                </div>
                <div class="col-sm-7">
                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                    <ul class="pagination">
                      <?php 
                        echo $this->Paginator->first('<<前へ', array(
                          'model' => 'User',
                          'tag' => 'li',
                          'after' => '&nbsp;',
                          'class' => 'paginate_button'
                        ));
                      ?><!--
                      --><?php 
                        echo $this->Paginator->numbers(array(
                          'model' => 'User',
                          'tag' => 'li',
                          'currentTag' => 'a',
                          'modulus' => 5,
                          'separator' => '',
                          'currentClass' => 'paginate_button active',
                        )); 
                      ?><!--
                      --><?php 
                        echo $this->Paginator->last('次へ>>', array(
                          'model' => 'User',
                          'tag' => 'li',
                          'before' => '&nbsp;',
                          'class' => 'paginate_button'
                        ));
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
</div>
