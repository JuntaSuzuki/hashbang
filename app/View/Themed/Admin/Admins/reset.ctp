
<?php echo $this->element('header'); ?>

<!-- RESET -->
<div class="be-content">
  <div class="main-content container-fluid">
    <div class="splash-container">
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading">
          <?php echo __('パスワードの再設定'); ?>
        </div>
        <div class="panel-body">
          <?php 
            echo $this->Form->create('Admin', array(
              'novalidate' => true
            ));
          ?>
            <div class="form-group">
              <?php 
                echo $this->Form->input('password', array(
                  'type' => 'password',
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('パスワード')
                ));
              ?>
            </div>
            <div class="form-group">
              <?php 
                echo $this->Form->input('password_confirm', array(
                  'type' => 'password',
                  'label' => false,
                  'class' => 'form-control',
                  'placeholder' => __('パスワード（確認）')
                ));
              ?>
            </div>
            <div class="form-group login-submit">
              <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">
                <?php echo __('設定する'); ?>
              </button>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END RESET -->
