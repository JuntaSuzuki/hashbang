
<?php echo $this->element('header'); ?>

<!-- FORGOT -->
<div class="be-content">
  <div class="main-content container-fluid">
    <div class="splash-container forgot-password">
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading">
          <span class="splash-description"><?php echo __('パスワードをお忘れですか？'); ?></span>
        </div>
        <div class="panel-body">
          <?php 
            echo $this->Form->create('Admin', array(
              'novalidate' => true
            ));
          ?>
            <p><?php echo __('メールアドレスを入力して送信すると、パスワード設定用の URL を送信します。'); ?></p>
            <div class="form-group xs-pt-20">
              <?php 
                echo $this->Form->input('email', array(
                  'label' => false,
                  'error' => false,
                  'class' => 'form-control',
                  'required' => false,
                  'autocomplete' => 'off',
                  'placeholder' => __('メールアドレス')
                ));
              ?>
            </div>
            <div class="form-group row login-tools">
              <div class="col-xs-12 login-forgot-password">
                <a href="/admins/sign_in"><?php echo __('ログイン画面に戻る'); ?></a>
              </div>
            </div>
            <div class="form-group login-submit">
              <button type="submit" class="btn btn-block btn-primary btn-xl"><?php echo __('送信'); ?></button>
            </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- FORGOT -->
