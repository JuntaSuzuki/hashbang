<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php echo $this->Html->charset(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="shortcut icon" href="assets/img/logo-fav.png"> -->
  <title><?php echo $this->fetch('title'); ?></title>
  <?php echo $this->Html->css('/lib/perfect-scrollbar/css/perfect-scrollbar.min'); ?>
  <?php echo $this->Html->css('/lib/material-design-icons/css/material-design-iconic-font.min'); ?>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <?php echo $this->Html->css('/lib/datatables/css/dataTables.bootstrap.min'); ?>
  <?php echo $this->Html->css('/css/style'); ?>
  <?php echo $this->fetch('css'); ?>

  <?php echo $this->Html->script('/lib/jquery/jquery.min'); ?>

  <!-- PHPからJavaScriptにデータを渡す -->
  <?php echo $this->element('Common/set_js_vars'); ?>
</head>
<body>
<!-- Application Content -->
<div class="be-wrapper be-fixed-sidebar <?php echo $offcanvas; ?>">
  <?php echo $this->Session->flash(); ?>

  <?php echo $this->fetch('content'); ?>

  <?php echo $this->Html->script('/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min'); ?>
  <?php echo $this->Html->script('/js/main'); ?>
  <?php echo $this->Html->script('/js/app'); ?>
  <?php echo $this->Html->script('/lib/bootstrap/dist/js/bootstrap.min'); ?>
  <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
      App.global();
    });
  </script>
  <?php echo $this->fetch('script'); ?>
  <?php echo $this->element('sql_dump'); ?>
</div>
</body>
</html>