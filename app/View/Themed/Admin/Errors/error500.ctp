<html lang="en">
<head>
  <?php echo $this->Html->charset(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="shortcut icon" href="assets/img/logo-fav.png"> -->
  <title><?php echo $this->fetch('title'); ?></title>
  <?php echo $this->Html->css('/lib/perfect-scrollbar/css/perfect-scrollbar.min'); ?>
  <?php echo $this->Html->css('/lib/material-design-icons/css/material-design-iconic-font.min'); ?>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <?php echo $this->Html->css('/css/style'); ?>
  <?php echo $this->fetch('css'); ?>
</head>
<body class="be-splash-screen">
  <div class="be-wrapper be-error be-error-404">
    <div class="be-content">
      <div class="main-content container-fluid">
        <div class="error-container">
          <div class="error-number">500エラー</div>
          <div class="error-goback-button">
            <a href="javascript:onClick=history.back()" class="btn btn-xl btn-primary">一つ前のページに戻る</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo $this->element('sql_dump'); ?>
</div>
</body>
</html>