<?php 

App::uses('AppHelper', 'View/Helper');

class DashboardHelper extends AppHelper {

/**
 * メニュー用のactiveクラス判定用
 *
 * @param string $active 
 * @param string $expected
 * @return string HTML
 */
    public function activeMenu($active, $expected) {
        if ($active == $expected) {
            return 'class="active"';
        } else {
            return '';
        }
    }

/**
 * メニュー用のactiveクラス判定用
 *
 * @param string $active 
 * @param string $expected
 * @return string HTML
 */
    public function activeAutoSpeedSetting($active, $expected) {
        if ($active == $expected) {
            return 'active';
        } else {
            return '';
        }
    }

/**
 * ハッシュタグ用メッセージ
 *
 * @param int $count 登録ハッシュタグ数 
 * @return string HTML
 */
    public function hashtagMsg($count) {
        $format = '<small id="hashtagMsg" class="ml-20">%s</small>';

        $maxCount = Configure::read('hashtag_max_count');
        $remainCount = $maxCount - $count;

        switch ($remainCount) {
            case 15:
                return sprintf($format, $maxCount . __(' 個登録可能'));
                break;

            case 0:
                return sprintf($format, __('これ以上登録できません'));
                break;

            default:
                return sprintf($format, __('あと ') . $remainCount . __(' 個 登録可能'));
                break;
        }
    }

/**
 * コメント用メッセージ
 *
 * @param int $count 登録コメント数 
 * @return string HTML
 */
    public function commentMsg($count) {
        $format = '<small id="commentMsg" class="ml-20">%s</small>';
        $maxCount = Configure::read('comment_max_count');
        $remainCount = $maxCount - $count;

        switch ($remainCount) {
            case $maxCount:
                return sprintf($format, $maxCount . __(' 個登録可能'));
                break;

            case 0:
                return sprintf($format, __('これ以上登録できません'));
                break;

            default:
                return sprintf($format, __('あと ') . $remainCount . __(' 個 登録可能'));
                break;
        }
    }

/**
 * フィルターキーワード用メッセージ
 *
 * @param int $count 登録フィルターキーワード数 
 * @return string HTML
 */
    public function filterMsg($count) {
        $format = '<small id="filterMsg" class="ml-20">%s</small>';
        $maxCount = Configure::read('filter_max_count');
        $remainCount = $maxCount - $count;

        switch ($remainCount) {
            case $maxCount:
                return sprintf($format, $maxCount . __(' 個登録可能'));
                break;

            case 0:
                return sprintf($format, __('これ以上登録できません'));
                break;

            default:
                return sprintf($format, __('あと ') . $remainCount . __(' 個 登録可能'));
                break;
        }
    }

/**
 * エリア用メッセージ
 *
 * @param int $count 登録エリア数 
 * @return string HTML
 */
    public function locationMsg($count) {
        $format = '<small id="locationMsg" class="ml-20">%s</small>';
        $maxCount = Configure::read('location_max_count');
        $remainCount = $maxCount - $count;

        switch ($remainCount) {
            case $maxCount:
                return sprintf($format, $maxCount . __(' 個登録可能'));
                break;

            case 0:
                return sprintf($format, __('これ以上登録できません'));
                break;

            default:
                return sprintf($format, __('あと ') . $remainCount . __(' 個 登録可能'));
                break;
        }
    }

/**
 * メッセージ用メッセージ
 *
 * @param int $count 登録メッセージ数 
 * @return string HTML
 */
    public function messageMsg($count) {
        $format = '<small id="messageMsg" class="ml-20">%s</small>';
        $maxCount = Configure::read('message_max_count');
        $remainCount = $maxCount - $count;

        switch ($remainCount) {
            case $maxCount:
                return sprintf($format, $maxCount . __(' 個登録可能'));
                break;

            case 0:
                return sprintf($format, __('これ以上登録できません'));
                break;

            default:
                return sprintf($format, __('あと ') . $remainCount . __(' 個 登録可能'));
                break;
        }
    }

/**
 * 自動設定のchecked判定用
 *
 * @param string $auto 
 * @return string HTML
 */
    public function activeAutoSetting($auto) {
        if ((boolean)$auto) {
            return 'checked';
        } else {
            return '';
        }
    }

/**
 * アクティブクラス判定用
 *
 * @param string $active 
 * @param string $expected
 * @return string HTML
 */
    public function activeClass($active, $expected) {
        if ($active == $expected) {
            return 'active';
        } else {
            return '';
        }
    }

/**
 * アクティビティ用の日付フォーマット
 *
 * @param string $dateString
 * @return string formatted time
 */
    public function activityTimeFormat($dateString) {
        return date('Y/m/d H:i', strtotime($dateString));
    }

/**
 * アクティビティ用のプロフィールフォーマット
 *
 * @param array $activity
 * @return string formatted profile
 */
    public function activityProfileFormat($activity) {
        $format = '<a href="%s" target="_blank"><div class="user-pic"><img width="33" height="33" src="%s" onerror="this.src=\'%s\';"></div></a>';
        return sprintf(
            $format,
            h('https://www.instagram.com/' . $activity['username']),
            h(str_replace('http://', 'https://', $activity['profile_picture'])),
            '/img/no-image.png'
        );
    }

/**
 * アクティビティ用のラベルフォーマット
 *
 * @param array $activity
 * @return string label
 */
    public function activityLabelFormat($activity) {
        if (!is_null($activity['Activity']['mtb_hashtag_id'])) {
            return $this->activityHashtagFormat($activity['MtbHashtag']);
        }
        else if (!is_null($activity['Activity']['mtb_location_id'])) {
            return $this->activityLocationFormat($activity['MtbLocation']);
        }
    }

/**
 * アクティビティ用のハッシュタグフォーマット
 *
 * @param array $hashtag
 * @return string formatted hashtag
 */
    public function activityHashtagFormat($hashtag) {
        $format = '<span class="taglabel label-primary"># %s</span>';
        return sprintf(
            $format,
            h($hashtag['name'])
        );
    }

/**
 * アクティビティ用のエリアフォーマット
 *
 * @param array $location
 * @return string formatted location
 */
    public function activityLocationFormat($location) {
        $format = '<span class="taglabel bg-amethyst"><i class="fa fa-fw fa-map-marker"></i> %s</span>';
        return sprintf(
            $format,
            h($location['name'])
        );
    }

/**
 * アクティビティ用のタイプフォーマット
 *
 * @param array $activity
 * @return string formatted time
 */
    public function activityTypeFormat($activity) {
        $type = $activity['Activity']['type'];

        $username = '<b>' . h($activity['Activity']['username']) . '</b>' . __('さん');

        $comment = '';
        if ($type == COMMENT) {
            $comment = $activity['Comment']['comment'];
        }

        $types = array(
            LIKE => $username . __('の写真に「いいね」しました'),
            FOLLOW => $username . __('を「フォロー」しました'),
            COMMENT => $username . __('の写真に「') . $comment . __('」とコメントしました'),
            MESSAGE => $username . __('に「メッセージ」しました'),
            UNFOLLOW => $username . __('を「フォロー解除」しました'),
        );

        return $types[$type];
    }

/**
 * 本日のアクティビティ用のメディアフォーマット
 *
 * @param array $activity
 * @return string formatted media
 */
    public function activityTodayMediaFormat($activity) {
        switch ($activity['type']) {
            case LIKE: // いいね
            case COMMENT: // コメント
                $format = '<a href="%s" target="_blank"><img class="media-object br-5" src="%s" onerror="this.src=\'%s\';"></a>';
                return sprintf(
                    $format,
                    h($activity['media_url']),
                    h(str_replace('http://', 'https://', $activity['media_picture_url'])),
                    '/img/no-image.png'
                );
                break;

            case FOLLOW: // フォロー
            case MESSAGE: // メッセージ
                $format = '<a href="%s" target="_blank"><img class="media-object br-5" src="%s" onerror="this.src=\'%s\';"></a>';
                return sprintf(
                    $format,
                    h('https://www.instagram.com/' . $activity['username']),
                    h(str_replace('http://', 'https://', $activity['profile_picture'])),
                    '/img/no-image.png'
                );
                break;

            case UNFOLLOW: // フォロー解除
                return '';
                break;
        }
    }

/**
 * アクティビティ用のメディアフォーマット
 *
 * @param array $activity
 * @return string formatted media
 */
    public function activityMediaFormat($activity) {
        switch ($activity['type']) {
            case LIKE: // いいね
            case COMMENT: // コメント
                $format = '<a href="%s" target="_blank"><div class="media-pic"><img src="%s" onerror="this.src=\'%s\';"></div></a>';
                return sprintf(
                    $format,
                    h($activity['media_url']),
                    h(str_replace('http://', 'https://', $activity['media_picture_url'])),
                    '/img/no-image.png'
                );
                break;

            case FOLLOW:
            case MESSAGE:
            case UNFOLLOW:
                return '';
                break;
        }
    }

/**
 * アクティビティ用のアイコンフォーマット
 *
 * @param array $activity
 * @return string formatted icon
 */
    public function activityIconFormat($activity) {

        $format = '<aside><div class="thumb wh30" style="background-image: url(/img/%s-icon-sm.png); background-size: cover;"></div></aside>';

        $icons = array(
            LIKE => sprintf($format, 'like'),
            FOLLOW => sprintf($format, 'follow'),
            COMMENT => sprintf($format, 'comment'),
            MESSAGE => sprintf($format, 'message'),
            UNFOLLOW => sprintf($format, 'unfollow'),
        );

        return $icons[$activity['type']];
    }

/**
 * ステータス用の数値フォーマット
 *
 * @param array $count
 * @return string formatted count
 */
    public function statusCountFormat($count) {
        $formattedCount = '';
        switch (true) {
            case $count > 1000000:
                return sprintf('%.1f', $count / 1000000) . __('百万');
                break;

            case $count > 100000:
                return sprintf('%.1f', $count / 10000) . __('万');
                break;
            
            default:
                return number_format($count);
                break;
        }
        //return number_format($count);
    }

/**
 * ハッシュタグ表示用
 *
 * @param array $hashtags
 * @return string formatted html
 */
    public function hashtagList($hashtags) {

        if (count($hashtags) == 0) {
            return __('ハッシュタグがまだ登録されていません');
        }

        $html = '<ul>';
        $format = '<li class="hashtag label label-info" data-id="%s">%s <i class="fa fa-times" aria-hidden="true"></i></li>';
        foreach ($hashtags as $hashtag) {
            $html .= sprintf(
                $format,
                h($hashtag['Hashtag']['id']),
                h($hashtag['MtbHashtag']['name'])
            );
        }

        $html .= '</ul>';
        return $html;
    }

/**
 * コメント表示用
 *
 * @param array $comments
 * @return string formatted html
 */
    public function commentList($comments) {

        if (count($comments) == 0) {
            return __('コメントがまだ登録されていません');
        }

        $html = '<ul>';
        $format = '<li class="comment label label-success" data-id="%s">%s <i class="fa fa-times" aria-hidden="true"></i></li>';
        foreach ($comments as $comment) {
            $html .= sprintf(
                $format,
                h($comment['Comment']['id']),
                h($comment['Comment']['comment'])
            );
        }

        $html .= '</ul>';
        return $html;
    }

/**
 * フィルター表示用
 *
 * @param array $filters
 * @return string formatted html
 */
    public function filterList($filters) {

        if (count($filters) == 0) {
            return __('キーワードがまだ登録されていません');
        }

        $html = '<ul>';
        $format = '<li class="filter label label-warning" data-id="%s">%s <i class="fa fa-times" aria-hidden="true"></i></li>';
        foreach ($filters as $filter) {
            $html .= sprintf(
                $format,
                h($filter['Filter']['id']),
                h($filter['Filter']['keyword'])
            );
        }

        $html .= '</ul>';
        return $html;
    }

/**
 * エリア表示用
 *
 * @param array $locations
 * @return string formatted html
 */
    public function locationList($locations) {

        if (count($locations) == 0) {
            return __('エリアがまだ登録されていません');
        }

        $html = '<ul>';
        $format = '<li class="location location-del label label-primary" data-id="%s">%s <i class="fa fa-times" aria-hidden="true"></i></li>';
        foreach ($locations as $location) {
            $html .= sprintf(
                $format,
                h($location['Location']['id']),
                h($location['MtbLocation']['name'])
            );
        }

        $html .= '</ul>';
        return $html;
    }

/**
 * メッセージ表示用
 *
 * @param array $messages
 * @return string formatted html
 */
    public function messageList($messages) {

        if (count($messages) == 0) {
            return __('メッセージがまだ登録されていません');
        }

        $html = '';
        $format = '
                <div class="clearfix message-item" data-id="%s" data-message="%s">
                    <div class="message-text">%s</div>
                    <div class="message-btn">
                        <button type="button" class="btn btn-rounded-20 btn-sm mr-5 message-edit btn-blue">%s</button><!--
                        --><button type="button" class="btn btn-rounded-20 btn-sm mr-5 message-delete btn-danger">%s</button>
                    </div>
                </div>';
        foreach ($messages as $message) {
            $html .= sprintf(
                $format,
                h($message['Message']['id']),
                h($message['Message']['message']),
                mb_strimwidth(h($message['Message']['message']), 0, 50, '...'),
                __('編集'),
                __('削除')
            );
        }

        return $html;
    }

/**
 * ホワイトリストユーザー表示用
 *
 * @param array $whitelists
 * @return string formatted html
 */
    public function whitelistList($whitelists) {

        if (count($whitelists) == 0) {
            return __('<p id="whitelist_msg">フォローユーザーはまだ登録されていません</p>');
        }

        $html = __('<p id="whitelist_msg">クリックするとホワイトリストから解除されます。</p>');

        $html .= '<ul>';
        $format = '
                <li class="whitelist whitelist-labeldel label" data-id="%s">
                    <img class="media-object br-5" src="%s" onerror="this.src=\'%s\';">
                    <span class="pr-10">%s</span>
                    <i class="fa fa-times" aria-hidden="true"></i>
                </li>';

        foreach ($whitelists as $whitelist) {
            $html .= sprintf(
                $format,
                h($whitelist['Activity']['id']),
                h(str_replace('http://', 'https://', $whitelist['Activity']['profile_picture'])),
                '/img/no-image.png',
                h($whitelist['Activity']['username'])
            );
        }

        $html .= '</ul>';
        return $html;
    }

/**
 * フォロ中ユーザー用のメディアフォーマット
 *
 * @param array $following
 * @return string formatted media
 */
    public function followingMediaFormat($following) {
        $format = '<a href="%s" target="_blank"><img class="media-object br-5" src="%s" onerror="this.src=\'%s\';" width="%s"></a>';
        return sprintf(
            $format,
            h('https://www.instagram.com/' . $following['username']),
            h(str_replace('http://', 'https://', $following['profile_picture'])),
            '/img/no-image.png',
            '50px'
        );
    }

/**
 * フォロ中ユーザー用のボタンフォーマット
 *
 * @param array $following
 * @return string formatted media
 */
    public function followingButtonFormat($following) {
        $format = '
            <button type="button" class="btn btn-rounded-20 btn-block btn-sm mr-5 %s" data-id="%s">
                 %s
            </button>';

        if ($following['whitelist_status']) {
            $className = 'whitelist-del btn-default';
            $text = __('ホワイトリストに追加中');
        } else {
            $className = 'whitelist-add btn-blue';
            $text = __('<i class="fa fa-plus" aria="hidden"></i> ホワイトリストに追加する');
        }

        return sprintf(
            $format,
            $className,
            h($following['id']),
            $text
        );
    }

/**
 * ハッシュタグ解析 獲得率掲載
 *
 * @param array $analy
 * @return string
 */
    public function calcObtainRate($analy) {    
        if ($analy['follow_count'] == 0) {
            return '0.0';
        } else {
            $obtainRate = sprintf(
                '%.1f',
                ($analy['followback_count'] / $analy['follow_count'] * 100)
            );
            return h($obtainRate);
        }
    }

/**
 * 課金ステータス
 *
 * @param array $payment
 * @return string
 */
    public function paymentStatus($payment) {
        if ($payment['Payment']['payment_status'] && $payment['Payment']['status']) {
            return 'class="payment-done"';
        }

        if ($payment['Payment']['charge_date'] < date('Y-m-d')) {
            return 'class="payment-pending"';
        } else {
            return '';
        }
    }

/**
 * 有効チェック
 *
 * @param array $user
 * @return string
 */
    public function isEnabled($user) {
        if ($this->isSlowStartPeriod($user)) {
            return 'disabled="disabled"';
        } else {
            return '';
        }
    }

/**
 * スロースタート期間チェック
 *
 * @param array $user
 * @return boolean
 */
    public function isSlowStartPeriod($user) {
        $addDate = date('Y/m/d', strtotime($user['created']));
        $timestammp = strtotime($addDate);
        $now = strtotime('now');
         
        $dateInterval = round(($now - $timestammp) / (60*60*24));

        return $dateInterval <= SLOW_START_PERIOD;
    }

/**
 * API制限
 *
 * @param array $instagramAccount
 * @return string
 */
    public function isEnabledApi($instagramAccount) {
        $htmlFormat = '
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-big alert-lightred alert-dismissable fade in br-5">
                    %s
                </div>
              </div>
            </div>
        ';

        // API制限チェック
        $apis = [
            ['like', 'いいね'],
            ['follow', 'フォロー'],
            ['comment', 'コメント'],
            ['message', 'メッセージ'],
            ['unfollow', 'フォロー解除']
        ];

        $message = '';
        $messageFormat = '
            <div class="api-message">
                <strong>%s</strong>
            </div>
        ';

        foreach ($apis as $api) {
            if ($instagramAccount['Instagram'][$api[0] . '_api_status'] === false) {
                $message .= sprintf(
                    $messageFormat,
                    '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' . $api[1] . '機能がインスタグラム制限に引っかかったため、現在' . $api[1] . '機能のアクティビティを停止しています。'
                );
            }
        }

        $html = '';
        if (!empty($message)) {
            $html = sprintf(
                $htmlFormat,
                $message
            );
        }

        return $html;
    }

}
