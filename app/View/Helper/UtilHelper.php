<?php 

App::uses('AppHelper', 'View/Helper');

class UtilHelper extends AppHelper {

/**
 * activeクラス判定用
 *
 * @param string $active 
 * @param string $expected
 * @param boolean $classAttr
 * @return string HTML
 */
	public function activeClass($active, $expected, $classAttr = true) {
		if ($active == $expected) {
			return $classAttr ? 'class="active"' : 'active';
		} else {
			return '';
		}
	}

}
