
<?php echo $this->element('/Landing/loader'); ?>

<?php echo $this->element('/Pages/header'); ?>

<div class="pages-legal layouts-pages">
  <header class="header">
    <div class="container">
      <div class="row inner">
        <div class="titlebox">
          <h1>運営会社</h1>
          <h2>Corporate</h2>
        </div>
      </div>
      <?php echo $this->element('/Pages/menu'); ?>
    </div>
  </header>
</div>


<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <main role="main">
        <section class="corporate">
          <table>
            <tbody>
              <tr>
                <th><?php echo __('会社名'); ?></th>
                <td><?php echo __('株式会社N・B・T'); ?></td>
              </tr>
              <tr>
                <th><?php echo __('事業内容'); ?></th>
                <td>
                  <p><?php echo __('Webサービスの企画・開発・運営'); ?></p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('お問い合わせ先'); ?></th>
                <td>
                  <p><?php echo __('<a href="/pages/contact">お問い合わせフォーム</a>をご利用ください'); ?></p>
                </td>
              </tr>
            </tbody>
          </table>
        </section>
      </main>

    </div>
  </div>
</div>

<?php echo $this->element('/Landing/footer'); ?>

<?php echo $this->element('/Landing/copyright'); ?>
