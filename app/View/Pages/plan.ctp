
<?php echo $this->element('/Landing/loader'); ?>

<?php echo $this->element('/Pages/header'); ?>

<div class="pages-legal layouts-pages">
  <header class="header">
    <div class="container">
      <div class="row inner">
        <div class="titlebox">
          <h1><?php echo __('料金プラン'); ?></h1>
          <h2><?php echo __('各プランの機能と価格を比較検討できます'); ?></h2>
        </div>
      </div>
    </div>
  </header>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <main role="main">
        <section class="plan">
          <div class="container-fluid pl00 pr00">
            <h2 class="bang-section__title"><?php echo __('プラン'); ?></h2>
            <div class="row bang-compare-plan">
              <div class="col-sm-6 col-md-4">
                <div class="bang-compare-plan__title">
                  <span class="bang-compare-plan__plan"><?php echo __('ベーシックプラン'); ?></span><br>
                  <strong><?php echo __('2,000 <small>円</small>'); ?></strong>
                </div>
                <p><?php echo __('まずはお試しという方はこちら'); ?></p>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="bang-compare-plan__title">
                  <span class="bang-compare-plan__plan"><?php echo __('メッセージプラン'); ?></span><br>
                  <strong><?php echo __('2,000 <small>円</small>'); ?></strong>
                </div>
                <p><?php echo __('メッセージを自動化させたい！'); ?></p>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="bang-compare-plan__title">
                  <span class="bang-compare-plan__plan"><?php echo __('セットプラン'); ?></span><br>
                  <strong><?php echo __('3,500 <small>円</small>〜'); ?></strong>
                </div>
                <p><?php echo __('#BANGの全機能を使いたいはこちら'); ?></p>
              </div>
            </div>

            <h2 class="bang-section__title"><?php echo __('プラン比較表'); ?></h2>
            <div class="bang-table-scroll">
              <table class="bang-table">
                <thead>
                  <tr>
                    <th></th>
                    <th scope="col">
                      <div >
                        <div class="compare-table-features__title">
                          <span class="visible-lg-block"><?php echo __('ベーシックプラン'); ?></span>
                          <span class="visible-xs-block visible-sm-block visible-md-block"><?php echo __('ベーシック<br>プラン'); ?></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col">
                      <div class="compare-table-features--lite">
                        <div class="compare-table-features__title">
                          <span class="visible-lg-block"><?php echo __('メッセージプラン'); ?></span>
                          <span class="visible-xs-block visible-sm-block visible-md-block"><?php echo __('メッセージ<br>プラン'); ?></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col">
                      <div class="compare-table-features--standard">
                        <div class="compare-table-features__title"><?php echo __('セットプラン'); ?></div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      <img src="/img/like-icon.png" width="50px" alt="<?php echo __('自動いいね'); ?>">
                      <?php echo __('自動いいね'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/follow-icon.png" width="50px" alt="<?php echo __('自動フォロー'); ?>">
                      <?php echo __('自動フォロー'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/comment-icon.png" width="50px" alt="<?php echo __('自動コメント'); ?>">
                      <?php echo __('自動コメント'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/area-icon.png" width="50px" alt="<?php echo __('エリア選択'); ?>">
                      <?php echo __('エリア選択'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/adjust-icon.png" width="50px" alt="<?php echo __('スピード調整機能'); ?>">
                      <?php echo __('スピード調整機能'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/message-icon.png" width="50px" alt="<?php echo __('自動メッセージ'); ?>">
                      <?php echo __('自動メッセージ'); ?>
                    </th>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/unfollow-icon.png" width="50px" alt="<?php echo __('自動フォロー解除'); ?>">
                      <?php echo __('自動フォロー解除'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/filter-icon.png" width="50px" alt="<?php echo __('フィルター機能'); ?>">
                      <?php echo __('フィルター機能'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/analytics-icon.png" width="50px" alt="<?php echo __('タグ解析'); ?>">
                      <?php echo __('タグ解析'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/actionlog-icon.png" width="50px" alt="<?php echo __('アクティビティログ'); ?>">
                      <?php echo __('アクティビティログ'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/report-icon.png" width="50px" alt="<?php echo __('レポート機能'); ?>">
                      <?php echo __('レポート機能'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      -
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <img src="/img/account-bulk-icon.png" width="50px" alt="<?php echo __('アカウント一括管理'); ?>">
                      <?php echo __('アカウント一括管理'); ?>
                    </th>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                    <td class="hash-table__checked">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <h2 class="bang-section__title"><?php echo __('料金比較表'); ?></h2>
            <table class="bang-table mb15">
              <thead>
                <tr>
                  <th></th>
                  <th scope="col">
                    <div >
                      <div class="compare-table-features__title">
                        <span class="visible-lg-block"><?php echo __('ベーシックプラン'); ?></span>
                        <span class="visible-xs-block visible-sm-block visible-md-block"><?php echo __('ベーシック<br>プラン'); ?></span>
                      </div>
                    </div>
                  </th>
                  <th scope="col">
                    <div class="compare-table-features--lite">
                      <div class="compare-table-features__title">
                        <span class="visible-lg-block"><?php echo __('メッセージプラン'); ?></span>
                        <span class="visible-xs-block visible-sm-block visible-md-block"><?php echo __('メッセージ<br>プラン'); ?></span>
                      </div>
                    </div>
                  </th>
                  <th scope="col">
                    <div class="compare-table-features--standard">
                      <div class="compare-table-features__title"><?php echo __('セットプラン'); ?></div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">
                    <?php echo __('1ヶ月プラン'); ?>
                  </th>
                  <td>
                    <?php echo __('2,000<span>円</span>'); ?>
                  </td>
                  <td>
                    <?php echo __('2,000<span>円</span>'); ?>
                  </td>
                  <td>
                    <?php echo __('3,500<span>円</span>'); ?>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <?php echo __('3ヶ月プラン'); ?>
                  </th>
                  <td>-</td>
                  <td>-</td>
                  <td>
                    <div class="hash-table__note"><?php echo __('長期割でお得！'); ?></div>
                    <b><?php echo __('9,000<span>円</span>'); ?></b>
                    <p><?php echo __('(1ヶ月当たり3,000<span>円</span>)'); ?></p>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <?php echo __('6ヶ月プラン'); ?>
                  </th>
                  <td>-</td>
                  <td>-</td>
                  <td>
                    <div class="hash-table__note"><?php echo __('長期割でお得！'); ?></div>
                    <b><?php echo __('15,000<span>円</span>'); ?></b>
                    <p><?php echo __('(1ヶ月当たり2,500<span>円</span>)'); ?></p>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <?php echo __('12ヶ月プラン'); ?>
                  </th>
                  <td>-</td>
                  <td>-</td>
                  <td>
                    <div class="hash-table__note"><?php echo __('長期割でお得！'); ?></div>
                    <b><?php echo __('24,000<span>円</span>'); ?></b>
                    <p><?php echo __('(1ヶ月当たり2,000<span>円</span>)'); ?></p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p class="mb50">※ 表示価格は税込です。</p>
          </div>
        </section>

        <div class="text-center mb50">
          <a href="/users/sign_up" class="btn btn-custom btn-lg"><?php echo __('7日間 全機能無料で使ってみる!'); ?></a>
        </div>
      </main>
    </div>
  </div>
</div>

<?php echo $this->element('/Landing/footer'); ?>

<?php echo $this->element('/Landing/copyright'); ?>
