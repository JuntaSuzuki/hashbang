
<?php echo $this->element('/Landing/loader'); ?>

<div class="pages-legal layouts-pages">
  <header class="header">

    <div class="container">
      <div class="row inner">
        <div class="titlebox">
          <h1>よくある質問</h1>
          <h2>Faq</h2>
        </div>
      </div>
      <div id="js-menu-fixed" class="menu">
        <div class="row">
          <ul class="clearfix">
            <li>
              <a href="/pages/faq">よくある質問</a>
            </li>
            <li>
              <a href="/pages/term">利用規約</a>
            </li>
            <li>
              <a href="/pages/privacy">プライバシーポリシー</a>
            </li>
            <li class="selected">
              <a href="/pages/legal">特定商取引法に基づく表記</a>
            </li>
            <li class="rfloat">
              <a href="/pages/contact">
                <i class="fa fa-paper-plane"></i>お問い合わせ
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </header>
</div>


<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <main role="main">
        <section class="term">
        </section>
      </main>
    </div>
  </div>
</div>

<?php echo $this->element('/Landing/footer'); ?>

<?php echo $this->element('/Landing/copyright'); ?>
