
<?php echo $this->element('/Landing/loader'); ?>

<?php echo $this->element('/Pages/header'); ?>

<div class="pages-legal layouts-pages">
  <header class="header">
    <div class="container">
      <div class="row inner">
        <div class="titlebox">
          <h1>特定商取引法に基づく表記</h1>
          <h2>Legal</h2>
        </div>
      </div>
      <?php echo $this->element('/Pages/menu'); ?>
    </div>
  </header>
</div>


<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1 col-sm-12">
      <main role="main">
        <section class="legal">
          <table>
            <tbody>
              <tr>
                <th><?php echo __('事業者'); ?></th>
                <td><?php echo __('株式会社N・B・T'); ?></td>
              </tr>
              <tr>
                <th><?php echo __('運営責任者'); ?></th>
                <td><?php echo __('曽我義道'); ?></td>
              </tr>
              <tr>
                <th><?php echo __('サービスの対価'); ?></th>
                <td>
                  <p><?php echo __('各サービスのご購入ページにて表示する価格'); ?></p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('支払方法'); ?></th>
                <td>
                  <p><?php echo __('クレジットカード（VISA、MasterCard、JCB、Amex、Dinners、Discover）'); ?></p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('支払時期'); ?></th>
                <td>
                  <p><?php echo __('&lt;月額プラン&gt;<br>
初回購入時、翌月以降契約終了日の翌日<br>
例：現在のご契約終了日が2017/03/15の場合、2017/03/16に自動的に次回ご契約分の決済・更新が完了します。<br><br>

&lt;まとめ払い&gt;<br>
3か月プラン：初回購入時、翌月以降契約終了日の翌日<br>
6か月プラン：初回購入時、翌月以降契約終了日の翌日<br>
12か月プラン：初回購入時、翌月以降契約終了日の翌日<br><br>
ご請求日は決済事業者により異なります。'); ?>
</p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('サービスの提供時期'); ?></th>
                <td>
                  <p><?php echo __('代金決済手続きの完了確認後直ちに'); ?></p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('返金・キャンセル'); ?></th>
                <td>
                  <p><?php echo __('本サイトで提供するサービスについては、購入手続き完了後のキャンセルをお受けいたしません。'); ?></p>
                </td>
              </tr>
              <tr>
                <th><?php echo __('お問い合わせ'); ?></th>
                <td>
                  <p>support<i class="fa fa-at" aria-hidden="true"></i>hashbang.jp</p>
                </td>
              </tr>
            </tbody>
          </table>
        </section>
      </main>

    </div>
  </div>
</div>

<?php echo $this->element('/Landing/footer'); ?>

<?php echo $this->element('/Landing/copyright'); ?>
