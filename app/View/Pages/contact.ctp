
<?php echo $this->Html->script('contact', array("inline" => false)); ?>

<?php echo $this->element('/Landing/loader'); ?>

<?php echo $this->element('/Pages/header'); ?>

<div class="pages-legal layouts-pages">
  <header class="header">
    <div class="container">
      <div class="row inner">
        <div class="titlebox">
          <h1><?php echo __('お問い合わせ'); ?></h1>
          <h2>Contact</h2>
        </div>
      </div>
      <?php echo $this->element('/Pages/menu'); ?>
    </div>
  </header>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <main role="main">
        <section class="contact">
          <?php 
            echo $this->Form->create('Contact', [
              'class' => 'contact-form margin-t-20',
              'id' => 'contactForm',
              'novalidate' => true
            ]);
          ?>
            <div class="row">
              <div class="col-sm-12">
                <?php 
                  echo $this->Form->input('name', [
                    'label' => __('お名前'),
                    'class' => 'form-control',
                    'placeholder' => __('お名前を入力してください')
                  ]);
                ?>
              </div>
              <div class="col-sm-12">
                <?php 
                  echo $this->Form->input('email', [
                    'label' => __('メールアドレス'),
                    'class' => 'form-control',
                    'placeholder' => __('メールアドレスを入力してください')
                  ]);
                ?>
              </div>
              <div class="col-sm-12">
                <?php 
                  echo $this->Form->input('subject', [
                    'label' => __('お問い合わせタイトル'),
                    'class' => 'form-control',
                    'placeholder' => __('お問い合わせタイトルを入力してください')
                  ]);
                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <label for="ContactContent"><?php echo __('お問い合わせ内容'); ?></label>
                <?php 
                  echo $this->Form->textarea('content', [
                    'class' => 'form-control',
                    'rows' => 6,
                    'placeholder' => __('お問い合わせ内容を入力してください')
                  ]);
                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-center">
                <button type="button" class="btn btn-custom btn-lg" id="contactBtn"><?php echo __('送信'); ?></button>
              </div>
            </div>
          <?php echo $this->Form->end(); ?>

          <div class="line-box">
            <a href="http://line.me/ti/p/%40mfy6882w"><img height="36" border="0" alt="'stats.label.addfriend' (MISSING TRANSLATION)" src="http://biz.line.naver.jp/line_business/img/btn/addfriends_ja.png"></a>
            <div><?php echo __('お問い合わせはLINEでも受け付けています。友達追加の上ご連絡ください。'); ?></div>
          </div>
        </section>
      </main>
    </div>
  </div>
</div>

<?php echo $this->element('/Landing/footer'); ?>

<?php echo $this->element('/Landing/copyright'); ?>
