
<!-- Banner -->
<?php echo $this->element('/Public/banner'); ?>

<!-- Header -->


<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img class="thumbnail alignnone size-full" src="http://portfoliotheme.org/fusion/wp-content/uploads/sites/2/2014/12/app-screens4.png" alt="App screen 2">
    </div>
    <div class="col-md-6">
      <div class="text-content align-right">
        <div class="heading">
          <h3 class="title">Seamless sync across all devices</h3>
        </div>
        <p>Lorem epsum dollar eveniet nec hendrerit, aliquid inceptos aliquip iure magnam nam donec! Varius, venenatis laudantium ad vulputate architecto, non sunt dicta ridiculus numquam sociis leo, duis vero, faucibus non sunt dicta ridiculus numquam sociis leo duis vero, faucibus lorem epsum dollar.</p>
        <p class="platform-icons">
          <a href="#" title="iOS Download">
            <i class="icon-apple"></i>
          </a>
          <a href="#" title="Android Download">
            <i class="icon-android"></i>
          </a>
          <a href="#" title="Windows Download">
            <i class="icon-windows"></i>
          </a>
        </p>
      </div>
    </div>
  </div>
</div>

<!-- Feature -->

<!-- Subscription -->
<?php echo $this->element('/Public/subscription'); ?>

<!-- Faq -->
<?php echo $this->element('/Public/faq'); ?>

<!-- Footer -->
<?php echo $this->element('/Public/footer'); ?>

