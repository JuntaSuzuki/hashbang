<?php

require __DIR__ . '/../../Vendor/autoload.php';

// 必要クラスの読み込み
use Facebook\WebDriver;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\Remote;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;


class Batch {

/**
 * プロパティ
 */
	private $selector            = "";      // CSSセレクター

	private $userId              = "";      // 自動実行ユーザー番号

	private $url                 = "";      // 投稿URL

	private $host                = "";      // ホスト情報

	private $driver              = null;    // ドライバ情報

	private $username            = "";      // ログインユーザー名

	private $data                = array(); // 自動アクション情報

	private $follows             = array(); // 登録済みコメント配列

	private $comments            = array(); // 登録済みコメント配列

	private $media               = array(); // ハッシュタグを持つ投稿配列

	private $mediaUrl            = "";      // 自動アクション対象の投稿のURL

	private $mediaUsername       = "";      // 自動アクション対象の投稿のユーザー名

	private $mediaProfilePicture = "";      // 自動アクション対象の投稿のユーザープロフィール写真

	private $actions             = array(); // 実行したアクション配列

/**
 * セッター
 *
 * @param array $data 自動アクション用データ
 * @return void
 */
	public function setData($data) {
		$this->data = $data;
	}

/**
 * コンストラクタ
 * host capabilitiesの設定
 *
 * @param array $option 
 * @return void
 */
	public function __construct($option = array()) {

		$this->url = $option['url'];

		$this->host = $option['host'];

		if ($option['capabilities'] == 'chrome') {
			$this->capabilities = DesiredCapabilities::chrome();
		}

		if ($option['capabilities'] == 'phantomjs') {
			$this->capabilities = array(
				WebDriverCapabilityType::BROWSER_NAME => 'phantomjs',
				'phantomjs.page.settings.userAgent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:25.0) Gecko/20100101 Firefox/25.0',
				// WebDriverCapabilityType::PROXY => array(
				// 	'proxyType' => 'manual',
				// 	'httpProxy' => '124.88.67.22:81',
				// 	'sslProxy' => '124.88.67.22:81',
				// 	'ftpProxy' => '124.88.67.22:81',
				// )
			);
		}
	}

/**
 * RemoteWebDriverの設定
 *
 * @return void
 */
	public function create() {
		if (empty($this->host) || empty($this->capabilities)) {
			throw new Exception("Error Batch : RemoteWebDriver create");
		}
		$this->driver = RemoteWebDriver::create($this->host, $this->capabilities, 60 * 1000, 60 * 1000);
	}

/**
 * seleniumブラウザを閉じる
 *
 * @return void
 */
	public function quit() {
		$this->driver->quit();
	}

/**
 * CSSセレクター情報を設定する
 *
 * @return void
 */
	public function setSelector() {
		$urlFormat = "%s/api/selector";
		$url = sprintf($urlFormat, $this->url);
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERPWD => "hashbang:hashbang"
		));
		$res = curl_exec($ch);
		curl_close($ch);

		$this->selector = json_decode($res, true);
	}

/**
 * バッチユーザー情報を取得し、プロパティに設定する
 *
 * @param int $userId ユーザー番号
 * @return string JSONデータ
 */
	public function loadBatchJSON($userId) {
		return $this->loadBatchData('batchJson', $userId);
	}


/**
 * フォロー情報を取得し、プロパティに設定する
 *
 * @param int $userId ユーザー番号
 * @return void
 */
	public function loadBatchFollows($userId = null) {
		$userId = empty($userId) ? $this->data['id'] : $userId;
		$follows = $this->loadBatchData('follow', $userId);
		$this->follows = $follows['data'];
	}

/**
 * コメント情報を取得し、プロパティに設定する
 *
 * @param int $userId ユーザー番号
 * @return void
 */
	public function loadBatchComments($userId = null) {
		$userId = empty($userId) ? $this->data['id'] : $userId;
		$comments = $this->loadBatchData('comment', $userId);
		$this->comments = $comments['data'];
	}

/**
 * 指定したアクションのユーザー番号のデータをcurlで取得する
 *
 * @param string $action アクション
 * @param int $userId ユーザー番号
 * @return array 配列情報
 */
	public function loadBatchData($action, $userId) {
		$urlFormat = "%s/api/%s/?user_id=%s";
		$url = sprintf($urlFormat, $this->url, $action, $userId);
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERPWD => "hashbang:hashbang"
		));
		$res = curl_exec($ch);
		curl_close($ch);

		return json_decode($res, true);
	}

/**
 * インスタグラムへログインする
 *
 * @param string $username ユーザー名
 * @param string $password パスワード
 * @return void
 */
	public function login($username, $password, $isUseCookie = true) {
		try {
			$isLogin = false;

			if ($isUseCookie) {
				// クッキー情報格納ディレクトリ
				$dir = __DIR__ . "/cookie";
				if (!file_exists($dir)) {
					mkdir($dir, 0705);
				}

				// クッキー情報がある場合
				$cookieFile = $dir . "/" . $username . ".cookie";
				$sessionid = "";
				if (file_exists($cookieFile)) {
					$data = file_get_contents($cookieFile);
					$data = unserialize($data);

					// sessionidの検索
					foreach ($data as $value) {
						if ($value['name'] == 'sessionid') {
							$sessionid = $value['value'];
							break;
						}
					}
				}

				// sessionidがある場合
				if (!empty($sessionid)) {
					$cookie = array(
						'name' => 'sessionid',
						'value' => $sessionid,
						'domain' => "www.instagram.com"
					);
					try {
						$this->driver->manage()->addCookie($cookie);
					} catch (Exception $e) {
					}

					// セッションが有効であるか確認
					$this->driver->get('https://www.instagram.com/');
					if ($this->wait($this->selector['loginCheck'], 5)) {
						$isLogin = true;
						//echo "Cookieが有効です\n";
					}
				}
			}

			// クッキー情報がない場合
			if (!$isLogin) {
				$this->driver->get('https://www.instagram.com/');

				// ログインフォームを表示
				if (!$this->wait($this->selector['loginForm'], 10)) {
					throw new Exception("ログインフォームを表示に失敗しました。class名を確認してください。\n");
				}
				$this->driver->findElement( WebDriverBy::cssSelector($this->selector['loginForm']) )->click();

				// メールアドレス入力
				$this->driver->wait(10, 500)->until(
					WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::NAME('username'))
				);
				$this->driver->findElement(
					WebDriverBy::NAME('username')
				)->sendKeys($username);

				// パスワード入力
				$this->driver->wait(10, 500)->until(
					WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::NAME('password'))
				);
				$this->driver->findElement(
					WebDriverBy::NAME('password')
				)->sendKeys($password);

				// ログインボタンをクリック
				if (!$this->wait($this->selector['loginButton'], 10)) {
					throw new Exception("ログインボタンの取得に失敗しました。class名を確認してください。\n");
				}
				$this->driver->findElement(
					WebDriverBy::cssSelector($this->selector['loginButton'])
				)->click();

				// ログイン終了確認
				$this->wait($this->selector['loginCheck'], 20);

				// クッキー情報を保存
				if ($isUseCookie) {
					$cookie = $this->driver->manage()->getCookies();
					file_put_contents($cookieFile, serialize($cookie));
				}
				//echo "Cookieを保存しました\n";
			}

			// ログインユーザー名を設定
			$this->username = $username;

		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
			throw new Exception("Error Batch : Instagram Login\n");
		}
	}

/**
 * ハッシュタグ名の投稿を検索し、$mediaプロパティに設定
 *
 * @param string $hashtag ハッシュタグ名
 * @return void
 */
	public function searchHashtagMedia($hashtag) {
		try {
			// 対象のハッシュタグのページへ遷移
			$this->driver->get('https://www.instagram.com/explore/tags/' . urlencode($hashtag) . '/');

			// ページが表示されるまで待つ
			$this->wait($this->selector['hashtagResult'], 10);

			// HTMLの整形
			$html = $this->driver->getPageSource();
			$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

			// 投稿を取得
			preg_match_all($this->selector['hashtagPostPattern'], $html, $posts);

			if (isset($posts[0])) {
				$this->media = array();
				foreach ($posts[0] as $post) {
					preg_match($this->selector['hashtagPostDetailPattern'], $post, $match);
					if (!isset($match[0])) {
						continue;
					}

					// フィルターチェック、キーワードを含む投稿はスキップ
					$filterFlag = false;
					foreach ($this->data['filter'] as $filter) {
						if (strpos($match[2], $filter) !== false) {
							$filterFlag = true;
							break;
						}
					}
					if ($filterFlag) {
						continue;
					}

					$this->media[] = array(
						'url' => 'https://www.instagram.com' . $match[1],
						'comment' => $match[2],
						'image' => $match[4]
					);
				}
			}

		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
			throw new Exception("Error Batch : searchHashtagMedia method");
		}
	}

/**
 * 自動アクション処理
 *
 * @param int $userId ユーザー番号
 * @param int $mtbHashtagId ハッシュタグマスタ番号
 * @param void
 */
	public function autoAction($userId, $mtbHashtagId) {
		try {
			// 投稿の存在確認
			if (empty($this->media)) {
				throw new Exception("Error Batch : autoAction method\n");
			}

			// 投稿をシャッフルする
			shuffle($this->media);

			// アクティビティタイプ
			$types = array(
				'like'     => 1, 'follow'   => 2, 'comment'  => 3,
				'unfollow' => 4, 'message'  => 5
			);

			// 投稿URLの整形
			preg_match('/(.*)\?tagged=.*/ism', $this->media[0]['url'], $link);
			$this->mediaUrl = isset($link[1]) ? $link[1] : $this->media[0]['url'];

			// 投稿ページに遷移
			$this->driver->get($this->mediaUrl);

			// ユーザー名の取得
			$this->wait($this->selector['mediaUsername'], 5);
			$this->mediaUsername = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['mediaUsername']) )->getText();

			// ユーザー写真の取得
			$this->wait($this->selector['mediaProfilePicture'], 5);
			$this->mediaProfilePicture = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['mediaProfilePicture']) )->getAttribute('src');

			// フォロー情報の読み込み
			$this->loadBatchFollows();

			// いいね実行
			$this->like();

			// フォロー実行
			$this->follow();

			// コメント実行
			$this->comment();

			// アクティビティの登録
			foreach ($this->actions as $action) {
				$postData = array(
					'user_id'         => $userId,
					'mtb_hashtag_id'  => $mtbHashtagId,
					'type'            => $types[$action],
					'username'        => $this->mediaUsername,
					'profile_picture' => $this->mediaProfilePicture,
					'link'            => $this->mediaUrl,
					'media_picture'   => $this->media[0]['image'],
					'action_date'     => date("Y-m-d H:i:s")
				);
				$this->curlPost($this->url . "api/activity", $postData);
				sleep(1);
			}

		} catch (Exception $e) {
			// 想定外の例外
			throw new Exception($e->getMessage() . "\n" . $e->getTraceAsString());
		}
	}

/**
 * 表示中の投稿にいいねする
 * いいねの上限回数：一日最大 500〜900 回
 *
 * @param void
 */
	public function like() {
		try {
			// 自動設定の確認
			if (!(bool)$this->data['setting']['auto_like']) {
				echo "Error Batch : No Auto Like Setting\n";
				return;
			}

			// すでにフォローしているユーザーだった場合、いいねをしない
			if (in_array($this->mediaUsername, $this->follows)) {
				echo $this->mediaUsername . "さんはすでにフォローしているので、いいねをしません\n";
				return;
			}

			// いいね数の取得　いいねが少ない場合は件数が取れないので０件にする
			try {
				$this->wait($this->selector['likeCount'], 5);
				$likeCount = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['likeCount']) )->getText();
			} catch (Exception $e) {
				$likeCount = 0;
			}

			// いいね数のチェック
			if ($likeCount < $this->data['setting']['min_like_count'] 
				|| $likeCount > $this->data['setting']['max_like_count']) {
				echo $likeCount . "件は自動いいねする投稿のいいね数の範囲外です\n";
				return;
			}

			// いいねボタン取得
			$this->wait($this->selector['likeButton'], 5);
			$like = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['likeButton']) );
			$likeStatus = $like->getAttribute('class');

			// すでにいいねしている投稿だった場合
			$likeCheck = str_replace(array('.', '#'), '', $this->selector['likeComplete']);
			if (strpos($likeStatus, $likeCheck) !== false) {
				echo $this->mediaUsername . "さんのこの投稿にすでにいいねしています\n";
				return;
			}

			// いいね実行
			$like->click();

			// いいねが完了するまで待つ（10秒間）
			$this->wait($this->selector['likeComplete'], 10);

			$this->actions[] = 'like';

			echo $this->data['username'] . "さんが" . $this->mediaUsername . "さんの写真にいいねしました\n";

			sleep(3);
		} catch (Exception $e) {
			throw new Exception($e->getMessage() . "\n" . $e->getTraceAsString());
		}
	}

/**
 * 表示中の投稿のユーザーをフォローする
 * - フォローの上限回数：一日最大 200〜350 回
 *
 * @param void
 */
	public function follow() {
		try {
			// 自動設定の確認
			if (!(bool)$this->data['setting']['auto_follow']) {
				echo "Error Batch : No Auto Follow Setting\n";
				return;
			}

			// すでにフォローしているユーザーだった場合
			if (in_array($this->mediaUsername, $this->follows)) {
				echo $this->mediaUsername . "さんはすでにフォローしています\n";
				return;
			}

			// フォローボタン取得 _ah57t _csba8 _i46jh _rmr7s ._aj7mu
			if (!$this->wait($this->selector['followButton'], 5)) {
				throw new Exception("フォローボタン取得に失敗しました。class名を確認してください\n");
			}
			$follow = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['followButton']) );

			// フォロー実行
			$follow->click();

			// フォローが完了するまで待つ
			if (!$this->wait($this->selector['followComplete'], 10)) {
				echo "フォローが完了しませんでした\n";
				return;
			}

			$this->actions[] = 'follow';

			echo $this->data['username'] . "さんが" . $this->mediaUsername . "さんをフォローしました\n";

			sleep(3);
		} catch (Exception $e) {
			// $html = $this->driver->getPageSource();
			// $html = str_replace(array("\r\n","\r","\n","\t"), '', $html);
			// file_put_contents('html.txt', $html);
			throw new Exception($e->getMessage() . "\n" . $e->getTraceAsString());
		}
	}

/**
 * 表示中の投稿にコメントする
 * - いいねの上限回数：一日最大 500〜900 回
 *
 * @param void
 */
	public function comment() {
		try {
			// 自動設定の確認
			if (!(bool)$this->data['setting']['auto_comment']) {
				echo "Error Batch : No Auto Comment Setting\n";
				return;
			}

			// すでにフォローしているユーザーだった場合、コメントをしない
			if (in_array($this->mediaUsername, $this->follows)) {
				echo $this->mediaUsername . "さんはすでにフォローしているので、コメントをしません\n";
				return;
			}

			// コメントの存在確認
			if (empty($this->data['comment'])) {
				echo "コメントが登録されていません\n";
				return;
			}

			// すでにコメントしている投稿だった場合
			$this->loadBatchComments();
			if (in_array($this->mediaUrl, $this->comments)) {
				echo $this->mediaUsername . "さんのこの投稿にすでにコメントしています\n";
				return;
			}

			// コメントフォーム取得
			$this->wait($this->selector['commentForm'], 5);
			$comment = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['commentForm']) );

			// コメントをランダムに１つ取得
			$key = array_rand($this->data['comment']);
			$commentText = $this->data['comment'][$key];

			// コメント投稿
			$comment->sendKeys($commentText);
			$this->driver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

			// コメント投稿が完了するまで待つ（10秒間）
			$this->wait($this->selector['commentForm'], 5);

			$this->actions[] = 'comment';

			echo $this->data['username'] . "さんが" . $this->mediaUsername . "さんの写真にコメントしました\n";

			sleep(3);
		} catch (Exception $e) {
			throw new Exception($e->getMessage() . "\n" . $e->getTraceAsString());
		}
	}

/**
 * プロパティ$mediaの投稿のユーザーをフォロー解除する
 * - フォロー解除の上限回数：一日最大 150〜220 回
 *
 * @param string $username
 * @param void
 */
	public function unfollow($username) {
		try {
			// プロフィールページに遷移
			$this->driver->get('https://www.instagram.com/' . $username . '/');

			// ページが表示されるまで待つ
			if (!$this->wait($this->selector['untilDisplay'], 10)) {
				echo "{$username}\n";
				echo "ページが表示されませんでした\n";
				return 0;
			}
			echo "プロフィールページ表示完了\n";

			$element = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['profile']) );
			if (empty($element)) {
				echo "プロフィールが表示されませんでした\n";
				return 0;
			}

			// フォローボタン取得 _ah57t _csba8 _i46jh _rmr7s ._aj7mu
			if (!$this->wait($this->selector['followButton'], 5)) {
				throw new Exception("フォローボタン取得に失敗しました。class名を確認してください\n");
			}
			$follow = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['followButton']) );

			// フォロー済みか確認
			// if (!$this->wait($this->selector['followComplete'], 5)) {
			// 	echo $this->username . "さんは" . $username . "さんをフォローしていませんでした\n";
			// 	return 2;
			// }

			// アンフォローー実行
			// $follow->click();

			// if (!$this->wait($this->selector['followButtonNormal'], 5)) {
			// 	echo $username . "さんのアンフォローが完了しませんでした\n";
			// 	return 0;
			// }

			echo $this->username . "さんが" . $username . "さんをアンフォローしました\n";

			// HTMLの整形
			$html = $this->driver->getPageSource();
			$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

			// 投稿を取得
			preg_match("/<script type=\"text\/javascript\">window\._sharedData = (.*)<\/script>/ismU", $html, $match);

			$jsonData = substr($match[1], 0, -1);
			return $jsonData;
		} catch (Exception $e) {
			throw new Exception($e->getMessage() . "\n" . $e->getTraceAsString());
			return 0;
		}
	}

/**
 * インスタグラムへログインできるか確認する
 *
 * @param string $username ユーザー名
 * @param string $password パスワード
 * @return string $jsonData JSONデータ
 */
	public function loginCheck($username, $password, $isUseCookie = true) {
		$this->login($username, $password, $isUseCookie);

		sleep(5);

		// プロフィールページに遷移
		$this->driver->get('https://www.instagram.com/' . $username . '/');

		// ページが表示されるまで待つ
		$this->driver->wait(10, 500)->until(
			WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector($this->selector['untilDisplay']))
		);

		$element = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['profile']) );
		if (empty($element)) {
			return false;
		}

		// HTMLの整形
		$html = $this->driver->getPageSource();
		$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

		// 投稿を取得
		preg_match("/<script type=\"text\/javascript\">window\._sharedData = (.*)<\/script>/ismU", $html, $match);

		$jsonData = substr($match[1], 0, -1);
		return $jsonData;
	}

/**
 * プロフィールページからフォロー数とフォロワー数を取得する
 *
 * @param string $username ユーザー名
 * @return string $jsonData JSONデータ
 */
	public function getFollowFollowBy($username) {
		// プロフィールページに遷移
		$this->driver->get('https://www.instagram.com/' . $username . '/');

		// ページが表示されるまで待つ
		if (!$this->wait($this->selector['untilDisplay'], 10)) {
			try {
				$this->driver->takeScreenshot('/var/www/html/instafollow/app/tmp/getFollowFollowBy.png');
				shell_exec('sudo mv /var/www/html/instafollow/app/tmp/getFollowFollowBy.png /var/www/html/instafollow/app/webroot/img/getFollowFollowBy.png');
			} catch(Exception $e) {
				echo $e->getMessage()."\n";
			}
			echo "{$username}\n";
			echo "ページが表示されませんでした\n";
			return false;
		}

		$element = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['profile']) );
		if (empty($element)) {
			echo "プロフィールが表示されませんでした\n";
			return false;
		}

		// HTMLの整形
		$html = $this->driver->getPageSource();
		$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

		// プロフィールを取得
		preg_match("/<script type=\"text\/javascript\">window\._sharedData = (.*)<\/script>/ismU", $html, $match);

		$jsonData = substr($match[1], 0, -1);
		return $jsonData;
	}

/**
 * フォロワー取得
 *
 * @param string $username ユーザー名
 * @param string $password パスワード
 * @return string $jsonData JSONデータ
 */
	public function getFollowers($username, $password) {
		try {
			// Instagramにログイン
			$this->login($username, $password);

			// プロフィールページに遷移
			$this->driver->get('https://www.instagram.com/' . $username . '/');

			// ページが表示されるまで待つ
			if (!$this->wait($this->selector['untilDisplay'], 10)) {
				echo "{$username}\n";
				echo "ページが表示されませんでした\n";
				return false;
			}

			$element = $this->driver->findElement( WebDriverBy::cssSelector($this->selector['profile']) );
			if (empty($element)) {
				echo "プロフィールが表示されませんでした\n";
				return false;
			}
			// $html = $this->driver->getPageSource();
			// $html = str_replace(array("\r\n","\r","\n","\t"), '', $html);
			// file_put_contents('html.txt', $html);

			// if (!$this->wait('._bkw5z._kjym7', 10)) {
			// 	echo "フォロワーリンクがが表示されませんでした\n";
			// 	return false;
			// }

			$followerLink = $this->driver->findElement( WebDriverBy::cssSelector('a[href="/' . $username . '/followers/"]') );

			// フォロワー表示
			$followerLink->click();

			// フォロワーリストが表示されるまで待つ
			if (!$this->wait($this->selector['followerList'], 10)) {
				echo "モーダルが表示されませんでした\n";
				return false;
			}

			// フォロワー取得
			$followerCount = 0;
			while (true) {
				sleep(1);
				$this->driver->executeScript($this->selector['followerScroll'], array());

				// HTMLの整形
				$html = $this->driver->getPageSource();
				$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

				// フォロワー名を取得
				preg_match_all($this->selector['followerUsername'], $html, $match);
				$newCount = count($match[1]);

				// 終了判定
				if ($newCount == $followerCount) {
					break;
				}

				echo "newCount:$newCount\n";
				echo "followerCount:$followerCount\n";

				// フォロワー数を更新
				if ($newCount > $followerCount) {
					$followerCount = $newCount;
				}
			}

			// 空チェック
			if (!isset($match[1]) || empty($match[1])) {
				return false;
			}

			return json_encode($match[1]);
		} catch (Exception $e) {
			echo $e->getMessage() . "\n" . $e->getTraceAsString();
			return false;
		}
	}

/**
 * 要素の表示を待つ
 *
 * @param string $cssSelector CSSセレクタ
 * @param int $timeout_in_second タイムアウトミリ秒
 * @param int $interval_in_millisecond タイムアウトインターバルミリ秒
 * @param void
 */
	public function wait(
		$cssSelector,
		$timeout_in_second = 30,
		$interval_in_millisecond = 250) {
		try {
			$this->driver->wait($timeout_in_second, $interval_in_millisecond)->until(
				WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector($cssSelector))
			);
		} catch (Exception $e) {
			return false;
		}

		return true;
	}

/**
 * ハッシュタグを取得する
 *
 * @param string $username ユーザー名
 * @return false || string シリアル化した配列
 */
	public function getHashtag($username) {

		// プロフィールページに遷移
		$this->driver->get('https://www.instagram.com/' . $username . '/');

		// ページが表示されるまで待つ
		if (!$this->wait($this->selector['untilDisplay'], 10)) {
			echo "{$username}\n";
			echo "ページが表示されませんでした\n";
			return false;
		}

		// HTMLの整形
		$html = $this->driver->getPageSource();
		$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

		// 投稿を取得
		preg_match_all($this->selector['hashtagPostPattern'], $html, $posts);

		if (isset($posts[0])) {
			$this->media = array();
			foreach ($posts[0] as $post) {
				preg_match($this->selector['hashtagPostDetailPattern'], $post, $match);
				if (!isset($match[0])) {
					continue;
				}

				$this->media[] = array(
					'url' => 'https://www.instagram.com' . $match[1]
				);
			}
		}

		// 各投稿からハッシュタグを取得
		$hashtags = array();
		foreach ($this->media as $media) {
			$this->driver->get($media['url']);
			$html = $this->driver->getPageSource();
			$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);
			//file_put_contents('html.txt', $html);

			// ハッシュタグを取得
			preg_match_all($this->selector['hashtagPattern'], $html, $matches);
			if (!empty($matches[1])) {
				$hashtags = array_merge($hashtags, $matches[1]);
			}
		}

		// ハッシュタグがない場合
		if (empty($hashtags)) {
			return false;
		}

		// 出現回数をカウント
		$hashtags = array_count_values($hashtags);

		// 回数が多い順にソート
		arsort($hashtags);

		// 最大10個分だけに整形
		$formattedHashtags = array();
		if (count($hashtags) > 10) {
			$count = 0;
			foreach ($hashtags as $hashtag => $v) {
				$formattedHashtags[] = $hashtag;
				$count++;
				if ($count == 10) {
					break;
				}
			}
			$hashtags = $formattedHashtags;
		}

		// シリアル化して返す
		return serialize($hashtags);
	}

/**
 * curlでPOST送信を行う
 *
 * @param string $url URL
 * @param array $postData POSTデータ
 * @param void
 */
	public function curlPost($url, $postData) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  // オレオレ証明書対策
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 
		curl_setopt($curl, CURLOPT_TIMEOUT, 500);
		curl_exec($curl);
	}

}
