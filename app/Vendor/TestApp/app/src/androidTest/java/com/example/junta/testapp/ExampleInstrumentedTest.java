package com.example.junta.testapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import android.os.Environment;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiAutomatorTestCase;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import static android.os.Environment.DIRECTORY_PICTURES;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends UiAutomatorTestCase {
    //private static final String APP_PACKAGE = "com.example.junta.testapp";
    private static final String APP_PACKAGE = "com.instagram.android";

    private static final int LAUNCH_TIMEOUT = 5000;

    private static final String STRING_TO_BE_TYPED = "UiAutomator";

    private UiDevice mDevice;

    @Before
    public void startMainActivityFromHomeScreen() throws Exception {
        // 初期化
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // ホームボタン押下
        mDevice.pressHome();

        // ランチャーアプリ起動後、5秒間待機
        final String launcherPackage = getLauncherPackageName();
        //assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        // アプリ起動
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(APP_PACKAGE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        Thread.sleep(5000);

        // 5秒間待機
        //Context appContext = InstrumentationRegistry.getTargetContext();
        //mDevice.wait(Until.hasObject(By.pkg(appContext.getPackageName()).depth(0)), LAUNCH_TIMEOUT);
    }

    @Test
    public void testInstagram() throws Exception {
        // ログインボタンをクリックし、ログインフォームに遷移
        UiObject log_in_button = mDevice.findObject(new UiSelector()
                .resourceId("com.instagram.android:id/log_in_button"));
        try {
            log_in_button.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

        // ユーザー名を入力
        UiObject login_username = mDevice.findObject(new UiSelector()
                .resourceId("com.instagram.android:id/login_username"));
        try {
            login_username.setText("filiosclothes");
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

        // パスワードを入力
        UiObject login_password = mDevice.findObject(new UiSelector()
                .resourceId("com.instagram.android:id/login_password"));
        try {
            login_password.click();
            login_password.setText("Y2ip3BwN");
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

        // ログインボタンをクリック
        UiObject next_button = mDevice.findObject(new UiSelector()
                .resourceId("com.instagram.android:id/next_button"));
        try {
            next_button.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

        // 検索アイコンをクリック
        Thread.sleep(5000);
        mDevice.click(324, 1704);

        // フォロワー検索
        Thread.sleep(2000);
        mDevice.findObject(By.res("com.instagram.android", "action_bar_search_edit_text")).click();
        Thread.sleep(2000);
        mDevice.findObject(By.res("com.instagram.android", "action_bar_search_edit_text")).setText("fil__ios");

        // PEOPLEタグを選択
        Thread.sleep(3000);
        mDevice.click(405, 285);
        //mDevice.findObject(By.res("com.instagram.android", "fixed_tabbar_tabs_container").depth(2)).click();

        // フォロワーを選択
        Thread.sleep(2000);
        mDevice.click(540, 447);
        //mDevice.findObject(By.res("com.instagram.android", "list").depth(1)).click();

        // メニューをクリック
        Thread.sleep(3000);
        mDevice.click(1010, 150);

        // メッセージ送信をクリック
        Thread.sleep(2000);
        mDevice.click(540, 1063);

        // メッセージを入力
        Thread.sleep(2000);
        mDevice.findObject(By.res("com.instagram.android", "row_thread_composer_edittext")).click();
        Thread.sleep(2000);
        String message = "こんにちは\nよろしくお願いします\nちょっと休憩😜";
        mDevice.findObject(By.res("com.instagram.android", "row_thread_composer_edittext")).setText(message);

        // 送信ボタンをクリック
        mDevice.click(1010, 1704);
        //getUiDevice().pressKeyCode(66);
    }

//    @Test
//    public void useAppContext() throws Exception {
//        Thread.sleep(10000);
//
//        UiDevice.getInstance().click(1010, 150);
//
//        Thread.sleep(5000);
//
//        // Context of the app under test.
//        Context appContext = InstrumentationRegistry.getTargetContext();
//
//        assertEquals("com.example.junta.testapp", appContext.getPackageName());
//    }

    /**
     * ランチャーアプリのパッケージ名取得
     */
    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }
}
