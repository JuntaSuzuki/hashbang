#!/bin/sh

./gradlew assembleAndroidTest
#/Applications/XAMPP/htdocs/hashbang/app/Console/cake Message

#インスタグラムのアンインストール
adb uninstall com.instagram.android

#インスタグラムのインストール
adb install instagram.apk

#アプリを端末に転送
adb push app/build/outputs/apk/app-debug.apk /data/local/tmp/com.example.junta.testapp
adb shell pm install -r \"/data/local/tmp/com.example.junta.testapp\"

#テストアプリを端末に転送
adb push app/build/outputs/apk/app-debug-androidTest.apk /data/local/tmp/com.example.junta.testapp.test
adb shell pm install -r \"/data/local/tmp/com.example.junta.testapp.test\"

#uiautomator実行
adb shell am instrument -w -r   -e debug false -e class com.example.junta.testapp.ExampleInstrumentedTest com.example.junta.testapp.test/android.support.test.runner.AndroidJUnitRunner

#処理時間 計測終了
end_time=`date +%s`
time=$((end_time - start_time))
echo \$time