#!/bin/sh

#処理時間 計測開始
start_time=`date +%s`

#テストアプリのビルド
./gradlew assembleAndroidTest

#エミュレータの起動 バックグラウンドで実行
#emulator -netdelay none -netspeed full -avd Nexus_6_API_22 -no-window &
# emulator -netdelay none -netspeed full -avd Nexus_6_API_22
# EMULATOR_PID=$!

# #起動が完了するまで待つ
# WAIT_CMD="adb wait-for-device shell getprop init.svc.bootanim"
# until $WAIT_CMD | grep -m 1 stopped;
# do
#   echo "Waiting..."
#   sleep 1
# done

# echo $EMULATOR_PID

#インスタグラムのアンインストール
adb uninstall com.instagram.android

#インスタグラムのインストール
adb install /Users/junta/Desktop/TestApp/app/build/outputs/apk/instagram.apk

#アプリを端末に転送
adb push /Users/junta/Desktop/TestApp/app/build/outputs/apk/app-debug.apk /data/local/tmp/com.example.junta.testapp
adb shell pm install -r "/data/local/tmp/com.example.junta.testapp"

#テストアプリを端末に転送
adb push /Users/junta/Desktop/TestApp/app/build/outputs/apk/app-debug-androidTest.apk /data/local/tmp/com.example.junta.testapp.test
adb shell pm install -r "/data/local/tmp/com.example.junta.testapp.test"

#uiautomator実行
adb shell am instrument -w -r   -e debug false -e class com.example.junta.testapp.ExampleInstrumentedTest com.example.junta.testapp.test/android.support.test.runner.AndroidJUnitRunner

#エミュレータの停止
#kill -9 `ps auxw | grep qemu | grep -v grep | grep -v sudo | awk '{print $2}'`
kill $EMULATOR_PID

#処理時間 計測終了
end_time=`date +%s`
time=$((end_time - start_time))
echo $time
