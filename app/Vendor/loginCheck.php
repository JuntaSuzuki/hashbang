<?php

set_time_limit(180);
require 'Batch.php';

try {
	// selenium確認用
	// $Batch = new Batch(array(
	// 	'host' => 'http://127.0.0.1:4444/wd/hub',
	// 	'capabilities' => 'chrome'
	// ));

	// phantomjs用
	$Batch = new Batch(array(
		'host' => 'http://127.0.0.1:45000',
		'capabilities' => 'phantomjs'
	));

	$Batch->create();
	$res = $Batch->loginCheck($argv[1], $argv[2]);
	$Batch->quit();

	// if ($res == false) {
	// 	return json_encode(array('success' => false));
	// }

	echo $res;
} catch (Exception $e) {
	// 例外内容の表示
	echo $e->getMessage();

	// ブラウザを閉じる
	//$Batch->quit();
}
