<?php

App::uses('AppShell', 'Console/Command');
App::uses('WorkersController', 'Controller');

/**
 * ワーカーコントローラー動作確認シェル
 * 
 * 手動で実行
 */
class WorkerCheckShell extends AppShell {
 
    public function startup() {
        parent::startup();
        $this->WorkersController = new WorkersController();
    }
 
    public function like() {
        $message = [
            'type' => 'hashtag_like',
            //'type' => 'hashtag_follow',
            //'type' => 'hashtag_comment',
            //'type' => 'location_like',
            //'type' => 'location_follow',
            //'type' => 'location_comment',
            //'type' => 'unfollow',
            //'type' => 'message',
            //'type' => 'followback',
            //'type' => 'follower_check',
            //'type' => 'account_check',
            //'type' => 'following_follower_check',
            'instagram_id' => 1,
            'unfollow_day' => 5,
            'datetime' => date('Y-m-d H:i:s')
        ];
        $this->WorkersController->test(serialize($message));
    }
 
}