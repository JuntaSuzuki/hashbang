<?php

App::uses('AppShell', 'Console/Command');

use Aws\Sqs\SqsClient;
use Aws\Sqs\Exception\SqsException;
use Aws\Credentials\Credentials;

/**
 * SQSメッセージ送信 シェル
 *
 * @package       app.Console.Command
 */
class SendSQSMessageShell extends AppShell {

/**
 * 自動いいね
 *
 * @return void
 */
    public function autoLike() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoLikeSpeedId($time);

        // スピード設定のインスタグラムアカウントを取得
        $this->loadModel('AutoLike');
        $accounts = $this->AutoLike->fetchInstagrams($speedId);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            // ハッシュタグとロケーションの登録がないアカウントはスキップ
            if (empty($account['Hashtag']) && empty($account['Location'])) {
                continue;
            }

            // 自動アクションタイプを設定
            $types = ['hashtag_like', 'location_like'];
            $key = array_rand($types);
            $type = $types[$key];

            // ロケーションの登録がなかった場合、ハッシュタグでの自動アクションを実行
            if (empty($account['Location']) && $type == 'location_like') {
                $type = 'hashtag_like';
            }

            $message = [
                'type' => $type,
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];
            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動フォロー
 *
 * @return void
 */
    public function autoFollow() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoFollowSpeedId($time);

        // スピード設定のインスタグラムアカウントを取得
        $this->loadModel('AutoFollow');
        $accounts = $this->AutoFollow->fetchInstagrams($speedId);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            // ハッシュタグとロケーションの登録がないアカウントはスキップ
            if (empty($account['Hashtag']) && empty($account['Location'])) {
                continue;
            }

            // 自動アクションタイプを設定
            $types = ['hashtag_follow', 'location_follow'];
            $key = array_rand($types);
            $type = $types[$key];

            // ロケーションの登録がなかった場合、ハッシュタグでの自動アクションを実行
            if (empty($account['Location']) && $type == 'location_follow') {
                $type = 'hashtag_follow';
            }

            $message = [
                'type' => $type,
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動コメント
 *
 * @return void
 */
    public function autoComment() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoCommentSpeedId($time);

        // スピード設定のインスタグラムアカウントを取得
        $this->loadModel('AutoComment');
        $accounts = $this->AutoComment->fetchInstagrams($speedId);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            // コメントの登録がないアカウントはスキップ
            if (empty($account['Comment'])) {
                continue;
            }

            // ハッシュタグとロケーションの登録がないアカウントはスキップ
            if (empty($account['Hashtag']) && empty($account['Location'])) {
                continue;
            }

            // 自動アクションタイプを設定
            $types = ['hashtag_comment', 'location_comment'];
            $key = array_rand($types);
            $type = $types[$key];

            // ロケーションの登録がなかった場合、ハッシュタグでの自動アクションを実行
            if (empty($account['Location']) && $type == 'location_comment') {
                $type = 'hashtag_comment';
            }

            $message = [
                'type' => $type,
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動フォロー解除
 *
 * @return void
 */
    public function autoUnfollow() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoUnfollowSpeedId($time);

        // スピード設定のインスタグラムアカウントを取得
        $this->loadModel('AutoUnfollow');
        $accounts = $this->AutoUnfollow->fetchInstagrams($speedId);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            // フォロー解除経過日数のチェック
            if (empty($account['AutoSetting']['unfollow_day'])) {
                continue;
            }

            $message = [
                'type' => 'unfollow',
                'instagram_id' => $account['Instagram']['id'],
                'unfollow_day' => $account['AutoSetting']['unfollow_day'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動一括フォロー解除
 *
 * @return void
 */
    public function autoBulkUnfollow() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoUnfollowSpeedId($time);

        // 自動フォロー解除の「早い」のスケジュールを利用
        if ($speedId != 3) return;

        // 一括フォロー解除を設定しているインスタグラムアカウントを取得
        $this->loadModel('AutoBulkUnfollow');
        $accounts = $this->AutoBulkUnfollow->fetchInstagrams();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'bulk_unfollow',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動メッセージ
 *
 * @return void
 */
    public function autoMessage() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoMessageSpeedId($time);

        // 対象スピード設定で有効なインスタグラムアカウントを取得
        $this->loadModel('AutoMessage');
        $accounts = $this->AutoMessage->fetchInstagrams($speedId);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            // 対象フォロワーがいない場合はスキップ
            if (empty($account['Follower'])) {
                continue;
            }

            // メッセージがない場合はスキップ
            if (empty($account['Message'])) {
                continue;
            }

            $message = [
                'type' => 'message',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * 自動フォローバック
 *
 * @return void
 */
    public function autoFollowBack() {
        // 実行時間が対象のスピード設定を取得
        $this->loadModel('AutoSchedule');
        $time = date('H:i:00');
        $speedId = $this->AutoSchedule->fetchAutoFollowbackSpeedId($time);

        // 「早い」のスケジュールを利用
        if ($speedId != 3) return;

        // 有効なインスタグラムアカウントを取得
        $this->loadModel('User');
        $accounts = $this->User->fetchFollowBack();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'followback',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * フォロワー情報の更新チェック
 * 自動設定をONにしているユーザーのみ
 *
 * @return void
 */
    public function followerCheck() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->fetchValidAccounts();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'follower_check',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * フォローユーザー情報の更新チェック
 * 自動設定をONにしているユーザーのみ
 *
 * @return void
 */
    public function followingCheck() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->fetchValidAccounts();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'follower_check',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * アカウント情報の更新チェック
 * 自動設定をONにしているユーザーのみ
 *
 * @return void
 */
    public function accountCheck() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->fetchValidAccounts();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'account_check',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * フォロー数とフォロワー数の更新チェック
 *
 * @return void
 */
    public function followingFollowerCheck() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->find('all', [
            'fields' => ['id'],
            'conditions' => [
                'status' => true,
                'instagram_auth' => true,
            ],
            'recursive' => -1
        ]);


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'following_follower_check',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * ハッシュタグ解析
 *
 * @return void
 */
    public function analytics() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->fetchValidAccounts();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'analytics',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * スケジュール更新
 *
 * @return void
 */
    public function scheduleUpdate() {
        $message = [
            'type' => 'schedule',
            'datetime' => date('Y-m-d H:i:s')
        ];

        $this->sendSQS(json_encode($message));
    }

/**
 * 契約更新
 *
 * @return void
 */
    public function subscription() {
        // 契約更新対象ユーザーを取得
        $this->loadModel('Payment');
        $users = $this->Payment->find('all', [
            'contain' => [
                'User' => [
                    'fields' => [
                        'id',
                    ]
                ],
            ],
            'conditions' => [
                // 自動課金ステータス
                'User.auto_subscription_status' => true,
                // 退会ステータス
                'User.remove' => false,
                // 契約開始日
                'Payment.contract_start_date' => date('Y-m-d'),
                // 課金処理結果ステータス
                'Payment.payment_status' => false,
                // 課金処理実行ステータス
                'Payment.status' => false,
                // PAYJP 課金ID
                'Payment.payjp_ch_id IS NULL'
            ]
        ]);

        // SQSにメッセージ送信
        foreach ($users as $user) {
            // 念のため空チェック
            if (!isset($user['User']['id'])) {
                continue;
            }

            $message = [
                'type' => 'subscription',
                'user_id' => $user['User']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * API制限解除
 *
 * @return void
 */
    public function apiRestrictRelease() {
        // インスタグラムアカウントを取得
        $this->loadModel('Instagram');
        $accounts = $this->Instagram->fetchApiRestrictRelease();


        // SQSにメッセージ送信
        foreach ($accounts as $account) {
            // 念のため空チェック
            if (!isset($account['Instagram']['id'])) {
                continue;
            }

            $message = [
                'type' => 'api_restrict_release',
                'instagram_id' => $account['Instagram']['id'],
                'datetime' => date('Y-m-d H:i:s')
            ];

            $this->sendSQS(json_encode($message));
        }
    }

/**
 * SQSメッセージ送信
 *
 * @param array $message
 * @return void
 */
    protected function sendSQS($message) {
        try {
            $credentials = new Credentials(
                'AKIAJ5RIN26K5SSMC24Q',
                'Ld+LB0KJ5NK6/ZH1EUR6bd37quWhN/syG2qESNqw'
            );

            $client = new SqsClient([
                'credentials' => $credentials,
                'region'   => 'ap-northeast-1',
                'version'  => 'latest',
            ]);

            $client->sendMessage([
                'QueueUrl' => 'https://sqs.ap-northeast-1.amazonaws.com/708818608836/awseb-e-mrs4n4xp84-stack-AWSEBWorkerQueue-CRMB104W781G',
                'MessageBody' => $message,
            ]);
        } catch(SqsException $e) {
            // エラー処理
            $this->log($e->getMessage(), SQS);
        }
    }

}
