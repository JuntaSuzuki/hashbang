<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * ホワイトリスト コントローラー
 *
 *
 * @package     app.Controller
 */
class WhitelistsController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'Activity'
    ];

    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * ホワイトリスト登録
 *
 * @return string
 */
    public function add() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['id'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // 存在チェック
            $following = $this->Activity->find('first', [
                'conditions' => [
                    'Activity.id' => $this->data['id'],
                    'Activity.instagram_id' => $this->instagramId
                ],
                'recursive' => -1
            ]);
            if (!$following) {
                throw new ForbiddenException('フォロワー存在チェック');
            }

            // ホワイトリストをON
            $data = [
                'id' => $this->data['id'],
                'whitelist_status' => true
            ];
            if ($this->Activity->save($data)) {
                return json_encode(['success' => true]);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }

        return json_encode(['success' => false]);
    }

/**
 * ホワイトリスト削除
 *
 * @return string
 */
    public function delete() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['id'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // 存在チェック
            $following = $this->Activity->find('first', [
                'conditions' => [
                    'Activity.id' => $this->data['id'],
                    'Activity.instagram_id' => $this->instagramId
                ],
                'recursive' => -1
            ]);
            if (!$following) {
                throw new ForbiddenException('フォロワー存在チェック');
            }

            // ホワイトリストをON
            $data = [
                'id' => $this->data['id'],
                'whitelist_status' => false
            ];
            if ($this->Activity->save($data)) {
                return json_encode(['success' => true]);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }

        return json_encode(['success' => false]);
    }

/**
 * フォローユーザー取得（ページネーション）
 *
 * @return string html
 */
    public function moreFollowing() {
        $this->autoRender = false;
        $this->layout = false;

        // HTTPチェック
        if (!$this->request->is('ajax')) {
            return null;
        }

        try {
            // フォローユーザー取得
            $this->paginate = $this->Activity->fetchFollowingsOption($this->instagramId, [
                'limit' => $this->data['limit'],
                'offset' => $this->data['offset']
            ]);
            $followings = $this->paginate('Activity');

            // フォローユーザーがこれ以上取得できない場合は、空文字を返す
            if (empty($followings)) {
                return '';
            } else {
                $this->set('followings', $followings);
                $this->render('/Elements/Dashboard/more_following');
            }
        } catch(Exception $e) {
            return null;
        }
    }

}
