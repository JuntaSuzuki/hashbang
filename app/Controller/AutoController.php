<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * 自動設定 コントローラー
 *
 *
 * @package     app.Controller
 */
class AutoController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'AutoSetting', 'User'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * 自動設定
 *
 * Ajax dedicated
 * URL:/auto/setting
 *
 * @return void
 */
    public function setting() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['auto'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // 自動設定テーブルを更新
            $fields = [
                'auto_' . $this->data['type'] => $this->data['auto']
            ];
            $conditions = [
                'AutoSetting.instagram_id' => $this->instagramId,
            ];

            // 設定の更新実行
            if ($this->AutoSetting->updateAll($fields, $conditions)) {
                // 自動アクション実施の更新
                $this->autoAction($this->data['type'], $this->data['auto']);

                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

/**
 * 自動設定のスピード調整
 *
 * Ajax dedicated
 * URL:/auto/speedSetting
 *
 * @return void
 */
    public function speedSetting() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['speed'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // 自動設定テーブルを更新
            $fields = [
                'auto_' . $this->data['type'] . '_speed' => $this->data['speed']
            ];
            $conditions = [
                'AutoSetting.instagram_id' => $this->instagramId,
            ];

            // 設定の更新実行
            if ($this->AutoSetting->updateAll($fields, $conditions)) {
                // 自動アクション実施の更新
                $this->autoAction($this->data['type'], 'true', $this->data['speed']);

                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

/**
 * 自動いいねする投稿のいいね数の設定
 *
 * Ajax dedicated
 * URL:/auto/likeCount
 *
 * @return void
 */
    public function likeCount() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの空チェック
            if ($this->data['min_like_count'] === '' 
                || $this->data['max_like_count'] === '') {
                throw new ForbiddenException('パラメータの空チェック');
            }

            // 範囲外チェック
            if ($this->data['min_like_count'] < 0 
                || $this->data['max_like_count'] > 1000) {
                throw new ForbiddenException('範囲外チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // 自動設定テーブルに追加
            $fields = [
                'min_like_count' => $this->data['min_like_count'],
                'max_like_count' => $this->data['max_like_count']
            ];
            $conditions = [
                'AutoSetting.instagram_id' => $this->instagramId,
            ];

            //設定の更新
            if ($this->AutoSetting->updateAll($fields, $conditions)) {
                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

/**
 * 自動フォロー解除経過日数の設定
 *
 * Ajax dedicated
 * URL:/auto/unfollowDay
 *
 * @return void
 */
    public function unfollowDay() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの空チェック
            if (empty($this->data['unfollow_day'])) {
                throw new ForbiddenException('パラメータの空チェック');
            }

            // 範囲外チェック
            if ($this->data['unfollow_day'] < 1 
                || $this->data['unfollow_day'] > 30) {
                throw new ForbiddenException('範囲外チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // 自動設定テーブルに追加
            $fields = [
                'unfollow_day' => $this->data['unfollow_day']
            ];
            $conditions = [
                'AutoSetting.instagram_id' => $this->instagramId,
            ];

            //設定の更新
            if ($this->AutoSetting->updateAll($fields, $conditions)) {
                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

/**
 * 自動アクションのリストに登録更新・削除
 *
 * @param string $type
 * @param string $auto
 * @param int $speedId
 * @return void
 */
    protected function autoAction($type, $auto, $speedId = null) {
        try {
            // 対象アクションか検索
            $types = [
                'like', 'follow', 'comment', 'unfollow', 'message', 'bulk_unfollow'
            ];
            if (array_search($type, $types) === false) return;

            // モデルを読み込む
            $modelName = 'Auto' . ucfirst(strtr(ucwords(strtr($type, ['_' => ' '])), [' ' => '']));
            $this->loadModel($modelName);

            if ($auto == 'true') {

                // 登録
                if (is_null($speedId)) {
                    // 現在のスピードを取得
                    $speedId = $this->AutoSetting->field(
                        'auto_' . $type . '_speed',
                        ['instagram_id' => $this->instagramId]
                    );
                    $data = [
                        'instagram_id' => $this->instagramId,
                        'mtb_auto_' . $type . '_speed_id' => $speedId
                    ];

                    // 登録
                    $this->$modelName->save($data);
                } else {
                    $id = $this->$modelName->field(
                        'id',
                        ['instagram_id' => $this->instagramId]
                    );
                    
                    if (empty($id)) {
                        throw new Exception();
                    }
                    $data = [
                        'id' => $id,
                        'mtb_auto_' . $type . '_speed_id' => $speedId
                    ];

                    // 更新
                    $this->$modelName->save($data);
                }
            } else {
                // 削除
                $id = $this->$modelName->field(
                    'id',
                    ['instagram_id' => $this->instagramId]
                );
                $this->$modelName->delete($id);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage() . "\n" . $e->getTraceAsString());
            throw new Exception();
        }
    }

}
