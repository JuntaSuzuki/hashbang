<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * ダッシュボード コントローラー
 *
 * @package     app.Controller
 */
class DashboardController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'Hashtag',   'MtbHashtag',  'Comment',
        'Filter',    'AutoSetting', 'Activity',
        'Instagram', 'User', 'Message'
    ];

/**
 * ヘルパー読み込み
 *
 * @var array
 */
    public $helpers = [
        'Dashboard'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();

        // 現在のインスタグラムアカウント情報を設定
        $this->set('instagramAccount', $this->instagramAccount);

        // 現在のインスタグラムアカウントの自動設定を設定
        $auto = $this->AutoSetting->findByInstagramId($this->instagramId);
        $this->set('auto', $auto);

        // ユーザーのインスタグラムアカウント情報（連携分も含む）
        $instagrams = $this->User->fetchInstagramAccount($this->Auth->user('id'));
        $this->set('instagrams', $instagrams);
    }

/**
 * 管理画面トップ
 *
 * URL:/dashboard
 * すでにログインしている場合、管理画面に遷移
 * @return void
 */
    public function index() {
        $this->title = __('ダッシュボード');
        $this->set('active', 'home');

        // 前日のいいね いいねはタイプが1
        $yesterlikeCount = $this->Activity->fetchYesterdayActivities($this->instagramId, LIKE, 'count');
        $likes = $this->Activity->fetchTodayActivities($this->instagramId, LIKE);
        $this->set('likes', $likes);

        // 前日のフォロー フォローはタイプが2
        $yesterfollowCount = $this->Activity->fetchYesterdayActivities($this->instagramId, FOLLOW, 'count');
        $follows = $this->Activity->fetchTodayActivities($this->instagramId, FOLLOW);
        $this->set('follows', $follows);

        // 前日のコメント コメントはタイプが3
        $yesterCommentCount = $this->Activity->fetchYesterdayActivities($this->instagramId, COMMENT, 'count');
        $comments = $this->Activity->fetchTodayActivities($this->instagramId, COMMENT);
        $this->set('comments', $comments);

        // 前日のメッセージ メッセージはタイプが4
        $yesterMessageCount = $this->Activity->fetchYesterdayActivities($this->instagramId, MESSAGE, 'count');
        $messages = $this->Activity->fetchTodayActivities($this->instagramId, MESSAGE);
        $this->set('messages', $messages);

        // フォロワー数とフォロー数の取得
        $status['following_count'] = $this->instagramAccount['Instagram']['following_count'];
        $status['follower_count'] = $this->instagramAccount['Instagram']['follower_count'];
        $status['like_count'] = $yesterlikeCount;
        $status['follow_count'] = $yesterfollowCount;
        $status['comment_count'] = $yesterCommentCount;
        $status['message_count'] = $yesterMessageCount;
        $this->set('status', $status);

        // ユーザーのプラン情報を取得
        $plan = $this->User->findById($this->Auth->user('id'));
        $this->set('plan', $plan);

        $this->jsVars['activity_limit'] = Configure::read('activity_limit');
    }

/**
 * ハッシュタグ管理
 *
 * URL:/dashboard/hashtag
 * @return void
 */
    public function hashtag() {
        $this->title = __('ハッシュタグ管理');
        $this->set('active', 'hashtag');

        $hashtags = $this->Hashtag->fetchHashtags($this->instagramId);
        $this->set('hashtags', $hashtags);
    }

/**
 * コメント管理
 *
 * URL:/dashboard/comment
 * @return void
 */
    public function comment() {
        $this->title = __('コメント管理');
        $this->set('active', 'comment');

        $comments = $this->Comment->fetchComments($this->instagramId);
        $this->set('comments', $comments);
    }

/**
 * メッセージ管理
 *
 * URL:/dashboard/message
 * @return void
 */
    public function message() {
        $this->title = __('メッセージ管理');
        $this->set('active', 'message');

        $messages = $this->Message->fetchMessage($this->instagramId);
        $this->set('messages', $messages);
    }

/**
 * フィルター設定
 *
 * URL:/dashboard/filter
 * @return void
 */
    public function filter() {
        $this->title = __('フィルター管理');
        $this->set('active', 'filter');

        $filters = $this->Filter->fetchFilters($this->instagramId);
        $this->set('filters', $filters);
    }

/**
 * ロケーション管理
 *
 * URL:/dashboard/location
 * @return void
 */
    public function location() {
        $this->title = __('ロケーション管理');
        $this->set('active', 'location');

        $this->loadModel('Location');
        $locations = $this->Location->fetchLocations($this->instagramId);
        $this->set('locations', $locations);
    }

/**
 * 自動設定
 *
 * URL:/dashboard/auto
 * @return void
 */
    public function auto() {
        $this->title = __('自動設定');
        $this->set('active', 'auto');

        // 自動設定の回数を取得
        $this->loadModel('Auto');
        $this->set('autoCount', $this->Auto->fetchCount());
    }

/**
 * ホワイトリスト
 *
 * URL:/dashboard/whitelist
 * @return void
 */
    public function whitelist() {
        $this->title = __('ホワイトリスト');
        $this->set('active', 'whitelist');

        // フォローユーザー取得
        $this->paginate = $this->Activity->fetchFollowingsOption($this->instagramId);
        $this->set('followings', $this->paginate('Activity'));

        // ホワイトリストユーザー取得
        $whitelists = $this->Activity->fetchWhitelistFollowings($this->instagramId);
        $this->set('whitelists', $whitelists);
    }

/**
 * アナリティクス
 *
 * URL:/dashboard/analytics
 * @return void
 */
    public function analytics() {
        $this->title = __('アナリティクス');
        $this->set('active', 'analytics');
    }

/**
 * ハッシュタグ解析
 *
 * URL:/dashboard/hashtag_analytics
 * @return void
 */
    public function hashtag_analytics() {
        $this->title = __('ハッシュタグ解析');
        $this->set('active', 'hashtag_analytics');

        // ハッシュタグ解析（初期値は過去3日間）
        $this->loadModel('Analytics');
        $analytics = $this->Analytics->hashtag($this->instagramId, 1);
        $this->set('analytics', $analytics);
    }

/**
 * アクティビティ
 *
 * URL:/dashboard/activity
 * @return void
 */
    public function activity() {
        $this->title = __('アクティビティ');
        $this->set('active', 'activity');

        $total = $this->Activity->find('count', [
            'conditions' => [
                'Activity.instagram_id' => $this->instagramId
            ]
        ]);

        $this->jsVars['activity_limit'] = Configure::read('activity_limit');
        $this->jsVars['activity_total'] = $total;

        $this->paginate = $this->Activity->fetchActivitiesOption($this->instagramId, [
            'limit' => Configure::read('activity_limit')
        ]);
        $this->set('activities', $this->paginate('Activity'));
    }

/**
 * インスタグラム設定
 *
 * URL:/dashboard/instagram
 * @return void
 */
    public function instagram() {
        $this->title = __('インスタグラム設定');
        $this->set('active', 'instagram');

        $this->request->data = $this->instagramAccount;

        // このページは認証エラーチェックのフラグを上書き
        $this->set('auth_error_off', true);
    }

/**
 * インスタグラム アカウント追加
 *
 * URL:/dashboard/instagramCreate
 * @return void
 */
    public function instagramCreate() {
        $this->title = __('インスタグラム設定');
        $this->set('active', 'instagram');
    }

/**
 * #BANGアカウント設定
 *
 * URL:/dashboard/account
 * @return void
 */
    public function account() {
        $this->title = __('アカウント設定');
        $this->set('active', 'account');

        if ($this->request->is('put')) {
            $this->request->data['User']['id'] = $this->Auth->user('id');
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('アカウントの更新が完了しました'));
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('アカウントの更新ができませんでした'));
                $email = $this->request->data['User']['email'];
                $this->request->data = $this->User->fetchAccount($this->Auth->user('id'));
                $this->request->data['User']['email'] = $email;
            }
        } else {
            $this->request->data = $this->User->fetchAccount($this->Auth->user('id'));
        }
    }

/**
 * #BANGアカウント追加
 *
 * URL:/dashboard/accountAdd
 * @return void
 */
    public function accountAdd() {
        $this->title = __('アカウント追加');
        $this->set('active', 'instagram');

        // HTTPチェック
        if ($this->request->is('post')) {
            // パラメータ
            $param = [
                'email' => $this->request->data['User']['email'],
                'password' => $this->request->data['User']['password'],
                'user_id' => $this->Auth->user('id')
            ];

            // バリデート
            if (!$this->User->accountAddValidates($param)) {
                return;
            }

            // アカウント連携
            $this->loadModel('UsersUser');
            if ($this->UsersUser->add($param)) {
                $this->Flash->success(__('アカウント追加が完了しました'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('アカウントの追加ができませんでした'));
            }
        }
    }

/**
 * #BANGアカウント切り替え
 *
 * URL:/dashboard/accountChange
 *
 * @param string $username
 * @return void
 */
    public function accountChange($username = null) {
        $this->autoRender = false;

        // パラメータチェック
        if (!$username) {
            throw new NotFoundException(__('Invalid post'));
        }

        // インスタグラムアカウントを持っているユーザーチェック
        $user = $this->User->hasInstagramAccount($this->Auth->user('id'), $username);
        if (!$user) {
            throw new NotFoundException(__('Invalid post'));
        }

        // アカウントの切り替え
        if (!$this->Auth->login($user)) {
            throw new ForbiddenException();
        }

        // 元の画面に戻す
        return $this->redirect($this->referer());
    }

/**
 * 料金プラン
 *
 * URL:/dashboard/subscription
 * @return void
 */
    public function subscription() {
        $this->title = __('ご利用プラン');
        $this->set('active', 'subscription');

        // ユーザーのプラン情報を取得
        $plan = $this->User->findById($this->Auth->user('id'));
        $this->set('plan', $plan);

        $this->loadModel('Plan');
        $this->set('planType1', $this->Plan->fetchPlanByType(1));
        $this->set('planType2', $this->Plan->fetchPlanByType(2));

        // 購入履歴
        $this->loadModel('Payment');
        $this->paginate = $this->Payment->fetchOptionByUserId($this->Auth->user('id'));
        $this->set('payments', $this->paginate('Payment'));

        // このページは有効期限切れのフラグを上書き
        $this->set('expiration_off', true);
    }

/**
 * 契約内容変更
 *
 * URL:/dashboard/subscriptionChange
 * @return void
 */
    public function subscriptionChange() {
        $this->title = __('契約内容変更');
        $this->set('active', 'subscription');

        // ユーザーの次回契約情報を取得
        $this->loadModel('Payment');
        $nextCharge = $this->Payment->fetchNextCharge($this->Auth->user('id'));

        // HTTPチェック
        if ($this->request->is('post')) {

            // 更新するプランの情報を取得
            $this->loadModel('Plan');
            $plan = $this->Plan->findByid($this->request->data['Payment']['plan_id']);

            // 契約内容更新
            $date = new DateTime($nextCharge['Payment']['contract_start_date']);
            $contract_end_date = $date->modify('+' . ($plan['Plan']['day'] - 1) . ' days');

            $data = [
                'id' => $nextCharge['Payment']['id'],
                'amount' => $plan['Plan']['amount'],
                'plan_id' => $plan['Plan']['id'],
                'contract_end_date' => $contract_end_date->format('Y-m-d')
            ];
            if ($this->Payment->save($data)) {
                $this->Session->setFlash(__('契約内容を更新しました'));
                return $this->redirect('/dashboard/subscription');
            } else {
                $this->Session->setFlash(__('契約内容の更新に失敗しました'));
            }
        }

        // セットプランのプランIDを変更
        $setPlanIds = [4, 5, 6, 7];
        if (in_array($nextCharge['Plan']['id'], $setPlanIds)) {
            $nextCharge['Plan']['id'] = 4;
        }
        $this->set('nextCharge', $nextCharge);

        // このページは有効期限切れのフラグを上書き
        $this->set('expiration_off', true);

        $this->loadModel('Plan');
        $plans = $this->Plan->find('all');
        $this->jsVars['plan_price'] = Configure::read('plan_price');
    }

/**
 * 退会手続き
 *
 * URL:/dashboard/remove
 * @return void
 */
    public function remove() {
        $this->title = __('退会手続き');

        // HTTPチェック
        if ($this->request->is('post')) {

            // パラメータチェック
            if (empty($this->request->data['User']['password'])) {
                $this->Session->setFlash(__('パスワードが入力されていません'));
                return $this->redirect($this->referer());
            }

            // 存在チェック
            $condtions = [
                'id' => $this->Auth->user('id'),
                'password' => AuthComponent::password($this->request->data['User']['password'])
            ];

            if (!$this->User->hasAny($condtions)) {
                $this->Session->setFlash(__('パスワードが正しくありません'));
                return $this->redirect($this->referer());
            }

            try {
                // ユーザーステータス更新（トランザクション開始）
                $this->loadModel('TransactionManager');
                $transaction = $this->TransactionManager->begin();

                $data = [
                    'id' => $this->Auth->user('id'),
                    'status' => false,
                    'remove' => true
                ];
                if (!$this->User->save($data, ['callbacks' => false])) {
                    throw new Exception("ユーザーステータス更新（トランザクション開始）");
                }

                // インスタグラムアカウントを停止
                // 自動アクションを停止
                $instagram = $this->Instagram->find('first', [
                    'fields' => ['id'],
                    'conditions' => [
                        'user_id' => $this->Auth->user('id')
                    ],
                    'recursive' => -1
                ]);

                $data = [
                    'id' => $instagram['Instagram']['id'],
                    'status' => 0,
                ];
                if (!$this->Instagram->save($data)) {
                    throw new Exception("インスタグラムアカウントを停止");
                }

                // 自動設定をOFF
                $this->loadModel('AutoSetting');
                $this->AutoSetting->off($this->Auth->user('id'));

                // 自動アクションを停止
                $this->loadModel('AutoComment');
                $this->AutoComment->deleteAll([
                        'AutoComment.instagram_id' => $instagram['Instagram']['id']
                    ],
                    false
                );

                $this->loadModel('AutoFollow');
                $this->AutoFollow->deleteAll([
                        'AutoFollow.instagram_id' => $instagram['Instagram']['id']
                    ],
                    false
                );

                $this->loadModel('AutoLike');
                $this->AutoLike->deleteAll([
                        'AutoLike.instagram_id' => $instagram['Instagram']['id']
                    ],
                    false
                );

                $this->loadModel('AutoMessage');
                $this->AutoMessage->deleteAll([
                        'AutoMessage.instagram_id' => $instagram['Instagram']['id']
                    ],
                    false
                );

                $this->loadModel('AutoUnfollow');
                $this->AutoUnfollow->deleteAll([
                        'AutoUnfollow.instagram_id' => $instagram['Instagram']['id']
                    ],
                    false
                );


                // 課金予定情報を削除
                $this->loadModel('Payment');
                $this->Payment->deleteAll([
                        'Payment.user_id' => $this->Auth->user('id'),
                        'Payment.payment_status' => false,
                        'Payment.status' => false,
                    ],
                    false
                );

                $this->TransactionManager->commit($transaction);
                $this->Session->setFlash(__('退会手続きが完了しました'));

                // ログアウト
                return $this->redirect($this->Auth->logout());
            } catch (Exception $e) {
                $this->log($e->getMessage(), REMOVE);
                $this->log($e->getTraceAsString(), REMOVE);
                $this->TransactionManager->rollback($transaction);
                $this->Session->setFlash(__('退会手続きに失敗しました'));
                return $this->redirect($this->referer());
            }
        }
    }

/**
 * 友達招待でお得
 *
 * URL:/dashboard/invite
 * @return void
 */
    public function invite() {
        $this->title = __('友達招待でお得');

        $this->loadModel('Invite');
        $invites = $this->Invite->inviteUsers($this->Auth->user('id'));
        $this->set('invites', $invites);

        $this->set('invite_code', $this->Auth->user('invite_code'));
    }

/**
 * インスタグラム アカウント切り替え
 *
 * URL:/dashboard/instagramChange
 *
 * @param string $username
 * @return void
 */
    public function instagramChange($username = null) {
        $this->autoRender = false;

        // パラメータチェック
        if (!$username) {
            throw new NotFoundException(__('Invalid post'));
        }

        // インスタグラムユーザーの存在確認
        $instagram = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.user_id' => $this->Auth->user('id'),
                'Instagram.username' => $username
            ],
            'recursive' => -1
        ]);

        if (!$instagram) {
            throw new NotFoundException(__('Invalid post'));
        }

        // アカウントの切り替え
        $data = [
            'id' => $this->Auth->user('id'),
            'instagram_id' => $instagram['Instagram']['id']
        ];

        if (!$this->User->save($data, ['validate' => false, 'callbacks' => false])) {
            throw new ForbiddenException();
        }

        // 元の画面に戻す
        return $this->redirect($this->referer());
    }

/**
 * イントロダクション
 *
 * Ajax dedicated
 * URL:/dashboard/intro
 *
 * @return void
 */
    public function intro() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['status'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // ユーザー番号の存在チェック
            if (empty($this->Auth->user('id'))) {
                throw new ForbiddenException('ユーザー番号の存在チェック');
            }

            // 自動設定テーブルを更新
            $data = [
                'id' => $this->Auth->user('id'),
                'activate' => (bool)$this->data['status']
            ];

            // 設定の更新実行
            if ($this->User->save($data, ['callbacks' => false])) {
                $this->Session->delete('hashtag_count');
                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            return json_encode(array('success' => false));
        }
    }

}
