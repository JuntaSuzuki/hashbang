<?php

App::uses('DashboardBaseController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

/**
 * ロケーション コントローラー
 *
 *
 * @package		app.Controller
 */
class LocationsController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
	public $uses = array('Location', 'MtbLocation', 'User');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * ハッシュタグ登録
 *
 * @return string
 */
	public function add() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// パラメータの存在チェック
			if (empty($this->data['id'])) {
				throw new ForbiddenException('パラメータの存在チェック');
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// ハッシュタグマスタの存在チェック
			$check = $this->MtbLocation->findByInstagramLocationId($this->data['id']);

			// 存在しなければ、ハッシュタグマスタテーブルに追加
			$name = str_replace(PHP_EOL, '', $this->data['name']);
			if (empty($check)) {
				$data = array(
					'instagram_location_id' => $this->data['id'],
					'lat' => $this->data['lat'],
					'lng' => $this->data['lng'],
					'name' => $name
				);
				$this->MtbLocation->save($data);
				$mtbLocationId = $this->MtbLocation->getLastInsertID();
			} else {
				$mtbLocationId = $check['MtbLocation']['id'];
			}

			// すでにハッシュタグテーブルに追加している場合は、2を返す
			$conditions = array(
				'instagram_id' => $this->instagramId,
				'mtb_location_id' => $mtbLocationId
			);
			if ($this->Location->hasAny($conditions)) {
				return json_encode(array('code' => '400', 'message' => 'no data'));
			}

			// ハッシュタグテーブルに追加
			$data = $conditions;
			if ($this->Location->save($data)) {
				// 登録件数の更新
				$this->MtbLocation->updateCount($mtbLocationId);

				return json_encode(array(
					'code' => '200',
					'message' => h($name),
					'id' => $this->Location->getLastInsertID()
				));
			} else {
				return json_encode(array('code' => '500', 'message' => 'no data'));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('code' => '500', 'message' => 'no data'));
		}
	}

/**
 * ハッシュタグ削除
 *
 * @return string
 */
	public function delete() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// パラメータの存在チェック
			if (empty($this->data['id'])) {
				throw new ForbiddenException('パラメータの存在チェック');
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// データの存在チェック
			$location = $this->Location->findById($this->data['id']);
			if (empty($location)) {
				return json_encode(array('success' => false));
			}

			// 削除条件
			$conditions = array(
				'id' => $this->data['id'],
				'instagram_id' => $this->instagramId,
			);

			// 削除実行
			if ($this->Location->deleteAll($conditions, false)) {
				// 登録件数の更新
				$this->MtbLocation->updateCount($location['MtbLocation']['id']);

				return json_encode(array('success' => true));
			} else {
				return json_encode(array('success' => false));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

/**
 * ロケーション検索
 *
 * @return string
 */
	public function search() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// URL生成
			$url = 'https://api.instagram.com/v1/locations/search';
			$HttpSocket = new HttpSocket();
			$response = $HttpSocket->get($url, array(
				'lat' => $this->data['lat'],
				'lng' => $this->data['lng'],
				'access_token' => '3265028095.fa71964.35629bdd19e14ebb98e90de607d23146',
				'distance' => 750
			));

			$this->log($response, LOG_DEBUG);

			// ロケーション検索
			$data = json_decode($response['body']);
			$data = json_decode(json_encode($data), true);
			array_multisort(array_column($data['data'], 'media_count'), SORT_DESC, $data['data']);

			return json_encode($data['data']);
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

}
