<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('AppController', 'Controller');

/**
 * 静的コンテンツ コントローラー
 *
 * @package     app.Controller
 */
class PagesController extends AppController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'Contact'
    ];

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'Cookie', 'Mail'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'public';
        $this->Auth->allow();
    }

/**
 * トップページ
 *
 * @return void
 */
    public function index() {
        $this->title = __('#BANG[ハッシュバン]｜インスタグラマーのためのサポートツール');

        // // 招待コードがある場合、Cookeに書き込む
        // if (!empty($this->request->query['invite_code'])) {
        //     $this->Cookie->write('invite_code', $this->request->query['invite_code']);
        // }

        $this->render('/Landing/index');
    }

/**
 * 利用規約
 *
 * @return void
 */
    public function term() {
        $this->title = __('#BANG[ハッシュバン]｜利用規約');
    }

/**
 * プライバシーポリシー
 *
 * @return void
 */
    public function privacy() {
        $this->title = __('#BANG[ハッシュバン]｜プライバシーポリシー');
    }

/**
 * 特定商取引法に基づく表示
 *
 * @return void
 */
    public function legal() {
        $this->title = __('#BANG[ハッシュバン]｜特定商取引法に基づく表示');
        //$this->render('/Landing/index');
    }

/**
 * 運営会社
 *
 * @return void
 */
    public function corporate() {
        $this->title = __('#BANG[ハッシュバン]｜運営会社');
    }

/**
 * お問い合わせ
 *
 * @return void
 */
    public function contact() {
        // HTTPチェック
        if ($this->request->is('ajax')) {
            $this->autoRender = false;

            $args = [
                'name' => $this->data['name'],
                'email' => $this->data['email'],
                'subject' => $this->data['subject'],
                'content' => $this->data['content']
            ];

            // バリデーションチェック
            if ($this->data['validate']) {
                // バリデーション項目の設定
                $this->Contact->set($args);

                if (!$this->Contact->validates()) {
                    $errors = $this->Contact->validationErrors;
                    foreach ($errors as $key => $error) {
                        $check[$key] = $error;
                    }
                }

                // エラーがあった場合
                if (!empty($check)) {
                    return json_encode(['success' => false, 'data' => $check]);
                }

                // エラーがない場合
                return json_encode(['success' => true]);
            }

            if ($this->Mail->contact($args)) {
                $this->Session->setFlash(__('お問い合わせを受け付けました。'));
                return json_encode(['success' => true]);
            } else {
                $this->Session->setFlash(__('お問い合わせの送信に失敗しました。後ほどもう一度送信をお願いします。'));
            }

            return json_encode(['success' => true]);
        }

        $this->title = __('#BANG[ハッシュバン]｜お問い合わせ');
    }

/**
 * よくある質問
 *
 * @return void
 */
    public function faq() {
        $this->title = __('#BANG[ハッシュバン]｜よくある質問');
    }

/**
 * 料金プラン
 *
 * @return void
 */
    public function plan() {
        $this->title = __('#BANG[ハッシュバン]｜料金プラン');
    }

/**
 * 友達招待で無料
 *
 * @return void
 */
    public function invite() {
        $this->title = __('#BANG[ハッシュバン]｜友達招待で無料');
        $this->active = 'invite';
    }

/**
 * Displays a view
 *
 * @return void
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function test() {
        $this->autoRender = false;

        //$this->Schedule = $this->Components->load('Schedule');
        //print_r($this->Components);
        $this->Subscription->charge();
        exit;

        // スケジュール情報取得
        $this->loadModel('AutoSchedule');
        $schedule = $this->AutoSchedule->find('list', [
            'fields' => [
                'action_time'
            ]
        ]);

        $models = [
            'MtbAutoLikeSpeed', 'MtbAutoFollowSpeed', 'MtbAutoCommentSpeed',
            'MtbAutoMessageSpeed', 'MtbAutoUnfollowSpeed'
        ];

        // モデル毎に更新を行う
        foreach ($models as $model) {
            // 件数取得
            $this->loadModel($model);
            $processingNumbers = $this->$model->find('list', [
                'fields' => [
                    'processing_number'
                ]
            ]);

            // スケジュールをコピー
            $tmpSchedule = $schedule;

            // 件数分だけランダムにスケジュールを配分
            $data = [];
            foreach ($processingNumbers as $key => $processingNumber) {
                $data[$key] = $this->array_rand_value($tmpSchedule, $processingNumber);
                $tmpSchedule = array_diff($tmpSchedule, $data[$key]);
            }

            // カラム名
            $column = ltrim(strtolower(preg_replace('/[A-Z]/', '_\0', $model)), '_') . '_id';

            // スケジュールをリセット
            $fields = [
                'AutoSchedule.' . $column => 0
            ];
            $this->AutoSchedule->updateAll($fields);

            // スケジュール生成
            foreach ($data as $speed_id => $action_times) {
                foreach ($action_times as $action_time) {
                    $fields = [
                        'AutoSchedule.' . $column => $speed_id
                    ];
                    $conditions = [
                        'AutoSchedule.action_time' => $action_time
                    ];
                    $this->AutoSchedule->updateAll($fields, $conditions);
                }
            }
        }

        exit;

        // 実行時間が対象のスピード設定を取得
        $time = date('H:m:00');
        $speedId = $this->AutoSchedule->fetchAutoLikeSpeedId($time);

        // スピード設定のインスタグラムアカウントを取得
        $this->loadModel('AutoLike');
        $accounts = $this->AutoLike->fetchInstagrams($speedId);
        print_r($accounts);
        exit;
        // $this->loadModel('AutoLike');
        // $accounts = $this->AutoLike->fetchInstagrams();
        // print_r($accounts);
        // exit;
        // $this->loadModel('AutoSchedule');
        // $times = [];
        // $d = new DateTime('0:00'); // DateTimeクラスのオブジェクト生成

        // $this->AutoSchedule->create();
        // $data['action_time'] = $d->format('G:i');
        // $this->AutoSchedule->save($data);
        // //for ($i=1; $i < 100; $i++) {
        // while (true) {
        //     $d->add(new DateInterval('PT1M'));
        //     $time = $d->format('G:i');
        //     //$times[] = $time;
        //     $this->AutoSchedule->create();
        //     $data['action_time'] = $time;
        //     $this->AutoSchedule->save($data);
        //     if ($time == '23:59') break;
        // }
        // exit;

        // $this->loadModel('AutoSchedule');
        // foreach ($variable as $key => $value) {
        //    $this->AutoSchedule->create();
        // }
        // shuffle($times);
        // //$times = array_slice($times, 0, 1200);
        // $timeGroup = $this->array_divide($times, 3);
        // //     $d->add(new DateInterval('PT1M'));
        // //     $time = $d->format('G:i');
        // //     if ($time == '1:27') {
        // //         break;
        // //     }
        // //     $times[] = $time;
        // // }
        // // echo $d->format('G:i') . "\n"; // 表示
        // // $d->add(new DateInterval('PT1H')); // 1時間を足す
        // // echo $d->format('G:i') . "\n"; // 表示
        // print_r($timeGroup);
    }

    private function array_divide($array, $division) {
        $base_count = floor(count($array) / $division); // 部分配列1個あたりの要素数
        $remainder  = count($array) % $division;        // 余りになる要素数
     
        $ret = array();
        $offset = 0;
        for($i = 0; $i < $division; $i++) {
            /*
             * 余りの要素がある場合は、
             * 先頭の部分配列に1個ずつまぶしていく
             */
            if (empty($remainder)) {
                $length = $base_count;
            } else {
                $length = $base_count + 1;
                $remainder--;
            }
            $ret[] = array_slice($array, $offset, $length);
     
            $offset += $length;
        }
     
        return $ret;
    }

    /**
 * 配列から指定したエントリの数のバリューをランダムに抽出する
 * 
 * @param array $array 抽出元の配列
 * @param int $num 取得する要素数
 * @return array 抽出後の配列
 */
    function array_rand_value($arr, $num = 1) {
        if ($num >= count($arr)) {
            return $arr;
        }

        $result = array();
        $rand_keys = array_rand($arr, $num);
        foreach ($rand_keys as $key) {
            $result[] = $arr[$key];
        }

        return $result;
    }
}
