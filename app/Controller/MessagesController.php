<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * メッセージ コントローラー
 *
 *
 * @package     app.Controller
 */
class MessagesController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'Message'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * メッセージ登録
 *
 * @return void
 */
    public function add() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException();
            }

            // パラメータの存在チェック
            if (empty($this->data['message'])) {
                throw new ForbiddenException();
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // メッセージテーブルに追加
            $data = [
                'instagram_id' => $this->instagramId,
                'message' => h($this->data['message'])
            ];

            if ($this->Message->save($data)) {
                return json_encode([
                    'code' => '200',
                    'message' => mb_strimwidth(h($this->data['message']), 0, 50, '...'),
                    'fullmessage' => h($this->data['message']),
                    'id' => $this->Message->getLastInsertID()
                ]);
            } else {
                return json_encode(['code' => '500', 'message' => 'no data']);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(['success' => false]);
        }
    }

/**
 * メッセージ編集
 *
 * @return void
 */
    public function edit() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException();
            }

            // パラメータの存在チェック
            if (empty($this->data['id']) || empty($this->data['message'])) {
                throw new ForbiddenException();
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // データの存在チェック
            $message = $this->Message->findById($this->data['id']);
            if (empty($message)) {
                return json_encode(['success' => false]);
            }

            // メッセージを更新
            $data = [
                'id' => $message['Message']['id'],
                'message' => h($this->data['message'])
            ];

            if ($this->Message->save($data)) {
                return json_encode([
                    'code' => '200',
                    'message' => mb_strimwidth(h($this->data['message']), 0, 50, '...'),
                    'fullmessage' => h($this->data['message']),
                    'id' => $message['Message']['id']
                ]);
            } else {
                return json_encode(['code' => '500', 'message' => 'no data']);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(['success' => false]);
        }
    }

/**
 * メッセージ削除
 *
 * @return 0:success 1:error
 */
    public function delete() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['id'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // データの存在チェック
            $message = $this->Message->findById($this->data['id']);
            if (empty($message)) {
                return json_encode(['success' => false]);
            }

            // 削除条件
            $conditions = [
                'id' => $this->data['id'],
                'instagram_id' => $this->instagramId,
            ];

            // 削除実行
            if ($this->Message->deleteAll($conditions, false)) {
                return json_encode(['success' => true]);
            } else {
                return json_encode(['success' => false]);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(['success' => false]);
        }
    }

}
