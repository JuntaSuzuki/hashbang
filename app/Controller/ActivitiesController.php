<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * アクティビティ コントローラー
 *
 *
 * @package     app.Controller
 */
class ActivitiesController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = ['Activity'];

    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * アクティビティ取得（ページネーション）
 *
 * @return string html
 */
    public function more() {
        $this->autoRender = false;
        $this->layout = false;

        // HTTPチェック
        if (!$this->request->is('ajax')) {
            return null;
        }

        try {
            $this->paginate = $this->Activity->fetchActivitiesOption($this->instagramId, [
                'limit' => $this->data['limit'],
                'offset' => $this->data['offset']
            ]);
            $this->set('activities', $this->paginate('Activity'));
            $this->render('/Elements/Dashboard/activity_timeline');
        } catch(Exception $e) {
            return null;
        }
    }

/**
 * 本日のアクティビティ取得（ページネーション）
 *
 * @return string html
 */
    public function moreToday() {
        $this->autoRender = false;
        $this->layout = false;

        // HTTPチェック
        if (!$this->request->is('ajax')) {
            return null;
        }

        try {
            $this->paginate = $this->Activity->fetchActivitiesOption($this->instagramId, [
                'limit' => $this->data['limit'],
                'offset' => $this->data['offset'],
                'type' => [$this->data['type']],
                'conditions' => [
                    'Activity.action_date >' => date('Y-m-d', strtotime(date('Y-m-d') . '0 day')) . ' 00:00:00',
                    'Activity.action_date <' => date('Y-m-d', strtotime(date('Y-m-d') . '+1 day')) . ' 00:00:00'
                ]
            ]);
            $this->set('activities', $this->paginate('Activity'));
            $this->render('/Elements/Dashboard/today_activity');
        } catch(Exception $e) {
            return null;
        }
    }

}
