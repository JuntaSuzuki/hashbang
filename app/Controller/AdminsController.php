<?php

App::uses('AppController', 'Controller');

/**
 * 管理者 コントローラー
 *
 * @package     app.Controller
 */
class AdminsController extends AppController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'User'
    ];

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Admin',
                    'fields' => ['username' => 'email']
                ]
            ],
            'loginError' => 'パスワードもしくはログインIDをご確認下さい。',
            'authError' => 'ご利用されるにはログインが必要です。',
            'loginAction' => '/admins/sign_in',
            'loginRedirect' => '/admins/',
            'logoutRedirect' => '/admins/sign_in',
        ],
        'Search.Prg'
    ];

/**
 * ヘルパー読み込み
 *
 * @var array
 */
    public $helpers = [];

/**
 * 検索設定
 *
 * @var array
 */
    public $presetVars = [];

/**
 * ページネーション
 *
 * @var array
 */
    public $paginate = [
        'User' => [
            'limit' => 10,
        ]
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        $this->theme = 'Admin';
        parent::beforeFilter();
        AuthComponent::$sessionKey = 'Auth.admins';

        // $this->set('name', $this->Auth->user('name'));
        // $this->set('id', $this->Auth->user('id'));

        $this->Auth->allow('sign_in');
    }

/**
 * アクションの後で、ビューが描画される前に実行
 *
 */
    public function beforeRender() {
        $this->set('title', $this->title);
        $this->set('title_for_layout', $this->title);
        $this->set('active', $this->active);
        $this->set('offcanvas', $this->offcanvas);
        $this->set('authority', $this->Session->read('authority'));
    }

/**
 * 管理者ログイン
 *
 * URL:/admins/sign_in
 *
 * @return void
 */
    public function sign_in() {
        $this->title = __('ログイン');
        $this->offcanvas = 'be-offcanvas-menu';

        // HTTPチェック
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Session->write('authority', 'admins');
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__('メールアドレスかパスワードが違います。'));
            }
        }
    }

/**
 * ログアウト
 *
 * URL:/admins/logout
 *
 * @return void
 */
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

/**
 *
 * URL:/admins/
 *
 * @return void
 */
    public function index() {
        $this->redirect('/admins/sign_in');
    }

/**
 *
 * URL:/admins/user
 *
 * @return void
 */
    public function user() {
        // 検索パラメータ
        // $this->presetVars = [
        //     ['field' => 'cn', 'type' => 'value'],
        //     ['field' => 'cs', 'type' => 'checkbox'],
        // ];

        // 検索条件設定  
       // $this->Prg->commonProcess('User');

        // 検索条件取得  
        //$conditions = $this->User->parseCriteria($this->passedArgs);

        $this->paginate['User'] = [
            //'conditions' => $conditions,
            'group' => 'User.id',
            'order' => ['id' => 'desc'],
            'limit' => 10
        ];

//print_r($this->paginate('User'));
//exit;
        $this->set('users', $this->paginate('User'));
        $this->active = 'user';
        //$this->redirect(Configure::read('url') . 'admins/sign_in');
    }

/**
 *
 * URL:/admins/sale
 *
 * @return void
 */
    public function sale() {
        // 検索パラメータ
        // $this->presetVars = [
        //     ['field' => 'cn', 'type' => 'value'],
        //     ['field' => 'cs', 'type' => 'checkbox'],
        // ];

        // 検索条件設定  
       // $this->Prg->commonProcess('User');

        // 検索条件取得  
        //$conditions = $this->User->parseCriteria($this->passedArgs);

        $this->paginate['User'] = [
            //'conditions' => $conditions,
            'group' => 'User.id',
            'order' => ['id' => 'desc'],
            'limit' => 10
        ];

//print_r($this->paginate('User'));
//exit;
        $this->set('users', $this->paginate('User'));
        $this->active = 'sale';
        //$this->redirect(Configure::read('url') . 'admins/sign_in');
    }

}
