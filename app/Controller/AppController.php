<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $jsVars = [];

    public $title = '';

    public $active = '';

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'DebugKit.Toolbar',
        'Session',
        'Cookie' => [
            'name' => 'bang'
        ],
        'Flash',
        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'scope' => [
                        'User.status' => true
                    ]
                ]
            ],
            'loginError' => 'パスワードもしくはログインIDをご確認下さい。',
            'authError' => 'ご利用されるにはログインが必要です。',
            'loginAction' => '/users/sign_in',
            'loginRedirect' => '/dashboard',
            'logoutRedirect' => '/',
        ],
    ];

/**
 * ヘルパー読み込み
 *
 * @var array
 */
    public $helpers = ['Js', 'Util'];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        $this->Cookie->name = 'bang';

        // 経由AS情報がある場合、Cookeに書き込む
        if (!empty($this->request->query['a8'])) {
            $this->Cookie->write('a8', $this->request->query['a8'], false, '90 day');
        }

        // #BANGアフィリエイト情報がある場合、Cookeに書き込む
        if (!empty($this->request->query['cid'])) {
            $this->Cookie->write('cid', $this->request->query['cid'], false, '90 day');
        }
    }

/**
 * アクションの後で、ビューが描画される前に実行
 *
 */
    public function beforeRender() {
        $this->set('title', $this->title);
        $this->set('title_for_layout', $this->title);
        $this->set('active', $this->active);

        $this->jsVars['dashboard_url'] = Configure::read('dashboard_url');
        $this->set('js_vars', $this->jsVars);
    }

}
