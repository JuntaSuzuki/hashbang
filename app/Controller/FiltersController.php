<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * フィルター コントローラー
 *
 *
 * @package		app.Controller
 */
class FiltersController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
	public $uses = array('Filter', 'User');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * フィルター登録
 *
 * @return void
 */
	public function add() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// パラメータの存在チェック
			if (empty($this->data['filter'])) {
				throw new ForbiddenException('パラメータの存在チェック');
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// データの存在チェック
			$conditions = array(
				'instagram_id' => $this->instagramId,
				'keyword' => h($this->data['filter'])
			);
			if ($this->Filter->hasAny($conditions)) {
				return json_encode(array('code' => '400', 'message' => 'no data'));
			}

			// フィルターテーブルに追加
			$data = $conditions;

			if ($this->Filter->save($data)) {
				return json_encode(array(
					'code' => '200',
					'message' => h($this->data['filter']),
					'id' => $this->Filter->getLastInsertID()
				));
			} else {
				return json_encode(array('code' => '500', 'message' => 'no data'));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

/**
 * フィルター削除
 *
 * @return 0:success 1:error
 */
	public function delete() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// パラメータの存在チェック
			if (empty($this->data['id'])) {
				throw new ForbiddenException('パラメータの存在チェック');
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// データの存在チェック
			$filter = $this->Filter->findById($this->data['id']);
			if (empty($filter)) {
				return json_encode(array('success' => false));
			}

			// 削除条件
			$conditions = array(
				'id' => $this->data['id'],
				'instagram_id' => $this->instagramId,
			);

			// 削除実行
			if ($this->Filter->deleteAll($conditions, false)) {
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('success' => false));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

}
