<?php

App::uses('AppController', 'Controller');

/**
 * ユーザー コントローラー
 *
 * @package     app.Controller
 */
class UsersController extends AppController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'User', 'AutoSetting', 'Invite', 'Instagram', 'Hashtag'
    ];

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'scope' => [
                        'User.status' => true
                    ]
                ]
            ],
            'loginError' => 'パスワードもしくはログインIDをご確認下さい。',
            'authError' => 'ご利用されるにはログインが必要です。',
            'loginAction' => '/users/sign_in',
            'loginRedirect' => '/dashboard',
            'logoutRedirect' => '/',
        ],
        'Mail'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'public';

        $this->Auth->allow('sign_up', 'sign_in', 'forgot', 'reset', 'instagram');
    }

/**
 * 新規会員登録機能
 *
 * /users/sign_upにアクセス
 * すでにログインしている場合、管理画面に遷移
 * @return void
 */
    public function sign_up() {
        // GET送信の場合はセッション情報を削除
        if ($this->request->is('get')) {
            $this->Session->delete('User');
        }

        // ステップ1
        $step1Validate = false;
        if ($this->request->is('post')) {
            if ($step1Validate === false && !$this->Session->check('User')) {
                // 会員情報バリデーション設定
                $this->User->set([
                    'email' => $this->request->data['User']['email'],
                    'password' => $this->request->data['User']['password']
                ]);

                // バリデーション
                if ($this->User->validates()) {
                    $step1Validate = true;

                    // セッションに情報を格納
                    $this->Session->write('User', $this->request->data['User']);
                }
            }

            if ($this->Session->check('User')) {
                $step1Validate = true;
            }
        }

        // 登録処理
        if ($this->request->is('ajax') && $this->Session->check('User') && $step1Validate) {
            $this->autoRender = false;

            // バリデーションチェック
            if ($this->data['validate']) {
                $check = [];

                // バリデーション項目の設定
                $this->Instagram->set([
                    'username' => $this->data['username'],
                    'password' => $this->data['password']
                ]);
                if (!$this->Instagram->validates()) {
                    $errors = $this->Instagram->validationErrors;
                    foreach ($errors as $key => $error) {
                        $check[$key] = $error;
                    }
                }

                // エラーがあった場合
                if (!empty($check)) {
                    return json_encode(['success' => false, 'data' => $check]);
                }

                // エラーがない場合
                return json_encode(['success' => true]);
            }

            $sinUpFlag = false;

            try {
                $username = $this->data['username'];
                $password = $this->data['password'];

                $i = new \InstagramAPI\Instagram(false, false);
                $i->setUser($username, $password);

                // ログインチェック
                $i->login(true);
                $userInfo = $i->getSelfUserInfo();

                // 経由AS情報チェック
                $a8Code = NULL;
                if (($this->Cookie->check('a8'))) {
                    $a8Code = $this->Cookie->read('a8');
                }

                // #BANGアフィリエイトチェック
                $cid = NULL;
                if (($this->Cookie->check('cid'))) {
                    $cid = $this->Cookie->read('cid');
                }

                // 登録データ作成
                $requestData = $this->Session->read('User');
                $data = [
                    'email' => $requestData['email'],
                    'password' => $requestData['password'],
                    'plan_limit_date' => date("Y-m-d", strtotime("+" . Configure::read('expiration_days_of_trial') . " day")),
                    'a8_code' => $a8Code,
                    'cid' => $cid
                ];

                // ユーザー登録（トランザクション開始）
                $this->loadModel('TransactionManager');
                $transaction = $this->TransactionManager->begin();
                if ($this->User->save($data)) {
                    // ログイン処理
                    $user = $this->User->find('first', [
                        'conditions' => [
                            'User.email' => $requestData['email'],
                            'User.status' => 1
                        ],
                        'recursive' => -1
                    ]);
                    $this->Auth->login($user['User']);

                    // インスタグラムアカウント情報の登録
                    $data = [
                        'user_id' => $this->Auth->user('id'),
                        'username' => $username,
                        'password' => $password,
                        'username_id' => $userinfo->user->pk,
                        'profile_picture' => $userInfo->user->profile_pic_url,
                        'follower_count' => $userInfo->user->follower_count,
                        'following_count' => $userInfo->user->following_count,
                        'media_count' => $userInfo->user->media_count,
                        'instagram_auth_date' => date('Y-m-d H:i:s')
                    ];
                    $this->Instagram->save($data);

                    // 登録されたインスタグラムアカウントの番号取得
                    $instagramId = (int)$this->Instagram->field('id', [
                        'Instagram.user_id' => $this->Auth->user('id')
                    ]);

                    // インンスタグラムアカウント情報がない場合
                    if (!$instagramId) {
                        return json_encode(['success' => false]);
                    }

                    // 自動設定の初期化
                    $this->AutoSetting->init($this->Auth->user('id'), $instagramId);

                    // インスタグラムアカウントとユーザーを紐付ける
                    $data = [
                        'id' => $this->Auth->user('id'),
                        'instagram_id' => $instagramId
                    ];
                    $this->User->save($data, ['validate' => false, 'callbacks' => false]);

                    // ハッシュタグ取得
                    $hashtags = $this->getHashtags($i);

                    // ハッシュタグがあれば登録、自動アクティビティ設定
                    if (!empty($hashtags)) {
                        // ハッシュタグ一括登録
                        $this->Hashtag->addAll($hashtags, $instagramId);

                        // 自動いいね設定の更新
                        $this->AutoSetting->updateAll([
                            'auto_like' => true
                        ], [
                            'AutoSetting.instagram_id' => $instagramId
                        ]);

                        // 自動いいねを設定
                        $this->loadModel('AutoLike');
                        $this->AutoLike->add($instagramId, 1);


                        // 自動フォロー設定の更新
                        $this->AutoSetting->updateAll([
                            'auto_follow' => true
                        ], [
                            'AutoSetting.instagram_id' => $instagramId
                        ]);

                        // 自動フォローを設定
                        $this->loadModel('AutoFollow');
                        $this->AutoFollow->add($instagramId, 1);
                    }
                    $this->Session->write('hashtag_count', count($hashtags));

                    // コミット
                    $this->TransactionManager->commit($transaction);

                    // 登録完了メール送信
                    $this->Mail->signUp($this->Auth->user('email'));

                    // 登録完了
                    $sinUpFlag = true;

                    // セッション情報を削除
                    $this->Session->delete('User');

                    // 経由AS情報を削除
                    $this->Cookie->delete('a8');

                    // #BANGアフィリエイト情報を削除
                    $this->Cookie->delete('cid');
                }
            } catch (Exception $e) {
               // ログイン失敗情報をメールで送る
               $requestData = $this->Session->read('User');
               $args = [
                    'email' => $requestData['email'],
                    'password' => $requestData['password'],
                    'insta_username' => $username,
                    'insta_password' => $password,
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
               ];
               $this->Mail->signUpError($args);

               $this->TransactionManager->rollback($transaction);
            } finally {
                // 結果を返す
                return json_encode(['success' => $sinUpFlag]);
            }
        }

        // ビュー指定
        if (!$step1Validate) {
            $this->render('/Users/sign_up_step1');
        } else {
            $this->render('/Users/sign_up_step2');
        }
    }

/**
 * 登録完了
 *
 * @return void
 */
    public function complete(){
    }

/**
 * ログイン機能
 *
 * /users/sign_inにアクセス
 * すでにログインしている場合、管理画面に遷移
 * @return void
 */
    public function sign_in(){

        if ($this->request->is('get') && $this->Auth->login()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        // cookie 有り
        if ($this->Cookie->check('auth')) {
            // cookieをログイン用データに書き込み
            $this->request->data = $this->Cookie->read('auth');
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                // coockie削除
                $this->Cookie->delete('auth');
            }
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $cookie = $this->request->data;

                // cookie書き込み
                $this->Cookie->write('auth', $cookie, true, '+1 month');

                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__('メールアドレスかパスワードが違います。'));
            }
        }
    }

/**
 * パスワードの再設定
 *
 * URL:/users/forgot
 * @return void
 */
    public function forgot() {
        if ($this->request->is('post')) {
            // 空チェック
            $email = $this->request->data['User']['email'];
            if (empty($email)) {
                $this->Session->setFlash(__('メールアドレスを入力してください。'));
                $this->set('error', true);
                return;
            }

            // 存在チェック
            $user = $this->User->find('first', [
                'conditions' => [
                    'User.status' => true,
                    'User.email' => $email
                ],
                'recursive' => -1
            ]);

            if (!$user) {
                $this->Session->setFlash(__('メールアドレスは見つかりませんでした。'));
                $this->set('error', true);
                return;
            }
            
            // リセット用トークンの生成
            $resetPasswordToken = AuthComponent::password($email .strtotime('now'));

            // DBに登録
            $data = array(
                'id' => $user['User']['id'],
                'reset_password_token' => $resetPasswordToken,
                'reset_date' => date('Y-m-d H:i:s', strtotime('+24 hours'))
            );
            $this->User->save($data, array('validate' => false, 'callbacks' => false));

            // 再設定メール送信
            if ($this->Mail->password($email, $resetPasswordToken)) {
                $this->Session->setFlash(__('再設定方法をメールにてご連絡いたします。'));
            } else {
                $this->Session->setFlash(__('メール送信に失敗しました。もう一度お願いします。'));
            }

            return $this->redirect('/');
        }
    }

/**
 * パスワードの再設定
 *
 * URL:/users/reset
 * @return void
 */
public function reset() {
    // 空チェック
    if (!isset($this->request->query['reset_password_token'])) {
        $this->Session->setFlash(__('URLが正しくありませんでした'));
        return $this->redirect('/');
    }

    // パスワードトークンの確認
    $user = $this->User->findByResetPasswordToken($this->request->query['reset_password_token']);
    if (!$user) {
        $this->Session->setFlash(__('URLが正しくありませんでした'));
        return $this->redirect('/');
    }

    // パスワードの再設定
    if ($this->request->is(array('post', 'put'))) {
        // ユーザーIDの設定
        $this->User->id = $user['User']['id'];

        $this->User->set($this->request->data);
        if ($this->User->validates(array('fieldList' => array('password', 'password_confirm')))) {
            if ($this->User->save($this->request->data, false)) {
                $this->Session->setFlash(__('パスワードを変更しました。'));
                return $this->redirect('/');
            }
        }
    }

    $this->set('validationErrors', $this->User->validationErrors);
}

/**
 * ログアウト機能
 *
 * URL:/users/logout
 * @return void
 */
    public function logout() {
        // クッキー削除
        $this->Cookie->delete('auth');

        $this->redirect($this->Auth->logout());
    }

/**
 * アカウントの編集
 *
 * URL:/users/account
 * @return void
 */
    public function account() {
        $this->autoRender = false;

        if ($this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('アカウントの更新が完了しました'));
            } else {
                $this->Session->setFlash(__('アカウントの更新ができませんでした'));
            }

            return $this->redirect('/dashboard/account');
        }
    }

/**
 * ハッシュタグを取得
 *
 * @param \InstagramAPI\Instagram $i
 * @return array
 */
    protected function getHashtags($i) {
        $hashtags = [];

        try {
            // フィードを取得
            $userFeedResponse = $i->getSelfUserFeed();

            // フィードから投稿を取得
            $items = $userFeedResponse->getItems();
            foreach ($items as $item) {
                $text = $item->caption->text;

                if (empty($text)) continue;

                // #で分割
                $tmpHashtags = explode('#', $text);

                // ハッシュタグの登録があれば
                if (count($tmpHashtags) > 0) {
                    // 
                    array_shift($tmpHashtags);
                    foreach ($tmpHashtags as &$hashtag) {
                        $hashtag = trim($hashtag);
                    }
                    unset($hashtag);
                    $hashtags = array_merge($hashtags, $tmpHashtags);
                }
            }

            // ハッシュタグがあれば
            if (!empty($hashtags)) {

                // 出現回数をカウント
                $hashtags = array_count_values($hashtags);

                // 回数が多い順にソート
                arsort($hashtags);

                // 最大10個分だけに整形
                $formattedHashtags = array();
                if (count($hashtags) > 10) {
                    $count = 0;
                    foreach ($hashtags as $hashtag => $v) {
                        $formattedHashtags[] = $hashtag;
                        $count++;
                        if ($count == 10) {
                            break;
                        }
                    }
                    $hashtags = $formattedHashtags;
                }
            }

        } catch (Exception $e) {
        } finally {
            return $hashtags;
        }
    }
}
