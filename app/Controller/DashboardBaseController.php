<?php

App::uses('AppController', 'Controller');

/**
 * ダッシュボード ベースコントローラー
 *
 *
 * @package     app.Controller
 */
class DashboardBaseController extends AppController {

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'DebugKit.Toolbar',
        'Session',
        'Cookie' => [
            'name' => 'bang'
        ],
        'Flash',
        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'scope' => [
                        'User.status' => true
                    ]
                ]
            ],
            'loginError' => 'パスワードもしくはログインIDをご確認下さい。',
            'authError' => 'ご利用されるにはログインが必要です。',
            'loginAction' => '/users/sign_in',
            'loginRedirect' => '/dashboard',
            'logoutRedirect' => '/',
        ],
    ];

/**
 * インスタグラムアカウント
 *
 * @var array
 */
    public $instagramAccount;

/**
 * インスタグラムアカウント番号
 *
 * @var array
 */
    public $instagramId;

/**
 * ユーザー情報
 *
 * @var array
 */
    public $userinfo;

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->set('user', $this->Auth->user());

        // 現在のインスタグラムアカウント情報を取得
        $this->loadModel('Instagram');
        $this->instagramAccount = $this->Instagram->fetchAccount($this->Auth->user('id'));

        // 削除チェック
        if ($this->instagramAccount['Instagram']['status'] == 0) {
            $this->Session->setFlash(__('このユーザーはご利用いただけません。'));
            $this->Cookie->delete('auth');
            return $this->redirect($this->Auth->logout());
        }

        $this->set('instagramAccount', $this->instagramAccount);

        $this->instagramId = $this->instagramAccount['Instagram']['id'];

        // インスタグラムの認証エラーチェック
        $this->authErrorCheck();

        // プランの期限チェック
        $this->expirationCheck($this->Auth->user('id'));

        // 各種登録の件数設定
        $this->jsVars['hashtag'] = Configure::read('hashtag_max_count');
        $this->jsVars['comment'] = Configure::read('comment_max_count');
        $this->jsVars['filter'] = Configure::read('filter_max_count');
        $this->jsVars['location'] = Configure::read('location_max_count');
        $this->jsVars['message'] = Configure::read('message_max_count');
    }

/**
 * インスタグラムの認証エラーチェック
 *
 * @param int $userId
 */
    public function authErrorCheck() {
        $this->set('auth_error', !(bool)$this->instagramAccount['Instagram']['instagram_auth']);

        // 認証エラーチェックのアラートを非表示にしたい場合は、コントローラーでこの値を上書き
        $this->set('auth_error_off', false);
    }

/**
 * 有効期限チェック
 *
 * @param int $userId
 */
    public function expirationCheck($userId) {
        $this->loadModel('User');
        $this->userinfo = $this->User->expirationCheck($this->Auth->user('id'));
        $this->set('expiration', $this->userinfo['expiration']);
        $this->set('plan_id', $this->userinfo['plan_id']);
        $this->set('cus_id', $this->userinfo['cus_id']);
        $this->set('car_id', $this->userinfo['car_id']);
        $this->set('activate', $this->userinfo['activate']);

        // 有効期限のアラートを非表示にしたい場合は、コントローラーでこの値を上書き
        $this->set('expiration_off', false);
    }

}
