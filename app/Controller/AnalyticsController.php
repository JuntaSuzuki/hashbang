<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * 分析・解析 コントローラー
 *
 *
 * @package     app.Controller
 */
class AnalyticsController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = array('Analytics');

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * フォロワー数
 *
 * @return string JSONデータ
 */
    public function followBy() {
        $this->apiSetting();
        $this->option['user_id'] = $this->Auth->user('id');
        return $this->Analytics->followBy($this->option);
    }

/**
 * フォロー数
 *
 * @return string JSONデータ
 */
    public function follow() {
        $this->apiSetting();
        $this->option['user_id'] = $this->Auth->user('id');
        return $this->Analytics->follow($this->option);
    }

/**
 * 自動いいね
 *
 * @return string JSONデータ
 */
    public function autoLike() {
        $this->apiSetting();
        $this->option['user_id'] = $this->Auth->user('id');
        $this->option['type'] = 1;
        return $this->Analytics->activity($this->option);
    }

/**
 * 自動フォロー
 *
 * @return string JSONデータ
 */
    public function autoFollow() {
        $this->apiSetting();
        $this->option['user_id'] = $this->Auth->user('id');
        $this->option['type'] = 2;
        return $this->Analytics->activity($this->option);
    }

/**
 * コメント
 *
 * @return string JSONデータ
 */
    public function comment() {
        $this->apiSetting();
        return $this->Activity->fetchComments($this->params->query['user_id'], 'json');
    }

    private function apiSetting() {
        // ビューを使用しない
        $this->autoRender = false;

        // API用のオプション値を設定
        $period = [
            1 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-3 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '0 day')) . ' 00:00:00',
            ],
            2 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-7 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '0 day')) . ' 00:00:00',
            ],
            3 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-30 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '0 day')) . ' 00:00:00',
            ]
        ];

        $this->option = [
            'since' => $period[$this->data['period']]['since'],
            'until' => $period[$this->data['period']]['until'],
            'instagram_id' => $this->instagramId,
            'type' => '',
            'json'  => true // JSON形式で返す
        ];
    }

/**
 * アナリティクス
 *
 * @return string JSONデータ
 */
    public function analytics() {
        $this->autoRender = false;

        // API用のオプション値を設定
        $period = [
            1 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-4 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ],
            2 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-8 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ],
            3 => [
                'since' => date('Y-m-d', strtotime(date('Y-m-d') . '-31 day')) . ' 00:00:00',
                'until' => date('Y-m-d', strtotime(date('Y-m-d') . '-1 day')) . ' 00:00:00',
            ]
        ];

        $option = [
            'since' => $period[$this->data['period']]['since'],
            'until' => $period[$this->data['period']]['until'],
            'instagram_id' => $this->instagramId,
            'type' => $this->data['type'],
            'json'  => true // JSON形式で返す
        ];

        switch ($this->data['type']) {
            case LIKE:
            case FOLLOW:
            case COMMENT:
            case UNFOLLOW:
                return $this->Analytics->activity($option);
                break;

            case FOLLOWER_COUNT:
                return $this->Analytics->follower($option);
                break;

            case FOLLOWING_COUNT:
                return $this->Analytics->following($option);
                break;
        }
    }

/**
 * ハッシュタグ解析
 *
 * @return string HTML
 */
    public function hashtag() {
        $this->autoRender = false;
        $this->layout = false;

        $analytics = $this->Analytics->hashtag($this->instagramId, $this->data['period']);
        $this->set('analytics', $analytics);
        $this->render('/Elements/Dashboard/hashtag_analytics');
    }

}
