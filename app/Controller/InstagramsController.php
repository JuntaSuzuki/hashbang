<?php

App::uses('AppController', 'Controller');

class InstagramsController extends AppController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = [
        'Instagram'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * インスタグラムへのログイン
 *
 * URL:/instagrams/login
 * @return string JSONデータ
 */
    public function login() {
        $this->autoRender = false;

        // HTTPチェック Ajax以外リクエスト禁止
        if (!$this->request->is('ajax')) {
            throw new ForbiddenException();
        }

        $this->loadModel('Instagram');
        $instagram = $this->Instagram->fetchAccount($this->Auth->user('id'));

        // バリデーションチェック
        if ($this->data['validate']) {
            if ($instagram['Instagram']['username'] != $this->data['username']) {
                return json_encode(['success' => $this->validate()]);
            }
            return json_encode(['success' => true]);
        }

        // インンスタグラムアカウント情報がない場合
        if (!$instagram) {
            $this->log('インンスタグラムアカウント情報がない場合', SIGNUP);
            return json_encode(['success' => false]);
        }

        try {
            $username = $this->data['username'];
            $password = $this->data['password'];

            // 空チェック
            if (empty($username) || empty($password)) {
                $this->log('空チェック', SIGNUP);
                return json_encode(['success' => false]);
            }

            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);

            // ログインチェック
            try {
                $i->login(true);
                $userinfo = $i->getSelfUserInfo();
            } catch (Exception $e) {
                $this->log($e->getMessage(), SIGNUP);
                return json_encode(['success' => false]);
            }

            // インンスタグラムアカウント情報を更新
            $data = [
                'id' => $instagram['Instagram']['id'],
                'user_id' => $this->Auth->user('id'),
                'username' => $username,
                'password' => $password,
                'username_id' => $userinfo->user->pk,
                'profile_picture' => $userinfo->user->profile_pic_url,
                'follower_count' => $userinfo->user->follower_count,
                'following_count' => $userinfo->user->following_count,
                'instagram_auth' => true,
                'instagram_auth_date' => date('Y-m-d H:i:s')
            ];
            if (!$this->Instagram->save($data)) {
                $this->log($data);
                throw new Exception('インンスタグラムアカウント情報の更新失敗');
            }

            $this->Session->setFlash(__('アカウントの設定が完了しました'));

            // 全て成功
            return json_encode(['success' => true]);

        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(['success' => false]);
        }
    }

/**
 * バリデーション
 *
 * @return string JSONデータ
 */
    public function validate($isUnique = true) {
        $check = [];

        // バリデーション項目の設定
        $this->Instagram->set([
            'user_id' => $this->Auth->user('id'),
            'username' => $this->data['username']
        ]);

        if ($isUnique) {
            unset($this->Instagram->validate['username']['isUnique']);
        }

        if (!$this->Instagram->validates()) {
            $errors = $this->Instagram->validationErrors;
            foreach ($errors as $key => $error) {
                $check[$key] = $error;
            }
        }

        // エラーがあった場合
        if (!empty($check)) {
            return json_encode(['success' => false, 'data' => $check]);
        }

        // エラーがない場合
        return json_encode(['success' => true]);
    }

}
