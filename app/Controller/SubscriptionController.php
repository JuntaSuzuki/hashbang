<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

/**
 * 支払い コントローラー
 *
 *
 * @package     app.Controller
 */
class SubscriptionController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
    public $uses = [];

/**
 * コンポーネント読み込み
 *
 * @var array
 */
    public $components = [
        'Auth',
        'Mail'
    ];

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * プラン購入（初めて）
 *
 * URL：/subscription/pay
 *
 * @return void
 */
    public function pay() {
        // 支払い以外のアクセスは弾く
        if ($this->request->is('get')) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is('post') && isset($this->request->data['payjp-token'])) {

            // 送られてきた、顧客のカード情報を使って作成されたトークン
            $token = $this->request->data['payjp-token'];

            // プランID（#BANGのplansテーブルのID）
            $planId = $this->request->data['payjp-plan-id'];

            // プランを取得
            $this->loadModel('Plan');
            $plan = $this->Plan->findById($planId);

            // プランがない場合（不正なPOST）
            if (!$plan) {
                throw new NotFoundException(__('Invalid post'));
            }

            // 秘密鍵
            $secret = Configure::read('payjp-secret');

            try {
                Payjp\Payjp::setApiKey($secret);

                // descriptionの作成
                $format = "ユーザー番号:%s メールアドレス:%s 購入プラン:%s(%s日ごとに更新)";
                $description = sprintf(
                    $format,
                    $this->Auth->user('id'),
                    $this->Auth->user('email'),
                    $plan['Plan']['name'],
                    $plan['Plan']['day']
                );

                // 顧客を作成し、カード情報を登録
                $customer = Payjp\Customer::create([
                    'email'       => $this->Auth->user('email'),
                    'card'        => $token,
                    'description' => $description,
                ]);

                // 課金リクエスト
                $result = Payjp\Charge::create([
                    'customer'    => $customer->id,
                    'amount'      => $plan['Plan']['amount'],
                    'currency'    => 'jpy',
                    'description' => $description
                ]);

                // 課金情報
                $this->log($result, SUBSCRIPTION);

                if (isset($result->error)) {
                    throw new Exception();
                }


                try {
                    // トランザクション開始
                    $this->loadModel('TransactionManager');
                    $transaction = $this->TransactionManager->begin();

                    // 契約開始日
                    $contract_start_date = date('Y/m/d');

                    // 契約終了日
                    $contract_end_date = date('Y/m/d', strtotime('+' . ($plan['Plan']['day'] - 1). ' day'));

                    // 今回の課金情報を登録
                    $this->loadModel('Payment');
                    $this->Payment->create();
                    $data = [
                        'user_id'        => $this->Auth->user('id'),
                        'amount'         => $result->amount,
                        'plan_id'        => $planId,
                        'status'         => true,
                        'payment_status' => true,
                        'contract_start_date' => $contract_start_date,
                        'contract_end_date' => $contract_end_date,
                        'charge_date'    => date('Y/m/d H:i:s', $result->captured_at),
                        'payjp_cus_id'   => $result->card->customer,
                        'payjp_car_id'   => $result->card->id,
                        'payjp_ch_id'    => $result->id
                    ];
                    $this->Payment->save($data);


                    // 次回契約開始日
                    $date = new DateTime($contract_end_date);
                    $next_contract_start_date = $date->modify('+1 days')->format('Y-m-d');

                    // 次回契約終了日
                    $next_contract_end_date = $date->modify('+' . ($plan['Plan']['day'] - 1) . ' days')->format('Y-m-d');

                    // 次回課金日
                    $next_charge_date = $next_contract_start_date;

                    // 次回の課金情報を登録
                    $this->Payment->create();
                    $data = [
                        'user_id'        => $this->Auth->user('id'),
                        'amount'         => $result->amount,
                        'plan_id'        => $planId,
                        'status'         => false,
                        'payment_status' => false,
                        'contract_start_date' => $next_contract_start_date,
                        'contract_end_date' => $next_contract_end_date,
                        'charge_date'    => $next_charge_date,
                        'payjp_cus_id'   => $result->card->customer,
                        'payjp_car_id'   => $result->card->id,
                    ];
                    $this->Payment->save($data);


                    // 有効期限日
                    $plan_limit_date = $contract_end_date;

                    // 課金情報を更新
                    $this->loadModel('User');
                    $data = [
                        'id'               => $this->Auth->user('id'),
                        'plan_id'          => $planId,
                        'plan_limit_date'  => $plan_limit_date,
                        'next_charge_date' => $next_charge_date,
                        'cus_id'           => $result->card->customer,
                        'car_id'           => $result->card->id,
                    ];
                    $this->User->save($data, ['callbacks' => false]);

                    // コミット
                    $this->TransactionManager->commit($transaction);

                    // メール送信
                    $params = [
                        'plan' => $plan['Plan']['name'],
                        'period' => $plan['Plan']['day'] . '日',
                        'contract_start_date' => $contract_start_date,
                        'contract_end_date' => $contract_end_date,
                        'amount' => number_format($result->amount)
                    ];
                    $this->Mail->payment($this->Auth->user('email'), $params);

                    $this->Session->setFlash(__('支払いが完了しました'));

                    // A8用の注文番号と経由情報を設定
                    $user = $this->User->find('first', [
                        'fields' => [
                            'a8_code', 'cid'
                        ],
                        'conditions' => [
                            'User.id' => $this->Auth->user('id')
                        ],
                        'recursive' => -1
                    ]);
                    $orderId = date('Ymd') . '-' . $this->Auth->user('id');
                    $this->Session->write('a8_order_id', $orderId);
                    $this->Session->write('a8_code', $user['User']['a8_code']);

                    // #BANGアフィリエイト トラッキング実行
                    if (!is_null($user['User']['cid'])) {
                        $HttpSocket = new HttpSocket();
                        $HttpSocket->get('https://affiliate.hashbang.jp/track.php', [
                            'p' => '58ffcf0b06cbd',
                            't' => '58ffcf0b',
                            'cid' => $user['User']['cid'],
                            'price' => $result->amount,
                            'args' => $orderId
                        ]);
                    }

                    // 経由AS情報を削除
                    $data = [
                        'id' => $this->Auth->user('id'),
                        'a8_code' => NULL
                    ];
                    $this->User->save($data, ['callbacks' => false]);
                } catch (Exception $e) {
                    $this->SlackWebhook->send($e->getMessage() . "\n" . $e->getTraceAsString());
                    $this->TransactionManager->rollback($transaction);
                }
            } catch(Payjp\Error\Card $e) {
                $error = $this->parseError($e);
            } catch (Payjp\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Payjp's API
                $error = $this->parseError($e);
            } catch (Payjp\Error\Authentication $e) {
                // Authentication with Payjp's API failed
                $error = $this->parseError($e);
            } catch (Payjp\Error\ApiConnection $e) {
                // Network communication with Payjp failed
                $error = $this->parseError($e);
            } catch (Payjp\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $error = $this->parseError($e);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Payjp
                $error = $this->parseError($e);
            }

            // エラーがある場合
            if (!empty($error)) {
                $this->log($error['error'], SUBSCRIPTION);
                $this->Session->setFlash(__('支払いが完了しました'));
            }

            return $this->redirect($this->referer());
        }
    }

/**
 * 自動更新を設定する
 *
 * URL：/subscription/auto/:status
 *
 * @return void
 */
    public function auto($status = null) {
        // HTTPチェック
        if (!$this->request->is('post')) {
            $this->Session->setFlash(__('自動更新設定に失敗しました'));
            return $this->redirect($this->referer());
        }

        // パラメータチェック
        if (is_null($status)) {
            $this->Session->setFlash(__('自動更新設定に失敗しました'));
            return $this->redirect($this->referer());
        }

        // 自動更新ステータスの更新
        $this->loadModel('User');
        $data = [
            'id' => $this->Auth->user('id'),
            'auto_subscription_status' => $status
        ];

        if ($this->User->save($data, ['callbacks' => false])) {
            $this->Session->setFlash(__('自動更新設定が完了しました'));
        } else {
            $this->Session->setFlash(__('自動更新設定に失敗しました'));
        }

        return $this->redirect($this->referer());
    }

/**
 * プラン購入（2回目以降）
 *
 * URL：/subscription/payPlan
 *
 * @return void
 */
    public function payPlan($planId = null) {
        // 支払い以外のアクセスは弾く
        if ($this->request->is('get')) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is('post')) {

            // パラメータチェック
            if (empty($planId)) {
                throw new NotFoundException(__('Invalid post'));
            }

            // プランを取得
            $this->loadModel('Plan');
            $plan = $this->Plan->findById($planId);

            // プランがない場合（不正なPOST）
            if (!$plan) {
                throw new NotFoundException(__('Invalid post'));
            }

            // 秘密鍵
            $secret = Configure::read('payjp-secret');

            try {
                Payjp\Payjp::setApiKey($secret);

                // descriptionの作成
                $format = "ユーザー番号:%s メールアドレス:%s 購入プラン:%s(%s日ごとに更新)";
                $description = sprintf(
                    $format,
                    $this->Auth->user('id'),
                    $this->Auth->user('email'),
                    $plan['Plan']['name'],
                    $plan['Plan']['day']
                );

                // 課金リクエスト
                $result = Payjp\Charge::create([
                    'customer'    => $this->Auth->user('cus_id'),
                    'amount'      => $plan['Plan']['amount'],
                    'currency'    => 'jpy',
                    'description' => $description
                ]);

                // 課金情報
                $this->log($result, SUBSCRIPTION);

                if (isset($result->error)) {
                    throw new Exception();
                }

                try {
                    // トランザクション開始
                    $this->loadModel('TransactionManager');
                    $transaction = $this->TransactionManager->begin();

                    // 契約開始日
                    $contract_start_date = date('Y/m/d');

                    // 契約終了日
                    $contract_end_date = date('Y/m/d', strtotime('+' . ($plan['Plan']['day'] - 1). ' day'));

                    // 今回の課金情報を登録
                    $this->loadModel('Payment');
                    $this->Payment->create();
                    $data = [
                        'user_id'        => $this->Auth->user('id'),
                        'amount'         => $result->amount,
                        'plan_id'        => $planId,
                        'status'         => true,
                        'payment_status' => true,
                        'contract_start_date' => $contract_start_date,
                        'contract_end_date' => $contract_end_date,
                        'charge_date'    => date('Y/m/d H:i:s', $result->captured_at),
                        'payjp_cus_id'   => $result->card->customer,
                        'payjp_car_id'   => $result->card->id,
                        'payjp_ch_id'    => $result->id
                    ];
                    $this->Payment->save($data);


                    // 次回契約開始日
                    $date = new DateTime($contract_end_date);
                    $next_contract_start_date = $date->modify('+1 days')->format('Y-m-d');

                    // 次回契約終了日
                    $next_contract_end_date = $date->modify('+' . ($plan['Plan']['day'] - 1) . ' days')->format('Y-m-d');

                    // 次回課金日
                    $next_charge_date = $next_contract_start_date;

                    // 次回の課金情報を登録
                    $this->Payment->create();
                    $data = [
                        'user_id'        => $this->Auth->user('id'),
                        'amount'         => $result->amount,
                        'plan_id'        => $planId,
                        'status'         => false,
                        'payment_status' => false,
                        'contract_start_date' => $next_contract_start_date,
                        'contract_end_date' => $next_contract_end_date,
                        'charge_date'    => $next_charge_date,
                        'payjp_cus_id'   => $result->card->customer,
                        'payjp_car_id'   => $result->card->id,
                    ];
                    $this->Payment->save($data);


                    // 有効期限日
                    $plan_limit_date = $contract_end_date;

                    // 課金情報を更新
                    $this->loadModel('User');
                    $data = [
                        'id'               => $this->Auth->user('id'),
                        'plan_id'          => $planId,
                        'plan_limit_date'  => $plan_limit_date,
                        'next_charge_date' => $next_charge_date,
                    ];
                    $this->User->save($data, ['callbacks' => false]);

                    // コミット
                    $this->TransactionManager->commit($transaction);

                    // メール送信
                    $params = [
                        'plan' => $plan['Plan']['name'],
                        'period' => $plan['Plan']['day'] . '日',
                        'contract_start_date' => $contract_start_date,
                        'contract_end_date' => $contract_end_date,
                        'amount' => number_format($result->amount)
                    ];
                    $this->Mail->payment($this->Auth->user('email'), $params);

                    $this->Session->setFlash(__('支払いが完了しました'));
                } catch (Exception $e) {
                    $this->SlackWebhook->send($e->getMessage() . "\n" . $e->getTraceAsString());
                    $this->TransactionManager->rollback($transaction);
                }
            } catch(Payjp\Error\Card $e) {
                $error = $this->parseError($e);
            } catch (Payjp\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Payjp's API
                $error = $this->parseError($e);
            } catch (Payjp\Error\Authentication $e) {
                // Authentication with Payjp's API failed
                $error = $this->parseError($e);
            } catch (Payjp\Error\ApiConnection $e) {
                // Network communication with Payjp failed
                $error = $this->parseError($e);
            } catch (Payjp\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $error = $this->parseError($e);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Payjp
                $error = $this->parseError($e);
            }

            // エラーがある場合
            if (!empty($error)) {
                $this->log($error['error'], SUBSCRIPTION);
                $this->Session->setFlash(__('支払いに失敗しました'));
            }

            return $this->redirect($this->referer());
        }
    }

/**
 * PAY.JPのエラーをパース
 *
 * @param Exception $e
 * @return array
 */
    protected function parseError($e) {
        $body = $e->getJsonBody();
        $err  = $body['error'];
        $errorLog  = 'Status is:' . $e->getHttpStatus() . "\n";
        $errorLog .= 'Type is:' . $err['type'] . "\n";
        $errorLog .= 'Code is:' . $err['code'] . "\n";
        // param is '' in this case
        $errorLog .= 'Param is:' . $err['param'] . "\n";
        $errorLog .= 'Message is:' . $err['message'] . "\n";

        $error = [
            'code' => $err['code'],
            'error' => $errorLog
        ];

        return $error;
    }

}
