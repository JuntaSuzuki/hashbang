<?php

App::uses('AppController', 'Controller');

/**
 * 友達招待 コントローラー
 *
 *
 * @package		app.Controller
 */
class InvitesController extends AppController {

/**
 * 各アクションの前に実行
 *
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('code');
	}

/**
 * 友達を招待確認
 *
 * URL:/invites/code/$code
 * @return void
 */
	public function code($code = null) {
		if (!$code) {
			throw new NotFoundException(__('ページが見つかりません'));
		}

		$this->loadModel('User');
		$invite = $this->User->find('first', array(
			'conditions' => array(
				'User.invite_code' => $code,
			)
		));

		if (!$invite) {
			throw new NotFoundException(__('ページが見つかりません'));
		}

		// Cookieに招待コードをセット
		$this->Cookie->write('invite_code', $code);
		$this->redirect(Router::url('/'));
	}

}