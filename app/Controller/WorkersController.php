<?php
/**
 * Created by IntelliJ IDEA.
 * User: kenc
 * Date: 2017/02/26
 * Time: 15:58
 */

App::uses('AppController', 'Controller');

/**
 * ワーカー コントローラー
 *
 *
 * @package   app.Controller
 */
class WorkersController extends AppController {
    public $uses = [
        'Instagram',
        'Activity',
        'Filter'
    ];

    public $components = [
        'SlackWebhook' => [
          'webhook_url' => 'https://hooks.slack.com/services/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'
        ],
        'Mail',
        'Schedule',
        'Subscription',
        'Analytics'
    ];

    public $autoRender = false;
    public $autoLayout = false;

/**
 * 各アクションの前に実行
 *
 */
    public function beforeFilter() {
        $this->Auth->allow();
    }

/**
 * テスト用
 *
 */
    public function test($type = null, $instagram_id = 1) {

        if (is_null($type)) return;

        $message = [
            //'type' => 'hashtag_like',
            //'type' => 'hashtag_follow',
            //'type' => 'hashtag_comment',
            //'type' => 'location_like',
            //'type' => 'location_follow',
            //'type' => 'location_comment',
            //'type' => 'unfollow',
            //'type' => 'bulk_unfollow',
            //'type' => 'message',
            //'type' => 'followback',
            //'type' => 'follower_check',
            //'type' => 'following_check',
            //'type' => 'account_check',
            //'type' => 'following_follower_check',
            //'type' => 'schedule',
            //'type' => 'analytics',
            //'test' => 'check',
            'type' => $type,
            'instagram_id' => $instagram_id,
            'unfollow_day' => 1,
            'datetime' => date('Y-m-d H:i:s')
        ];

        // タイプによって処理を分ける
        $this->switchAction($message);

        // try {

        //     $this->loadModel('Follower');
        //     $followers = $this->Follower->fetchNoFollowBack(1);
        //     print_r($followers);
        //     exit;

        //     $ig = new \InstagramAPI\Instagram(false, false);

        //     //exit;
        //     $ig->setUser('fil__ios', 'y4t6gGdT4AlR');
        //     $ig->login();

        //     // $i = new \InstagramAPI\Instagram(false, false);
        //     // $i->setUser($username, $password);
        //     // $i->login();

        //     $followers = [];
        //     // Starting at "null" means starting at the first page.
        //     $maxId = null;
        //     do {
        //         // Request the page corresponding to maxId.
        //         $response = $ig->getSelfUserFollowers($maxId);
        //         // In this example we're merging the response array, but we can do anything.
        //         $followers = array_merge($followers, $response->getUsers());
        //         // Now we must update the maxId variable to the "next page".
        //         // This will be a null value again when we've reached the last page!
        //         // And we will stop looping through pages as soon as maxId becomes null.
        //         $maxId = $response->getNextMaxId();
        //     } while ($maxId !== null);

        //     foreach ($followers as $follower) {
        //         //$this->log($follower->pk);
        //         $response = $ig->getUserFriendship($follower->pk);
        //         $this->log($response);

        //         // すでにフォローしているか
        //         if ($response->following) {
        //             continue;
        //         }

        //         // フォローバック実行
        //         $ig->follow($follower->pk);
        //         //$this->log($response->following);
        //         //echo '- '.$follower->getUsername().".\n";
        //         //$this->log($follower);
        //         exit;
        //     }
        //     // フォロワーを取得
        //     //$followers = $i->getSelfUserFollowers()->getFollowers();

        //     //$userId = $i->getUsernameId('filiosclothes');

        //     // $this->log($userId);
        //     // $info = $i->getUsernameInfo($userId);
        //     // $this->log($info);

        //     // $userId = $i->getUsernameId('kanekokatsumi1');
        //     // $this->log($userId);
        //     // $info = $i->getUsernameInfo($userId);
        //     // $this->log($info);

        //     $recipients[] = $userId;
        //     $text = 'フォローありがとうございます';
        //     $i->directMessage($recipientsm, $text);

        // } catch(Exception $e) {
        //     echo 'something went wrong '.$e->getMessage()."\n";
        //     exit(0);
        // }
        // exit;
    }

/**
 * 確認用
 *
 */
    public function check($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // インスタグラムアカウント取得
        $res = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            'recursive' => -1
        ]);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィードを取得
            $userFeedResponse = $i->getSelfUserFeed();

            $hashtags = [];

            // フィードから投稿を取得
            $items = $userFeedResponse->getItems();
            foreach ($items as $item) {
                $text = $item->caption->text;

                if (empty($text)) continue;

                // #で分割
                $tmpHashtags = explode('#', $text);

                // ハッシュタグの登録があれば
                if (count($tmpHashtags) > 0) {
                    // 
                    array_shift($tmpHashtags);
                    foreach ($tmpHashtags as &$hashtag) {
                        $hashtag = trim($hashtag);
                    }
                    unset($hashtag);
                    $hashtags = array_merge($hashtags, $tmpHashtags);
                }
            }

            $this->log("====================================");
            $this->log($hashtags);

            // ハッシュタグがあれば
            if (!empty($hashtags)) {

                // 出現回数をカウント
                $hashtags = array_count_values($hashtags);

                // 回数が多い順にソート
                arsort($hashtags);

                // 最大10個分だけに整形
                $formattedHashtags = array();
                if (count($hashtags) > 10) {
                    $count = 0;
                    foreach ($hashtags as $hashtag => $v) {
                        $formattedHashtags[] = $hashtag;
                        $count++;
                        if ($count == 10) {
                            break;
                        }
                    }
                    $hashtags = $formattedHashtags;
                }
            }
            $this->log("====================================");
            $this->log($hashtags);
            exit;

            $this->log($userFeedResponse);
            // // ハッシュタグをランダムに１つ取得
            // $key = array_rand($hashtags);
            // $hashtag = $hashtags[$key];

            // // ハッシュタグのあるフィードを取得
            // $feed = $i->getHashtagFeed($hashtag['name']);

            // // フィードから投稿を取得
            // $items = $feed->getItems();
            // foreach ($items as $item) {
            //     // 投稿者を取得
            //     $itemUser = $item->getUser();

            //     // 自分の投稿だったらスキップ
            //     if ($username == $itemUser->getUsername()) continue;

            //     // フィルターチェック
            //     if (!$this->filterCheck($item, $filters)) continue;

            //     // すでにいいねしていたらスキップ
            //     $isExistAction = $this->Activity->hasAny([
            //         'instagram_id' => $instagramId,
            //         'type' => LIKE,
            //         'media_id' => $item->id
            //     ]);
            //     if ($isExistAction) continue;

            //     // 投稿画像取得
            //     $photos = $item->image_versions2->candidates;

            //     // 画像が取得できない場合はスキップ
            //     if (is_null($photos)) continue;

            //     // 投稿画像を1枚取得
            //     $photo = end($photos);

            //     // いいね実行
            //     $response = $i->like($item->id);

            //     // いいねに成功したら、アクティビティの登録
            //     if ($response->status == 'ok') {
            //         $this->Activity->create();
            //         $data = [
            //             'instagram_id'          => $instagramId,
            //             'mtb_hashtag_id'        => $hashtag['id'],
            //             'type'                  => LIKE,
            //             'username'              => $itemUser->username,
            //             'full_name'             => $itemUser->full_name,
            //             'profile_picture'       => $itemUser->profile_pic_url,
            //             'instagram_username_id' => $itemUser->pk,
            //             'media_id'              => $item->id,
            //             'media_picture_url'     => $photo->url,
            //             'media_url'             => 'https://www.instagram.com/p/' . $item->code,
            //             'action_date'           => date("Y-m-d H:i:s")
            //         ];
            //         if ($this->Activity->save($data)) return 200;
            //     }
            // }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * SQSのメッセージがPOSTされる
 *
 * URL:/workers/index
 *
 */
    public function index() {
        $message = file_get_contents('php://input');
        $message = json_decode($message, true);

        // タイプによって処理を分ける
        $this->switchAction($message);

        return 200;
    }

/**
 * アクションを切り替える
 *
 * @param array $message
 *
 */
    public function switchAction($message) {
        // タイプによって処理を分ける
        switch ($message['type']) {
            // ハッシュタグからいいねを行う
            case 'hashtag_like':
                $this->hashtagLike($message);
                break;

            // ハッシュタグからフォローを行う
            case 'hashtag_follow':
                $this->hashtagFollow($message);
                break;

            // ハッシュタグからコメントを行う
            case 'hashtag_comment':
                $this->hashtagComment($message);
                break;

            // ロケーションからいいねを行う
            case 'location_like':
                $this->locationLike($message);
                break;

            // ロケーションからフォローを行う
            case 'location_follow':
                $this->locationFollow($message);
                break;

            // ロケーションからコメントを行う
            case 'location_comment':
                $this->locationComment($message);
                break;

            // フォロー解除を行う
            case 'unfollow':
                $this->unfollow($message);
                break;

            // 一括フォロー解除を行う
            case 'bulk_unfollow':
                $this->bulkUnfollow($message);
                break;

            // メッセージを行う
            case 'message':
                $this->message($message);
                break;

            // フォローバックを行う
            case 'followback':
                $this->followback($message);
                break;

            // フォロワー情報の更新を行う
            case 'follower_check':
                $this->followerCheck($message);
                break;

            // フォロー情報の更新を行う
            case 'following_check':
                $this->followingCheck($message);
                break;

            // アカウント情報の更新を行う
            case 'account_check':
                $this->accountCheck($message);
                break;

            // フォロー数とフォロワー数の更新を行う
            case 'following_follower_check':
                $this->followingFollowerCheck($message);
                break;


            // ハッシュタグ解析
            case 'analytics':
                $this->Analytics->hashtag($message);
                break;


            // スケジュール生成
            case 'schedule':
                $this->Schedule->update();
                break;


            // 契約更新
            case 'subscription':
                $this->Subscription->charge($message);
                break;

            // 確認用
            case 'check':
                $this->check($message);
                break;

            default:
                # code...
                break;
        }
    }

/**
 * ハッシュタグからいいねを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function hashtagLike($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ハッシュタグ情報を取得
        $res = $this->Instagram->fetchHashtags($instagramId);

        // ハッシュタグ情報がなければ、キューを削除
        if (count($res['Hashtag']) == 0) return 200;

        // ハッシュタグ情報を整形
        $hashtags = $this->Instagram->formatHashtags($res['Hashtag']);

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];


        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ハッシュタグをランダムに１つ取得
            $key = array_rand($hashtags);
            $hashtag = $hashtags[$key];

            // ハッシュタグのあるフィードを取得
            $feed = $i->getHashtagFeed($hashtag['name']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->getUsername()) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにいいねしていたらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => LIKE,
                    'media_id' => $item->id
                ]);
                if ($isExistAction) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // いいね実行
                $response = $i->like($item->id);

                // いいねに成功したら、アクティビティの登録
                if ($response->status == 'ok') {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_hashtag_id'        => $hashtag['id'],
                        'type'                  => LIKE,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * ハッシュタグからフォローを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function hashtagFollow($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ハッシュタグ情報を取得
        $res = $this->Instagram->fetchHashtags($instagramId);

        // ハッシュタグがなければ、キューを削除
        if (count($res['Hashtag']) == 0) return 200;

        // ハッシュタグ情報を整形
        $hashtags = $this->Instagram->formatHashtags($res['Hashtag']);

        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false
               // ,[
               //     'type'             => 'custom',
               //     'class'            => \InstagramAPI\SettingsAdapter\Memcached::class,
               //     'persistent_id'    => 'instagram',
               //     'memcache_options' => [
               //         Memcached::OPT_PREFIX_KEY => 'instagram_',
               //     ],
               //     'servers' => [[
               //         'host'   => 'hashbang-session.xknfad.0001.apne1.cache.amazonaws.com',
               //         'port'   => 11211,
               //         'weight' => 0,
               //     ]],
               // ]
            );
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ハッシュタグをランダムに１つ取得
            $key = array_rand($hashtags);
            $hashtag = $hashtags[$key];

            // ハッシュタグのあるフィードを取得
            $feed = $i->getHashtagFeed($hashtag['name']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->username) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにフォローしていたらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => FOLLOW,
                    'instagram_username_id' => $itemUser->pk
                ]);
                if ($isExistAction) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // フォロー実行
                $friendshipResponse = $i->follow($itemUser->pk);

                // フォローに成功したら、アクティビティの登録
                if ($friendshipResponse->status == 'ok' && $friendshipResponse->friendship_status->following) {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_hashtag_id'        => $hashtag['id'],
                        'type'                  => FOLLOW,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * ハッシュタグからコメントを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function hashtagComment($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ハッシュタグ情報を取得
        $res = $this->Instagram->fetchHashtags($instagramId);

        // ハッシュタグがなければ、キューを削除
        if (count($res['Hashtag']) == 0) return 200;

        // ハッシュタグ情報を整形
        $hashtags = $this->Instagram->formatHashtags($res['Hashtag']);

        // コメント情報を取得
        $res = $this->Instagram->fetchComments($instagramId);

        // コメント情報がなければ、キューを削除
        if (count($res['Comment']) == 0) return 200;

        // コメント情報を整形
        $comments = $this->Instagram->formatComments($res['Comment']);

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];


        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ハッシュタグをランダムに１つ取得
            $key = array_rand($hashtags);
            $hashtag = $hashtags[$key];

            // ハッシュタグのあるフィードを取得
            $feed = $i->getHashtagFeed($hashtag['name']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->username) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにコメントしているユーザーだったらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => COMMENT,
                    'instagram_username_id' => $itemUser->pk,
                ]);
                if ($isExistAction) continue;

                // すでにコメントしている投稿だったらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => COMMENT,
                    'media_id' => $item->id
                ]);
                if ($isExistAction) continue;

                // コメントをランダムに１つ取得
                $key = array_rand($comments);
                $comment = $comments[$key];

                // 念のため空チェック
                if (empty($comment)) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // コメント実行
                $commentResponse = $i->comment($item->id, $comment['comment']);

                // コメントに成功したら、アクティビティの登録
                if ($commentResponse->status == 'ok') {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_hashtag_id'        => $hashtag['id'],
                        'comment_id'            => $comment['id'],
                        'type'                  => COMMENT,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * ロケーションからいいねを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function locationLike($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ロケーション情報を取得
        $res = $this->Instagram->fetchLocations($instagramId);

        // ロケーション情報がなければ、キューを削除
        if (count($res['Location']) == 0) return 200;

        // ロケーション情報を整形
        $locations = $this->Instagram->formatLocations($res['Location']);

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];


        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ロケーションをランダムに１つ取得
            $key = array_rand($locations);
            $location = $locations[$key];

            // ロケーションのフィードを取得
            $feed = $i->getLocationFeed($location['location_id']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->username) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにいいねしていたらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => LIKE,
                    'media_id' => $item->id
                ]);
                if ($isExistAction) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // いいね実行
                $response = $i->like($item->id);

                // いいねに成功したら、アクティビティの登録
                if ($response->status == 'ok') {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_location_id'       => $location['id'],
                        'type'                  => LIKE,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * ロケーションからフォローを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function locationFollow($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ロケーション情報を取得
        $res = $this->Instagram->fetchLocations($instagramId);

        // ロケーション情報がなければ、キューを削除
        if (count($res['Location']) == 0) return 200;

        // ロケーション情報を整形
        $locations = $this->Instagram->formatLocations($res['Location']);

        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];


        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ロケーションをランダムに１つ取得
            $key = array_rand($locations);
            $location = $locations[$key];

            // ロケーションのフィードを取得
            $feed = $i->getLocationFeed($location['location_id']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->username) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにフォローしていたらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => FOLLOW,
                    'instagram_username_id' => $itemUser->pk
                ]);
                if ($isExistAction) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // フォロー実行
                $friendshipResponse = $i->follow($itemUser->pk);

                // フォローに成功したら、アクティビティの登録
                if ($friendshipResponse->status == 'ok' && $friendshipResponse->friendship_status->following) {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_location_id'       => $location['id'],
                        'type'                  => FOLLOW,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * ロケーションからコメントを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function locationComment($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ロケーション情報を取得
        $res = $this->Instagram->fetchLocations($instagramId);

        // ロケーション情報がなければ、キューを削除
        if (count($res['Location']) == 0) return 200;

        // ロケーション情報を整形
        $locations = $this->Instagram->formatLocations($res['Location']);


        // コメント情報を取得
        $res = $this->Instagram->fetchComments($instagramId);

        // コメント情報がなければ、キューを削除
        if (count($res['Comment']) == 0) return 200;

        // コメント情報を整形
        $comments = $this->Instagram->formatComments($res['Comment']);

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];


        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フィルター取得
            $filters = $this->Filter->fetchFormattedFilters($instagramId);

            // ロケーションをランダムに１つ取得
            $key = array_rand($locations);
            $location = $locations[$key];

            // ロケーションのフィードを取得
            $feed = $i->getLocationFeed($location['location_id']);

            // フィードから投稿を取得
            $items = $feed->getItems();
            foreach ($items as $item) {
                // 投稿者を取得
                $itemUser = $item->getUser();

                // 自分の投稿だったらスキップ
                if ($username == $itemUser->username) continue;

                // フィルターチェック
                if (!$this->filterCheck($item, $filters)) continue;

                // すでにコメントしているユーザーだったらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => COMMENT,
                    'instagram_username_id' => $itemUser->pk,
                ]);
                if ($isExistAction) continue;

                // すでにコメントしている投稿だったらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => COMMENT,
                    'media_id' => $item->id
                ]);
                if ($isExistAction) continue;

                // コメントをランダムに１つ取得
                $key = array_rand($comments);
                $comment = $comments[$key];

                // 念のため空チェック
                if (empty($comment)) continue;

                // 投稿画像取得
                $photos = $item->image_versions2->candidates;

                // 画像が取得できない場合はスキップ
                if (is_null($photos)) continue;

                // 投稿画像を1枚取得
                $photo = end($photos);

                // コメント実行
                $commentResponse = $i->comment($item->id, $comment['comment']);

                // コメントに成功したら、アクティビティの登録
                if ($commentResponse->status == 'ok') {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'mtb_location_id'       => $location['id'],
                        'comment_id'            => $comment['id'],
                        'type'                  => COMMENT,
                        'username'              => $itemUser->username,
                        'full_name'             => $itemUser->full_name,
                        'profile_picture'       => $itemUser->profile_pic_url,
                        'instagram_username_id' => $itemUser->pk,
                        'media_id'              => $item->id,
                        'media_picture_url'     => $photo->url,
                        'media_url'             => 'https://www.instagram.com/p/' . $item->code,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    if ($this->Activity->save($data)) return 200;
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * フォロー解除を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function unfollow($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // フォロー解除対象ユーザーを取得
        $following = $this->Activity->fetchUnfollowFollowings($instagramId, $message['unfollow_day']);

        // フォロー解除対象ユーザーがいなければ、キューを削除
        if (count($following) == 0) return 200;

        // ユーザー情報
        $res = $this->Instagram->fetchInstagramInfo($instagramId);

        // 自動設定チェック
        if ($res['AutoSetting']['auto_unfollow'] === false) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フォロー解除をするユーザー番号取得
            if (!empty($following['Activity']['username_id'])) {
                $userId = $following['Activity']['username_id'];
            } else {
                $userId = $i->getUsernameId($following['Activity']['username']);
            }

            // ユーザー番号がなければ、キューを削除
            if (empty($userId)) return 200;

            // フォロー解除実行
            $friendshipResponse = $i->unfollow($userId);

            // フォロー解除に成功したら、フォローユーザーから削除し、アクティビティ登録
            if ($friendshipResponse->status == 'ok') {
                // ユーザー情報取得
                $userInfo = $i->getUserInfoById($userId);

                // アクティビティの登録
                $this->Activity->create();
                $data = [
                    'instagram_id'          => $instagramId,
                    'type'                  => UNFOLLOW,
                    'username'              => $userInfo->user->username,
                    'full_name'             => $userInfo->user->full_name,
                    'profile_picture'       => $userInfo->user->profile_pic_url,
                    'instagram_username_id' => $userInfo->user->pk,
                    'action_date'           => date("Y-m-d H:i:s")
                ];
                $this->Activity->save($data);
            }

            // フォロー解除済みに更新
            $this->Activity->save([
                'id' => $following['Activity']['id'],
                'unfollow' => true
            ]);

        } catch (Exception $e) {
            // ユーザー存在エラー パターン1
            if (strpos($e->getMessage(), 'User not found') !== false) {
                // フォロー解除済みに変更
                $this->Activity->save([
                    'id' => $following['Activity']['id'],
                    'unfollow' => true
                ]);
                return 200;
            }

            // ユーザー存在エラー パターン2
            if (strpos($e->getMessage(), 'No response from server') !== false) {
                // トレース内容からユーザー存在エラーによる例外か判定
                foreach ($e->getTrace() as $trace) {
                    if (strpos($trace['file'], 'instagram-php') !== false 
                        && strpos($trace['function'], 'getMappedResponseObject') !== false 
                        && empty($trace['args'][0]->user) 
                        && empty($trace['args'][0]->status)
                    ) {
                        // フォロー解除済みに変更
                        $this->Activity->save([
                            'id' => $following['Activity']['id'],
                            'unfollow' => true
                        ]);
                        return 200;
                    }
                }
            }

            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }


/**
 * 一括フォロー解除を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function bulkUnfollow($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // ホワイトリストに登録していないフォローユーザーを取得
        $following = $this->Activity->fetchNonWhitelistFollowing($instagramId);

        // 対象ユーザー存在チェック
        if (empty($following)) return 200;

        // ユーザー情報
        $res = $this->Instagram->fetchInstagramInfo($instagramId);

        // 自動設定チェック
        if ($res['AutoSetting']['auto_bulk_unfollow'] === false) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フォロー解除をするユーザー番号取得
            if (!empty($following['Activity']['username_id'])) {
                $userId = $following['Activity']['username_id'];
            } else {
                $userId = $i->getUsernameId($following['Activity']['username']);
            }

            // ユーザー番号がなければ、キューを削除
            if (empty($userId)) return 200;

            // フォロー解除実行
            $friendshipResponse = $i->unfollow($userId);

            // フォロー解除に成功したら、フォローユーザーから削除し、アクティビティ登録
            if ($friendshipResponse->status == 'ok') {
                // ユーザー情報取得
                $userInfo = $i->getUserInfoById($userId);

                // アクティビティの登録
                $this->Activity->create();
                $data = [
                    'instagram_id'          => $instagramId,
                    'type'                  => UNFOLLOW,
                    'username'              => $userInfo->user->username,
                    'full_name'             => $userInfo->user->full_name,
                    'profile_picture'       => $userInfo->user->profile_pic_url,
                    'instagram_username_id' => $userInfo->user->pk,
                    'action_date'           => date("Y-m-d H:i:s")
                ];
                $this->Activity->save($data);
            }

            // フォロー解除済みに更新
            $this->Activity->save([
                'id' => $following['Activity']['id'],
                'unfollow' => true
            ]);

        } catch (Exception $e) {
            // ユーザー存在エラー パターン1
            if (strpos($e->getMessage(), 'User not found') !== false) {
                // フォロー解除済みに変更
                $this->Activity->save([
                    'id' => $following['Activity']['id'],
                    'unfollow' => true
                ]);
                return 200;
            }

            // ユーザー存在エラー パターン2
            if (strpos($e->getMessage(), 'No response from server') !== false) {
                // トレース内容からユーザー存在エラーによる例外か判定
                foreach ($e->getTrace() as $trace) {
                    if (strpos($trace['file'], 'instagram-php') !== false 
                        && strpos($trace['function'], 'getMappedResponseObject') !== false 
                        && empty($trace['args'][0]->user) 
                        && empty($trace['args'][0]->status)
                    ) {
                        // フォロー解除済みに変更
                        $this->Activity->save([
                            'id' => $following['Activity']['id'],
                            'unfollow' => true
                        ]);
                        return 200;
                    }
                }
            }

            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }


/**
 * メッセージを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function message($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // メッセージ送信対象フォロワーを取得
        $res = $this->Instagram->fetchUnsentMessage($instagramId);

        // メッセージ送信対象フォロワーがいなければ、キューを削除
        if (count($res['Follower']) == 0) return 200;

        // メッセージがなければ、キューを削除
        if (count($res['Message']) == 0) return 200;

        // フォロワーを設定
        $followers = $res['Follower'];

        // メッセージをランダムに１つ取得
        $key = array_rand($res['Message']);
        $message = $res['Message'][$key]['message'];

        // 念のため空チェック
        if (empty($message)) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フォロワーをランダムに１つ取得
            $key = array_rand($followers);
            $follower = $followers[$key];

            // ユーザー番号取得
            if (!empty($follower['instagram_username_id'])) {
                $userId = $follower['instagram_username_id'];
            } else {
                $userId = $i->getUsernameId($follower['username']);
            }

            // ユーザー番号がなければ、キューを削除
            if (empty($userId)) return 200;

            // フォローされているかチェック、されていなければ削除
            $friendshipStatus = $i->getUserFriendship($userId);
            if ((bool)$friendshipStatus->followed_by === false) {
                $this->loadModel('Follower');
                $this->Follower->delete($follower['id']);
                return 200;
            }

            //IDを使ってダイレクトメッセージを送付
            $response = $i->directMessage($userId, $message);

            // ステータスの更新
            if ($response->status == 'ok') {
                $this->loadModel('Follower');
                $data = [
                    'id'             => $follower['id'],
                    'message_status' => true,
                ];
                if ($this->Follower->save($data)) {
                    // ユーザー情報取得
                    $userinfo = $i->getUserInfoById($userId);

                    // アクティビティの登録
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'type'                  => MESSAGE,
                        'username'              => $userinfo->user->username,
                        'full_name'             => $userinfo->user->full_name,
                        'profile_picture'       => $userinfo->user->profile_pic_url,
                        'instagram_username_id' => $userinfo->user->pk,
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    $this->Activity->save($data);
                }
            }

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        //return 200;
    }

/**
 *  フォローバックを行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function followback($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // フォローバック実施対象のインスタグラムアカウント取得
        $res = $this->Instagram->fetchFollowBack($instagramId);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        // フォローをしていないフォロワーを取得
        $this->loadModel('Follower');
        $followers = $this->Follower->fetchNoFollowBack($instagramId, 1);

        // フォロワーがいなければ、キューを削除
        if (count($followers) == 0) return 200;

        try {
            // ログイン
            $i = new \InstagramAPI\Instagram(false, false
               // ,[
               //     'type'             => 'custom',
               //     'class'            => \InstagramAPI\SettingsAdapter\Memcached::class,
               //     'persistent_id'    => 'instagram',
               //     'memcache_options' => [
               //         Memcached::OPT_PREFIX_KEY => 'instagram_',
               //     ],
               //     'servers' => [[
               //         'host'   => 'hashbang-session.xknfad.0001.apne1.cache.amazonaws.com',
               //         'port'   => 11211,
               //         'weight' => 0,
               //     ]],
               // ]
            );
            $i->setUser($username, $password);
            $i->login();

            // フォローバックを行う
            foreach ($followers as $follower) {
                // すでにフォローしていたらスキップ
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => FOLLOW,
                    'instagram_username_id' => $follower['Follower']['username_id']
                ]);

                if ($isExistAction) {
                    // フォローステータスを更新
                    $data = [
                        'id' => $follower['Follower']['id'],
                        'following_status' => true
                    ];
                    $this->Follower->save($data);
                    continue;
                }

                // すでにフォローしているかチェック
                $response = $i->getUserFriendship($follower['Follower']['username_id']);

                // フォローしているか、非公開アカウントの場合
                if ($response->following || $response->is_private) {
                    // フォローステータスを更新
                    $data = [
                        'id' => $follower['Follower']['id'],
                        'following_status' => true
                    ];
                    $this->Follower->save($data);
                    continue;
                }

                // フォローバック実行
                $friendshipResponse = $i->follow($follower['Follower']['username_id']);

                // フォローバックに成功したら、アクティビティを登録
                if ($friendshipResponse->status == 'ok' && $friendshipResponse->friendship_status->following) {
                    $this->Activity->create();
                    $data = [
                        'instagram_id'          => $instagramId,
                        'type'                  => FOLLOW,
                        'followback'            => true,
                        'username'              => $follower['Follower']['username'],
                        'profile_picture'       => $follower['Follower']['profile_picture'],
                        'instagram_username_id' => $follower['Follower']['username_id'],
                        'action_date'           => date("Y-m-d H:i:s")
                    ];
                    $this->Activity->save($data);

                    // フォローステータスを更新
                    $data = [
                        'id' => $follower['Follower']['id'],
                        'following_status' => true
                    ];
                    $this->Follower->save($data);
                }
            }
        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * フォロワー情報の更新を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function followerCheck($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // インスタグラムアカウント取得
        $res = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            'recursive' => -1
        ]);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フォロワーを取得
            $followers = [];
            // Starting at "null" means starting at the first page.
            $maxId = null;
            do {
                // Request the page corresponding to maxId.
                $response = $i->getSelfUserFollowers($maxId);
                // In this example we're merging the response array, but we can do anything.
                $followers = array_merge($followers, $response->getUsers());
                // Now we must update the maxId variable to the "next page".
                // This will be a null value again when we've reached the last page!
                // And we will stop looping through pages as soon as maxId becomes null.
                $maxId = $response->getNextMaxId();
            } while ($maxId !== null);

            if (empty($followers)) return 200;

            // フォロワーがいれば一旦ステータスをリセット
            $this->loadModel('Follower');
            $check = $this->Follower->hasAny([
                'instagram_id' => $instagramId
            ]);
            if ($check) {
                $fields = [
                    'status' => false,
                    'modified' => "'" . date('Y-m-d H:i:s') . "'"
                ];
                $conditions = [
                    'instagram_id' => $instagramId
                ];
                $this->Follower->updateAll($fields, $conditions);
            }

            foreach ($followers as $follower) {
                try {
                    // フォロー状態チェック（アクティビティから検索）
                    $conditions = [
                        'instagram_id' => $instagramId,
                        'instagram_username_id' => $follower->pk,
                        'type' => FOLLOW
                    ];
                    if ($this->Activity->hasAny($conditions)) {
                        $following_status = true;
                    } 
                    // アクティビティにない場合は、APIでチェック
                    else {
                        $friendshipResponse = $i->getUserFriendship($follower->pk);
                        $following_status = $friendshipResponse->following;
                    }

                    // 存在チェック
                    $dbFollower = $this->Follower->find('first', [
                        'fields' => ['id'],
                        'conditions' => [
                            'instagram_id' => $instagramId,
                            'username_id'  => $follower->pk
                        ],
                        'recursive' => -1
                    ]);

                    if ($dbFollower) {
                        // フォロワー情報更新
                        $data = [
                            'id' => $dbFollower['Follower']['id'],
                            'following_status' => $following_status,
                            'status' => true
                        ];
                        $this->Follower->save($data);
                    } else {
                        // フォロワー登録
                        $this->Follower->create();
                        $data = [
                            'instagram_id'     => $instagramId,
                            'username_id'      => $follower->pk,
                            'username'         => $follower->username,
                            'profile_picture'  => $follower->profile_pic_url,
                            'message_status'   => false,
                            'whitelist_status' => false,
                            'status'           => true,
                            'following_status' => $following_status
                        ];
                        $this->Follower->save($data);
                    }
                } catch (Exception $e) {
                }
            }
        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * アカウント情報の更新を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function accountCheck($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // インスタグラムアカウント取得
        $res = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            'recursive' => -1
        ]);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // アカウント情報取得
            $userInfo = $i->getSelfUserInfo();

            // フォロー数・フォロワー数の更新
            $data = [
                'id'              => $instagramId,
                'following_count' => $userInfo->user->following_count,
                'follower_count'  => $userInfo->user->follower_count,
                'media_count'     => $userInfo->user->media_count
            ];
            $this->Instagram->save($data);

        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * フォローユーザー情報の更新を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function followingCheck($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // インスタグラムアカウント取得
        $res = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            'recursive' => -1
        ]);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // フォローユーザーを取得
            $followings = [];

            $maxId = null;
            do {
                $response = $i->getSelfUsersFollowing($maxId);
                $followings = array_merge($followings, $response->getUsers());
                $maxId = $response->getNextMaxId();
            } while ($maxId !== null);

            if (empty($followings)) return 200;

            // トランザクション開始
            $this->loadModel('TransactionManager');
            $transaction = $this->TransactionManager->begin();

            // フォローを登録済みの場合、ステータスを一旦リセット
            $this->loadModel('Following');
            $check = $this->Following->hasAny([
                'instagram_id' => $instagramId
            ]);
            if ($check) {
                $fields = [
                    'status' => false,
                    'modified' => "'" . date('Y-m-d H:i:s') . "'"
                ];
                $conditions = [
                    'instagram_id' => $instagramId
                ];
                $this->Following->updateAll($fields, $conditions);
            }

            foreach ($followings as $following) {
                // アクティビティチェック
                $isExistAction = $this->Activity->hasAny([
                    'instagram_id' => $instagramId,
                    'type' => FOLLOW,
                    'instagram_username_id' => $following->pk
                ]);

                // 存在チェック
                $user = $this->Following->find('first', [
                    'fields'=> [
                        'id'
                    ],
                    'conditions' => [
                        'instagram_id' => $instagramId,
                        'username_id'  => $following->pk
                    ],
                    'recursive' => -1
                ]);
                if ($user) {
                    // ステータスを更新
                    $data = [
                        'id' => $user['Following']['id'],
                        'status' => 1,
                        'following_type' => $isExistAction
                    ];
                    $this->Following->save($data);
                    continue;
                }

                try {
                    $this->Following->create();
                    $data = [
                        'instagram_id'    => $instagramId,
                        'username_id'     => $following->pk,
                        'username'        => $following->username,
                        'profile_picture' => $following->profile_pic_url,
                        'following_type'  => $isExistAction
                    ];
                    $this->Following->save($data);
                } catch (Exception $e) {
                }
            }
            // コミット
            $this->TransactionManager->commit($transaction);

        } catch (Exception $e) {
            $this->TransactionManager->rollback($transaction);
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * フォロー数とフォロワー数の更新を行う
 *
 * @param array $message
 * 
 * @return int $code
 */
    public function followingFollowerCheck($message) {
        // インスタグラム番号設定
        $instagramId = $message['instagram_id'];

        // インスタグラムアカウント取得
        $res = $this->Instagram->find('first', [
            'conditions' => [
                'Instagram.id' => $instagramId
            ],
            'recursive' => -1
        ]);

        // 存在チェック
        if (!$res) return 200;

        // ユーザー名とパスワードを設定
        $username = $res['Instagram']['username'];
        $password = $res['Instagram']['password'];

        try {
            $i = new \InstagramAPI\Instagram(false, false);
            $i->setUser($username, $password);
            $i->login();

            // アカウント情報取得
            $userInfo = $i->getSelfUserInfo();

            // フォロー数登録
            $this->loadModel('FollowingCount');
            $this->FollowingCount->create();
            $data = [
                'instagram_id'    => $instagramId,
                'following_count' => $userInfo->user->following_count
            ];
            $this->FollowingCount->save($data);

            // フォロワー数登録
            $this->loadModel('FollowerCount');
            $this->FollowerCount->create();
            $data = [
                'instagram_id'    => $instagramId,
                'follower_count'  => $userInfo->user->follower_count
            ];
            $this->FollowerCount->save($data);
        } catch (Exception $e) {
            if (!$this->errorCheck($e, $instagramId)) {
                $this->sendSlack($e, $message);
            }
        }

        return 200;
    }

/**
 * フィルターチェック
 *
 * @param InstagramAPI\Item $item
 * @param array $filters
 * 
 * @return boolean
 */
    public function filterCheck($item, $filters) {
        if (empty($filters)) {
            return true;
        }

        $caption = $item->getCaption();
        if (is_null($caption)) {
            return true;
        }

        $text = $caption->getText();

        // フィルターチェック、キーワードを含む投稿はスキップ
        $filterFlag = false;
        foreach ($filters as $filter) {
            if (strpos($text, $filter) !== false) {
                return false;
            }
        }

        return true;
    }

/**
 * エラーチェック
 *
 * @param Exception $e
 * @param int $instagramId
 * @return boolean
 */
    protected function errorCheck($e, $instagramId) {
        // パラメータチェック
        if (empty($instagramId)) {
            return false;
        }

        // フィードバックエラー
        $flag = false;
        if (strpos($e->getMessage(), 'feedback_required') !== false) {

            // メッセージ制限チェック
            // トレース内容からメッセージ送信による例外か判定
            foreach ($e->getTrace() as $trace) {
                if (strpos($trace['file'], 'instagram-php') !== false 
                    && strpos($trace['function'], 'getMappedResponseObject') !== false　
                    && strpos($trace['args'][1]->feedback_message, 'It looks like you were misusing this feature by going too fast') !== false
                ) {
                    // メッセージ機能をOFF
                    $data = [
                        'id' => $instagramId,
                        'message_api_status' => false,
                        'message_api_restrict_date' => date('Y-m-d H:i:s')
                    ];
                    $flag = (bool)$this->Instagram->save($data);
                    break;
                }
            }

            // コメント制限チェック
            // トレース内容からメッセージ送信による例外か判定
            foreach ($e->getTrace() as $trace) {
                if (strpos($trace['file'], 'instagram-php') !== false 
                    && strpos($trace['function'], 'getMappedResponseObject') !== false 
                    && strpos($trace['args'][1]->feedback_url, 'instagram_comment') !== false
                ) {
                    // メッセージ機能をOFF
                    $data = [
                        'id' => $instagramId,
                        'comment_api_status' => false,
                        'comment_api_restrict_date' => date('Y-m-d H:i:s')
                    ];
                    $flag = (bool)$this->Instagram->save($data);
                    break;
                }
            }
        }
        // 認証エラー
        elseif (strpos($e->getMessage(), 'checkpoint_required') !== false) {
            // インスタグラム認証をOFF
            $data = [
                'id' => $instagramId,
                'instagram_auth' => false
            ];
            $flag = (bool)$this->Instagram->save($data);
        }
        // 認証エラー
        elseif (strpos($e->getMessage(), 'The username you entered') !== false) {
            // 認証エラーチェック
            // トレース内容から認証エラーによる例外か判定
            foreach ($e->getTrace() as $trace) {
                if (strpos($trace['file'], 'instagram-php') !== false 
                    && strpos($trace['function'], 'getMappedResponseObject') !== false 
                    && $trace['args'][1]->invalid_credentials == true
                ) {
                    // インスタグラム認証をOFF
                    $data = [
                        'id' => $instagramId,
                        'instagram_auth' => false
                    ];
                    $flag = (bool)$this->Instagram->save($data);
                    break;
                }
            }
        }

        return $flag;
    }

/**
 * Slackにエラー送信
 *
 * @param Exception $e
 * @param array $message
 */
    protected function sendSlack($e, $message) {
        $str  = "========================\n";
        $str .= "Activity:" . $message['type'] . "\n";
        $str .= "Instagram ID:" . $message['instagram_id'] . "\n";
        $str .= "========================\n";
        $str .= "Error:" . $e->getMessage() . "\n";
        $str .= "Stack Trace:" . $e->getTraceAsString() . "\n";
        $this->SlackWebhook->send($str);
    }
}
