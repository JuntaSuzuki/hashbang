<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Batch', 'Vendor');

class ApiController extends AppController {

/**
 * モデル読み込み
 *
 * @var array
 */
	public $uses = array(
		'User', 'Hashtag', 'Activity', 'Instagram',
		'Selector', 'AutoSetting', 'Follower'
	);

/**
 * 各アクションの前に実行
 *
 */
	public function beforeFilter() {
		parent::beforeFilter();

		// 認証の許可
		$this->Auth->allow();

		// ビューを使用しない
		$this->autoRender = false;
	}

/**
 * バッチ用JSON
 *
 * @return string JSONデータ
 */
	public function batchJson() {
		return $this->User->fetchBatchJson($this->params->query['user_id'], 'json');
	}

/**
 * フォロー
 *
 * @return string JSONデータ
 */
	public function follow() {
		return $this->Activity->fetchFollows($this->params->query['user_id'], 'json');
	}

/**
 * コメント
 *
 * @return string JSONデータ
 */
	public function comment() {
		return $this->Activity->fetchComments($this->params->query['user_id'], 'json');
	}

/**
 * セレクター取得
 *
 * @return string JSONデータ
 */
	public function selector() {
		return $this->Selector->fetchSelectors('json');
	}

	public function test() {
		$this->autoRender = false;

		$followers = $this->Follower->find('list', array(
			'fields' => array('username'),
			'conditions' => array(
				'instagram_id' => 1
			)
		));

		$follows = $this->Activity->find('all', array(
			'fields' => array(
				'DISTINCT username', '*'
			),
			'conditions' => array(
				'Activity.action_date >= date(current_timestamp - interval 5 day)',
				'Activity.action_date < date(current_timestamp - interval 4 day)',
				'type !=' => 4, // 4はアンフォロー
				'follow_back' => 0,
				'unfollow' => 0,
				'instagram_id' => 1,
				'NOT' => array(
					'Activity.username' => $followers
				)
			),
			'group' => array(
				'username'
			),
			'limit' => 1
		));

print_r($follows);
		exit;

		// 5日前でフォローバックがないフォローを取得
		$follows = $this->Activity->find('all', array(
			'fields' => array(
				'DISTINCT username', 'action_date'
			),
			'conditions' => array(
				'Activity.action_date >= date(current_timestamp - interval 5 day)',
				'Activity.action_date < date(current_timestamp - interval 4 day)',
				//'Activity.action_date < date(now())',
				'type !=' => 4, // 4はアンフォロー
				'follow_back' => 0,
				'unfollow' => 0
			),
			'group' => array(
				'username'
			),
			'limit' => 5
		));
		print_r($follows);
		exit;

		$users = $this->Instagram->find('all', array(
			'fields' => array(
				'id', 'username', 'password'
			),
			// 'contain' => array(
			// 	'User' => array(
			// 		'fields' => array(
			// 			'email'
			// 		),
			// 		'Instagram' => array(
			// 			'username', 'password'
			// 		),
			// 	)
			// ),
			'conditions' => array(
				'AutoSetting.auto_unfollow' => 1,
			)
		));
		print_r($users);
		exit;

		$users = $this->AutoSetting->find('all', array(
			'fields' => array(
				'id'
			),
			'contain' => array(
				'User' => array(
					'fields' => array(
						'email'
					),
					'Instagram' => array(
						'username', 'password'
					),
				)
			),
			'conditions' => array(
				'auto_unfollow' => 1,
			)
		));
		print_r($users);
		exit;

		// 5日前でフォローバックがないフォローを取得
		$follows = $this->Activity->find('all', array(
			'fields' => array(
				'DISTINCT username', 'action_date'
			),
			'conditions' => array(
				'Activity.action_date >= date(current_timestamp - interval 5 day)',
				'Activity.action_date < date(current_timestamp - interval 4 day)',
				//'Activity.action_date < date(now())',
				'type !=' => 4, // 4はアンフォロー
				'follow_back' => 0,
				'unfollow' => 0
			),
			'conditions' => array(
				'auto_unfollow' => 1,
			),
			'limit' => 5
		));
		print_r($follows);
		exit;

		$results = $this->AutoSetting->find('first', array(
			'fields' => array(
				'id'
			),
			'contain' => array(
				'User' => array(
					'fields' => array(
						'email'
					),
					'Instagram' => array(
						'username', 'password'
					),
					'Activity' => array(
						'fields' => array(
							'username'
						),
						'conditions' => array(
							'Activity.action_date >= date(current_timestamp - interval 5 day)',
							'Activity.action_date < date(current_timestamp - interval 4 day)',
							//'Activity.action_date < date(now())',
							'type !=' => 4, // 4はアンフォロー
							'follow_back' => 0,
							'unfollow' => 0
						)
					)
				)
			),
			'conditions' => array(
				'auto_unfollow' => 1,
			)
		));

		// データの整形
		$data = array();
		print_r($users);
		exit;

		$users = $this->Activity->find('all', array(
			'fields' => array(
				'id', 'user_id', 'username'
			),
			'conditions' => array()
		));
		print_r($users);
		exit;
		$follows = unserialize('a:1:{s:4:"data";a:59:{i:0;s:7:"aiuzura";i:1;s:14:"muay_thai_kick";i:2;s:15:"borderlesswomen";i:3;s:10:"ta.045.721";i:4;s:18:"m.y.design_fashion";i:5;s:11:"maririn2926";i:6;s:13:"harang_market";i:7;s:10:"kana_s0130";i:8;s:7:"aath_td";i:9;s:7:"mk_nw20";i:10;s:11:"aoringo0630";i:11;s:9:"unayu0317";i:12;s:7:"uribo33";i:13;s:10:"mrs___shop";i:14;s:9:"djyosukep";i:15;s:11:"aoringo0630";i:16;s:13:"anelanalu_aus";i:17;s:11:"satooooyuri";i:18;s:7:"2015aoi";i:19;s:10:"ta.045.721";i:20;s:7:"legina2";i:21;s:8:"apoopoo2";i:22;s:10:"mrs___shop";i:23;s:13:"island_keikis";i:24;s:13:"harang_market";i:25;s:10:"sorte.shop";i:26;s:6:"yn.219";i:27;s:12:"yyyyuuuukkii";i:28;s:8:"kaotan_8";i:29;s:7:"hq_moni";i:30;s:13:"wada_michelle";i:31;s:8:"03nagisa";i:32;s:10:"sorte.shop";i:33;s:6:"jutiko";i:34;s:10:"aichin.520";i:35;s:12:"shantihearts";i:36;s:12:"shantihearts";i:37;s:10:"yu____ri.s";i:38;s:15:"katiusciiiiiiia";i:39;s:8:"erii_hrt";i:40;s:13:"kaio.gregorio";i:41;s:16:"yuriyuzunyankooo";i:42;s:10:"ta.045.721";i:43;s:13:"harang_market";i:44;s:11:"maririn2926";i:45;s:9:"chie.0214";i:46;s:10:"yuyuyu2326";i:47;s:9:"djyosukep";i:48;s:11:"coco.parody";i:49;s:9:"miyo_0101";i:50;s:5:"yr.18";i:51;s:12:"shantihearts";i:52;s:11:"coco.parody";i:53;s:13:"island_keikis";i:54;s:8:"yococono";i:55;s:12:"shop_bellcee";i:56;s:13:"harang_market";i:57;s:9:"chie.0214";i:58;s:12:"alphard_8000";}}');
		print_r($follows);
		echo "debug";
		exit;

$id = $this->Instagram->find('first', array(
				'conditions' => array(
					'user_id' => $this->Auth->user('id'),
				),
				'recursive' => -1
			));
print_r($id);
exit;

		#phantomjsブラウザ実行 バックグラウンドで実行
		$cmd = <<<EOF

phantomjs --webdriver-loglevel=NONE --webdriver=127.0.0.1:65535 &
PHANTOMJS_PID=$!

sleep 5

echo \$PHANTOMJS_PID

#kill \$PHANTOMJS_PID
EOF;
		$output = shell_exec( "{$cmd}");
		echo $output;

		$username = 'filiosclothes';
		$password = 'y4t6gGdT4AlR';

		$this->Batch = new Batch(array(
			'host' => 'http://127.0.0.1:4444/wd/hub',
			'capabilities' => 'phantomjs'
		));
		$this->Batch->create();
		$res = $this->Batch->loginCheck($username, $password);
		$this->Batch->quit();

		if ($res == false) {
			return json_encode(array('success' => false));
		}

		$obj = json_decode($res);

		$data = array(
			'user_id' => $this->Auth->user('id'),
			'username' => $username,
			'password' => $password,
			'profile_picture' => $obj->entry_data->ProfilePage[0]->user->profile_pic_url,
			'follows_count' => $obj->entry_data->ProfilePage[0]->user->follows->count,
			'followed_by_count' => $obj->entry_data->ProfilePage[0]->user->followed_by->count
		);

		$this->loadModel('Instagram');
		$this->Instagram->save($data);

		return json_encode(array('success' => true));
		exit;
		//phantomjs用
		// $Batch = new Batch(array(
		// 	'host' => 'http://127.0.0.1:8910',
		// 	'capabilities' => array(
		// 		WebDriverCapabilityType::BROWSER_NAME => 'phantomjs',
		// 		'phantomjs.page.settings.userAgent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:25.0) Gecko/20100101 Firefox/25.0',
		// 	)
		// ));

		try {
			$Batch = new Batch(array(
				'host' => 'http://127.0.0.1:4444/wd/hub',
				'capabilities' => DesiredCapabilities::chrome()
			));
			$Batch->create();

			$users = $Batch->getBatchUserData();
			foreach ($users['data'] as $user) {
				$user = json_decode($user, true);
				$Batch->login($user['username'], $user['password']);

				if ($user['setting']['auto_like'] != 1) {
					continue;
				}

				foreach ($user['hashtag'] as $key => $hashtag) {
					$Batch->searchHashtagMedia($hashtag['name']);

					try {
						$Batch->like($user['id'], $hashtag['id']);
					} catch (Exception $e) {
						echo $e->getMessage();
					}
				}
			}

			$Batch->quit();
		} catch (Exception $e) {
			echo $e->getMessage();
		}

		exit;
	}

	public function instagram() {
		$this->autoRender = false;

		//$host = 'http://localhost:4444/wd/hub';
		$host = 'http://localhost:8910';
		$capabilities = array(
			WebDriverCapabilityType::BROWSER_NAME => 'phantomjs',
			'phantomjs.page.settings.userAgent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:25.0) Gecko/20100101 Firefox/25.0',
		);
		$driver = RemoteWebDriver::create($host, $capabilities);
		//chromedriverを指定（chromedriverをダウンロードして/user/local/bin等に入れておく。
		//$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());

		// //テストするサイトに移動
		$driver->get('https://www.instagram.com/');

		$driver->takeScreenshot('/tmp/top.png');
exit;
		// ログインフォームを表示
		$driver->findElement( WebDriverBy::cssSelector('._fcn8k') )->click();
		// メールアドレス入力
        $driver->findElement(
            WebDriverBy::NAME('username')
        )->sendKeys('order_muco');

        // パスワード入力
        $driver->findElement(
            WebDriverBy::NAME('password')
        )->sendKeys('y4t6gGdT4AlR');

        // ログインボタンをクリック
        $driver->findElement(
            WebDriverBy::cssSelector('._aj7mu._taytv._ki5uo._o0442')
        )->click();

        sleep(1);

		// 対象のハッシュタグのページへ遷移
		$driver->get('https://www.instagram.com/explore/tags/%E3%81%84%E3%81%84%E3%81%AD%E3%81%97%E3%81%9F%E4%BA%BA%E5%85%A8%E5%93%A1%E3%83%95%E3%82%A9%E3%83%AD%E3%83%BC%E3%81%99%E3%82%8B/');
		$html = $driver->getPageSource();
		$html = str_replace(array("\r\n","\r","\n","\t"), '', $html);

		// 投稿を取得
		$posts = array();
        preg_match_all("/<a class=\"_8mlbc _vbtk2 _t5r8b\" href=\"(.*)\">(.*)<\/a>/ismU", $html, $posts);
        if ($posts[0]) {
        	foreach ($posts[0] as $post) {
        		preg_match("/<a class=\"_8mlbc _vbtk2 _t5r8b\" href=\"(.*)\"><div class=\"_22yr2\"><div class=\"_jjzlb\"><img alt=\"(.*)\" class=\"_icyx7\" id=\"(.*)\" src=\"(.*)\"/ismU", $post, $match);
        		$posts[] = array(
        			'url' => 'https://www.instagram.com' . $match[1],
        			'comment' => $match[2],
        			'image' => $match[4]
        		);
        	}
        }
        //print_r($posts);

        //$driver->get($posts[20]['url']);
        $driver->get('https://www.instagram.com/p/BN6cjDaDlHy/');

		$driver->takeScreenshot('/tmp/follow_media.png');


		// コメント投稿
		$driver->wait(10, 500)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector('input._7uiwk'))
        );
        $driver->findElement(
            WebDriverBy::cssSelector('input._7uiwk')
        )->sendKeys('いいね！');
        $driver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

		$driver->takeScreenshot('/tmp/comment.png');


		$driver->quit();
		exit;

        // フォローする
        $driver->findElement(
            WebDriverBy::cssSelector('._aj7mu._2hpcs._kenyh._o0442')
        )->click();
	}

/**
 * ハッシュタグ検索
 *
 * @return void
 */
	public function hashtag() {
		$this->autoRender = false;

		//if ($this->request->is('ajax')) {
			$url = 'https://api.instagram.com/v1/tags/search';
			$HttpSocket = new HttpSocket();
			$response = $HttpSocket->get($url, array(
				//'q' => $this->data['q'],
				'q' => '明日',
				'access_token' => '3265028095.fa71964.35629bdd19e14ebb98e90de607d23146'
			));

			$data = json_decode($response['body']);
			$data = json_decode(json_encode($data), true);
			array_multisort(array_column($data['data'], 'media_count'), SORT_DESC, $data['data']);

			return json_encode($data['data']);
		//}
	}

/**
 * アクティビティ登録 / アクティビティ取得（ページネーション）
 *
 * @return void
 */
	public function activity() {
		$this->autoRender = false;

		if ($this->request->is('post')) {
			try {
				$this->Activity->save($this->data);
			} catch(Exception $e) {
			}
		}
	}

/**
 * アクティビティ取得（ページネーション）
 *
 * @return string html
 */
	public function moreActivity() {
		$this->autoRender = false;

		if ($this->request->is('ajax')) {
			try {
				$this->paginate = $this->Activity->fetchActivitiesOption(1, $this->data['limit'], $this->data['offset']);
				$this->set('activities', $this->paginate('Activity'));
				$this->render('/Elements/Dashboard/activity_list');
			} catch(Exception $e) {
			}
		}
	}
}
