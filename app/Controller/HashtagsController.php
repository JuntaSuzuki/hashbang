<?php

App::uses('DashboardBaseController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

/**
 * ハッシュタグ コントローラー
 *
 *
 * @package     app.Controller
 */
class HashtagsController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
    public $uses = array('Hashtag', 'MtbHashtag', 'User');

    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * ハッシュタグ登録
 *
 * @return string
 */
    public function add() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['hashtagName'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // ハッシュタグマスタの存在チェック
            $check = $this->MtbHashtag->findByName($this->data['hashtagName']);

            // 存在しなければ、ハッシュタグマスタテーブルに追加
            if (empty($check)) {
                $data = array(
                    'name' => $this->data['hashtagName']
                );
                $this->MtbHashtag->save($data);
                $mtbHashtagId = $this->MtbHashtag->getLastInsertID();
            } else {
                $mtbHashtagId = $check['MtbHashtag']['id'];
            }

            // すでにハッシュタグテーブルに追加している場合は、2を返す
            $conditions = array(
                'instagram_id' => $this->instagramId,
                'mtb_hashtag_id' => $mtbHashtagId
            );
            if ($this->Hashtag->hasAny($conditions)) {
                return json_encode(array('code' => '400', 'message' => 'no data'));
            }

            // ハッシュタグテーブルに追加
            $data = $conditions;
            if ($this->Hashtag->save($data)) {
                // 登録件数の更新
                $this->MtbHashtag->updateCount($mtbHashtagId);

                return json_encode(array(
                    'code' => '200',
                    'message' => h($this->data['hashtagName']),
                    'id' => $this->Hashtag->getLastInsertID()
                ));
            } else {
                return json_encode(array('code' => '500', 'message' => 'no data'));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('code' => '500', 'message' => 'no data'));
        }
    }

/**
 * ハッシュタグ削除
 *
 * @return string
 */
    public function delete() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // パラメータの存在チェック
            if (empty($this->data['id'])) {
                throw new ForbiddenException('パラメータの存在チェック');
            }

            // インスタグラム番号の存在チェック
            if (empty($this->instagramId)) {
                throw new ForbiddenException('インスタグラム番号の存在チェック');
            }

            // データの存在チェック
            $hashtag = $this->Hashtag->findById($this->data['id']);
            if (empty($hashtag)) {
                return json_encode(array('success' => false));
            }

            // 削除条件
            $conditions = array(
                'id' => $this->data['id'],
                'instagram_id' => $this->instagramId,
            );

            // 削除実行
            if ($this->Hashtag->deleteAll($conditions, false)) {
                // 登録件数の更新
                $this->MtbHashtag->updateCount($hashtag['MtbHashtag']['id']);

                return json_encode(array('success' => true));
            } else {
                return json_encode(array('success' => false));
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

/**
 * ハッシュタグ検索
 *
 * @return string
 */
    public function search() {
        try {
            $this->autoRender = false;

            // Ajax以外はアクセス禁止
            if (!$this->request->is('ajax')) {
                throw new ForbiddenException('Ajax以外はアクセス禁止');
            }

            // URL生成
            $url = 'https://api.instagram.com/v1/tags/search';
            $HttpSocket = new HttpSocket();
            $response = $HttpSocket->get($url, array(
                'q' => $this->data['q'],
                'access_token' => '3265028095.fa71964.35629bdd19e14ebb98e90de607d23146'
            ));

            // ハッシュタグ検索
            $data = json_decode($response['body']);
            $data = json_decode(json_encode($data), true);
            array_multisort(array_column($data['data'], 'media_count'), SORT_DESC, $data['data']);

            return json_encode($data['data']);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return json_encode(array('success' => false));
        }
    }

}
