<?php

App::uses('DashboardBaseController', 'Controller');

/**
 * コメント コントローラー
 *
 *
 * @package		app.Controller
 */
class CommentsController extends DashboardBaseController {

/**
 * モデル読み込み
 *
 * @var array
 */
	public $uses = array('Comment', 'User');

	public function beforeFilter() {
		parent::beforeFilter();

		//$this->Auth->allow();
	}

/**
 * コメント登録
 *
 * @return void
 */
	public function add() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException();
			}

			// パラメータの存在チェック
			if (empty($this->data['comment'])) {
				throw new ForbiddenException();
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// データの存在チェック
			$conditions = array(
				'instagram_id' => $this->instagramId,
				'comment' => h($this->data['comment'])
			);
			if ($this->Comment->hasAny($conditions)) {
				return json_encode(array('code' => '400', 'message' => 'no data'));
			}

			// コメントテーブルに追加
			$data = $conditions;
			if ($this->Comment->save($data)) {
				return json_encode(array(
					'code' => '200',
					'message' => h($this->data['comment']),
					'id' => $this->Comment->getLastInsertID()
				));
			} else {
				return json_encode(array('code' => '500', 'message' => 'no data'));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

/**
 * コメント削除
 *
 * @return 0:success 1:error
 */
	public function delete() {
		try {
			$this->autoRender = false;

			// Ajax以外はアクセス禁止
			if (!$this->request->is('ajax')) {
				throw new ForbiddenException('Ajax以外はアクセス禁止');
			}

			// パラメータの存在チェック
			if (empty($this->data['id'])) {
				throw new ForbiddenException('パラメータの存在チェック');
			}

			// インスタグラム番号の存在チェック
			if (empty($this->instagramId)) {
				throw new ForbiddenException('インスタグラム番号の存在チェック');
			}

			// データの存在チェック
			$comment = $this->Comment->findById($this->data['id']);
			if (empty($comment)) {
				return json_encode(array('success' => false));
			}

			// 削除条件
			$conditions = array(
				'id' => $this->data['id'],
				'instagram_id' => $this->instagramId,
			);

			// 削除実行
			if ($this->Comment->deleteAll($conditions, false)) {
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('success' => false));
			}
		} catch (Exception $e) {
			$this->log($e->getMessage());
			return json_encode(array('success' => false));
		}
	}

}
