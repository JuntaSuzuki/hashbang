<?php

App::uses('Component', 'Controller');

/**
 * スケジュール コンポーネント
 *
 *
 * @package     app.Controller.Component
 */
class ScheduleComponent extends Component {

/**
 * 初期化
 * 
 * @param Controller $Controller コントローラー
 */
    public function initialize(Controller $Controller) {
        $this->Controller = $Controller;
    }

/**
 * スケジュール更新
 * 
 */
    public function update() {

        // スケジュール情報取得
        $this->Controller->loadModel('AutoSchedule');
        $this->AutoSchedule = $this->Controller->AutoSchedule;
        $schedule = $this->AutoSchedule->find('list', [
            'fields' => [
                'action_time'
            ]
        ]);

        foreach ($schedule as $key => $action_time) {
            $placeholder = ':schedule' . $key;
            $scheduleStrings[] = $placeholder;
            $binds[$placeholder] = $action_time;
        }
        $scheduleString = implode(',', $scheduleStrings);

        $models = [
            'MtbAutoLikeSpeed', 'MtbAutoFollowSpeed', 'MtbAutoCommentSpeed',
            'MtbAutoMessageSpeed', 'MtbAutoUnfollowSpeed', 'MtbAutoFollowbackSpeed'
        ];

        // モデル毎に更新を行う
        foreach ($models as $model) {
            // 件数取得
            $this->Controller->loadModel($model);
            $processingNumbers = $this->Controller->$model->find('list', [
                'fields' => [
                    'processing_number'
                ]
            ]);

            // スケジュールをコピー
            $tmpSchedule = $schedule;

            // 件数分だけランダムにスケジュールを配分
            $data = [];
            foreach ($processingNumbers as $key => $processingNumber) {
                $data[$key] = $this->array_rand_value($tmpSchedule, $processingNumber);
                $tmpSchedule = array_diff($tmpSchedule, $data[$key]);
            }

            // カラム名
            $column = ltrim(strtolower(preg_replace('/[A-Z]/', '_\0', $model)), '_') . '_id';

            // スピードリセット用データ
            $speedIds = [];
            foreach ($schedule as $action_time) {
                $key = ':speed_id_' . $action_time;
                $speedIds[$key] = 0;
            }

            // スピード更新用データ
            foreach ($data as $speed_id => $action_times) {
                foreach ($action_times as $action_time) {
                    $key = ':speed_id_' . $action_time;
                    $speedIds[$key] = $speed_id;
                }
            }

            // SQL文生成
            $sqlFormat = "
                UPDATE auto_schedule 
                SET %s = ELT(FIELD(action_time, %s), %s) 
                WHERE action_time IN (%s)";

            $sql = sprintf(
                $sqlFormat,
                $column,
                $scheduleString,
                implode(',', $speedIds),
                $scheduleString
            );
            $this->AutoSchedule->query($sql, $binds, false);
        }
    }

/**
 * 配列から指定したエントリの数のバリューをランダムに抽出する
 * 
 * @param array $array 抽出元の配列
 * @param int $num 取得する要素数
 * @return array 抽出後の配列
 */
    protected function array_rand_value($arr, $num = 1) {
        if ($num >= count($arr)) {
            return $arr;
        }

        $result = array();
        $rand_keys = array_rand($arr, $num);
        foreach ($rand_keys as $key) {
            $result[] = $arr[$key];
        }

        return $result;
    }

}
