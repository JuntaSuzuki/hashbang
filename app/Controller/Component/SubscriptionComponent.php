<?php

App::uses('Component', 'Controller');

/**
 * 課金 コンポーネント
 *
 *
 * @package     app.Controller.Component
 */
class SubscriptionComponent extends Component {

    public $components = array(
        'SlackWebhook',
        'Mail'
    );

/**
 * 初期化
 * 
 * @param Controller $Controller コントローラー
 */
    public function initialize(Controller $Controller) {
        $this->Controller = $Controller;
    }

/**
 * 課金処理
 * 
 */
    public function charge($message = null) {
        if (is_null($message)) return 200;

        // 課金情報を取得
        $this->Controller->loadModel('Payment');
        $this->Payment = $this->Controller->Payment;
        $payment = $this->Payment->find('first', [
            'contain' => [
                'User' => [
                    'fields' => [
                        'id', 'email'
                    ]
                ],
                'Plan' => [
                    'fields' => [
                        'id', 'name', 'day'
                    ]
                ]
            ],
            'conditions' => [
                //　ユーザー番号
                'User.id' => $message['user_id'],
                // 自動課金ステータス
                'User.auto_subscription_status' => true,
                // 退会ステータス
                'User.remove' => false,
                // 契約開始日
                'Payment.contract_start_date' => date('Y-m-d'),
                // 課金処理結果ステータス
                'Payment.payment_status' => false,
                // 課金処理実行ステータス
                'Payment.status' => false,
                // PAYJP 課金ID
                'Payment.payjp_ch_id IS NULL'
            ]
        ]);

        // 課金処理がない場合
        if (empty($payment)) return 200;

        // 秘密鍵
        $secret = Configure::read('payjp-secret');

        // エラー格納用
        $error = [];

        try {
            Payjp\Payjp::setApiKey($secret);

            // descriptionの作成
            $format = "ユーザー番号:%s メールアドレス:%s 購入プラン:%s(%s日ごとに更新)";
            $description = sprintf(
                $format,
                $payment['User']['id'],
                $payment['User']['email'],
                $payment['Plan']['name'],
                $payment['Plan']['day']
            );

            // 課金リクエスト
            $result = Payjp\Charge::create([
                'customer'    => $payment['Payment']['payjp_cus_id'],
                'amount'      => $payment['Payment']['amount'],
                'currency'    => 'jpy',
                'description' => $description
            ]);

            if (isset($result->error)) {
                throw new Exception();
            }

            try {
                // トランザクション開始
                $this->Controller->loadModel('TransactionManager');
                $this->TransactionManager = $this->Controller->TransactionManager;
                $transaction = $this->TransactionManager->begin();

                // 今回の課金情報を更新
                $data = [
                    'id'             => $payment['Payment']['id'],
                    'status'         => true,
                    'payment_status' => true,
                    'payjp_ch_id'    => $result->id
                ];
                $this->Payment->save($data);


                // 次回契約開始日
                $date = new DateTime($payment['Payment']['contract_end_date']);
                $next_contract_start_date = $date->modify('+1 days')->format('Y-m-d');

                // 次回契約終了日
                $next_contract_end_date = $date->modify('+' . ($payment['Plan']['day'] - 1) . ' days')->format('Y-m-d');

                // 次回課金日
                $next_charge_date = $next_contract_start_date;

                // 次回の課金情報を登録
                $this->Payment->create();
                $data = [
                    'user_id'        => $payment['User']['id'],
                    'amount'         => $payment['Payment']['amount'],
                    'plan_id'        => $payment['Plan']['id'],
                    'status'         => false,
                    'payment_status' => false,
                    'contract_start_date' => $next_contract_start_date,
                    'contract_end_date' => $next_contract_end_date,
                    'charge_date'    => $next_charge_date,
                    'payjp_cus_id'   => $payment['Payment']['payjp_cus_id'],
                    'payjp_car_id'   => $payment['Payment']['payjp_car_id'],
                ];
                $this->Payment->save($data);


                // 有効期限日
                $plan_limit_date = $payment['Payment']['contract_end_date'];

                // 課金情報を更新
                $this->Controller->loadModel('User');
                $this->User = $this->Controller->User;
                $data = [
                    'id'               => $payment['User']['id'],
                    'plan_id'          => $payment['Payment']['plan_id'],
                    'plan_limit_date'  => $plan_limit_date,
                    'next_charge_date' => $next_charge_date,
                ];
                $this->User->save($data, ['callbacks' => false]);

                // コミット
                $this->TransactionManager->commit($transaction);

                // メール送信
                $params = [
                    'plan' => $payment['Plan']['name'],
                    'period' => $payment['Plan']['day'] . '日',
                    'contract_start_date' => $payment['Payment']['contract_start_date'],
                    'contract_end_date' => $payment['Payment']['contract_end_date'],
                    'amount' => number_format($payment['Payment']['amount'])
                ];
                $this->Mail->subscription($payment['User']['email'], $params);

            } catch (Exception $e) {
                $this->SlackWebhook->send($e->getMessage() . "\n" . $e->getTraceAsString());
                $this->TransactionManager->rollback($transaction);

                $data = [
                    'id'               => $payment['Payment']['id'],
                    'status'           => true,
                ];
                $this->Payment->save($data);
            }
        } catch(Payjp\Error\Card $e) {
            $error = $this->parseError($e);
        } catch (Payjp\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Payjp's API
            $error = $this->parseError($e);
        } catch (Payjp\Error\Authentication $e) {
            // Authentication with Payjp's API failed
            $error = $this->parseError($e);
        } catch (Payjp\Error\ApiConnection $e) {
            // Network communication with Payjp failed
            $error = $this->parseError($e);
        } catch (Payjp\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $error = $this->parseError($e);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Payjp
            $error = $this->parseError($e);
        }

        // エラーがある場合
        if (!empty($error)) {
            $data = [
                'id'               => $payment['Payment']['id'],
                'status'           => true,
                'payjp_error_code' => $error['code'],
                'payjp_error'      => $error['error']
            ];
            $this->Payment->save($data);
        }

        return 200;
    }

/**
 * PAY.JPのエラーをパース
 *
 * @param Exception $e
 * @return array
 */
    protected function parseError($e) {
        $body = $e->getJsonBody();
        $err  = $body['error'];
        $errorLog  = 'Status is:' . $e->getHttpStatus() . "\n";
        $errorLog .= 'Type is:' . $err['type'] . "\n";
        $errorLog .= 'Code is:' . $err['code'] . "\n";
        // param is '' in this case
        $errorLog .= 'Param is:' . $err['param'] . "\n";
        $errorLog .= 'Message is:' . $err['message'] . "\n";

        $error = [
            'code' => $err['code'],
            'error' => $errorLog
        ];

        return $error;
    }

}
