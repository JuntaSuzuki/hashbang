<?php 

App::uses('Component', 'Controller');

class InviteComponentComponent extends Component {

	public $components = array(
		'Auth',
		'Session'
	);

	/**
	 * 招待情報の登録
	 */
	public function addInvite($invite_code) {

		// コンポーネントでモデルクラスを読み込んで使用する
		$User = ClassRegistry::init('User');
		$Invite = ClassRegistry::init('Invite');

		// 招待してくれたユーザを取得
		$user = $User->find('first', 
			array(
				'fields' => array('User.id')
				'conditions' => array('User.invite_code' => $invite_code),
				'recursive' => -1,
			)
		);

		// 招待情報を登録
		$data = array(
			'Invite' => array(
				'user_id' => $user['User']['id'],
				'invite_user_id' => $this->Auth->user('id'),
				'status' => 1
			)
		);
		$Invite->save($data);

		// // 招待されたユーザに有料会員1カ月分を付与
		// $this->_addUpgradePlan($this->Auth->user('id'));

		// $this->Session->setFlash('招待コードによって有料会員1カ月分を無料で獲得しました。');
	}


	/**
	 * 対象ユーザに有料会員1カ月分を付与
	 */
	protected function _addUpgradePlan($userId) {
		// コンポーネントでモデルクラスを読み込んで使用する
		$userInstance = ClassRegistry::init('User');

		// 1カ月プランに変更
		$valid_date_ts = strtotime("+30 day",time());
		$data = array(
			'User' => array(
				'id' => $userId,
				'plan' => 4,
				'valid_date' => date('Y-m-d', $valid_date_ts)
			)
		);
		$userInstance->save($data);
	}


	/**
	 * 招待コードの生成
	 */
	public function createCode($id) {
		// コンポーネントでモデルクラスを読み込んで使用する
		$userInstance = ClassRegistry::init('User');
		$code = "";

		// 重複しない招待コードが生成されるまで
		while (true) {
			$code = mb_substr(sha1($id), 0, 5);

			$user = $userInstance->find('first', 
				array(
					'fields' => array('User.id'),
					'conditions' => array('User.invite_code' => $code),
					'recursive' => -1, 
				)
			);
			if (empty($user))
				break;
			$id++; // 重複した場合はidを増やす
		}

		return $code;
	}



	/**
	 * 招待コードで登録した人数を取得
	 */
	public function inviteCount($userId) {
		// コンポーネントでモデルクラスを読み込んで使用する
		$inviteInstance = ClassRegistry::init('Invite');

		// 1カ月プランに変更
		$count = $inviteInstance->find('count', 
			array(
				'conditions' => array('user_id' => $userId),
			)
		);
		return $count;
	}
}