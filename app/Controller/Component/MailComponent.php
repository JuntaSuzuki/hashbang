<?php 

App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * メール送信 コンポーネント
 *
 *
 * @package     app.Controller.Component
 */
class MailComponent extends Component {

/**
 * 新規登録時のメール送信
 */
    public function signUp($email) {
        try {
            $cakeEmail = new CakeEmail();

            // パラメータ
            $args = [];

            $cakeEmail
                    ->template('sign_up')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from(array(Configure::read('mail.from_email') => Configure::read('mail.from_name')))
                    ->to($email);

            // BCC設定
            if (Configure::read('mail.bcc_email') && !empty(Configure::read('mail.bcc_email'))) {
                $cakeEmail->bcc(Configure::read('mail.bcc_email'));
            }

            $sent = $cakeEmail->subject('【#BANG】新規登録のお知らせ')->send();

            if ($sent) {
                $this->log(__('契約更新メール送信 成功'), MAIL);
            } else {
                $this->log(__('契約更新メール送信 失敗'), MAIL);
            }

        } catch (Exception $e) {
            $this->log(__('契約更新メール送信 失敗'), MAIL);
        }
    }

/**
 * プラン購入時のメール送信
 *
 * @param string $email
 * @param array $args
 */
    public function payment($email, $args) {
        try {
            $cakeEmail = new CakeEmail();

            $cakeEmail
                    ->template('payment')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from(array(Configure::read('mail.no_reply_from_email') => Configure::read('mail.from_name')))
                    ->to($email);

            // BCC設定
            if (Configure::read('mail.bcc_email') && !empty(Configure::read('mail.bcc_email'))) {
                $cakeEmail->bcc(Configure::read('mail.bcc_email'));
            }

            $sent = $cakeEmail->subject('【#BANG】プラン購入完了のお知らせ')->send();

            if ($sent) {
                $this->log(__('契約更新メール送信 成功'), MAIL);
            } else {
                $this->log(__('契約更新メール送信 失敗'), MAIL);
            }

        } catch (Exception $e) {
            $this->log(__('契約更新メール送信 失敗'), MAIL);
        }
    }

/**
 * プラン購入時のメール送信
 *
 * @param string $email
 * @param array $args
 */
    public function subscription($email, $args) {
        try {
            $cakeEmail = new CakeEmail();

            $cakeEmail
                    ->template('subscription')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from(array(Configure::read('mail.no_reply_from_email') => Configure::read('mail.from_name')))
                    ->to($email);

            // BCC設定
            if (Configure::read('mail.bcc_email') && !empty(Configure::read('mail.bcc_email'))) {
                $cakeEmail->bcc(Configure::read('mail.bcc_email'));
            }

            $sent = $cakeEmail->subject('【#BANG】契約更新のお知らせ')->send();

            if ($sent) {
                $this->log(__('契約更新メール送信 成功'), MAIL);
            } else {
                $this->log(__('契約更新メール送信 失敗'), MAIL);
            }

        } catch (Exception $e) {
            $this->log(__('契約更新メール送信 失敗'), MAIL);
        }
    }

/**
 * お問い合わせのメール送信
 *
 * @param array $args
 * @return boolean
 */
    public function contact($args) {
        try {
            $cakeEmail = new CakeEmail();

            $cakeEmail
                    ->template('contact')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from(array(Configure::read('mail.no_reply_from_email') => Configure::read('mail.contact_from_name')))
                    ->to(Configure::read('mail.from_email'));

            // BCC設定
            if (Configure::read('mail.bcc_email') && !empty(Configure::read('mail.bcc_email'))) {
                $cakeEmail->bcc(Configure::read('mail.bcc_email'));
            }

            $sent = $cakeEmail->subject('#BANGにお問い合わせがありました' . date('Y-m-d H:i:s'))->send();

            if ($sent) {
                $this->log(__('お問い合わせのメール送信 成功'), MAIL);
            } else {
                $this->log(__('お問い合わせのメール送信 失敗'), MAIL);
            }

            return (boolean)$sent;

        } catch (Exception $e) {
            $this->log(__('お問い合わせのメール送信 失敗'), MAIL);
        }

        return false;
    }

/**
 * パスワード再設定
 *
 * @param string $email
 * @param string $resetPasswordToken
 * @return boolean
 */
    public function password($email, $resetPasswordToken) {
        try {
            $cakeEmail = new CakeEmail();

            // パラメータ
            $args = [
                'reset_password' => Configure::read('url') . '/users/reset?reset_password_token=' . $resetPasswordToken
            ];

            // メール送信
            $cakeEmail
                    ->template('password')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from([Configure::read('mail.from_email') => Configure::read('mail.from_name')])
                    ->to($email);

            // BCC設定
            if (Configure::read('mail.bcc_email') && !empty(Configure::read('mail.bcc_email'))) {
                $cakeEmail->bcc(Configure::read('mail.bcc_email'));
            }

            $sent = $cakeEmail->subject('【#BANG】パスワードの再設定方法をお知らせします')->send();

            if ($sent) {
                $this->log(__('再設定メール送信 成功'), MAIL);
            } else {
                $this->log(__('再設定メール送信 失敗'), MAIL);
            }

            return (boolean)$sent;

        } catch (Exception $e) {
            $this->log(__('再設定メール送信 失敗'), MAIL);
        }

        return false;
    }

/**
 * 新規登録時の失敗メール送信
 *
 * @param array $args
 */
    public function signUpError($args) {
        try {
            $cakeEmail = new CakeEmail();

            $cakeEmail
                    ->template('sign_up_error')
                    ->viewVars($args)
                    ->emailFormat('text')
                    ->from(array(Configure::read('mail.from_email') => Configure::read('mail.from_name')))
                    ->to(Configure::read('mail.admin_email'));

            $sent = $cakeEmail->subject('【#BANG】新規登録失敗' . date('Y-m-d H:i:s'))->send();

            if ($sent) {
                $this->log(__('新規登録時の失敗メール送信 成功'), MAIL);
            } else {
                $this->log(__('新規登録時の失敗メール送信 失敗'), MAIL);
            }

        } catch (Exception $e) {
            $this->log(__('新規登録時の失敗メール送信 失敗'), MAIL);
        }
    }

}
