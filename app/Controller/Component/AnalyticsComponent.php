<?php

App::uses('Component', 'Controller');

/**
 * 解析 コンポーネント
 *
 *
 * @package     app.Controller.Component
 */
class AnalyticsComponent extends Component {

/**
 * 初期化
 * 
 * @param Controller $Controller コントローラー
 */
    public function initialize(Controller $Controller) {
        $this->Controller = $Controller;
    }

/**
 * ハッシュタグ解析
 * 
 * @param array $message
 */
    public function hashtag($message) {

        // フォロワー取得
        $this->Controller->loadModel('Follower');
        $this->Follower = $this->Controller->Follower;
        $followerIds = $this->Follower->find('list', [
            'fields' => [
                'username_id'
            ],
            'conditions' => [
                'instagram_id' => $message['instagram_id']
            ]
        ]);

        $this->Controller->loadModel('Activity');
        $this->Activity = $this->Controller->Activity;
        foreach ($followerIds as $followerId) {
            $id = $this->Activity->field('id', [
                'type' => FOLLOW,
                'instagram_username_id' => $followerId
            ]);

            if (!$id) continue;

            $this->Activity->save([
                'id' => $id,
                'followback' => true
            ]);
        }
    }

}
